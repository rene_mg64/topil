<?php
/**
 * Complemento ajax para guardar los datos del armamento 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=UTF-8');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_cambios.class.php';
    include $path . 'includes/class/config/system.class.php';
    $objSys  = new System();  
    $objUsr  = new Usuario();  
    $objCambios  = new AdmtblCambios();
    
    //Recibe la variables por POST 
    //Convierte a codificacion iso-8859
    //asigna el nombre de variable a la variable recibida
    foreach($_POST as $nombre_campo => $valor){ 
        $asignacion = "\$" . $nombre_campo . "='" . utf8_decode($valor) . "';"; 
        eval($asignacion); 
    }
    
    $curp = $objSys->decrypt( $_POST["curp"] ); 
    
    // Datos de los Cambios
    $objCambios->curp = $curp;
    //$objCambios->curp_permuta = $curp
    $objCambios->id_cambio = $_POST["id_cambio"];
    $objCambios->folio = $txtNoFolio;
    $objCambios->fecha_noti = $objSys->convertirFecha($_POST["txtFechaNoti"], 'yyyy-mm-dd');
    $objCambios->id_area = $_POST["cbxArea"];
    $objCambios->id_municipio = $_POST["cbxMpioAdscripcion"];
    $objCambios->id_tipo_cambio = $_POST["cbxTipoCamb"];
    $objCambios->id_ubicacion = $_POST["cbxUbicacion"];
    // Datos del Oficio
    $objCambios->no_oficio = $txtNoOficio;
    
    $objCambios->fecha_oficio = ($_POST["txtFechaOficio"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaOficio"], 'yyyy-mm-dd');
    $objCambios->no_soporte =$txtNoSoporte;
    $objCambios->fecha_soporte = ($_POST["txtFechaSoporte"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaSoporte"], 'yyyy-mm-dd');
    
    $objCambios->firmante_soporte = $txtFirmante;
    $objCambios->cargo_firmante = $txtCargoFirmante;
    
    
    
    $result = ($_POST["dtTypeOper"] == 1) ? $objCambios->update() : $objCambios->insert();
    $oper = $_POST["dtTypeOper"] == '1' ? 'Edt' : 'Ins';
    
   if ($result) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_cambios', $curp, $oper);
        $ajx_datos['rslt']  = true;
        
        $ajx_datos['html']  = utf8_encode($html);            
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';  
       // $ajx_datos['campos']= $objBaja->matricula . "-" .$objBaja->oficio. "-" .$objBaja->fecha_oficio . "-" .$objBaja->id_motivo. "-" .$objBaja->fecha_baja . "-" .$objBaja->id_usuario;
        $ajx_datos['error'] = $objCambios->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}

?>