var xGrid;
var xGrid2;
var xGrid3;
$(document).ready(function(){
    
   //-- Ajusta el tama�o del contenedor del grid...
    $('#dvGrid-Personal').css('height', ($(window).height() - 250) + 'px');
    //-- Crea e inicializa el control del grid principal datos del elemento...
    xGrid = $('#dvGrid-Personal').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });
    
    // Definici�n del control grid la muestra datos de los tramites    
    xGrid2 = $('#dvGrid-Tramites').xGrid({                
        xOnlySearch: true,                           
        xUrlData: xDcrypt($('#hdnUrlObtListTram').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });  
    
    
    //-- Definici�n del dialog como formulario para portacion de armas....
    $('#dvCambios').dialog({
        show: {
            effect: "fade",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        },
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 950,
        height: 520,
        modal: true,
        resizable: false,
        draggable:false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
     $('#dvTramites').dialog({
        
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 940,
        height: 600,
        modal: true,
        resizable: false,
        draggable:false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
    
     $('#dvNuevo').dialog({
        show: {
            effect: "fade",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        },
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 950,
        height: 550,
        modal: true,
        resizable: false,
        draggable:false,
        buttons:{            
            '-- Guardar --': function(){
                //funcion guardar los datos del cambio                
                GuardarFormulario();                
            },
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });     
    
    $(".classNuevo, .classModificar").live("click", function(e){
        var oper;
        var titulo;
        var id;
        //se cambia el flujo del script segu sea insercion o actualizacion. 
        if( $(this).attr('accion') == 'insertar' ){
            var curp = $(this).attr("id").substring(4);            
            oper=0;            
            titulo='Registro';                                         
        }else{
            var id_tramite= $(this).attr('id').substring(4);
            oper=1;
            titulo='Actualizaci�n';                    
        }        
        $.ajax({
            url: xDcrypt( $('#hdnUrlNuevo').val() ),            
            dataType: 'json',
            type: 'POST',
            data: { 'oper' : oper , 'curp' : curp, 'id_tramite' : id_tramite },
            async: true,
            cache: false,            
            success: function (xdata) {  
                    
                    $('#dvNuevo').html(xdata.html);                                        
                    $('#dvNuevo').dialog('option','title',titulo);
                    $('#dvNuevo').dialog('open');
                     
                                                                                                  
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    }); 
    
    
   
    
    $(".classAsigna").live("click", function(e){        
        $.ajax({
            url: xDcrypt( $('#hdnUrlObtDatosPer').val() ),  
            data: { 'curp': $(this).attr("id").substring(4) },          
            dataType: 'json',
            type: 'post',
            async: true,
            cache: false,            
            success: function (xdata) {

                $("#hdnUrlCurp").val( xdata.curp ) 
                xGrid2.resetAjaxParam();
                xGrid2.addParamAjax( "curp="+xdata.curp );  
                xGrid2.refreshGrid(); 
                         
                                           
                $('#dvDatos').html(xdata.html_dat);                                                             
                $('#dvTramites').dialog('option','title',xdata.titulo);
                $('#dvTramites').dialog('open');                    
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    });    

    $('#xReportes').click(function(){
        //$('#dvForm-Registro').dialog('open'); 
    });
    
   
    //-- Funcion para eliminar un registro del tramite en cuestion.
    $(".classDescarga").live("click", function(e){      
        
        var MAT = $(this).attr("rel");
        //se crea un mensaje de confirmacion para descargar el arma.
        var msj = $(getHTMLMensaje(' Confirme la eliminacion del registro', 2));        
        msj.dialog({
            autoOpen: true,                        
            minWidth: 450,
            resizable: false,
            modal: true,
            buttons:{
                'Aceptar': function(){
                    $(this).dialog('close');                                        
                    $.ajax({
                        url: xDcrypt( $('#hdnUrlElimTram').val() ),
                        data: { 'id_tramite' : MAT.substring(4), 'curp' : $('#hdnUrlCurp').val() },
                        dataType: 'json',
                        type: 'POST',
                        async: true,
                        cache: false,            
                        success: function (xdata) {  
                            
                                if( xdata.rslt ){                                                                          
                                    alert('Registro Eliminado.');
                                    //se actualiza el grid
                                    xGrid2.refreshGrid();  
                                }else{
                                    alert(xdata.error)
                                    alert('Existe un problema al realizar la acci�n.');
                                }                                                                                                      
                        },
                        error: function(objeto, detalle, otroobj){                            
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }                        
                    });                                        
                },
                'Cancelar': function(){
                    $(this).dialog('close');                    
                }
            }
        });
        
    }); 
    
    
    
    
    

});


    //-- Controla el bot�n para visualizar el formulario de alta o actualizacion de cambios...
    function GuardarFormulario(){    
        var valida = $('#frmRegistro').validate().form();
        var mensaje='';  
        var oper = $("#dtTypeOper").val();  
                 
        if (valida) {
            if( oper == '1' ){
                var id_cambio = $("#id_cambio").val();
                mensaje= 'Confirme la actualizaci�n de los datos.';
            }else{
                mensaje='Confirme el alta en el sistema.';
            }
            var frmPreg = $(getHTMLMensaje(mensaje, 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $.ajax({
                            url: xDcrypt( $('#hdnUrlAddRg').val() ),   
                            //data: $( "#frmRegistro" ).serialize(),  
                            data:$('#frmRegistro').find('input, select, textarea, radio, button').serialize(),        
                            dataType: 'json',
                            type: 'POST',
                            async: true,
                            cache: false,  
                            //contentType: 'application/x-www-form-urlencoded; charset=ISO-8859-1',  
                            //beforeSend: function(jqXHR) {
    //jqXHR.overrideMimeType('application/x-www-form-urlencoded; charset=ISO-8859-1');
//  },       
                            success: function ( xdata ) {
                                
                                
                                //alert(xdata.rslt) 
                                if( xdata.rslt == 1 ){  
                                    if( oper == '1' ){                                   
                                        $("#dvNuevo").dialog('close');
                                        
                                        xGrid2.refreshGrid(); 
                                              
                                    }else{                                        
                                                                                                                
                                        //$("#txtNoFolio").focus();
                                        //$("#xGridBody").html( xdata.html );
                                        
                                        $("#dvNuevo").dialog('close');
                                        
                                        xGrid.refreshGrid();                                        
                                        xGrid2.refreshGrid();    
                                                                            
                                    }                                    
                                                                                                      
                                }else{
                                    $(this).dialog('close');
                                    alert("No se guardaron los datos. " + xdata.rslt);                                                                        
                                }                                                                                               
                            },
                            error: function(objeto, detalle, otroobj){
                                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                            }
                        });  
                        $(this).dialog('close');                                              
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });                        
        }

    }
