<?php
/**
 * Complemento ajax para guardar los datos del armamento 
 * 
 */
 
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_cambio_horario.class.php';
    include $path . 'includes/class/config/system.class.php';
    $objSys  = new System();  
    $objUsr  = new Usuario();  
    $objCambHor  = new AdmtblCambioHorario();
    //Recibe la variables por POST 
    //Convierte a codificacion iso-8859
    //asigna el nombre de variable a la variable recibida
    foreach($_POST as $nombre_campo => $valor){ 
        $asignacion = "\$" . $nombre_campo . "='" . utf8_decode($valor) . "';"; 
        eval($asignacion); 
    }
    
    
    $curp = $objSys->decrypt( $curp ); 
    
    // Datos de los Cambios
    $objCambHor->curp = $curp;
    $objCambHor->id_cambio_horario = $id_tramite;
    $objCambHor->fecha_cambio = ($txtFechaCambio == "") ? NULL : $objSys->convertirFecha($txtFechaCambio, 'yyyy-mm-dd');
    $objCambHor->folio = $txtNoFolio;
    $objCambHor->observacion = trim($txtObservacion);
    $objCambHor->notificar = $_POST["rbnNotificar"];
    $objCambHor->status = $_POST["rbnStatus"];
    $objCambHor->id_horario = $_POST["cbxHorario"];
    // Datos del Oficio
    $objCambHor->no_oficio = $txtNoOficio;
    $objCambHor->fecha_oficio = ($_POST["txtFechaOficio"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaOficio"], 'yyyy-mm-dd');
    $objCambHor->no_soporte =$txtNoSoporte;
    $objCambHor->fecha_soporte = ($_POST["txtFechaSoporte"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaSoporte"], 'yyyy-mm-dd');
    $objCambHor->firmante_soporte = $txtFirmante;
    $objCambHor->cargo_firmante = $txtCargoFirmante;
    
   
    $result = ($_POST["dtTypeOper"] == 1) ? $objCambHor->update() : $objCambHor->insert();
    $oper = $_POST["dtTypeOper"] == '1' ? 'Edt' : 'Ins';
    
   if ($result) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_cambio_horario', $curp, $oper);
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($html);            
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';  
       // $ajx_datos['campos']= $objBaja->matricula . "-" .$objBaja->oficio. "-" .$objBaja->fecha_oficio . "-" .$objBaja->id_motivo. "-" .$objBaja->fecha_baja . "-" .$objBaja->id_usuario;
        $ajx_datos['error'] = $objCambHor->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}

?>