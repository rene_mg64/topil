<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_cambio_horario.class.php';
    include $path . 'includes/class/admcat_horarios.class.php';
    include $path . 'includes/class/admtbl_adscripcion.class.php';
    
    $objSys = new System();
    $objCambHor = new AdmtblCambioHorario();
    $objHorario = new AdmcatHorarios();
    $objAdscrip = new AdmtblAdscripcion();
    
    //se incluye archivo javascript para funcionalidad necesario
    $html .= '<script type="text/javascript" src="adm/_js/tramites/incidencias/horarios/ajx_frm_asigna.js"></script>';
    $html .= '<script type="text/javascript" src="includes/js/xgrid.js"></script>';
    $html .= '<link type="text/css" rel="stylesheet" href="includes/js/jquery_editor/jquery-te-1.4.0.css">';
    $html .= '<script type="text/javascript" src="includes/js/jquery_editor/jquery-te-1.4.0.min.js" charset="utf-8"></script>';
    
    
    
    //variables recibidas
    $curp = $_POST["curp"];
    
    $id_tramite = $_POST["id_tramite"];
    $oper = $_POST["oper"];
    
    // Arreglo de datos estatico
    //$tipoLic[0] = array('value'=>0,'descripcion'=>'');
    //$tipoLic[1] = array('value'=>1,'descripcion'=>'CON SUELDO');
    //$tipoLic[2] = array('value'=>2,'descripcion'=>'SIN SUELDO');

    
   
    if($oper == 0){   
        
        
        $objAdscrip->select($curp);
        $horario = $objAdscrip->id_horario;
        $fecha_cambio = "";
        $fecha_oficio = "";
        $fecha_soporte = "";
        $observacion = '';
        
        
        
    }
    else{
        
        $objCambHor->select($id_tramite);
        $curp = $objCambHor->curp;
        $horario = $objCambHor->id_horario;
        //$objAdscrip->select($curp);
        $fecha_cambio = ($objCambHor->fecha_cambio == NULL) ? "" : date('d/m/Y', strtotime($objCambHor->fecha_cambio));
        $fecha_oficio = ($objCambHor->fecha_oficio == NULL) ? "" : date('d/m/Y', strtotime($objCambHor->fecha_oficio));
        $fecha_soporte = ($objCambHor->fecha_soporte == NULL) ? "" : date('d/m/Y', strtotime($objCambHor->fecha_soporte));
        
        $observacion = $objCambHor->observacion;
        
        if ($objCambHor->notificar == 1) {
            $rbnNotificar1 = 'checked="true"';
            $rbnNotificar2 = '';
        } else {
            $rbnNotificar1 = '';
            $rbnNotificar2 = 'checked="true"';
        }
        
        if ($objCambHor->status == 1) {
            $rbnStatus1 = 'checked="true"';
            $rbnStatus2 = '';
        } else {
            $rbnStatus1 = '';
            $rbnStatus2 = 'checked="true"';
        }
    }
        
     

    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//      
    $html .= ' 
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 850px;">
            <!--<span class="dvForm-Data-pTitle"><img src="'.PATH_IMAGES.'icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Nuevo</span>-->                                
            <div id="tabs" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">
                <ul>
                    <li><a href="#tab-1" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">1</span>Datos Generales</a></li>
                    <li><a href="#tab-2" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">2</span>Datos del Oficio</a></li>
                  
                </ul>         
                <!-- Datos Generales -->
                <div id="tab-1">
                    <fieldset class="fsetForm-Data" style="width: auto;">                                
                        <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                            <tr>
                                <td><label for="txtNoFolio">No. Folio:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoFolio" id="txtNoFolio" value="'.$objCambHor->folio.'" maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                    
                                </td>
                            </tr> 
                            <tr>
                                <td><label for="txtFechaCambio">Fecha de Cambio Horario:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaCambio" id="txtFechaCambio" value="'.$fecha_cambio.'" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>   
                             
                           
                            <tr>
                                <td><label>Status</label></td>
                                <td class="validation">
                                    <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnStatus" id="rbnStatus1" value="1" '.$rbnStatus1.' />Pendiente</label>
                                    <label class="label-Radio"><input type="radio" name="rbnStatus" id="rbnStatus2" value="2" '.$rbnStatus2.' />Teminado</label>
                                    <span class="pRequerido">*</span>
                                </td>
                                
                            </tr>
                            
                            <tr>
                                <td><label>Notificar</label></td>
                                <td class="validation">
                                    <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnNotificar" id="rbnNotificar1" value="1" '.$rbnNotificar1.' />Si</label>
                                    <label class="label-Radio"><input type="radio" name="rbnNotificar" id="rbnNotificar2" value="2" '.$rbnNotificar2.' />No</label>
                                    <span class="pRequerido">*</span>
                                </td>
                                
                            </tr>
                            <tr>
                            <td><label for="cbxHorario">Horario Laboral:</label></td>
                                <td class="validation">
                                    <select name="cbxHorario" id="cbxHorario" style="max-width: 500px;">
                                        '.
                                         $objHorario->shwHorarios($horario)
                                        .'
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <script>
                            $(".editor").jqte();
                            
                            
                            </script>
                            <tr>
                                <td><label for="txtObservacion">Observacion:</label></td>
                                <td class="validation" >
                                    <textarea class="editor" name="txtObservacion" id="txtObservacion"  >
                                    '.$observacion.'
                                    </textarea>
                                    
                                </td>
                            </tr> 
                        </table>
                    </fieldset>
                </div>
                <script> 
                    $("#tabs").tabs (); 
                    $("#txtFechaCambio").datepicker({ yearRange: "1920:", }); 
                    $("#txtFechaOficio").datepicker({ yearRange: "1920:", });
                    $("#txtFechaSoporte").datepicker({ yearRange: "1920:", });
                </script>
                <div id="tab-2">
                    <fieldset class="fsetForm-Data" style="width: auto;">                                
                        <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                            <tr>
                                <td><label for="txtNoOficio">No. Oficio:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoOficio" id="txtNoOficio" value="'.$objCambHor->no_oficio.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                    
                                </td>
                            </tr>              
                            <tr>
                                <td><label for="txtFechaOficio">Fecha Oficio:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaOficio" id="txtFechaOficio" value="'. $fecha_oficio . '"  maxlength="35" title="..." style="width: 120px;" />
                                </td>
                            </tr>
                             <tr>
                                <td><label for="txtNoSoporte">No. de Soporte:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoSoporte" id="txtNoSoporte" value="'.$objCambHor->no_soporte.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                </td>
                            </tr>  
                            <tr>
                                <td><label for="txtFechaSoporte">Fecha de Soporte:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaSoporte" id="txtFechaSoporte" value="'. $fecha_soporte . '"  maxlength="35" title="..." style="width: 120px;" />
                                    
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtFirmante">Firmante del Soporte:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtFirmante" id="txtFirmante" value="'.$objCambHor->firmante_soporte.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 350px;" />
                                   
                                </td>
                            </tr>   
                            <tr>
                                <td><label for="txtCargoFirmante">Cargo del Firmante:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtCargoFirmante" id="txtCargoFirmante" value="'.$objCambHor->cargo_firmante.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 350px;" />
                                    
                                </td>
                            </tr>     
                            
                        </table>
                    </fieldset>
                </div>
                
            </div>
            
        </div>
         <input type="hidden" id="dtTypeOper" name="dtTypeOper" value="' . $oper . '" />
        <input type="hidden" id="id_tramite" name="id_tramite" value="' . $id_tramite . '" />
        <input type="hidden" id="curp" name="curp" value="' . $objSys->encrypt( $curp ) . '" />
        <input type="hidden" name="safety_precautions" id="safety_precautions">
    </form>     
   
    ';    
                  
   
  
    
    //se inyecta la informacion en los contenedores
    $ajx_datos["html"]  = utf8_encode($html);      
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>