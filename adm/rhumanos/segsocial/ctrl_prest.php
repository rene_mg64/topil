<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_adscripcion.class.php';
include 'includes/class/admtbl_doctos_ident.class.php';
include 'includes/class/admtbl_prestaciones.class.php';
include 'includes/class/admtbl_dependientes.class.php';

$objAdscrip = new AdmtblAdscripcion();
$objDocIdent = new AdmtblDoctosIdent();
$objPrest = new AdmtblPrestaciones();
$objDepend = new AdmtblDependientes();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Seguridad Social',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/rhumanos/segsocial/_js/ctrlprest.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <style type="text/css">
    .tdTituloTab{ color: #666362; font-style: italic; font-size: 10pt; font-weight: bold; padding-left: 5px; text-align: left; width: 70%; }
    </style>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <?php                    
                    $urlCancel = 'index.php?m=' . $_SESSION['xIdMenu'];
                    ?>
                    <!-- Botones de opci�n... -->                    
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="" title="Cancelar la alta del nuevo elemento...">
                        <img src="includes/css/imgs/icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <?php
    $_SESSION["xCurp"] = ( isset($_GET["id"]) ) ? $objSys->decrypt($_GET["id"]) : $_SESSION["xCurp"];
    $objAdscrip->select($_SESSION["xCurp"]);
    // Foto
    $objDocIdent->select($_SESSION['xCurp']);
    $foto_fte = ( !empty($objDocIdent->foto_frente) ) ? 'adm/expediente/fotos/' . $objDocIdent->foto_frente : '';
    if( empty($foto_fte) )
        $foto_fte = ( $objDatPer->genero == 1 ) ? 'adm/expediente/sin_foto_h.jpg' : 'adm/expediente/sin_foto_m.jpg';
    ?>    
    <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 1000px;">
        <span class="dvForm-Data-pTitle"><img src="includes/css/imgs/icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Prestaciones del empleado</span>
        
        <fieldset class="fsetForm-Data" style="width: 900px;">
            <legend>Datos Generales</legend>   
            <table class="tbForm-Data" style="table-layout: fixed;">
                <tr>
                    <td rowspan="11" style="text-align: center; width: 290px;">
                        <div class="dvFoto-Normal" style="background-image: url(<?php echo $foto_fte;?>);vertical-align: bottom;"></div>
                    </td>
                </tr>
                <tr>                        
                    <td style="width: 145px;"><label for="txtCurp">C.U.R.P.:</label></td>
                    <td class="control-group" style="width: 450px;">
                        <input type="text" name="txtCurp" id="txtCurp" value="<?php echo $objAdscrip->AdmtblDatosPersonales->curp;?>" title="..." readonly="true" style="width: 200px;" />
                    </td>
                </tr>
                <tr>
                    <td><label for="txtCuip">C.U.I.P.:</label></td>
                    <td class="control-group" style="width: 450px;">
                        <input type="text" name="txtCuip" id="txtCuip" value="<?php echo $objAdscrip->AdmtblDatosPersonales->cuip;?>" title="..." readonly="true" style="width: 200px;" />
                    </td>
                </tr>
                <tr>
                    <td><label for="txtNombre">Nombre Completo:</label></td>
                    <td class="control-group">
                        <?php
                        $nombrePersona = $objAdscrip->AdmtblDatosPersonales->nombre . " " . $objAdscrip->AdmtblDatosPersonales->a_paterno . " " . $objAdscrip->AdmtblDatosPersonales->a_materno;
                        ?>
                        <input type="text" name="txtNombrePersona" id="txtNombrePersona" value="<?php echo $nombrePersona;?>" title="..." readonly="true" style="width: 350px;" />                                    
                    </td>
                </tr>                    
                <tr>
                    <td><label for="txtCategoria">Categor�a:</label></td>
                    <td class="control-group">                                        
                        <input type="text" name="txtCategoria" id="txtCategoria" value="<?php echo $objAdscrip->AdmcatCategorias->categoria;?>" title="..." readonly="true" style="width: 350px;" />                                    
                    </td>
                </tr>   
                <tr>
                    <td><label for="txtNivelMando">Nivel de mando</label>:</label></td>
                    <td class="control-group">
                        <input type="text" name="txtNivelMando" id="txtNivelMando" value="<?php echo $objAdscrip->AdmcatNivelMando->nivel_mando;?>" title="..." readonly="true" style="width: 350px;" />
                    </td>
                </tr>
                <tr>
                    <td><label for="txtArea">�rea de adscripci�n:</label></td>
                    <td class="control-group">
                        <input type="text" name="txtArea" id="txtArea" value="<?php echo $objAdscrip->AdmcatAreas->area;?>" title="..." readonly="true" style="width: 350px;" />
                    </td>
                </tr>
            </table>
        </fieldset>
                    
        <div id="tabs" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">
            <ul>
                <li><a href="#tabs-1" style="font-size: 11pt; min-width: 150px; text-align: center;">Prestaciones</a></li>
                <li><a href="#tabs-2" style="font-size: 11pt; min-width: 150px; text-align: center;">Dependientes Econ�micos</a></li>
            </ul>
            <!-- Prestaciones -->
            <div id="tabs-1">    
                <div class="dvBar-Normal" style="">
                    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <tr>
                            <td class="tdTituloTab">Prestaciones registradas</td>
                            <td style="padding: 3px; text-align: right; width: 30%;">
                                <a href="#" id="btnAddPrest" class="Tool-Bar-Btn gradient" style="height: 30px; min-width: 60px;" title="Agregar una nueva prestaci�n...">
                                    <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" />
                                </a>
                            </td>                                
                        </tr>
                    </table>
                </div>                 
                <div id="dvGrid-Prest" style="border: none; margin-top: 5px;  width: 100%;">
                    <div class="xGrid-dvHeader gradient">
                        <table class="xGrid-tbSearch">
                            <tr>
                                <td style="padding: 5px; text-align: center;">&nbsp;</td>
                            </tr>
                        </table>
                        <table class="xGrid-tbCols">
                            <tr>
                                <th style="width: 5%;">&nbsp;</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 40%;">PRESTACI�N</th>                     
                                <th class="xGrid-tbCols-ColSortable" style="width: 17%;">FECHA ALTA</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 20%;">STATUS ACTUAL</th>
                                <th style="width: 18%;">&nbsp;</th>
                            </tr>
                        </table>
                    </div>
                    <div class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 200px; overflow-y: hidden;">
                    <?php
                    $datos = $objPrest->selectAll("a.curp='" . $_SESSION['xCurp'] . "'");
                    if( count($datos) > 0 ){
                        $html = '<table class="xGrid-tbBody" id="dvGrid-Cursos-tbBody">';
                        foreach( $datos As $reg => $dato ){
                            $html .= '<tr id="dvGrid-Prest-' . $dato["id_prest_personal"] . '">';
                                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';                
                                $html .= '<td style="text-align: left; width: 40%; ' . $sty_color . '">' . $dato["prestacion"] . '</td>';
                                $fecha_alta = (!empty($dato["fecha_alta"])) ? date("d/m/Y", strtotime($dato["fecha_alta"])) : '';
                                $html .= '<td style="text-align: center; width: 17%; ' . $sty_color . '">' . $fecha_alta . '</td>';
                                $html .= '<td style="text-align: center; width: 20%; ' . $sty_color . '">' . $dato["status_prest"] . '</td>';
                                $html .= '<td style="text-align: center; width: 18%; ' . $sty_color . '">';
                                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_prest_personal"] . '" title="Moficar esta prestaci�n..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                                $html .= '</td>';
                            $html .= '</tr>';
                        }
                        $html .= '</table>';
                        echo $html;
                    }                   
                    ?>    
                    </div>
                </div>
                
                <div id="dvFormPrest" title="">
                    <form id="frmPrest" method="post" action="#" enctype="multipart/form-data">
                    <table class="tbForm-Data">
                        <tr>
                            <td><label for="cbxPrestaciones">Tipo de Prestaci�n:</label></td>
                            <td class="validation">
                                <input type="text" name="txtPrestacion" id="txtPrestacion" value="" readonly="true" style="display: none; width: 300px;" />
                                <input type="hidden" name="hdnIdPrestacion" id="hdnIdPrestacion" value="0" />
                                <select name="cbxPrestaciones" id="cbxPrestaciones" style="max-width: 350px;"> 
                                    <option value="0"></option>                               
                                    <?php
                                    echo $objPrest->AdmcatPrestaciones->shwPrestaciones(0, $_SESSION["xCurp"]);
                                    ?>
                                </select>
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="cbxStatusPrest">Status actual:</label></td>
                            <td class="validation">
                                <select name="cbxStatusPrest" id="cbxStatusPrest" style="max-width: 300px;"> 
                                    <option value="-1"></option>                               
                                    <?php
                                    echo $objPrest->AdmcatStatusPrestacion->shwStatusPrest();
                                    ?>
                                </select>
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtFechaAlta">Fecha de Alta:</label></td>
                            <td class="validation">
                                <input type="text" name="txtFechaAlta" id="txtFechaAlta" value="" readonly="true" title="..." style="text-align: center; width: 120px;" />
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtObservaPrest">Observaciones:</label></td>
                            <td class="validation">
                                <textarea name="txtObservaPrest" id="txtObservaPrest" style="height: 60px; width: 410px;" ></textarea>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" name="hdnTypeOperPrest" id="hdnTypeOperPrest" value="0" />
                    <input type="hidden" name="hdnIdPrestPer" id="hdnIdPrestPer" value="0" />
                    </form>
                </div>
                <input type="hidden" id="hdnUrlProcesPrest" value="<?php echo $objSys->encrypt('adm/rhumanos/segsocial/ajx_proces_prest.php');?>" />
                <input type="hidden" id="hdnUrlGetPrest" value="<?php echo $objSys->encrypt('adm/rhumanos/segsocial/ajx_obt_prest.php');?>" />                
            </div>
            
            <!-- Depend. econ�micos -->
            <div id="tabs-2">
                <div class="dvBar-Normal" style="">
                    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <tr>
                            <td class="tdTituloTab">Dependientes econ�micos</td>
                            <td style="padding: 3px; text-align: right; width: 30%;">
                                <a href="#" id="btnAddDependEco" class="Tool-Bar-Btn gradient" style="height: 30px; min-width: 60px;" title="Agregar un dependiente econ�mico...">
                                    <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" />
                                </a>
                            </td>                                
                        </tr>
                    </table>
                </div>
                <div id="dvGrid-DependEco" style="border: none; margin-top: 5px;  width: 100%;">
                    <div class="xGrid-dvHeader gradient">
                        <table class="xGrid-tbSearch">
                            <tr>
                                <td style="padding: 5px; text-align: center;">&nbsp;</td>
                            </tr>
                        </table>
                        <table class="xGrid-tbCols">
                            <tr>
                                <th style="width: 5%;">&nbsp;</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 40%;">NOMBRE COMPLETO</th>                     
                                <th class="xGrid-tbCols-ColSortable" style="width: 17%;">FECHA NAC.</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 20%;">PARENTESCO</th>
                                <th style="width: 18%;">&nbsp;</th>
                            </tr>
                        </table>
                    </div>
                    <div class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 200px; overflow-y: hidden;">
                    <?php                    
                    $datos = $objDepend->selectAll("a.curp='" . $_SESSION['xCurp'] . "'", "a.id_parentesco Asc");
                    if( count($datos) > 0 ){
                        $html = '<table class="xGrid-tbBody" id="dvGrid-DependEco-tbBody">';
                        foreach( $datos As $reg => $dato ){
                            $html .= '<tr id="dvGrid-DependEco-' . $dato["id_dependiente"] . '">';
                                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';         
                                $nombreDependiente = $dato["a_paterno"] . ' ' . $dato["a_materno"] . ' ' . $dato["nombre"];       
                                $html .= '<td style="text-align: left; width: 40%; ' . $sty_color . '">' . $nombreDependiente . '</td>';
                                $html .= '<td style="text-align: center; width: 17%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha_nac"])) . '</td>';
                                $html .= '<td style="text-align: center; width: 20%; ' . $sty_color . '">' . $dato["parentesco"] . '</td>';            
                                $html .= '<td style="text-align: center; width: 18%;">';
                                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_dependiente"] . '" title="Moficar este curso..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                                $html .= '</td>';
                            $html .= '</tr>';
                        }
                        $html .= '</table>';
                        echo $html;
                    }
                    ?>                         
                    </div>
                </div>
                
                <div id="dvFormDependEco" title="">
                    <form id="frmDependEco" method="post" action="#" enctype="multipart/form-data">
                    <table class="tbForm-Data">
                        <tr>
                            <td><label for="txtNombre">Nombre(s):</label></td>
                            <td class="validation">
                                <input type="text" name="txtNombre" id="txtNombre" value="" maxlength="200" title="..." style="width: 250px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtAPaterno">Apellido Paterno:</label></td>
                            <td class="validation">
                                <input type="text" name="txtAPaterno" id="txtAPaterno" value="" maxlength="200" title="..." style="width: 250px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr> 
                        <tr>
                            <td><label for="txtAMaterno">Apellido Materno:</label></td>
                            <td class="validation">
                                <input type="text" name="txtAMaterno" id="txtAMaterno" value="" maxlength="200" title="..." style="width: 250px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>                         
                        <tr>
                            <td><label for="txtFechaNac">Fecha de Nacimiento:</label></td>
                            <td class="validation">
                                <input type="text" name="txtFechaNac" id="txtFechaNac" value="" readonly="true" title="..." style="text-align: center; width: 120px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="cbxParentesco">Parentesco:</label></td>
                            <td class="validation">
                                <select name="cbxParentesco" id="cbxParentesco" style="max-width: 300px;"> 
                                    <option value="0"></option>                               
                                    <?php
                                    echo $objDepend->AdmcatParentescos->shwParentescos();
                                    ?>
                                </select>
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" name="hdnTypeOperDependEco" id="hdnTypeOperDependEco" value="0" />
                    <input type="hidden" name="hdnIdDependEco" id="hdnIdDependEco" value="0" />
                    </form>
                </div>
                <input type="hidden" id="hdnUrlProcesDependEco" value="<?php echo $objSys->encrypt('adm/rhumanos/segsocial/ajx_proces_depend.php');?>" />
                <input type="hidden" id="hdnUrlGetDependEco" value="<?php echo $objSys->encrypt('adm/rhumanos/segsocial/ajx_obt_depend.php');?>" />                
            </div>            
        </div>
    </div>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>