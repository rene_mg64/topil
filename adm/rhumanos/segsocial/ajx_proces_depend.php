<?php
/**
 * Complemento ajax para procesar los datos de un dependiente econ�mico. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_dependientes.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objDepend = new AdmtblDependientes();    
    
    $idDependiente = $_POST["hdnIdDependEco"];
    $objDepend->id_dependiente = $idDependiente;
    $objDepend->curp = $_SESSION["xCurp"];
    $objDepend->id_parentesco = $_POST["cbxParentesco"];
    $objDepend->nombre = utf8_decode($_POST["txtNombre"]);
    $objDepend->a_paterno = utf8_decode($_POST["txtAPaterno"]);
    $objDepend->a_materno = utf8_decode($_POST["txtAMaterno"]);
    $objDepend->fecha_nac = (!empty($_POST["txtFechaNac"])) ? $objSys->convertirFecha($_POST["txtFechaNac"], "yyyy-mm-dd") : null;
    
    if( $_POST["hdnTypeOperDependEco"] == 1 ){
        $result = $objDepend->insert();
        $oper = "Ins";
        $id_reg = $result;
    } else if( $_POST["hdnTypeOperDependEco"] == 2 ){
        $result = $objDepend->update();
        $oper = "Edt";
        $id_reg = $idPrestPer;
    } else if( $_POST["hdnTypeOperDependEco"] == 3 ){
        $result = $objDepend->delete();
        $oper = "Dlt";
        $id_reg = $idPrestPer;
    }
       
    if ($result) {
        $ajx_datos['rslt']  = true;
                
        $objSys->registroLog($objUsr->idUsr, "admtbl_dependientes", $id_reg, $oper);
        
        // Se obtienen todos los dependientes econ�micos de la persona y se genera la lista
        $datos = $objDepend->selectAll("a.curp='" . $_SESSION['xCurp'] . "'");
        $html = '<table class="xGrid-tbBody" id="dvGrid-DependEco-tbBody">';
        foreach( $datos As $reg => $dato ){
            if( $result === true )
                $sty_color = ( $idDependiente == $dato["id_dependiente"] ) ? 'color: #2b60de;' : '';
            else
                $sty_color = ( $result == $dato["id_dependiente"] ) ? 'color: #2b60de;' : '';
            $html .= '<tr id="dvGrid-DependEco-' . $dato["id_dependiente"] . '">';
                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';         
                $nombreDependiente = $dato["a_paterno"] . ' ' . $dato["a_materno"] . ' ' . $dato["nombre"];       
                $html .= '<td style="text-align: left; width: 40%; ' . $sty_color . '">' . $nombreDependiente . '</td>';
                $html .= '<td style="text-align: center; width: 17%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha_nac"])) . '</td>';
                $html .= '<td style="text-align: center; width: 20%; ' . $sty_color . '">' . $dato["parentesco"] . '</td>';            
                $html .= '<td style="text-align: center; width: 18%;">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_dependiente"] . '" title="Moficar los datos del dependiente..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $ajx_datos['html']  = utf8_encode($html);
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objDepend->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>