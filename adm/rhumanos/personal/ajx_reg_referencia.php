<?php
/**
 * Complemento ajax para guardar los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_referencias.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objReferencia = new AdmtblReferencias();    
    
    $idReferencia = $_POST["hdnIdRef"];
    $objReferencia->id_referencia = $idReferencia;
    $objReferencia->curp = $_SESSION["xCurp"];
    $objReferencia->id_tipo_referencia = $_POST["cbxTipoReferencia"];
    $objReferencia->id_tipo_relacion = $_POST["cbxTipoRelacion"];
    $objReferencia->nombre = utf8_decode($_POST["txtNombre"]);
    $objReferencia->a_paterno = utf8_decode($_POST["txtAPaterno"]);
    $objReferencia->a_materno = utf8_decode($_POST["txtAMaterno"]);
    $objReferencia->sexo = $_POST["rbnSexo"];
    $objReferencia->ocupacion = utf8_decode($_POST["txtOcupacion"]);
    $objReferencia->calle_num = utf8_decode($_POST["txtCalle"]);
    $objReferencia->colonia = utf8_decode($_POST["txtColonia"]);
    $objReferencia->ciudad = utf8_decode($_POST["txtCiudad"]);
    $objReferencia->cp = $_POST["txtCodPostal"];
    $objReferencia->id_entidad = $_POST["cbxEntDomicilio"];
    $objReferencia->id_municipio = $_POST["cbxMpioDomicilio"];
    $objReferencia->tel_fijo = $_POST["txtTelFijo"];
    $objReferencia->tel_movil = $_POST["txtTelMovil"];
    
    $result = ($_POST["hdnTypeOper"] == 1) ? $objReferencia->insert() : $objReferencia->update();
    if ($result) {
        $ajx_datos['rslt']  = true;
        if( $_POST["hdnTypeOper"] == 1 ){
            $oper = "Ins";
            $id_reg = $result;    
        } else {
            $oper = "Edt";
            $id_reg = $idReferencia;
        }        
        $objSys->registroLog($objUsr->idUsr, "admtbl_referencias", $id_reg, $oper);
        // Se obtienen todas las referencias de la persona y se genera la lista
        $datos = $objReferencia->selectAll("a.curp='" . $_SESSION['xCurp'] . "'", "a.a_paterno, a.a_materno, a.nombre Asc");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Referencias-tbBody">';
        foreach( $datos As $reg => $dato ){
            if( $result === true )
                $sty_color = ( $idReferencia == $dato["id_referencia"] ) ? 'color: #2b60de;' : '';
            else
                $sty_color = ( $result == $dato["id_referencia"] ) ? 'color: #2b60de;' : '';
            $html .= '<tr id="dvGrid-Referencias-' . $dato["id_referencia"] . '">';
                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';
       			$nombrePersona = $dato["a_paterno"] . ' ' . $dato["a_materno"] . ' ' .$dato["nombre"];                
                $html .= '<td style="text-align: left; width: 32%; ' . $sty_color . '">' . $nombrePersona . '</td>';
                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . $dato["tipo_referencia"] . '</td>';                            
                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . $dato["tipo_relacion"] . '</td>';
                $html .= '<td style="text-align: center; width: 20%; ' . $sty_color . '">' . $dato["ocupacion"] . '</td>';            
                $html .= '<td style="text-align: center; width: 13%; ' . $sty_color . '">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="edt-' . $dato["id_referencia"] . '" title="Men� de opciones del personal..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';                
                //$html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="dlt-' . $dato["id_referencia"] . '" title="Dar de baja a esta persona..." ><img src="' . PATH_IMAGES . 'icons/delete24.png" alt="baja" /></a>';
                $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $ajx_datos['html']  = utf8_encode($html);
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objReferencia->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>