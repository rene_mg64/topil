<?php
/**
 * Ficha de Informaci�n Personal 
 */ 
session_start();
if( isset($_SESSION["admitted_xsisp"]) ){
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_datos_personales.class.php';
    include $path . 'includes/class/admtbl_adscripcion.class.php';
    include $path . 'includes/class/admtbl_domicilio.class.php';
    include $path . 'includes/class/admtbl_nivel_estudios.class.php';
    include $path . 'includes/class/admtbl_doctos_ident.class.php';
    include $path . 'includes/lib/fpdf16/pdf.php';
    
    $objSys = new System();
    $objDatPer = new AdmtblDatosPersonales();
    $objAdscrip = new AdmtblAdscripcion();
    $objDomi = new AdmtblDomicilio();
    $objNivelEst = new AdmtblNivelEstudios();
    $objDocIdent = new AdmtblDoctosIdent();
        
    $pdf = new PDF("P", "mm", "letter");
    
    $curp = $_SESSION["xCurpFicha"];
    
    $pdf->xInFooter = true;
    $pdf->xPageNo = true;    
    $pdf->txt_footer_l = "SISP - Ficha de informaci�n personal";
    $pdf->StartPageGroup();
    $pdf->AddPage();
    $pdf->SetMargins(10, 5, 10);
        
    // Encabezado
    $pdf->Image($path . "includes/css/imgs/plantilla/logo_gob_gro.jpg", 10, 5, 35, 27);    
    $pdf->SetFont("Arial", "B", 12);
    $pdf->SetXY(55, 10);
    $pdf->Cell(160, 5, "SECRETAR�A DE SEGURIDAD P�BLICA Y PROTECCI�N CIVIL");
    
    $pdf->SetFillColor(150);
    $pdf->Rect(12, 41, 195, 7, "F");
    $pdf->SetFillColor(255);
    $pdf->SetY(40);
    $pdf->SetFont("Arial", "B", 11);
    $pdf->Cell(0, 7, "FICHA DE INFORMACI�N PERSONAL", 1, 0, "C", true);
    
    $y = 55;
    /*
    *--------------------------------------------------------------------------------------*
    * Datos personales
    *--------------------------------------------------------------------------------------*
    */
    $objDatPer->select($curp);
    // Marco del bloque: Datos personales
    $pdf->SetFillColor(190);
    $pdf->Rect(11, $y-1, 196, 50, "F");
    $pdf->SetFillColor(255);
    $pdf->Rect(10, $y-2, 196, 50, "FD");
    // Foto
    $objDocIdent->select($curp);
    $foto_fte = ( !empty($objDocIdent->foto_frente) ) ? $path . 'adm/expediente/fotos/' . $objDocIdent->foto_frente : '';
    if( empty($foto_fte) )
        $foto_fte = ( $objDatPer->genero == 1 ) ? $path . 'adm/expediente/sin_foto_h.jpg' : 'adm/expediente/sin_foto_m.jpg';
    $pdf->SetFillColor(255);
    $pdf->Rect(17, $y - 0.5, 38, 47, "FD");
    $pdf->Image($foto_fte, 17.5, $y, 37, 46);    
    // Curp
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(70, $y);
    $pdf->Cell(40, 5, "C.U.R.P.:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, $objDatPer->curp);
    // Cuip
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(70, $y);
    $pdf->Cell(40, 5, "C.U.I.P.:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(95, 5, ( !empty($objDatPer->cuip) ) ? $objDatPer->cuip : "- - -");
    // Nombre
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(70, $y);
    $pdf->Cell(40, 5, "Nombre:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, $objDatPer->nombre . " " . $objDatPer->a_paterno . " " . $objDatPer->a_materno);
    // Fecha de nacimiento
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(70, $y);
    $pdf->Cell(40, 5, "Fecha de nacimiento:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, date("d/m/Y", strtotime($objDatPer->fecha_nac)));
    // Sexo
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(70, $y);
    $pdf->Cell(40, 5, "Sexo:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, ($objDatPer->genero == 1) ? "MASCULINO" : "FEMENINO");
    // Cartilla
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(70, $y);
    $pdf->Cell(40, 5, "Cartilla del S.M.N.:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, ( !empty($objDatPer->mat_cartilla) ) ? $objDatPer->mat_cartilla : "- - -");
    // Estado civil
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(70, $y);
    $pdf->Cell(40, 5, "Estado civil:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, $objDatPer->AdmcatEstadoCivil->estado_civil);
    // Gpo. sanguineo
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(70, $y);
    $pdf->Cell(40, 5, "Grupo sanguineo:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, $objDatPer->AdmcatTipoSangre->tipo_sangre);
    /*
    *--------------------------------------------------------------------------------------*
    * Domicilio
    *--------------------------------------------------------------------------------------*
    */
    $objDomi->select($curp);
    $y += 10;
    // Marco del bloque: Domicilio
    $pdf->SetFillColor(190);
    $pdf->Rect(10, $y, 196, 47);
    $pdf->SetY($y);
    $pdf->SetFont("Arial", "B", 11);
    $pdf->Cell(196, 5, "DOMICILIO:", 1, 0, "C", true);
    // Calle y n�mero
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Domicilio particular:");
    $pdf->SetFont("Arial", "", 10);
    $num_int = ( !empty($objDomi->n_interior) ) ? ", NUM. INT. " . $objDomi->n_interior : "";
    $pdf->CellFitScale(140, 5, $objDomi->calle . ", NUM. EXT. " . $objDomi->n_exterior . $num_int . ", COL. " . $objDomi->colonia);
    if( !empty($objDomi->entre_calle1) ){
        $y += 5;
        $pdf->SetXY(60, $y);
        $pdf->CellFitScale(140, 5, "ENTRE LA CALLE " . $objDomi->entre_calle1 . " Y LA CALLE " . $objDomi->entre_calle2);
    }
    // C�digo postal
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "C�digo postal:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, ( !empty($objDomi->cod_postal) ) ? $objDomi->cod_postal : "- - -");
    // Ciudad
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Ciudad o localidad:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objDomi->ciudad);
    // Municipio
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Municipio:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objDomi->AdmcatMunicipios->municipio);
    // Estado
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Entidad federativa:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objDomi->AdmcatMunicipios->AdmcatEntidades->entidad);
    // Tel�fonos
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Tel�fonos:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objDomi->tel_fijo . ", " . $objDomi->tel_movil);
    /*
    *--------------------------------------------------------------------------------------*
    * Nivel de estudios
    *--------------------------------------------------------------------------------------*
    */
    $objNivelEst->select($curp);
    $y += 12;
    // Marco del bloque: Nivel de estudios
    $pdf->SetFillColor(190);
    $pdf->Rect(10, $y, 196, 30);
    $pdf->SetY($y);
    $pdf->SetFont("Arial", "B", 11);
    $pdf->Cell(196, 5, "NIVEL DE ESTUDIOS:", 1, 0, "C", true);
    // Nivel
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Nivel de estudios:");
    $pdf->SetFont("Arial", "", 10);
    if( $objNivelEst->eficencia_term == 1 ){
        $efi_term = " [CONCLUIDO]";
    } else if ($objNivelEst->eficencia_term == 2 ){
        $efi_term = " [CURSANDO]";
    } else if ($objNivelEst->eficencia_term == 3 ){
        $efi_term = " [TRUNCA]";
    } else {
        $efi_term = "";
    }
    $pdf->Cell(130, 5, $objNivelEst->AdmcatNivelEstudios->nivel_estudios . $efi_term);
    // Instituci�n
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Instituci�n:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objNivelEst->institucion);
    // Carrera
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Carrera:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->CellFitScale(140, 5, ( !empty($objNivelEst->carrera) ) ? $objNivelEst->carrera : "- - -");
    // C�dula
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "C�dula profesional:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, ( !empty($objNivelEst->n_cedula) ) ? $objNivelEst->n_cedula : "- - -");
    /*
    *--------------------------------------------------------------------------------------*
    * Adscripci�n laboral
    *--------------------------------------------------------------------------------------*
    */
    $objAdscrip->select($curp);
    $y += 8;
    // Marco del bloque: Adscripci�n
    $pdf->SetFillColor(190);
    $pdf->Rect(10, $y, 196, 65);
    $pdf->SetY($y);
    $pdf->SetFont("Arial", "B", 11);
    $pdf->Cell(196, 5, "ADSCRIPCI�N LABORAL:", 1, 0, "C", true);
    // �rea
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "�rea de adscripci�n:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->CellFitScale(140, 5, $objAdscrip->AdmcatAreas->area);
    // Categor�a
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Categor�a:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objAdscrip->AdmcatCategorias->categoria);
    /*
    // Categor�a
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Categor�a:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objAdscrip->AdmcatCategorias->categoria);
    */
    // Tipo de funciones
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Tipo de funciones:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objAdscrip->AdmcatTipoFunciones->tipo_funcion);
    // Especialidad
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Especialidad:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objAdscrip->AdmcatEspecialidades->especialidad);
    // Nivel de mando
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Nivel de mando:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objAdscrip->AdmcatNivelMando->nivel_mando);
    // Fecha de ingreso
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Fecha de ingreso:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, date("d/m/Y", strtotime($objAdscrip->fecha_ing)));
    // Horario
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Horario de labores:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objAdscrip->AdmcatHorarios->horario);
    // Ubicaci�n
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Ubicaci�n:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->CellFitScale(140, 5, $objAdscrip->AdmcatUbicaciones->ubicacion);    
    // Municipio/region
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Municipio:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(130, 5, $objAdscrip->AdmcatMunicipios->municipio . ", " . $objAdscrip->AdmcatMunicipios->AdmcatRegiones->region);
    
    $pdf->AddPage();
    $y = 10;
    /*
    *--------------------------------------------------------------------------------------*
    * Reguardo de armamento
    *--------------------------------------------------------------------------------------*
    */
    if( $objAdscrip->id_tipo_funcion == 1 ){
        // Marco del bloque: Armamento
        $pdf->SetFillColor(190);
        $pdf->Rect(10, $y, 196, 40);
        $pdf->SetY($y);
        $pdf->SetFont("Arial", "B", 11);
        $pdf->Cell(196, 5, "PORTACI�N DE ARMAMENTO:", 1, 0, "C", true);
        $y += 5;
        
        include $path . 'includes/class/logtbl_arm_asignacion.class.php';
        $objAsigArma = new LogtblArmAsignacion();
        $datArmas = $objAsigArma->selectLstAsignadas("curp='" . $objDatPer->curp . "'");
        
        $pdf->SetFont("Arial", "B", 9);
        $pdf->SetFillColor(220);
        $pdf->SetY($y);
        $pdf->Cell(36, 5, "MATRICULA", 1, 0, "C", "true");
        $pdf->Cell(25, 5, "TIPO", 1, 0, "C", "true");
        $pdf->Cell(45, 5, "MARCA", 1, 0, "C", "true");
        $pdf->Cell(30, 5, "MODELO", 1, 0, "C", "true");
        $pdf->Cell(30, 5, "CLASE", 1, 0, "C", "true");
        $pdf->Cell(30, 5, "CALIBRE", 1, 0, "C", "true");
        $y += 5;
        $pdf->SetFont("Arial", "", 9);
        foreach( $datArmas As $reg => $arma ){
            $pdf->SetY($y);
            $pdf->CellFitScale(36, 5, $arma["matricula"], 1, 0, "C");
            $pdf->CellFitScale(25, 5, $arma["tipo"], 1, 0, "C");
            $pdf->CellFitScale(45, 5, $arma["marca"], 1, 0, "C");
            $pdf->CellFitScale(30, 5, $arma["modelo"], 1, 0, "C");
            $pdf->CellFitScale(30, 5, $arma["clase"], 1, 0, "C");
            $pdf->CellFitScale(30, 5, $arma["calibre"], 1, 0, "C");
            $y += 5;
        }
        
        $y += 30;
    }
    
    /*
    *--------------------------------------------------------------------------------------*
    * Resguardo de veh�culo
    *--------------------------------------------------------------------------------------*
    */
    // Marco del bloque: Veh�culos
    $pdf->SetFillColor(190);
    $pdf->Rect(10, $y, 196, 30);
    $pdf->SetY($y);
    $pdf->SetFont("Arial", "B", 11);
    $pdf->Cell(196, 5, "RESGUARDO DE VEH�CULO:", 1, 0, "C", true);
    $y += 5;
        
    include $path . 'includes/class/logtbl_veh_datos.class.php';
    $objVehic = new LogtblVehDatos();
    
    $datVehic = $objVehic->selectAll("a.curp='" . $objDatPer->curp . "'");
    $pdf->SetFont("Arial", "B", 9);
    $pdf->SetFillColor(220);
    $pdf->SetY($y);
    $pdf->Cell(50, 5, "No. SERIE", 1, 0, "C", "true");
    $pdf->Cell(35, 5, "PLACAS", 1, 0, "C", "true");
    $pdf->Cell(50, 5, "MARCA", 1, 0, "C", "true");
    $pdf->Cell(40, 5, "TIPO", 1, 0, "C", "true");
    $pdf->Cell(21, 5, "MODELO", 1, 0, "C", "true");    
    $y += 5;
    $pdf->SetFont("Arial", "", 9);
    foreach( $datVehic As $reg => $veh ){
        $pdf->SetY($y);
        $objVehic->LogtblVehiculos->select($veh["id_vehiculo"]);
        
        $pdf->CellFitScale(50, 5, $objVehic->LogtblVehiculos->num_serie, 1, 0, "C");
        $placas = ( !empty($objVehic->LogtblVehiculos->placas) ) ? $objVehic->LogtblVehiculos->placas : "- - -";
        $pdf->CellFitScale(35, 5, $placas, 1, 0, "C");
        $pdf->CellFitScale(50, 5, $objVehic->LogtblVehiculos->LogcatVehTipo->LogcatVehMarca->marca, 1, 0, "C");
        $pdf->CellFitScale(40, 5, $objVehic->LogtblVehiculos->LogcatVehTipo->tipo, 1, 0, "C");
        $pdf->CellFitScale(21, 5, $objVehic->LogtblVehiculos->LogcatVehModelo->modelo, 1, 0, "C");
        
        $y += 5;
    }
    /*
    *--------------------------------------------------------------------------------------*
    * Resguardo de equipo de comunicaci�n
    *--------------------------------------------------------------------------------------*
    */
    
    $pdf->Output();   
}
?>