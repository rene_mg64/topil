<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admcat_municipios.class.php';
include 'includes/class/admcat_estado_civil.class.php';
include 'includes/class/admcat_nivel_estudios.class.php';
include 'includes/class/admcat_areas.class.php';
include 'includes/class/admcat_tipo_funciones.class.php';
include 'includes/class/admcat_especialidades.class.php';
include 'includes/class/admcat_categorias.class.php';

$objMunicipio = new AdmcatMunicipios();
$objEdoCivil = new AdmcatEstadoCivil();
$objNivelEstudios = new AdmcatNivelEstudios();
$objArea = new AdmcatAreas();
$objTipoFuncion = new AdmcatTipoFunciones();
$objEspecialidad = new AdmcatEspecialidades();
$objCategoria  = new AdmcatCategorias();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="includes/js/curp.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/personal/_js/aspirante.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Reclutamiento');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('ctrl_aspi');
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('aspi_rg');
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">                    
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los datos del nuevo recluta...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo recluta...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 850px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Registro</span>
            <fieldset class="fsetForm-Data" style="width: 90%;">  
                <legend>Datos Personales</legend>                              
                <table id="tbForm-DatPer" class="tbForm-Data"> 
                    <tr>
                        <td><label for="txtCurp">C.U.R.P.:</label></td>
                        <td class="validation" style="width: 400px;">
                            <input type="text" name="txtCurp" id="txtCurp" value="" maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>              
                    <tr>
                        <td><label for="txtNombre">Nombre:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNombre" id="txtNombre" value="" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtAPaterno">Apellido Paterno:</label></td>
                        <td class="validation">
                            <input type="text" name="txtAPaterno" id="txtAPaterno" value="" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtAMaterno">Apellido Materno:</label></td>
                        <td class="validation">
                            <input type="text" name="txtAMaterno" id="txtAMaterno" value="" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFechaNac">Fecha de Nacimiento:</label></td>
                        <td class="validation">
                            <input type="text" name="txtFechaNac" id="txtFechaNac" value="" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Sexo:</label></td>
                        <td class="validation">
                            <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnSexo" id="rbnSexo1" value="1" />Masculino</label>
                            <label class="label-Radio"><input type="radio" name="rbnSexo" id="rbnSexo2" value="2" />Femenino</label>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxEdoCivil">Estado Civil:</label></td>
                        <td class="validation">
                            <select name="cbxEdoCivil" id="cbxEdoCivil" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objEdoCivil->shwEstadoCivil();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                </table>
            </fieldset>
               
            <fieldset class="fsetForm-Data" style="width: 90%;">
                <legend>Domicilio</legend>
                <table class="tbForm-Data">
                    <tr>
                        <td><label for="txtCalle">Nombre de la Calle:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCalle" id="txtCalle" value="" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>              
                    <tr>
                        <td><label for="txtNumExt">No. Exterior:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumExt" id="txtNumExt" value="" maxlength="5" title="..." style="width: 70px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                    
                    <tr>
                        <td><label for="txtColonia">Colonia:</label></td>
                        <td class="validation">
                            <input type="text" name="txtColonia" id="txtColonia" value="" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCiudad">Ciudad o Localidad:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCiudad" id="txtCiudad" value="" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxMpioDomicilio">Municipio:</label></td>
                        <td class="validation">
                            <select name="cbxMpioDomicilio" id="cbxMpioDomicilio" style="max-width: 500px;">    
                                <option value="0"></option>                                    
                                <?php
                                echo $objMunicipio->shwMunicipios(0, 12); // Entidad: 12=Guerrero
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxEntDomicilio">Entidad Federativa:</label></td>
                        <td class="validation">
                            <select name="cbxEntDomicilio" id="cbxEntDomicilio">                                        
                                <?php
                                echo $objMunicipio->AdmcatEntidades->shwEntidades(12, 1);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtTelFijo">Tel�fono Fijo:</label></td>
                        <td class="validation">
                            <input type="text" name="txtTelFijo" id="txtTelFijo" value="" maxlength="15" title="..." style="width: 150px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtTelMovil">Tel�fono M�vil:</label></td>
                        <td class="validation">
                            <input type="text" name="txtTelMovil" id="txtTelMovil" value="" maxlength="15" title="..." style="width: 150px;" />
                        </td>
                    </tr>
                </table>
            </fieldset>
                
            <fieldset class="fsetForm-Data" style="width: 90%;">
                <legend>Nivel de Estudios</legend>
                <table class="tbForm-Data">
                    <tr>
                        <td><label for="cbxNivelEstudios">Nivel de Estudios:</label></td>
                        <td class="validation">
                            <select name="cbxNivelEstudios" id="cbxNivelEstudios">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objNivelEstudios->shwNivelEstudios();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <!--
                    <tr style="height: 35px;">
                        <td><label>Eficiencia Terminal:</label></td>
                        <td class="validation">
                            <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnEfiTerminal" value="1" />Concluido</label>
                            <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnEfiTerminal" value="2" />Cursando</label>
                            <label class="label-Radio"><input type="radio" name="rbnEfiTerminal" value="3" />Truncado</label>
                        </td>
                    </tr>
                    -->
                </table>
            </fieldset>
                
            <fieldset class="fsetForm-Data" style="width: 90%;">
                <legend>Datos de Adscripci�n</legend>
                <table class="tbForm-Data">
                    <tr>
                        <td class="validation" colspan="2" style="border-bottom: 1px dotted gray; padding-bottom: 12px;">
                            <label for="cbxArea">�rea de Adscripci�n:</label><br />
                            <select name="cbxArea" id="cbxArea" style="max-width: 600px;">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objArea->shwAreas(70);
                                ?>                                        
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                            
                    <tr>
                        <td><label for="cbxTipoFuncion">Tipo de Funciones:</label></td>
                        <td class="validation">
                            <select name="cbxTipoFuncion" id="cbxTipoFuncion">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objTipoFuncion->shwTipoFunciones();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxEspecialidad">Especialidad:</label></td>
                        <td class="validation">
                            <select name="cbxEspecialidad" id="cbxEspecialidad" style="max-width: 500px;">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objEspecialidad->shwEspecialidades();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxCategoria">Categor�a Propuesta:</label></td>
                        <td class="validation">
                            <select name="cbxCategoria" id="cbxCategoria" style="max-width: 500px;">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objCategoria->shwCategorias();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCargo">Puesto o Cargo:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCargo" id="txtCargo" value="" maxlength="100" title="..." style="width: 450px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td><label for="cbxRegion">Regi�n:</label></td>
                        <td class="validation">
                            <select name="cbxRegion" id="cbxRegion" style="max-width: 500px;">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objMunicipio->AdmcatRegiones->shwRegiones();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
        <input type="hidden" name="dtTypeOper" value="1" />
        <input type="hidden" id="hdnUrlMpio" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_municipios.php');?>" />
    </form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>