<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
$path = '';
include_once $path . 'includes/class/admtbl_domicilio.class.php';
$objDomi = new AdmtblDomicilio();

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/rhumanos/personal/_js/datosrg.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Actualización');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    $curp = $_SESSION['xCurp'];
    // Domicilio
    $objDomi->curp = $curp;
    $objDomi->calle = $_POST['txtCalle'];
    $objDomi->n_exterior = $_POST['txtNumExt'];
    $objDomi->n_interior = $_POST['txtNumInt'];
    $objDomi->entre_calle1 = $_POST['txtEntreCalle1'];
    $objDomi->entre_calle2 = $_POST['txtEntreCalle2'];
    $objDomi->colonia = $_POST['txtColonia'];
    $objDomi->ciudad = $_POST['txtCiudad'];
    $objDomi->cod_postal = (!empty($_POST['txtCodPostal'])) ? $_POST['txtCodPostal'] : null;
    $objDomi->tel_fijo = $_POST['txtTelFijo'];
    $objDomi->tel_movil = $_POST['txtTelMovil'];
    $objDomi->id_municipio = $_POST['cbxMpioDomicilio'];
    $objDomi->id_entidad = $_POST['cbxEntDomicilio'];
           
    //-------------------------------------------------------------------//            
    if ($objDomi->update()) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_domicilio', $curp, "Edt");
    } else {        
        $error = (!empty($objDomi->msjError)) ? $objDomi->msjError : 'Error al guardar los datos del Domicilio';        
    }
    
    $mod = $objSys->encrypt("domicilio_edit");//(empty($error)) ? $objSys->encrypt("persona_menu") : $objSys->encrypt("domicilio_edit"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>