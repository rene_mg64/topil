<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_referencias.class.php';
include 'includes/class/admcat_municipios.class.php';
include 'includes/class/admcat_tipo_relacion.class.php';

$objReferencia = new AdmtblReferencias();
$objMunicipio = new AdmcatMunicipios();
$objTipoRel = new AdmcatTipoRelacion();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/personal/_js/referencias.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    $urlBack = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('persona_menu');
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <a href="<?php echo $urlBack;?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Regresar al men�...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; max-width: 1100px;">
        <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Gesti�n de Referencias</span>
        <div class="dvBar-Normal" style="margin: auto auto; margin-top: 7px; padding: 3px 1px 3px 1px; text-align: right; width: 98%;">
            <?php
            if( $_GET["st"] != 2 ){
            ?>
            <a href="#" id="btnAgregar" class="Tool-Bar-Btn gradient" style="width: 80px;" title="Regresar al men�...">
                <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Agregar
            </a>
            <?php
            }
            ?>
        </div>
        <div id="dvGrid-Referencias" style="border: none; height: 350px; margin: auto auto; margin-top: 7px; width: 98%;">
            <div class="xGrid-dvHeader gradient">
                <table class="xGrid-tbSearch">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <table class="xGrid-tbCols">
                    <tr>  
                        <th style="width: 5%;">N.P.</th>
                        <th style="width: 32%;" class="xGrid-thNo-Class">NOMBRE COMPLETO</th>  
                        <th style="width: 15%;" class="xGrid-thNo-Class">TIPO REFERENCIA</th>
                        <th style="width: 15%;" class="xGrid-thNo-Class">TIPO RELACI�N</th>
                        <th style="width: 20%;" class="xGrid-thNo-Class">OCUPACI�N</th>
                        <th style="width: 13%;" class="xGrid-thNo-Class">&nbsp;</th>
                    </tr>
                </table>
            </div>
            <div class="xGrid-dvBody" style="overflow: hidden;">
                <?php
                $datos = $objReferencia->selectAll("a.curp='" . $_SESSION['xCurp'] . "'", "a.a_paterno, a.a_materno, a.nombre Asc");
                $html = '<table class="xGrid-tbBody" id="dvGrid-Referencias-tbBody">';
                foreach( $datos As $reg => $dato ){
                    $html .= '<tr id="dvGrid-Referencias-' . $dato["id_referencia"] . '">';
                        $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';
               			$nombrePersona = $dato["a_paterno"] . ' ' . $dato["a_materno"] . ' ' .$dato["nombre"];                
                        $html .= '<td style="text-align: left; width: 32%;">' . $nombrePersona . '</td>';
                        $html .= '<td style="text-align: center; width: 15%;">' . $dato["tipo_referencia"] . '</td>';                            
                        $html .= '<td style="text-align: center; width: 15%;">' . $dato["tipo_relacion"] . '</td>';
                        $html .= '<td style="text-align: center; width: 20%;">' . $dato["ocupacion"] . '</td>';            
                        $html .= '<td style="text-align: center; width: 13%;">';
                        $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="edt-' . $dato["id_referencia"] . '" title="Men� de opciones del personal..." ><img src="' . PATH_IMAGES . 'icons/edit24.png" alt="modificar" /></a>';                
                        //$html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="dlt-' . $dato["id_referencia"] . '" title="Dar de baja a esta persona..." ><img src="' . PATH_IMAGES . 'icons/delete24.png" alt="baja" /></a>';
                        $html .= '</td>';
                    $html .= '</tr>';
                }
                $html .= '</table>';
                
                echo $html;
                ?>
            </div>        
        </div>
    </div>        
       
    <div id="dvFormReferencia" title="SISP :: ">
        <form id="frmRegistro" method="post" action="#" enctype="multipart/form-data">       
            <fieldset class="fsetForm-Data">
                <table class="tbForm-Data">                                          
                    <tr>
                        <td><label for="cbxTipoReferencia">Tipo de Referencia:</label></td>
                        <td class="validation">
                            <select name="cbxTipoReferencia" id="cbxTipoReferencia" style="max-width: 300px;"> 
                                <option value="0"></option>                               
                                <?php
                                echo $objTipoRel->AdmcatTipoReferencia->shwTipoReferencias();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxTipoRelacion">Tipo de Relaci�n:</label></td>
                        <td class="validation">
                            <select name="cbxTipoRelacion" id="cbxTipoRelacion" style="max-width: 300px;">
                                <option value="0"></option>
                            </select>
                            <span class="pRequerido">*</span>
                            <input type="hidden" id="hdnUrlRel" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_tiporelaciones.php');?>" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtNombre">Nombre:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNombre" id="txtNombre" value="" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtAPaterno">Apellido Paterno:</label></td>
                        <td class="validation">
                            <input type="text" name="txtAPaterno" id="txtAPaterno" value="" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtAMaterno">Apellido Materno:</label></td>
                        <td class="validation">
                            <input type="text" name="txtAMaterno" id="txtAMaterno" value="" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Sexo:</label></td>
                        <td class="validation">                            
                            <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnSexo" id="rbnSexo1" value="1" />Masculino</label>
                            <label class="label-Radio"><input type="radio" name="rbnSexo" id="rbnSexo2" value="2" />Femenino</label>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtOcupacion">Ocupaci�n:</label></td>
                        <td class="validation">
                            <input type="text" name="txtOcupacion" id="txtOcupacion" value="" maxlength="50" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCalle">Calle y N�mero:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCalle" id="txtCalle" value="" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtColonia">Colonia:</label></td>
                        <td class="validation">
                            <input type="text" name="txtColonia" id="txtColonia" value="" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCiudad">Ciudad o Localidad:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCiudad" id="txtCiudad" value="" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCodPostal">C�digo Postal:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCodPostal" id="txtCodPostal" value="" maxlength="5" title="..." style="width: 70px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxEntDomicilio">Entidad Federativa:</label></td>
                        <td class="validation">
                            <select name="cbxEntDomicilio" id="cbxEntDomicilio">
                                <?php
                                echo $objMunicipio->AdmcatEntidades->shwEntidades(12, 1); // Entidad: 12=Guerrero
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr> 
                    <tr>
                        <td><label for="cbxMpioDomicilio">Municipio:</label></td>
                        <td class="validation">
                            <select name="cbxMpioDomicilio" id="cbxMpioDomicilio" style="max-width: 470px;">                                                                   
                                <?php
                                echo $objMunicipio->shwMunicipios(0, 12); // Entidad: 12=Guerrero
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                            <input type="hidden" id="hdnUrlMpio" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_municipios.php');?>" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtTelFijo">Tel�fono Fijo:</label></td>
                        <td class="validation">
                            <input type="text" name="txtTelFijo" id="txtTelFijo" value="" maxlength="15" title="..." style="width: 150px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtTelMovil">Tel�fono M�vil:</label></td>
                        <td class="validation">
                            <input type="text" name="txtTelMovil" id="txtTelMovil" value="" maxlength="15" title="..." style="width: 150px;" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
            
            <input type="hidden" name="hdnTypeOper" id="hdnTypeOper" value="1" />
            <input type="hidden" name="hdnUrlSave" id="hdnUrlSave" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_reg_referencia.php');?>" />
            <input type="hidden" name="hdnUrlGet" id="hdnUrlGet" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_referencia.php');?>" />
            <input type="hidden" name="hdnIdRef" id="hdnIdRef" value="0" />
        </form>
    </div>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>