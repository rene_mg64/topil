<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_datos_personales.class.php';
include 'includes/class/admtbl_doctos_ident.class.php';
$objDatPer = new AdmtblDatosPersonales();
$objDocIdent = new AdmtblDoctosIdent();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                /*'scripts' => array('<script type="text/javascript" src="adm/rhumanos/personal/_js/persona.js"></script>'), */
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <!-- Botones de opci�n... -->                    
                    <a href="index.php" id="btnRegresar" class="Tool-Bar-Btn" style="" title="Regresar...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <?php
    $_SESSION['xCurp'] = ( isset($_GET['id']) ) ? $objSys->decrypt($_GET['id']) : $_SESSION['xCurp'];
    $objDatPer->select($_SESSION['xCurp']);
    // Foto
    $objDocIdent->select($_SESSION['xCurp']);
    $foto_fte = ( !empty($objDocIdent->foto_frente) ) ? 'adm/expediente/fotos/' . $objDocIdent->foto_frente : '';
    if( empty($foto_fte) )
        $foto_fte = ( $objDatPer->genero == 1 ) ? 'adm/expediente/sin_foto_h.jpg' : 'adm/expediente/sin_foto_m.jpg';
    ?>    
    
    <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; margin-bottom: 60px; text-align: left; width: 600px;">
        <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Opciones del Personal</span>
        
        <table class="tbForm-Data" style="margin: 10px; width: 100%">
            <tr>
                <td id="tdFoto" rowspan="10" style="text-align: center; vertical-align: middle;">
                    <div class="dvFoto-Grande" style="background-image: url(<?php echo $foto_fte;?>); vertical-align: bottom;"></div>
                    <p style="color: #696565; font-size: 11pt; font-weight: bold;">
                        <?php echo $objDatPer->a_paterno .' ' . $objDatPer->a_materno . ' ' . $objDatPer->nombre;?>                        
                        <span style="display: block; font-size: 10pt; margin-top: 5px;">[<?php echo $objDatPer->curp;?>]</span>                        
                        <span style="display: block; font-size: 9pt; margin-top: 5px;"><?php echo $objDatPer->AdmcatStatus->status;?></span>
                    </p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <a href="<?php echo "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('datper_edit');?>" class="btnMenu-Opciones gradient" style="max-width: 250px;">Datos Personales</a>
                </td>
            </tr>          
            <tr>
                <td style="text-align: center;">
                    <a href="<?php echo "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('domicilio_edit');?>" class="btnMenu-Opciones gradient" style="max-width: 250px;">Domicilio</a>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <a href="<?php echo "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('nivelest_edit');?>" class="btnMenu-Opciones gradient" style="max-width: 250px;">Nivel de Estudios</a>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <a href="<?php echo "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('adscrip_edit');?>" class="btnMenu-Opciones gradient" style="max-width: 250px;">Adscripci�n Laboral</a>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <a href="<?php echo "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('datos_nomina');?>" class="btnMenu-Opciones gradient" style="max-width: 250px;">Datos de la N�mina</a>
                </td>
            </tr>  
            <tr>
                <td style="text-align: center;">
                    <a href="<?php echo "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('referencias') . '&st=' . $objDatPer->id_status;?>" class="btnMenu-Opciones gradient" style="max-width: 250px;">Referencias</a>
                </td>                
            </tr>
            <tr>
                <td style="text-align: center;">
                    <a href="<?php echo "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('doc_ident');?>" class="btnMenu-Opciones gradient" style="max-width: 250px;">Documentos de Identidad</a>
                </td>                
            </tr>
        </table>
    </div>
    
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>