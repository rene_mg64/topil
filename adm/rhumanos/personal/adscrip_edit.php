<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_adscripcion.class.php';
include 'includes/class/admcat_municipios.class.php';
include 'includes/class/admcat_areas.class.php';
include 'includes/class/admcat_tipo_funciones.class.php';
include 'includes/class/admcat_especialidades.class.php';
include 'includes/class/admcat_categorias.class.php';
include 'includes/class/admcat_nivel_mando.class.php';
include 'includes/class/admcat_horarios.class.php';
include 'includes/class/admcat_ubicaciones.class.php';

$objAdscrip = new AdmtblAdscripcion();
$objMunicipio = new AdmcatMunicipios();
$objArea = new AdmcatAreas();
$objTipoFuncion = new AdmcatTipoFunciones();
$objEspecialidad = new AdmcatEspecialidades();
$objCategoria  = new AdmcatCategorias();
$objNivelMando = new AdmcatNivelMando();
$objHorario = new AdmcatHorarios();
$objUbicacion = new AdmcatUbicaciones();

$objAdscrip->select($_SESSION['xCurp']);

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/_js/rhumanos/personal/adscrip.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('persona_menu');
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('adscrip_rg');
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <?php
                    if( $objAdscrip->AdmtblDatosPersonales->id_status != 2 ){// diferente de BAJA
                    ?>
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los cambios realizados...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <?php
                    }
                    ?>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la modificaci�n de datos...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 850px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Actualizaci�n : Adscripci�n Laboral</span>
            <fieldset class="fsetForm-Data">
                <table class="tbForm-Data">
                    <tr>
                        <td class="validation" colspan="2" style="border-bottom: 1px dotted gray; padding-bottom: 12px;">
                            <label for="cbxArea">�rea de Adscripci�n:</label><br />
                            <select name="cbxArea" id="cbxArea" style="max-width: 600px;">                                
                                <?php
                                echo $objArea->shwAreas(70, $objAdscrip->id_area);
                                ?>                                        
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                            
                    <tr>
                        <td><label for="cbxTipoFuncion">Tipo de Funciones:</label></td>
                        <td class="validation">
                            <select name="cbxTipoFuncion" id="cbxTipoFuncion">                                
                                <?php
                                echo $objTipoFuncion->shwTipoFunciones($objAdscrip->id_tipo_funcion);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxEspecialidad">Especialidad:</label></td>
                        <td class="validation">
                            <select name="cbxEspecialidad" id="cbxEspecialidad" style="max-width: 500px;">
                                <?php
                                echo $objEspecialidad->shwEspecialidades($objAdscrip->id_especialidad);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxCategoria">Categor�a Asignada:</label></td>
                        <td class="validation">
                            <select name="cbxCategoria" id="cbxCategoria" style="max-width: 500px;">
                                <?php
                                echo $objCategoria->shwCategorias($objAdscrip->id_categoria);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCargo">Puesto o Cargo:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCargo" id="txtCargo" value="<?php echo $objAdscrip->cargo;?>" maxlength="100" title="..." style="width: 450px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxNivelMando">Nivel de Mando:</label></td>
                        <td class="validation">
                            <select name="cbxNivelMando" id="cbxNivelMando" style="max-width: 500px;">
                                <?php
                                echo $objNivelMando->shwNivelMando($objAdscrip->id_nivel_mando);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFechaIng">Fecha de Ingreso:</label></td>
                        <td class="validation">
                            <input type="text" name="txtFechaIng" id="txtFechaIng" value="<?php echo date('d/m/Y', strtotime($objAdscrip->fecha_ing));?>" maxlength="35" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxHorario">Horario Laboral:</label></td>
                        <td class="validation">
                            <select name="cbxHorario" id="cbxHorario" style="max-width: 500px;">
                                <?php
                                echo $objHorario->shwHorarios($objAdscrip->id_horario);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxRegion">Regi�n:</label></td>
                        <td class="validation">
                            <select name="cbxRegion" id="cbxRegion" style="max-width: 500px;">
                                <?php
                                $objMunicipio->select($objAdscrip->id_municipio);
                                echo $objMunicipio->AdmcatRegiones->shwRegiones($objMunicipio->id_region);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxMpioAdscripcion">Municipio:</label></td>
                        <td class="validation">
                            <select name="cbxMpioAdscripcion" id="cbxMpioAdscripcion" style="max-width: 500px;">
                                <?php
                                echo $objMunicipio->shwMunicipios($objAdscrip->id_municipio, 0, $objMunicipio->id_region);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxUbicacion">Ubicaci�n:</label></td>
                        <td class="validation">
                            <select name="cbxUbicacion" id="cbxUbicacion" style="max-width: 500px;">
                                <?php
                                echo $objUbicacion->shwUbicaciones($objAdscrip->id_ubicacion);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
        <input type="hidden" name="dtTypeOper" value="1" />
        <input type="hidden" id="hdnUrlMpio" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_municipios.php');?>" />
    </form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>