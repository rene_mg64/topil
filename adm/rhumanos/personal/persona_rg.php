<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_datos_personales.class.php';
include 'includes/class/admtbl_adscripcion.class.php';
include 'includes/class/admtbl_domicilio.class.php';
include 'includes/class/admtbl_nivel_estudios.class.php';
include 'includes/class/admtbl_aspirantes.class.php';

if (!class_exists('MySqlPdo')){
    require 'includes/class/config/mysql.class.php';    
}
$conexBD = new MySQLPDO();
$objDatPer = new AdmtblDatosPersonales();
$objAdscrip = new AdmtblAdscripcion();
$objDomi = new AdmtblDomicilio();
$objNivelEst = new AdmtblNivelEstudios();
$objAspi = new AdmtblAspirantes();

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/rhumanos/personal/_js/personarg.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Registro');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    $curp = $_POST["txtCurp"];
    // Datos Personales
    $objDatPer->curp = $curp;
    $objDatPer->nombre = $_POST["txtNombre"];
    $objDatPer->a_paterno = $_POST["txtAPaterno"];
    $objDatPer->a_materno = $_POST["txtAMaterno"];
    $objDatPer->fecha_nac = $objSys->convertirFecha($_POST["txtFechaNac"], 'yyyy-mm-dd');
    $objDatPer->genero = $_POST["rbnSexo"];
    $objDatPer->rfc = $_POST["txtRfc"];
    $objDatPer->cuip = $_POST["txtCuip"];
    $objDatPer->folio_ife = $_POST["txtFolioIfe"];
    $objDatPer->mat_cartilla = $_POST["txtCartilla"];
    $objDatPer->licencia_conducir = $_POST["txtLicencia"];
    $objDatPer->pasaporte = $_POST["txtPasaporte"];
    $objDatPer->id_estado_civil = $_POST["cbxEdoCivil"];
    $objDatPer->id_tipo_sangre = ($_POST["cbxTipoSangre"] != 0) ? $_POST["cbxTipoSangre"] : null;
    $objDatPer->id_nacionalidad = 1;
    $objDatPer->id_entidad = ($_POST["cbxEntNacimiento"] != 0) ? $_POST["cbxEntNacimiento"] : null;
    $objDatPer->id_municipio = ($_POST["cbxMpioNacimiento"] != 0) ? $_POST["cbxMpioNacimiento"] : null;
    $objDatPer->lugar_nac = $_POST["txtLugarNacimiento"];
    $objDatPer->id_status = 1;
    // Adscripción
    $objAdscrip->curp = $curp;
    $objAdscrip->id_corporacion = 1;
    $objAdscrip->id_area = $_POST["cbxArea"];
    $objAdscrip->id_tipo_funcion = $_POST["cbxTipoFuncion"];
    $objAdscrip->id_especialidad = $_POST["cbxEspecialidad"];
    $objAdscrip->id_categoria = $_POST["cbxCategoria"];
    $objAdscrip->cargo = $_POST["txtCargo"];
    $objAdscrip->id_nivel_mando = $_POST["cbxNivelMando"];
    $objAdscrip->fecha_ing = $objSys->convertirFecha($_POST["txtFechaIng"], 'yyyy-mm-dd');
    $objAdscrip->id_horario = $_POST["cbxHorario"];
    $objAdscrip->id_municipio = $_POST["cbxMpioAdscripcion"];
    $objAdscrip->id_ubicacion = $_POST["cbxUbicacion"];
    $objAdscrip->id_entidad = 12;//Guerrero
    //$objAdscrip->id_plaza = ?;
    // Domicilio
    $objDomi->curp = $curp;
    $objDomi->calle = $_POST['txtCalle'];
    $objDomi->n_exterior = $_POST['txtNumExt'];
    $objDomi->n_interior = $_POST['txtNumInt'];
    $objDomi->entre_calle1 = $_POST['txtEntreCalle1'];
    $objDomi->entre_calle2 = $_POST['txtEntreCalle2'];
    $objDomi->colonia = $_POST['txtColonia'];
    $objDomi->ciudad = $_POST['txtCiudad'];
    $objDomi->cod_postal = (!empty($_POST['txtCodPostal'])) ? $_POST['txtCodPostal'] : null;
    $objDomi->tel_fijo = $_POST['txtTelFijo'];
    $objDomi->tel_movil = $_POST['txtTelMovil'];
    $objDomi->id_municipio = $_POST['cbxMpioDomicilio'];
    $objDomi->id_entidad = $_POST['cbxEntDomicilio'];
    // Nivel de Estudios
    $objNivelEst->curp = $curp;
    $objNivelEst->id_nivel_estudios = $_POST['cbxNivelEstudios'];
    $objNivelEst->eficencia_term = (isset($_POST['rbnEfiTerminal'])) ? $_POST['rbnEfiTerminal'] : 1;
    $objNivelEst->institucion = $_POST['txtInstitucion'];
    $objNivelEst->cct = $_POST['txtCct'];
    $objNivelEst->carrera = $_POST['txtCarrera'];
    $objNivelEst->especialidad = $_POST['txtEspecialidad'];    
    $objNivelEst->documento = $_POST['txtDocumento'];
    $objNivelEst->folio = $_POST['txtFolioDocto'];
    $objNivelEst->n_cedula = $_POST['txtCedula'];
    $objNivelEst->promedio = null;
    
    // Inicia la transacción    
    $conexBD->beginTransaction();    
    //-------------------------------------------------------------------//            
    if ($objDatPer->insert()) {
        if ($objAdscrip->insert()) {
            if ($objDomi->insert()) {
                if ($objNivelEst->insert()) {
                    $objSys->registroLog($objUsr->idUsr, 'admtbl_datos_personales', $curp, "Ins");
                    $objSys->registroLog($objUsr->idUsr, 'admtbl_adscripcion', $curp, "Ins");
                    $objSys->registroLog($objUsr->idUsr, 'admtbl_domicilio', $curp, "Ins");
                    $objSys->registroLog($objUsr->idUsr, 'admtbl_nivel_estudios', $curp, "Ins");
                    $conexBD->commit();
                    $objAspi->updStatus($_POST["dtCurpAspi"]);
                    $_SESSION['xCurp'] = $curp;
                    $error = '';
                }
                else {
                    $conexBD->rollBack();
                    $error = (!empty($objNivelEst->msjError)) ? $objNivelEst->msjError : 'Error al guardar los datos del Nivel de Estudios';
                }
            }
            else {
                $conexBD->rollBack();
                $error = (!empty($objDomi->msjError)) ? $objDomi->msjError : 'Error al guardar los datos del Domicilio';
            }
        }
        else {
            $conexBD->rollBack();
            $error = (!empty($objAdscrip->msjError)) ? $objAdscrip->msjError : 'Error al guardar los datos de la Adscripción Laboral';
        }
    }
    else {
        $conexBD->rollBack();
        $error = (!empty($objDatPer->msjError)) ? $objDatPer->msjError : 'Error al guardar los Datos Personales';        
    }
    
    $mod = (empty($error)) ? $objSys->encrypt("persona_menu") : $objSys->encrypt("persona_add"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>