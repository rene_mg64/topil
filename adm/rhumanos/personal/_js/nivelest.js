$(document).ready(function(){            
    // Convierte a may�sculas el contenido de los textbox y textarea    
    $('#txtInstitucion').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCct').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCarrera').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtEspecialidad').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtDocumento').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtFolioDocto').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtDocumento').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
       
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    $('#frmRegistro').validate({
        rules:{
            // Nivel de Estudios
            cbxNivelEstudios:{
                required: true,
                min: 1,
            },
            txtInstitucion: 'required',
            txtCedula:{
                digits: true,
                minlength: 7,
            },
    	},
    	messages:{          
            // Nivel de Estudios
            cbxNivelEstudios:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtInstitucion: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCedula:{
                digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
                minlength: '<span class="ValidateError" title="El tama�o de la C�dula Profesional debe de ser 7 digitos"></span>',
            },            
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
    
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida) {
            var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los cambios realizados al Nivel de Estudios del elemento?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 390,
                buttons:{
                    'Aceptar': function(){
                        $('#frmRegistro').submit();                        
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });            
        }
    });
});