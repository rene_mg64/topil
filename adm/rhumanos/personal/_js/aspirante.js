$(document).ready(function(){
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaNac').datepicker({
        yearRange: '1920:2002',
    });
        
    // M�scara de los campos tel�fonos
    $('#txtTelFijo').mask('(999) 99-99999');
    $('#txtTelMovil').mask('(999) 99-99999');
            
    // Convierte a may�sculas el contenido de los textbox y textarea
    $('#txtNombre').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAPaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAMaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    //$('#txtRfc').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCalle').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtNumExt').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtColonia').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCiudad').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCargo').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    // Control especial del campo CURP.
    $('#txtCurp').blur(function(){
        $(this).val(xTrim( $(this).val().toUpperCase() ));
        if (msjErrorCURP != '') {
            shwError(msjErrorCURP);
        }
        else if ($(this).val() != '') {
            // Obtiene la fecha de nacimiento
            var fecha = $(this).val().substr(4, 6);
            $('#txtFechaNac').val(fecha.substr(4, 2) + '/' + fecha.substr(2, 2) + '/19' + fecha.substr(0, 2) );
            /*
            // Asigna el rfc autom�tico
            $("#txtRfc").val( $(this).val().substr(0, 10) );
            */
            // Asigna el g�nero autom�tico
            var genero = $(this).val().substr(10, 1);  
            if (genero == "H")
                $("#rbnSexo1").attr('checked', 'checked');
            else if (genero == "M" )
                $("#rbnSexo2").attr('checked', 'checked');
        }            
    });
    
    $('#cbxEntDomicilio').change(function(){
        obtenerMunicipios('cbxMpioDomicilio', $(this).val());  
    });
    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    jQuery.validator.addMethod("valCURP", function(value, element) {
        return validaCURP(value);
    });
    $('#frmRegistro').validate({
        rules:{
            // Datos Personales
            txtCurp:{
                required: true,
                minlength: 18,
                valCURP: true,    
            },
            txtNombre: 'required',            
            txtAPaterno: 'required',  
            txtAMaterno: 'required',
            rbnSexo: 'required',
            txtFechaNac: 'required',
            cbxEdoCivil:{
                required: true,
                min: 1,
            },
            // Domicilio
            txtCalle: 'required',
            txtNumExt: 'required',
            txtColonia: 'required',
            txtCiudad: 'required',
            cbxEntDomicilio:{
                required: true,
                min: 1,
            },
            cbxMpioDomicilio:{
                required: true,
                min: 1,
            },
            // Nivel de Estudios
            cbxNivelEstudios:{
                required: true,
                min: 1,
            },
            // Adscripci�n
            cbxArea:{
                min: 1,
            },
            cbxTipoFuncion:{
                min: 1,
            },
            cbxEspecialidad:{
                min: 1,
            },
            cbxCategoria:{
                min: 1,
            },
            txtCargo: 'required',
            cbxRegion:{
                min: 1,
            },
    	},
    	messages:{
    	   txtCurp:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                minlength: '<span class="ValidateError" title="El tama�o de la CURP debe de ser de 18 caracteres"></span>',
                valCURP: '<span class="ValidateError" title="La CURP especificada no es v�lida"></span>',    
            },
    		txtNombre: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAPaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAMaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            rbnSexo: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaNac: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            //txtRfc: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxEdoCivil:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            }, 
            // Domicilio
            txtCalle: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtNumExt: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtColonia: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCiudad: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxEntDomicilio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxMpioDomicilio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },           
            // Nivel de Estudios
            cbxNivelEstudios:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            // Adscripci�n
            cbxArea:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxTipoFuncion:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxEspecialidad:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxCategoria:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtCargo: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxRegion:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
       
    
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida) {
            var frmPreg = $(getHTMLMensaje('�Est� seguro de continuar con el registro del elemento?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $('#frmRegistro').submit();                        
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });            
        }
    });  
    
    $('#txtCurp').focus();
    
    /*--------------------------------------------------------------------//
     * Controla el scroll para seguir mostrando el header y el toolbar...
     *-------------------------------------------------------------------*/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();    
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');            
        }
    });    
});

function obtenerMunicipios(contenedor, id){
    var tp_filtro = ( contenedor != 'cbxMpioAdscripcion' ) ? 1 : 2;
    $.ajax({
        url: xDcrypt($('#hdnUrlMpio').val()),
        data: {'id_entidad': id, 'id_region': id, 'filtro': tp_filtro},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#' + contenedor).html('<option>Cargando municipios...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#' + contenedor).html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}