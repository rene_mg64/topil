$(document).ready(function(){
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaIng').datepicker({
        yearRange: '1950:',
    });
            
    // Convierte a may�sculas el contenido de los textbox y textarea    
    $('#txtCargo').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
        
    // Control para la carga din�mica de municipios
    $('#cbxRegion').change(function(){
        obtenerMunicipios($(this).val());  
    });
    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario   
    $('#frmRegistro').validate({
        rules:{
            // Adscripci�n
            cbxArea:{
                min: 1,
            },
            cbxTipoFuncion:{
                min: 1,
            },
            cbxEspecialidad:{
                min: 1,
            },
            cbxCategoria:{
                min: 1,
            },
            txtCargo: 'required',
            cbxNivelMando:{
                min: 1,
            },
            txtFechaIng: 'required',
            cbxHorario:{
                min: 1,
            },
            cbxRegion:{
                min: 1,
            },
            cbxMpioAdscripcion:{
                min: 1,
            },
            cbxUbicacion:{
                min: 1,
            },
    	},
    	messages:{
            // Adscripci�n
            cbxArea:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxTipoFuncion:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxEspecialidad:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxCategoria:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtCargo: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxNivelMando:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtFechaIng: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxHorario:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxRegion:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxMpioAdscripcion:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxUbicacion:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
    
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida) {
            var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los cambios realizados a la Adscripci�n Laboral del elemento?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 390,
                buttons:{
                    'Aceptar': function(){
                        $('#frmRegistro').submit();                        
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });            
        }
    });  
    
    /*--------------------------------------------------------------------//
     * Controla el scroll para seguir mostrando el header y el toolbar...
     *-------------------------------------------------------------------*/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();    
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');            
        }
    });    
});

function obtenerMunicipios(id){    
    $.ajax({
        url: xDcrypt($('#hdnUrlMpio').val()),
        data: {'id_region': id, 'filtro': 2},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#cbxMpioAdscripcion').html('<option>Cargando municipios...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#cbxMpioAdscripcion').html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}