<?php
/**
 * Complemento del llamado ajax para eliminar los archivos en una carpeta temporal del servidor.
 * Lista de parámetros recibidos por POST 
 * @param String file, contiene el nombre del archivo a eliminar en el servidor.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    $path = '../../../';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_archivo_exped.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objExped = new AdmtblArchivoExped();
    
    $datos = explode("-", $_GET["prm"]);
    if( $objExped->delete($datos[2]) ){
        $objSys->registroLog($objUsr->idUsr, 'admtbl_archivo_exped', $datos[2], 'Dlt');
        
        $path_exped = '../../expediente/doctos/';
        $archivo_normal = $path_exped . $_SESSION["xCurp"] . '/' . $_SESSION["xCurp"] . '_' . $datos[0] . '_' . $datos[1] . '_' . $datos[2] . $datos[3]; 
        $archivo_mini = $path_exped . $_SESSION["xCurp"] . '/' . $_SESSION["xCurp"] . '_' . $datos[0] . '_' . $datos[1] . '_' . $datos[2] . '-thb' . $datos[3];
        
        unlink($uploaddir . $archivo_normal);
        unlink($uploaddir . $archivo_mini);
        
        $ajx_datos['rslt'] = true;
        $ajx_datos["error"] = '' . $datos[2];
    } else {
        $ajx_datos['rslt'] = false;
        $ajx_datos["error"] = $objExped->msjError;
    }
    
    echo json_encode($ajx_datos);
}
?>