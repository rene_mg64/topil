<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p�gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_datos_personales.class.php';
    $objSys = new System();
    $objDatPer = new AdmtblDatosPersonales();

    //--------------------- Recepci�n de par�metros --------------------------//
    // B�squeda...
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {
        $sql_where = '(a.nombre LIKE ? OR a.a_paterno LIKE ? OR a.a_materno LIKE ?) '
                   . 'OR a.curp LIKE ? '
                   . 'OR j.especialidad LIKE ? '
                   . 'OR h.categoria LIKE ? '
                   . 'OR i.area LIKE ?';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',);
    }
    // Ordenaci�n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'a.a_paterno ' . $_GET['typeSort']  . ', a.a_materno ' . $_GET['typeSort']  . ', a.nombre '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
        $sql_order = 'a.curp ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'a.genero ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'j.especialidad ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'h.categoria ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 6) {
        $sql_order = 'i.area ' . $_GET['typeSort'];
    }   
    // Paginaci�n...    
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];    
    
    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];    
    //------------------------------------------------------------------------//
       
    $datos = $objDatPer->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];
    
    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $objSys->encrypt($dato["curp"]);
            $color_row = ( $dato["id_status"] == 2 ) ? 'color: #a09f9d;' : '';
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["curp"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 5%;">';
                   $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';               
                $html .= '</td>';
       			$nombrePersona = $dato["a_paterno"] . ' ' . $dato["a_materno"] . ' ' .$dato["nombre"];                
                $html .= '<td style="text-align: left; width: 17%;"><a href="#" class="toolTipTrigger" rel="' . $id_crypt . '" style="' . $color_row . '">' . $nombrePersona . '</a></td>';
                $html .= '<td style="text-align: center; width: 14%; ' . $color_row . '">' . $dato["curp"] . '</td>';
                $sexo = ( $dato["genero"] == 1 ) ? "MASCULINO" : "FEMENINO";
                $html .= '<td style="text-align: center; width: 9% ' . $color_row . ';">' . $sexo . '</td>';
                $html .= '<td style="text-align: center; width: 10%; ' . $color_row . '">' . $dato["especialidad"] . '</td>';
                $html .= '<td style="text-align: left; width: 15%; ' . $color_row . '">' . $dato["categoria"] . '</td>';
                $html .= '<td style="text-align: left; width: 20%; ' . $color_row . '">' . $dato["area"] . '</td>';
                $url_edit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("ctrl_exped") . "&id=" . $id_crypt;
                $html .= '<td style="text-align: center; width: 10%;">';
                $html .= '  <a href="' . $url_edit . '" class="lnkBtnOpcionGrid" title="Ir al expediente..." ><img src="' . PATH_IMAGES . 'icons/folder24.png" alt="expediente" /></a>';                
                //$html .= '  <a href="' . $url_inf . '" class="lnkBtnOpcionGrid" rel="inf;' . $dato["curp"] . '" title="Ficha de informaci�n..." ><img src="' . PATH_IMAGES . 'icons/ficha_info24.png" alt="ficha" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';  		
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron personas en esta consulta...';
        $html .= '</span>';
    } else {        
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>