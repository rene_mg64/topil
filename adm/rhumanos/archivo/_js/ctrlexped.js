var hWind = 0,
    wWind = 0,
    cancelUpload = false,
    loadComplete = false;
    
$(document).ready(function(){
    hWind = $(window).height();
    wWind = $(window).width();
    
    if( $('#txtNumExpediente').val() == '' )
        $('#txtNumExpediente').focus();
        
    //-- Ajusta el tama�o de la ventana...
    $('#dvExpediente').css('height', (hWind - 250) + 'px');
    
    //-- Guarda el n�mero de expediente...
    $('#lnkBtnSaveExped').click(function(){
        gardarNumExped();
    });
    
    //-- Ocultar los documentos...
    $('.lnkBtnColapsUp').live('click', function(){
        var div_doc = $(this).attr('rel');
        $('#' + div_doc).hide('speed');
        $(this).removeClass('lnkBtnColapsUp').addClass('lnkBtnColapsDown');
    });
    //-- Mostrar los documentos...
    $('.lnkBtnColapsDown').live('click', function(){
        var div_doc = $(this).attr('rel');
        $('#' + div_doc).show('speed');
        $(this).removeClass('lnkBtnColapsDown').addClass('lnkBtnColapsUp');
        var prm = div_doc.split('-');
        cargarDoctos(prm[1]);
        //$('#' + div_doc).load(xDcrypt($('#hdnUrlLoadDocs').val()), {'id_docto': prm[1]});
    });
    //-- Ampliar imagen...
    $('a.lnkImgDoc').live('click', function(){
        var prm = $(this).attr('rel');
        var titulo = $(this).attr('title');
        $('#dvForm-Prev').dialog({
            open: function(){
                $('#dvShowImg').load(xDcrypt($('#hdnUrlShwImg').val()), {'prm': prm});
            },
        });
        $('#dvForm-Prev').dialog('option', 'title', titulo);
        $('#dvForm-Prev').dialog('open');
    });
    $('#dvForm-Prev').dialog({
        autoOpen: false,
        draggable: true,
        height: hWind - 100,
        modal: true,        
        resizable: false,
        width: 1020,
    });
    $('a.lnkDocDlt').live('click', function(){
        var prm = $(this).attr('rel');
        borrarDocto(prm);
    });
    /*
    $('.dvDocto').draggable({ 
        containment: "#dvExpediente",
        opacity: 0.50,
        snap: true,
        snapMode: "inner",
        stack: ".dvDoctos" 
    });
    */
    
    /*
     * Funciones para el control de carga de im�genes
     */
    new AjaxUpload('#lnkBtnAddDocto', {
        action: xDcrypt($('#hdnUrlUpload').val()),        
		onSubmit : function(file, ext){
            loadComplete = false;
  		    if (! (ext && /^(jpg|png|jpeg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg, .jpeg y .png)', 300);
  			   // cancela upload
  			   return false;
  		    } else {
  		        $('#dvPrevImg').html('<span class="spnLoading"></span>');
                $('#dvForm-Add').dialog('open'); 
  		    }
        },
		onComplete: function(file, response){
            var result = $.parseJSON(response);
            if( !result.rslt ){
                $('#dvForm-Add').dialog('close');
                shwError(result.error, 400);
            } else {
                loadComplete = true;
                $('#hdnFileImg').val(result.file);
                var imgHtml = '<img src="' + xDcrypt($('#hdnUrlDirFiles').val()) + result.file_thb + '" />';
                $('#dvPrevImg').html(imgHtml);
            }
        }
    });
    //-- Formulario para agregar los detalles de una imagen...
    $('#dvForm-Add').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 750,   
        open: function(){
            resetForm();
        },
        buttons: {
            "Guardar": function(){
                cancelUpload = false;
                if( loadComplete ){
                    guardarDocto();
                }
            },
            "Cancelar": function(){
                cancelUpload = true;
                $(this).dialog('close');
            }
        },
        beforeClose: function(){
            //-- Se elimina el archivo previamente cargado...
            if( cancelUpload )
                borrarImgTemp();
        }
    });
    //-- Fecha...
    $('#txtFechaDocto').datepicker({yearRange: '1950:'});
    //-- Validaci�n de los campos...
    $('#frmAddFile').validate({
        rules:{
            cbxDocumento: {
                required: true,
                min: 1,
            },
            /*txtFechaDocto: 'required',*/
            txtDescripcion: 'required',                        
    	},
    	messages:{   	   
            cbxDocumento:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            /*txtFechaDocto: '<span class="ValidateError" title="Este campo es obligatorio"></span>',*/
            txtDescripcion: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
    
    /*----------------------------------------------------------------------------------
     *-- Funciones
     *----------------------------------------------------------------------------------*/
    function gardarNumExped(){
        if( $('#txtNumExpediente').val() != '' ){
            // Se genera el di�logo de espera
            var frmWait = $('<div><div class="xGrid-dvWait-LoadData" style="margin-top: 20px; padding-top: 50px;">Guardando, por favor espere...</div></div>');
            // Se procesa la petici�n ajax
            $.ajax({
                url: xDcrypt($('#hdnUrlSaveExped').val()),
                data: {'exped': $('#txtNumExpediente').val()},
                type: 'post',
                dataType: 'json',
                async: true,
                cache: false,
                beforeSend: function () {
                    // D�alogo de espera                     
                    frmWait.dialog({
                        autoOpen: true,
                        modal: true,
                        width: 300,
                    });
                },
                success: function (xdata) {
                    // Cierra el di�logo de espera
                    frmWait.dialog('close');
                    // Verifica el resultado del proceso
                    if (xdata.rslt) {                       
                        $('#lnkDescripExped').html( $('#txtNumExpediente').val() );
                        $('#lnkBtnAddDocto').css('display', 'block');                        
                    } else {
                        shwError('Error: ' + xdata.error);
                    }
                },
                error: function(objeto, detalle, otroobj){
                    frmWait.dialog('close');
                    shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                }
            });
        } else {
            shwError('Debe especificar un n�mero de expediente para esta persona', 450);
        }
    }   
    
    function guardarDocto(){
        var valida = $('#frmAddFile').validate().form();
        if (valida) {
            /*
            var frmPreg = $(getHTMLMensaje('�Est� seguro de continuar con el proceso actual?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 380,
                buttons:{
                    'Aceptar': function(){
            */
                        // Se genera el di�logo de espera
                        var frmWait = $('<div><div class="xGrid-dvWait-LoadData" style="margin-top: 20px; padding-top: 50px;">Guardando documento, por favor espere...</div></div>');
                        // Se procesan los datos v�a ajax
                        $.ajax({
                            url: xDcrypt($('#hdnUrlSaveDoc').val()),
                            data: $('#frmAddFile').serialize(),
                            type: 'post',
                            dataType: 'json',
                            async: true,
                            cache: false,
                            beforeSend: function () {
                                //frmPreg.dialog('close');
                                // D�alogo de espera                     
                                frmWait.dialog({
                                    autoOpen: true,
                                    modal: true,
                                    width: 350,
                                });
                            },
                            success: function (xdata) {
                                // Cierra el di�logo de espera
                                frmWait.dialog('close');
                                // Verifica el resultado del proceso
                                if (xdata.rslt) {
                                    // Se cierra el formulario principal
                                    $('#dvForm-Add').dialog('close');
                                    // Muestra y actualiza la subcarpeta correspondiente al documento cargado
                                    var id_docto = $('#cbxDocumento').val();
                                    var btn_colaps = $('#lnkColaps-' + id_docto);
                                    var div_docto = $('#dv-' + id_docto);
                                    if( btn_colaps.hasClass('lnkBtnColapsDown') ){
                                        div_docto.show('speed');
                                        btn_colaps.removeClass('lnkBtnColapsDown').addClass('lnkBtnColapsUp');                                        
                                    }
                                    cargarDoctos(id_docto);                                    
                                    // Mueve el foco a la subcarpeta en cuesti�n
                                    $('#dvExpediente').animate({scrollTop: div_docto.offset().top}, 2000);
                                    
                                } else {
                                    shwError('Error: ' + xdata.error);
                                }
                            },
                            error: function(objeto, detalle, otroobj){
                                frmWait.dialog('close');
                                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                            }
                        });
            /*
                    },
                    'Cancelar': function(){
                        frmPreg.dialog('close');
                    }
                }
            });
            */
        }
    }
    
    function cargarDoctos(id_docto){
        $.ajax({
            url: xDcrypt($('#hdnUrlLoadDocs').val()),
            data: {'id_docto': id_docto},
            type: 'post',
            dataType: 'json',
            async: true,
            cache: false,
            beforeSend: function () {
                $('#dv-' + id_docto).html('<div><div class="xGrid-dvWait-LoadData" style="margin-top: 20px; padding-top: 50px;">Cargando documentos, por favor espere...</div></div>');
            },
            success: function (xdata) {
                $('#dv-' + id_docto).html(xdata.doctos);
                $('#cbxDocumento').val(id_docto);
                $('#spnTitulo-' + id_docto).html($('#cbxDocumento option:selected').text() + ' (' + xdata.total + ')');
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    }

    function borrarImgTemp(){
        $.ajax({
            url: xDcrypt($('#hdnUrlDltFileTemp').val()),
            data: {'file': $('#hdnFileImg').val()},
            type: 'post',
            dataType: 'html',
            async: true,
            cache: false,
            beforeSend: function () {
                
            },
            success: function (xdata) {
            
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    }
    
    function borrarDocto(prm){
        var prms = prm.split('-');
        var frmPreg = $(getHTMLMensaje('�Est� seguro de eliminar el documento seleccionado?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 420,
            buttons:{
                'S� estoy seguro': function(){                    
                    $.ajax({
                        url: xDcrypt($('#hdnUrlDltFile').val()),
                        data: {'prm': prm},
                        type: 'get',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        beforeSend: function(){
                            frmPreg.dialog('close');
                        },
                        success: function(xdata){
                            if( xdata.rslt ){
                                cargarDoctos(prms[1]);                                
                            } else {
                                shwError(xdata.error, 400);
                            }
                        },
                        error: function(objeto, detalle, otroobj){                
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 400);
                        }
                    });
                },
                'Cancelar': function(){
                    $(this).dialog('close');
                }
            }
        });
    }
    
    function resetForm(){
        $('#cbxDocumento').val('0');
        $('#txtFechaDocto').val('');
        $('#txtDescripcion').val('');
    }
});