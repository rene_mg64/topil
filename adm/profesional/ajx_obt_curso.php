<?php
/**
 * Complemento ajax para obtener los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../';
    include $path . 'includes/class/admtbl_cursos.class.php';
    $objCurso = new AdmtblCursos();
    
    $objCurso->select($_GET["id"]);
    if ($objCurso->id_curso > 0) {
        $ajx_datos['rslt']  = true;
        $ajx_datos["id_tipo_curso"] = $objCurso->id_tipo_curso;
        $ajx_datos["curso"] = utf8_encode($objCurso->curso);
        $ajx_datos["institucion"] = utf8_encode($objCurso->institucion);
        $ajx_datos["fecha_ini"] = date("d/m/Y", strtotime($objCurso->fecha_ini));
        $ajx_datos["fecha_fin"] = date("d/m/Y", strtotime($objCurso->fecha_fin));
        $ajx_datos["duracion"] = $objCurso->duracion;
        $ajx_datos["efi_term"] = $objCurso->eficiencia_term;
        $ajx_datos["observa"] = utf8_encode($objCurso->observaciones);
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;        
        $ajx_datos['error'] = $objCurso->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>