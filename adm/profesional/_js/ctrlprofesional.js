$(document).ready(function(){ 
    $('#tabs').tabs();
    // Inicializaci�n de los tabs
    $('#tabsForm').tabs({
        disabled: [1,2,3]
    });    
    
    /**---------------------------------------------------------------------------------------
     *-- Implementaci�n del m�dulo: Formaci�n continua
     *----------------------------------------------------------------------------------------*/  
    // Convierte a may�sculas el contenido de los textbox y textarea
    $('#txtNombreCurso').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtInstitucion').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaIniCurso').datepicker({
        yearRange: '1980:',
    });
    $('#txtFechaFinCurso').datepicker({
        yearRange: '1980:',
    });
    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    $('#frmCurso').validate({
        rules:{
            cbxTipoCurso:{
                required: true,
                min: 1,
            },            
            txtNombreCurso: 'required',            
            txtInstitucion: 'required',           
            txtFechaIniCurso: 'required',
            txtFechaFinCurso: 'required',
            txtDuracionCurso: 'required',
    	},
    	messages:{    
            cbxTipoCurso:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtNombreCurso: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtInstitucion: '<span class="ValidateError" title="Este campo es obligatorio"></span>',           
            txtFechaIniCurso: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaFinCurso: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtDuracionCurso: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    }); 
    //-- Ventana para captura de datos...
    $('#dvFormCurso').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        open: function(){
            resetForm(1);
            if ($('#hdnTypeOperCurso').val() == 1) {
                $(this).dialog('option', 'title', 'SISP :: Nuevo Curso');
            } else if ($('#hdnTypeOperCurso').val() == 2) {
                $(this).dialog('option', 'title', 'SISP :: Modificar Curso');
                datosCurso();
            }                
        },
        buttons: {
            'Guardar datos': function(){
                guardarCurso();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Boton para agregar un nuevo curso...
    $('#btnAddCurso').click(function(){
        $('#hdnIdCurso').val('0');
        $('#hdnTypeOperCurso').val('1');
        $('#dvFormCurso').dialog('open');
    });
    $('#dvGrid-Cursos a.lnkBtnOpcionGrid').live('click', function(){
        var xId = $(this).attr("rel");
        $('#hdnIdCurso').val(xId);
        $('#hdnTypeOperCurso').val('2');
        $('#dvFormCurso').dialog('open');
    });
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGrid-Cursos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGrid-Cursos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
    
    
    /*----------------------------------------------------------------------------------------
     *-- Implementaci�n del m�dulo: Evaluaciones
     *----------------------------------------------------------------------------------------*/
    // Convierte a may�sculas el contenido de los textbox y textarea
    $('#txtNombreEval').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
        
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaIniEval').datepicker({
        yearRange: '1990:',
    });
    $('#txtFechaFinEval').datepicker({
        yearRange: '1990:',
    });
    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    $('#frmEvaluacion').validate({
        rules:{
            cbxTipoEval:{
                required: true,
                min: 1,
            },            
            txtNombreEval: 'required',
            txtFechaIniEval: 'required',
            txtFechaFinEval: 'required',
            txtDuracionEval: 'required',
            cbxResultEval:{
                required: true,
                min: 1,
            }, 
    	},
    	messages:{    
            cbxTipoEval:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtNombreEval: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaIniEval: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaFinEval: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtDuracionEval: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxResultEval:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    }); 
    //-- Ventana para captura de datos...
    $('#dvFormEvaluacion').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        open: function(){
            resetForm(2);
            if ($('#hdnTypeOperEval').val() == 1) {
                $(this).dialog('option', 'title', 'SISP :: Nueva Evaluaci�n');
            } else if ($('#hdnTypeOperEval').val() == 2) {
                $(this).dialog('option', 'title', 'SISP :: Modificar Evaluaci�n');
                datosEvaluacion();
            }                
        },
        buttons: {
            'Guardar datos': function(){
                guardarEvaluacion();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Boton para agregar un nuevo curso...
    $('#btnAddEvaluacion').click(function(){
        $('#hdnIdEval').val('0');
        $('#hdnTypeOperEval').val('1');
        $('#dvFormEvaluacion').dialog('open');
    });
    $('#dvGrid-Evaluaciones a.lnkBtnOpcionGrid').live('click', function(){
        var xId = $(this).attr("rel");
        $('#hdnIdEval').val(xId);
        $('#hdnTypeOperEval').val('2');
        $('#dvFormEvaluacion').dialog('open');
    });
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGrid-Evaluaciones table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGrid-Evaluaciones table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
     
    /*----------------------------------------------------------------------------------------
     *-- Implementaci�n del m�dulo: Est�mulos
     *----------------------------------------------------------------------------------------*/
    // Convierte a may�sculas el contenido de los textbox y textarea
    $('#txtEstimulo').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
        
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaEstim').datepicker({
        yearRange: '1980:',
    });
    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    $('#frmEstimulo').validate({
        rules:{
            txtEstimulo: 'required',
            txtFechaEstim: 'required', 
    	},
    	messages:{
            txtEstimulo: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaEstim: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    }); 
    //-- Ventana para captura de datos...
    $('#dvFormEstimulo').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        open: function(){
            resetForm(3);
            if ($('#hdnTypeOperEstim').val() == 1) {
                $(this).dialog('option', 'title', 'SISP :: Nuevo Est�mulo');
            } else if ($('#hdnTypeOperEstim').val() == 2) {
                $(this).dialog('option', 'title', 'SISP :: Modificar Est�mulo');
                datosEstimulo();
            }                
        },
        buttons: {
            'Guardar datos': function(){
                guardarEstimulo();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Boton para agregar un nuevo curso...
    $('#btnAddEstimulo').click(function(){
        $('#hdnIdEstim').val('0');
        $('#hdnTypeOperEstim').val('1');
        $('#dvFormEstimulo').dialog('open');
    });
    $('#dvGrid-Estimulos a.lnkBtnOpcionGrid').live('click', function(){
        var xId = $(this).attr("rel");
        $('#hdnIdEstim').val(xId);
        $('#hdnTypeOperEstim').val('2');
        $('#dvFormEstimulo').dialog('open');
    });
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGrid-Estimulos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGrid-Estimulos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
     
     
    /*----------------------------------------------------------------------------------------
     *-- Implementaci�n del m�dulo: Sanciones
     *----------------------------------------------------------------------------------------*/    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    $('#frmSancion').validate({
        rules:{
            cbxTipoSancion:{
                required: true,
                min: 1,
            },
            cbxMotivoSancion:{
                required: true,
                min: 1,
            },
            txtDuracionSancion: 'required', 
    	},
    	messages:{    
            cbxTipoSancion:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxMotivoSancion:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtDuracionSancion: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    }); 
    //-- Ventana para captura de datos...
    $('#dvFormSancion').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        open: function(){
            resetForm(4);
            if ($('#hdnTypeOperSancion').val() == 1) {
                $(this).dialog('option', 'title', 'SISP :: Nueva Sanci�n');
            } else if ($('#hdnTypeOperSancion').val() == 2) {
                $(this).dialog('option', 'title', 'SISP :: Modificar Sanci�n');
                datosSancion();
            }                
        },
        buttons: {
            'Guardar datos': function(){
                guardarSancion();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Boton para agregar un nuevo curso...
    $('#btnAddSancion').click(function(){
        $('#hdnIdSancion').val('0');
        $('#hdnTypeOperSancion').val('1');
        $('#dvFormSancion').dialog('open');
    });
    $('#dvGrid-Sanciones a.lnkBtnOpcionGrid').live('click', function(){
        var xId = $(this).attr("rel");
        $('#hdnIdSancion').val(xId);
        $('#hdnTypeOperSancion').val('2');
        $('#dvFormSancion').dialog('open');
    });
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGrid-Sanciones table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGrid-Sanciones table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
});

function guardarCurso(){
    var valida = $('#frmCurso').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los datos del curso actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el di�logo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando el curso, por favor espere...</div></div>');
                    // Se procesan los datos v�a ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSaveCurso').val()),
                        data: $('#frmCurso').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // D�alogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el di�logo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGrid-Cursos div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGrid-Cursos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGrid-Cursos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se cierra el formulario principal
                                $('#dvFormCurso').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function datosCurso(){
    $.ajax({
        url: xDcrypt($('#hdnUrlGetCurso').val()),
        data: {'id': $('#hdnIdCurso').val()},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            
        },
        success: function (xdata) {
            if (xdata.rslt) {                
                $('#cbxTipoCurso').val(xdata.id_tipo_curso);                
                $('#txtNombreCurso').val(xdata.curso);
                $('#txtInstitucion').val(xdata.institucion);
                $('#txtFechaIniCurso').val(xdata.fecha_ini);
                $('#txtFechaFinCurso').val(xdata.fecha_fin);
                $('#txtDuracionCurso').val(xdata.duracion);
                if( xdata.efi_term == 1 ) {
                    $('input:radio[name=rbnEfiTerm][value=1]').attr('checked', true);
                } else if( xdata.efi_term == 2 ) {
                    $('input:radio[name=rbnEfiTerm][value=2]').attr('checked', true);                
                } else if( xdata.efi_term == 3 ) {
                    $('input:radio[name=rbnEfiTerm][value=3]').attr('checked', true);
                }
                $('#txtObservaCurso').val(xdata.observa);
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ' : ' + otroobj, 450);
        }
    });
}

function guardarEvaluacion(){
    var valida = $('#frmEvaluacion').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los datos de la evaluaci�n actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el di�logo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando la evaluaci�n, por favor espere...</div></div>');
                    // Se procesan los datos v�a ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSaveEval').val()),
                        data: $('#frmEvaluacion').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // D�alogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el di�logo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGrid-Evaluaciones div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGrid-Evaluaciones table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGrid-Evaluaciones table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se cierra el formulario principal
                                $('#dvFormEvaluacion').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function datosEvaluacion(){
    $.ajax({
        url: xDcrypt($('#hdnUrlGetEval').val()),
        data: {'id': $('#hdnIdEval').val()},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            
        },
        success: function (xdata) {
            if (xdata.rslt) {
                $('#cbxTipoEval').val(xdata.id_tipo_eval);                
                $('#txtNombreEval').val(xdata.evaluacion);                
                $('#txtFechaIniEval').val(xdata.fecha_ini);
                $('#txtFechaFinEval').val(xdata.fecha_fin);
                $('#txtDuracionEval').val(xdata.duracion);                
                if( xdata.importancia == 1 ) {
                    $('input:radio[name=rbnImportEval][value=1]').attr('checked', true);
                } else if( xdata.importancia == 2 ) {
                    $('input:radio[name=rbnImportEval][value=2]').attr('checked', true);                
                }
                $('#cbxResultEval').val(xdata.id_result_eval);
                $('#txtObservaEval').val(xdata.observa);
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ' : ' + otroobj, 450);
        }
    });
}

function guardarEstimulo(){
    var valida = $('#frmEstimulo').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los datos del est�mulo actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el di�logo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando los datos, por favor espere...</div></div>');
                    // Se procesan los datos v�a ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSaveEstim').val()),
                        data: $('#frmEstimulo').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // D�alogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el di�logo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGrid-Estimulos div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGrid-Estimulos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGrid-Estimulos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se cierra el formulario principal
                                $('#dvFormEstimulo').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function datosEstimulo(){
    $.ajax({
        url: xDcrypt($('#hdnUrlGetEstim').val()),
        data: {'id': $('#hdnIdEstim').val()},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            
        },
        success: function (xdata) {
            if (xdata.rslt) {                
                $('#txtEstimulo').val(xdata.descripcion);                
                $('#txtFechaEstim').val(xdata.fecha);
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ' : ' + otroobj, 450);
        }
    });
}

function guardarSancion(){
    var valida = $('#frmSancion').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los datos de la sanci�n actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el di�logo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando la sanci�n, por favor espere...</div></div>');
                    // Se procesan los datos v�a ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSaveSancion').val()),
                        data: $('#frmSancion').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // D�alogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el di�logo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGrid-Sanciones div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGrid-Sanciones table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGrid-Sanciones table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se cierra el formulario principal
                                $('#dvFormSancion').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function datosSancion(){
    $.ajax({
        url: xDcrypt($('#hdnUrlGetSancion').val()),
        data: {'id': $('#hdnIdSancion').val()},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            
        },
        success: function (xdata) {
            if (xdata.rslt) {
                $('#cbxTipoSancion').val(xdata.id_tipo_sancion);                
                $('#cbxMotivoSancion').val(xdata.id_motivo_sancion);
                $('#txtDuracionSancion').val(xdata.duracion);
                $('#txtObservaSancion').val(xdata.observa);
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ' : ' + otroobj, 450);
        }
    });
}

function resetForm(form){
    if( form == 1 ){
        $('#cbxTipoCurso').val(0);
        $('#txtNombreCurso').val('');
        $('#txtInstitucion').val('');
        $('#txtFechaIniCurso').val('');
        $('#txtFechaFinCurso').val('');        
        $('#txtDuracionCurso').val('');
        $('#rbnEfiTerm1').attr('checked', true);
        $('#txtObservaCurso').val('');
        
        $("#dvFormCurso .validation span:not([class='pRequerido'])").hide();
        $("#dvFormCurso .validation").removeClass("error");
        $("#dvFormCurso .validation").removeClass("success");
    } else if( form == 2 ){
        $('#cbxTipoEval').val(0);
        $('#txtNombreEval').val('');        
        $('#txtFechaIniEval').val('');
        $('#txtFechaFinEval').val('');        
        $('#txtDuracionEval').val('');
        $('#rbnImportEval1').attr('checked', true);
        $('#cbxResultEval').val(0);
        $('#txtObservaEval').val('');
                
        $("#dvFormEvaluacion .validation span:not([class='pRequerido'])").hide();
        $("#dvFormEvaluacion .validation").removeClass("error");
        $("#dvFormEvaluacion .validation").removeClass("success");
    } else if( form == 3 ){
        $('#txtEstimulo').val('');        
        $('#txtFechaEstim').val('');
        
        $("#dvFormEstimulo .validation span:not([class='pRequerido'])").hide();
        $("#dvFormEstimulo .validation").removeClass("error");
        $("#dvFormEstimulo .validation").removeClass("success");
    } else if( form == 4 ){
        $('#cbxTipoEval').val(0);
        $('#cbxMotivoEval').val(0);
        $('#txtDuracionSancion').val('');
        $('#txtObservaSancion').val('');
                
        $("#dvFormSancion .validation span:not([class='pRequerido'])").hide();
        $("#dvFormSancion .validation").removeClass("error");
        $("#dvFormSancion .validation").removeClass("success");
    }
}