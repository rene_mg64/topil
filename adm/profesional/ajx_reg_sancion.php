<?php
/**
 * Complemento ajax para guardar los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_sanciones.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objSancion = new AdmtblSanciones();    
    
    $idSancion = $_POST["hdnIdSancion"];
    $objSancion->id_sancion = $idSancion;
    $objSancion->curp = $_SESSION["xCurp"];
    $objSancion->id_tipo_sancion = $_POST["cbxTipoSancion"];
    $objSancion->id_motivo_sancion = $_POST["cbxMotivoSancion"];
    $objSancion->duracion = $_POST["txtDuracionSancion"];
    $objSancion->fecha_reg = date("Y-m-d");
    $objSancion->observaciones = utf8_decode($_POST["txtObservaSancion"]);
            
    $result = ($_POST["hdnTypeOperSancion"] == 1) ? $objSancion->insert() : $objSancion->update();
    if ($result) {
        $ajx_datos['rslt']  = true;
        if( $_POST["hdnTypeOperSancion"] == 1 ){
            $oper = "Ins";
            $id_reg = $result;    
        } else {
            $oper = "Edt";
            $id_reg = $idSancion;
        }        
        $objSys->registroLog($objUsr->idUsr, "admtbl_sanciones", $id_reg, $oper);
        // Se obtienen todas las sanciones de la persona y se genera la lista
        $datos = $objSancion->selectAll("a.curp='" . $_SESSION['xCurp'] . "'", "a.fecha_reg Asc");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Sanciones-tbBody">';
        foreach( $datos As $reg => $dato ){
            if( $result === true )
                $sty_color = ( $idSancion == $dato["id_sancion"] ) ? 'color: #2b60de;' : '';
            else
                $sty_color = ( $result == $dato["id_sancion"] ) ? 'color: #2b60de;' : '';
            $html .= '<tr id="dvGrid-Sanciones-' . $dato["id_sancion"] . '">';
                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';                
                $html .= '<td style="text-align: left; width: 45%; ' . $sty_color . '">' . $dato["motivo_sancion"] . '</td>';
                $html .= '<td style="text-align: center; width: 25%; ' . $sty_color . '">' . $dato["tipo_sancion"] . '</td>';
                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . $dato["duracion"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%; ' . $sty_color . '">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_sancion"] . '" title="Moficar esta sanci�n..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $ajx_datos['html']  = utf8_encode($html);
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objSancion->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>