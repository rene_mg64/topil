<?php
/**
 * Complemento ajax para guardar los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_evaluaciones.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objEval = new AdmtblEvaluaciones();    
    
    $idEval = $_POST["hdnIdEval"];
    $objEval->id_evaluacion = $idEval;
    $objEval->curp = $_SESSION["xCurp"];
    $objEval->id_tipo_eval = $_POST["cbxTipoEval"];
    $objEval->evaluacion = utf8_decode($_POST["txtNombreEval"]);
    $objEval->fecha_ini = $objSys->convertirFecha($_POST["txtFechaIniEval"], "yyyy-mm-dd");
    $objEval->fecha_fin = $objSys->convertirFecha($_POST["txtFechaFinEval"], "yyyy-mm-dd");
    $objEval->duracion = $_POST["txtDuracionEval"];
    $objEval->importancia = $_POST["rbnImportEval"];
    $objEval->id_result_eval = $_POST["cbxResultEval"];
    $objEval->observaciones = utf8_decode($_POST["txtObservaEval"]);
            
    $result = ($_POST["hdnTypeOperEval"] == 1) ? $objEval->insert() : $objEval->update();
    if ($result) {
        $ajx_datos['rslt']  = true;
        if( $_POST["hdnTypeOperEval"] == 1 ){
            $oper = "Ins";
            $id_reg = $result;    
        } else {
            $oper = "Edt";
            $id_reg = $idEval;
        }        
        $objSys->registroLog($objUsr->idUsr, "admtbl_evaluaciones", $id_reg, $oper);
        // Se obtienen todos las evaluaciones de la persona y se genera la lista
        $datos = $objEval->selectAll("a.curp='" . $_SESSION['xCurp'] . "'", "a.fecha_ini Asc");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Evaluaciones-tbBody">';
        foreach( $datos As $reg => $dato ){
            if( $result === true )
                $sty_color = ( $idEval == $dato["id_evaluacion"] ) ? 'color: #2b60de;' : '';
            else
                $sty_color = ( $result == $dato["id_evaluacion"] ) ? 'color: #2b60de;' : '';
            $html .= '<tr id="dvGrid-Evaluaciones-' . $dato["id_evaluacion"] . '">';
                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';                
                $html .= '<td style="text-align: left; width: 25%; ' . $sty_color . '">' . $dato["evaluacion"] . '</td>';
                $html .= '<td style="text-align: center; width: 17%; ' . $sty_color . '">' . $dato["tipo_eval"] . '</td>';
                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha_ini"])) . '</td>';            
                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha_fin"])) . '</td>';
                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . $dato["resultado"] . '</td>';
                $html .= '<td style="text-align: center; width: 8%; ' . $sty_color . '">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_evaluacion"] . '" title="Moficar este curso..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $ajx_datos['html']  = utf8_encode($html);
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objEval->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>