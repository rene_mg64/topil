<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/logtbl_vehiculos.class.php';
include 'includes/class/logcat_veh_estado_fisico.class.php';
include 'includes/class/logcat_veh_situacion.class.php';
include 'includes/class/logcat_veh_transmision.class.php';
include 'includes/class/logcat_veh_color.class.php';
include 'includes/class/logcat_veh_modelo.class.php';
include 'includes/class/logcat_veh_clasificacion.class.php';
include 'includes/class/logcat_veh_marca.class.php';
include 'includes/class/logcat_veh_tipo.class.php';
include 'includes/class/logcat_veh_operatividad.class.php';
include 'includes/class/logcat_veh_ubicacion.class.php';

$objVeh			= new LogtblVehiculos();
$objEdoFis		= new LogcatVehEstadoFisico();
$objSit			= new LogcatVehSituacion();
$objTra			= new LogcatVehTransmision();
$objColor		= new LogcatVehColor();
$objMod			= new LogcatVehModelo();
$objCla 		= new LogcatVehClasificacion();
$objMar			= new LogcatVehMarca();
$objTipo		= new LogcatVehTipo();
$objOper		= new LogcatVehOperatividad();
$objUbi			= new LogcatVehUbicacion();


//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="includes/js/curp.js"></script>',
                                   '<script type="text/javascript" src="includes/js/file_uploader/AjaxUpload.2.0.min.js"></script>',
                                   '<script type="text/javascript" src="log/pvehicular/_js/efuerza/vehiculo_add.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
$url_cancel = "index.php?m=" . $_SESSION["xIdMenu"];
$urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('vehiculo_rg');
    ?>
    <style type="text/css">
        .dvFoto{ border: 2px solid gray; cursor: pointer; height: 100px; margin: auto auto; width: 100px; }
        .dvFoto img{ height: 100px; width: 100px; }
    </style>
    
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los datos del nuevo elemento...">
                    <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                </a>
                <a href="<?php echo $url_cancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo elemento...">
                    <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                </a>
                </td>
            </tr>
        </table>
    </div>
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; margin-bottom: 60px; text-align: left; width: 850px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Caracteristicas</span>                                
            <div id="tabsForm" style="
            margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">                  
                       
                <!-- caracteristicas -->                                                                   
                <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                	<tr>
                        <td><label for="cbxClasificacion">Clasificaci&oacute;n:</label></td>
                        <td class="validation">
                            <select name="cbxClasificacion" id="cbxClasificacion">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objCla->shwClasificacion();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>  
                    <tr>
                        <td><label for="cbxMarca">Marca:</label></td>
                        <td class="validation">
                            <select name="cbxMarca" id="cbxMarca">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objMar->shwMarca();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                     
                    <tr>
                        <td><label for="cbxCategoria">Tipo:</label></td>
                        <td class="validation">
                            <select name="cbxTipo" id="cbxTipo">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objTipo->shwTipos();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtNumSerie">Serie:</label></td>
                        <td class="validation" style="width: 400px;">
                            <input type="text" name="txtNumSerie" id="txtNumSerie" value="" maxlength="20" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>              
                    <tr>
                        <td><label for="txtNumEco">N&uacute;mero econ&oacute;mico:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumEco" id="txtNumEco" value="" maxlength="14" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtNumMotor">N&uacute;mero de motor:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumMotor" id="txtNumMotor" value="" maxlength="25" title="..." style="width: 200px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtPoliza">P&oacute;liza:</label></td>
                        <td class="validation">
                            <input type="text" name="txtPoliza" id="txtPoliza" value="" maxlength="25" title="..." style="width: 200px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtInciso">Inciso:</label></td>
                        <td class="validation">
                            <input type="text" name="txtInciso" id="txtInciso" value="" maxlength="25" title="..." style="text-align: center; width: 120px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtPlacas">Placas:</label></td>
                        <td class="validation">
                            <input type="text" name="txtPlacas" id="txtPlacas" value="" maxlength="8" title="..." style="width: 140px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label>N&uacute;mero de placas:</label></td>
                        <td class="validation">
                            <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnNumPlacas" id="rbnNumPlacas" value="1" />una</label>
                            <label class="label-Radio"><input type="radio" name="rbnNumPlacas" id="rbnNumPlacas2" value="2" />dos</label>
                        </td>
                    </tr> 
                    <tr>
                        <td><label for="txtTarjetaCir">Tarjeta de Circulaci&oacute;n:</label></td>
                        <td class="validation">
                            <input type="text" name="txtTarjetaCir" id="txtTarjetaCir" value="<?php echo $objVeh->tarjeta_circulacion;?>" size="12" maxlength="12" title="..." style="width: 130px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txt">Registro federal vehicular:</label></td>
                        <td class="validation">
                            <input type="text" name="txtRegFedVeh" id="txtRegFedVeh" value="<?php echo $objVeh->reg_fed_veh;?>" size="20" maxlength="20" title="..." style="width: 200px;" />
                        </td>
                    </tr>   
                    <tr>
                        <td><label for="txt">N&uacute;mero de puertas:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumPuertas" id="txtNumPuertas" value="<?php echo $objVeh->num_puertas;?>" size="1" maxlength="1" title="..." style="width: 20px;" />
                        </td>
                    </tr>                            
                    <tr>
                        <td><label for="txtNumCilindros">N&uacute;mero de cilindros:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumCilindros" id="txtNumCilindros" value="" size="1" maxlength="2" title="..." style="width: 200px;" />
                        </td>
                    </tr>  
                    <tr>
                        <td><label for="cbxEdoFisico">Estado f&iacute;sico:</label></td>
                        <td class="validation">
                            <select name="cbxEdoFisico" id="cbxEdoFisico" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objEdoFis->shwEstadoFisico();
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxSituacion">Situaci&oacute;n:</label></td>
                        <td class="validation">
                            <select name="cbxSituacion" id="cbxSituacion">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objSit->shwSituacion();
                                ?>
                            </select>
                        </td>
                    </tr> 
                    <tr>
                        <td><label for="cbxModelo">Modelo:</label></td>
                        <td class="validation">
                            <select name="cbxModelo" id="cbxModelo">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objMod->shwModelo();
                                ?>
                            </select>
                        </td>
                    </tr> 
                    <tr>
                        <td><label for="cbxTransmision">Transmisi&oacute;n:</label></td>
                        <td class="validation">
                            <select name="cbxTransmision" id="cbxTransmision">
                                <option value="0">&nbsp;</option>
                                <?php
                               	echo $objTra->shwTransmision();
                                ?>
                            </select>
                        </td>
                    </tr>                                                                              
                    <tr>
                        <td><label for="cbxColor">Color:</label></td>
                        <td class="validation">
                            <select name="cbxColor" id="cbxColor">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objColor->shwColor();
                                ?>
                            </select>
                        </td>
                    </tr>  
                    <!-- imagenes -->
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr><td colspan="2" style="text-align: center;"><span style="font-weight: bold;">Fotografias del Vehiculo</span></td></tr>                             
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table style="border-width: 2px; border-style: solid;">
                            <td style="width : 20%; text-align: center;">
                                <span>Frente</span>
                                <div id="dvFotoFrente" class="dvFoto">                                                      
                                </div>
                                <input type="hidden" name="hdnFotoFrente" id="hdnFotoFrente" value="" />
                            </td>
                            <td style="width : 20%; text-align: center;">
                                <span>Derecha</span>
                                <div id="dvFotoDer" class="dvFoto">                                                         
                                </div>
                                <input type="hidden" name="hdnFotoDer" id="hdnFotoDer" value="" />
                            </td>
                            <td style="width : 20%; text-align: center;">
                                <span>Izquierda</span>
                                <div id="dvFotoIzq" class="dvFoto">                                                           
                                </div>
                                <input type="hidden" name="hdnFotoIzq" id="hdnFotoIzq" value="" />
                            </td>
                            <td style="width : 20%; text-align: center;">
                                <span>Posterior</span>
                                <div id="dvFotoPost" class="dvFoto">                                                              
                                </div>
                                <input type="hidden" name="hdnFotoPost" id="hdnFotoPost" value="" />
                            </td>                      
                            <td style="width : 20%; text-align: center;">
                                <span>Interior</span>
                                <div id="dvFotoInt" class="dvFoto">                                                               
                                </div>
                                <input type="hidden" name="hdnFotoInt" id="hdnFotoInt" value="" />
                            </td>
                            </table>
                        </td>
                    </tr>  
                                         
                </table>
                                                  
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
  <input type="hidden" name="dtTypeOper" value="1" />
  <!-- <input type="hidden" name="dtTypeOper" value="<?php //echo $e; ?>" /> -->
  <input type="hidden" id="hdnUrlMarca" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_obt_marcas.php');?>" />
  <input type="hidden" id="hdnUrlTipo" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_obt_tipos.php');?>" />
  <input type="hidden" id="hdnUrlUpload" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_upload.php');?>" />
  <input type="hidden" id="hdnUrlDirTemp" value="<?php echo $objSys->encrypt('log/_uploadfiles/');?>" />
    </form> 
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>