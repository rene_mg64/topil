<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
$path = '';
require_once $path . 'includes/class/config/mysql.class.php';
include_once $path . 'includes/class/logtbl_veh_baja.class.php';


$conexBD        = new MySQLPDO();
$objBaja        = new LogtblVehBaja();


//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="log/pvehicular/_js/efuerza/vehiculo_baja_rg.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Registro');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    //datos del vehiculo de baja
    $objBaja->id_vehiculo		= $_POST["hdnIdVehiculo"];
    $objBaja->num_lote  		= $_POST["txtNumLote"];
	$objBaja->id_motivo_baja	= $_POST["cbxMotivoBaja"];
    $objBaja->fecha_baja		= $objSys->convertirFecha( $_POST["txtFechaBaja"], "yyyy-mm-dd" );
    
    // Inicia la transacci�n    
    $conexBD->beginTransaction();    
    //-------------------------------------------------------------------//            

    //se agrega el registro de baja en la tabla de
	if ( $objBaja->insert() ) { 
	   if( $objBaja->update_baja() ){
    		$objSys->registroLog($objUsr->idUsr, 'logtbl_veh_baja', $objBaja->id_vehiculo, "Baja");                    
    		$conexBD->commit();
    		$error = '';
        }else{
            $conexBD->rollBack();
            $error = (!empty($objBaja->msjError)) ? $objBaja->msjError : 'Error al guardar los datos del vehiculo como baja.';    
        }
	}
	else {
		$conexBD->rollBack();
		$error = (!empty($objBaja->msjError)) ? $objBaja->msjError : 'Error al guardar los datos del vehiculo como baja.';
	}
    
    $mod = (empty($error)) ? $objSys->encrypt("index") : $objSys->encrypt("vehiculo_baja"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>