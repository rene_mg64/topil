<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
$path = '';
require_once $path . 'includes/class/config/mysql.class.php';
include_once $path . 'includes/class/logtbl_veh_siniestrado.class.php';


$conexBD        = new MySQLPDO();
$objSin         = new LogtblVehSiniestrado();


//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="log/pvehicular/_js/efuerza/vehiculo_sin_rg.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Registro');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    //datos del vehiculo siniestrado
    $objSin->id_vehiculo	= $_POST["hdnIdVehiculo"];
    $objSin->num_siniestro  = $_POST["txtNumSin"];
    $objSin->fecha_siniestro= $objSys->convertirFecha( $_POST["txtFechaSin"], "yyyy-mm-dd" );
    
    // Inicia la transacci�n    
    $conexBD->beginTransaction();    
    //-------------------------------------------------------------------//            

	if ( $objSin->insert() ) { 
	   if( $objSin->update_sin() ){
    		$objSys->registroLog($objUsr->idUsr, 'logtbl_veh_siniestrado', $objSin->id_vehiculo, "Sin");                    
    		$conexBD->commit();
    		$error = '';
        }else{    		                    
    		$conexBD->commit();
    		$error = (!empty($objSin->msjError)) ? $objSin->msjError : 'Error al guardar los datos del vehiculo siniestrado.';
	   }
    }
	else {
		$conexBD->rollBack();
		$error = (!empty($objSin->msjError)) ? $objSin->msjError : 'Error al guardar los datos del vehiculo siniestrado.';
	}

    
    $mod = (empty($error)) ? $objSys->encrypt("index") : $objSys->encrypt("vehiculo_sin"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>