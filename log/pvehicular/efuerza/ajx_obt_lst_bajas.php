<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p�gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_veh_baja.class.php';    
    $objSys = new System();
        
    $objDatos = new LogtblVehBaja();    

    //--------------------- Recepci�n de par�metros --------------------------//
    // B�squeda...
    $sql_where = '';
    $sql_values = null;
    
    if (!empty($_GET["txtSearch"])) {
        $sql_where = ' num_serie LIKE ? '
                   . ' OR clasificacion LIKE ? '
                   . ' OR marca LIKE ? '                   
                   . ' OR num_lote LIKE ? '
                   . ' OR motivo_baja LIKE ? '
                   . ' OR fecha_baja LIKE ? ';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',                            
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%');
    }
    
    // Ordenaci�n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'num_serie ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
        $sql_order = 'clasificacion ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'marca ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'num_lote ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'motivo_baja' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 6) {
        $sql_order = 'fecha_baja ' . $_GET['typeSort'];
    }            
    
    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];    
    //------------------------------------------------------------------------//
      
    $datos = $objDatos->selectLstBajas($sql_where, $sql_values, $sql_order);
    $totalReg = count ( $datos );

    $html = '';
    if ( $totalReg > 0 ) {
        $html = '<table class="xGrid-tbBody">';
        
        foreach ($datos As $reg => $dato) {            
            $html .= '<tr>';       			                    			                               
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["num_serie"] . '</td>';                                                
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["clasificacion"] . '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["marca"] . '</td>';                
                $html .= '<td style="text-align: center; width: 20%;">' . $dato["num_lote"] . '</td>';
                $html .= '<td style="text-align: center; width: 20%;">' . $dato["motivo_baja"] . '</td>';    
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["fecha_baja"] . '</td>';                                              
             	//---------------------------------------------------------------//
       		$html .= '</tr>';               		
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontro equipamento en esta consulta...';
        $html .= '</span>';
    } else {        
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>