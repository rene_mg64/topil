var xGrid;
$(document).ready(function(){ 
    var url_dir_tmp = xDcrypt($('#hdnUrlDirTemp').val());
    var url_upload = xDcrypt($('#hdnUrlUpload').val());
    
    //-- Script para cargar la Fotograf�a del frente...
    new AjaxUpload('#dvFotoFrente', {
        action: url_upload,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoFrente").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){  
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoFrente").val(result.file);
                $("#dvFotoFrente").empty();
                $("#dvFotoFrente").append(imgHtml);
            }
        }	
    });
    
    //-- Script para cargar la Fotograf�a derecha...
    new AjaxUpload('#dvFotoDer', {
        action: url_upload,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoDer").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){  
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoDer").val(result.file);
                $("#dvFotoDer").empty();
                $("#dvFotoDer").append(imgHtml);
            }
        }	
    });
    
    //-- Script para cargar la Fotograf�a izquierda...
    new AjaxUpload('#dvFotoIzq', {
        action: url_upload,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoIzq").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){  
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoIzq").val(result.file);
                $("#dvFotoIzq").empty();
                $("#dvFotoIzq").append(imgHtml);
            }
        }	
    });
    
    //-- Script para cargar la Fotograf�a posterior...
    new AjaxUpload('#dvFotoPost', {
        action: url_upload,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoPost").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){  
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoPost").val(result.file);
                $("#dvFotoPost").empty();
                $("#dvFotoPost").append(imgHtml);
            }
        }	
    });
    
    //-- Script para cargar la Fotograf�a interior...
    new AjaxUpload('#dvFotoInt', {
        action: url_upload,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoInt").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){  
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoInt").val(result.file);
                $("#dvFotoInt").empty();
                $("#dvFotoInt").append(imgHtml);
            }
        }	
    });

    
    // Inicializaci�n de los tabs
    $('#tabsForm').tabs({
        disabled: [1,2,3,4]
    });    
    
    // Definici�n del control grid para la b�squeda de elementos
    xGrid = $('#dvGrid-Personal').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 0,
        xTypeSearch: 2,
        xOnlySearch: true,                
        xTypeDataAjax: 'json'
    });
    
    //-- Definici�n del dialog como formulario para busqueda de elementos....
    $('#dvBuscar').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 930,
        height: 510,
        modal: true,
        resizable: false,
        draggable:false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });

    
    //-- Controla el bot�n para visualizar el formulario de busqueda...
    $(".classBuscar").live("click", function(e){                                
        //$('#dvExp').dialog('option','title',xdata.titulo);
        $('#dvBuscar').dialog('open');                    

    }); 
    
    // Seleccion de un registro
    $('.lnkBtnOpcionGrid').live('click', function(){
        // Se obtienen los datos del recluta seleccionado
        var datos = $(this).attr('rel').split('-');
        $('#hdnCurp').val(datos[0]);
        $('#txtNombre').val(datos[1]);
        $('#dvBuscar').dialog('close');
        
    });    
    
    // M�scara de los campos tel�fonos
    $('#txtSerie').mask('99aaa99999a99999999');
    //$('#txtNumEco').mask('99999999999999');
    $('#txtPlacas').mask('aaa9999');
    $('#txtNumCilindros').mask('9');
	$('#txtFechaRes').datepicker({
        yearRange: '2010:2030',
    });
        
    // Convierte a may�sculas el contenido de los textbox y textarea
    $('#txtNumMotor').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtPoliza').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtInciso').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    
    $('#frmRegistro').validate({
        rules:{
            //condiciones del vehiculo
            //txtSerie: 'required',            
            //txtNumEco: 'required',  
            //txtNumMotor: 'required',
            //txtPoliza: 'required',
            //txtInciso: 'required',
            //txtPlacas: 'required',
			//txtNumPlacas: 'required',
			//txtNumCilindros: 'required',
            txtFolioIfe: 'digits',
            cbxEdoCivil:{
                required: true,
                min: 1,
            },
            //datos del resguardo del vehiculo
			cbxMarca: 'required',
			cbxTipo: 'required',
            // condiciones del vehiculo
    	},
    	messages:{
    		txtNombre: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFolioIfe: '<span class="ValidateError" title="Este campo s�lo acepta d�gitos"></span>',
            cbxEdoCivil:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            }, 

            txtCodPostal:{
                digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
                minlength: '<span class="ValidateError" title="El tama�o del C�digo Postal debe de ser 5 digitos"></span>',
            },          
            //Herramientas incluidas en el vehiculo
            cbxNivelEstudios:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            //Accesorios incluidos en el vehiculo
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
       
        
    // Acci�n del bot�n Siguiente
    $('#btnSiguiente').click(function(){        
        var valida = $('#frmRegistro').validate().form();
        if (valida) {
            // Se obtiene el total de tabs
            var totalTabs = $('#tabsForm >ul >li').size();            
            // Se obtien el id del tab actual activo
            var tabActual = $('#tabsForm').tabs('option', 'active'); 
            // Deshabilita todos los tabs
            $('#tabsForm').tabs('option', 'disabled', [0,1,2,3,4]);           
            // Se habilita y se muestra el siguiente tab
            $('#tabsForm').tabs('enable', (tabActual + 1));
            $('#tabsForm').tabs('option', 'active', (tabActual + 1));                        
            if (tabActual == (totalTabs - 2)) {            
                $('#btnSiguiente').css('display', 'none');                
            }            
            if (tabActual == 0)
                $('#btnAnterior').css('display', 'inline-block');            
        }
    });
    // Acci�n del bot�n Anterior
    $('#btnAnterior').click(function(){
        // Se obtien el id del tab actual activo
        var tabActual = $('#tabsForm').tabs('option', 'active');
        // Deshabilita todos los tabs
        $('#tabsForm').tabs('option', 'disabled', [0,1,2,3,4]);
        if (tabActual > 0){            
            $('#tabsForm').tabs('enable', (tabActual - 1));
            $('#tabsForm').tabs('option', 'active', (tabActual - 1));
        }
        if (tabActual == 4) {            
            $('#btnSiguiente').css('display', 'inline-block');
        }
        else if (tabActual == 1) {
            $(this).css('display', 'none');
        }
    });
	// Control para la carga din�mica de 
    $('#cbxClasificacion').change(function(){
        obtenerTipos( 'cbxTipo', $(this).val() );  
    });
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida)
            $('#frmRegistro').submit();
    });    
});

function obtenerTipos(contenedor, id_clasificacion){
	//alert("nada");
    $.ajax({
        url: xDcrypt($('#hdnUrlTipo').val()),
        data: {'id_clasificacion': id_clasificacion },
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#' + contenedor).html('<option>Cargando tipos...</option>');
			obtenerMarcas( 'cbxMarca', id_clasificacion )
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#' + contenedor).html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}
function obtenerMarcas(contenedor, id_clasificacion){
    $.ajax({
        url: xDcrypt($('#hdnUrlMarca').val()),
        data: {'id_clasificacion': id_clasificacion },
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#' + contenedor).html('<option>Cargando marcas...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#' + contenedor).html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}