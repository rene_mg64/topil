<?php   
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string id_vehiculo, es la llave para obtener los resultados de la busqueda
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_veh_mantenimientos.class.php';
    $objSys = new System();
    $objMtto = new LogtblVehMantenimientos();                
                         
    // B�squeda...          
    //se estraen los registro segun el filtro.
    $sql_where = ' a.id_vehiculo = ' . $_GET["id_vehiculo"];    
    $datos = $objMtto->selectAll($sql_where, '', '');
    //total de registros
    $totalReg = $objMtto->selectAllCount($sql_where, '', '');
    $nombreGrid = "xGrid";        

    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos As $reg => $dato) {            
            $url_mtto = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("mtto_edit") . "&id=" . $objSys->encrypt( $dato["id_mantenimiento"] );
            $html .= '<tr id="' . $dato["id_mantenimiento"] . '" class="lknMtto">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["id_mantenimiento"] . '</td>';
                $html .= '<td style="text-align: center; width: 30%;">' . date('d/m/Y', strtotime($dato["fecha_ini"])) . '</td>';
                $html .= '<td style="text-align: center; width: 40%;">' . date('d/m/Y', strtotime($dato["fecha_fin"])) . '</td>';
                $html .= '<td style="text-align: center; width: 20%;"><a href="' . $url_mtto . '" class="lnkBtnOpcionGrid" title="Editar datos."><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a></td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';  		
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron mantenimientos para este vehiculo...';
        $html .= '</span>';
    } else {        
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    // Formatea los datos y los envia al grid...
    $ajx_datos["html"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
    
 } else {
    echo "Error de Sesi�n...";
}                 
?>