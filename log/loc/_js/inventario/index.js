var xGrid;
var XGrid2;
$(document).ready(function(){
    //-- Ajusta el tama�o del contenedor del grid...
    $('#dvGrid-Armas').css('height', ($(window).height() - 250) + 'px');
    $('#dvGrid-LstArmas').css('height', ($(window).height() - 250) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGrid-Armas').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 1,        
        xTypeDataAjax: 'json'
    });   
    //-- Crea e inicializa el control del grid para la lista de bajas de armamento...
    xGrid2 = $('#dvGrid-LstBajas').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlObtLstBajas').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });     
    
    //-- Definici�n del dialog como formulario para dar de alta armamento nuevas....
    $('#dvArma').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 850,
        height: 650,
        modal: true,
        resizable: false,
        buttons:{            
            '-- Guardar --': function(){
                //funcion guardar los datos del arma                
                datosArma();                
            },
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
    
    //-- Definici�n del dialog como formulario para la baja de armamento....
    $('#dvBaja').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 720,
        height: 330,
        modal: true,
        resizable: false,
        buttons:{            
            '-- Guardar --': function(){
                //funcion guardar los datos del arma                      
                rgBajaArma();                                                                    
            },
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
    
    //-- Definici�n del dialog para visualizar la lista de bajas de armamento....
    $('#dvLstBajas').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 930,
        height: 500,
        modal: true,
        resizable: false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });

    //-- Controla el bot�n para visualizar el formulario de alta de armamento...
    $("#btnArma, .classModificar").live("click", function(e){
        var oper;
        var titulo;
        var ID;
        //se cambia el flujo del script segu sea insercion o actualizacion. 
        if( $(this).attr('id') == 'btnArma' ){
            oper=0;            
            titulo='Registro';                                         
        }else{
            ID= $(this).attr('id').substring(4);
            oper=1;
            titulo='Actualizaci�n de caracteristicas';                    
        }        
        $.ajax({
            url: xDcrypt( $('#hdnUrlAdd').val() ),            
            dataType: 'json',
            type: 'POST',
            data: { 'oper' : oper , 'matricula' : ID },
            async: true,
            cache: false,            
            success: function (xdata) {                    
                    $('#dvArmaCont').html(xdata.html);
                    $('#dvArma').dialog('option','title',titulo);                                                           
                    $('#dvArma').dialog('open');                                                                              
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    });          
    
    //-- Controla el bot�n para visualizar las listas de equipamento...
    $(".classBaja").live("click", function(e){
        var ID = $(this).attr("id");
        $.ajax({
            url: xDcrypt( $('#hdnUrlBaja').val() ),  
            data: { 'matricula': ID.substring(4) },          
            dataType: 'json',
            type: 'POST',
            async: true,
            cache: false,            
            success: function (xdata) {                    
                    $('#dvBajaCont').html(xdata.html);
                    $('#dvBaja').dialog('open');
                    $('#txtOficio').focus();
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    });   
    
    //-- Controla el bot�n para visualizar el formulario de alta de armamento...
    $("#btnLstBajas").live("click", function(e){  
        $('#dvLstBajas').dialog('open');         
    });         
    
    // Control para la carga din�mica de las clases.
    $('#cbxTipo').live("change", function(e){        
        obtenerClases( $(this).val() );  
        obtenerMarcas( $("#cbxClase").val(), $("#cbxModelo").val() );
    });   
    // Control para la carga din�mica de las modelos.
    $('#cbxCalibre').live("change", function(e){        
        obtenerModelos( $(this).val() );   
        obtenerMarcas( $("#cbxClase").val(), $("#cbxModelo").val() );       
    }); 
    // Control para la carga din�mica de las marcas. 
    $('#cbxModelo').live("change", function(e){            
        obtenerMarcas( $("#cbxClase").val(), $("#cbxModelo").val() );
    });   
    // Control para la carga din�mica de los folios d  
    $('#cbxFolioc').live("change", function(e){            
        obtenerFoliod( $("#cbxFolioc").val() );
    });

});

 //-- Controla el bot�n para visualizar el formulario de alta o actualizacion de armamento...
    function datosArma(){    
        var valida = $('#frmAlta').validate().form();
        var mensaje='';  
        var oper = $("#dtTypeOper").val();             
        if (valida) {
            if( oper == '1' ){
                mensaje= 'Confirme la actualizaci�n de los datos.';
            }else{
                mensaje='Confirme la alta en sistema.';
            }
            var frmPreg = $(getHTMLMensaje(mensaje, 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $.ajax({
                            url: xDcrypt( $('#hdnUrlAddRg').val() ),   
                            data: $( "#frmAlta" ).serialize(),         
                            dataType: 'json',
                            type: 'POST',
                            async: true,
                            cache: false,            
                            success: function ( xdata ) {                                                                   
                                if( xdata.rslt == 1 ){  
                                    if( oper == '1' ){                                   
                                        $("#dvArma").dialog('close');
                                        xGrid.refreshGrid();       
                                    }else{                                        
                                        $("#txtMatricula").attr("value","");                                                                        
                                        $("#txtMatricula").focus();
                                        $("#xGridBody").html( xdata.html );                                        
                                        xGrid.refreshGrid();    
                                                                            
                                    }                                    
                                                                                                      
                                }else{
                                    $(this).dialog('close');
                                    alert("No se guardaron los datos. " + xdata.rslt);                                                                        
                                }                                                                                               
                            },
                            error: function(objeto, detalle, otroobj){
                                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                            }
                        });  
                        $(this).dialog('close');                                              
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });                        
        }
    }
    
    //-- Controla el bot�n para visualizar el formulario de baja de armamento...
    function rgBajaArma(){
        //alert("hola jojo");
        var valida2 = $('#frmBaja').validate().form();        
        if (valida2) {
            var frmPreg2 = $(getHTMLMensaje('Confirme la baja del arma.', 2));
            frmPreg2.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $.ajax({
                            url: xDcrypt( $('#hdnUrlBajaRg').val() ),   
                            data: $( "#frmBaja" ).serialize(),         
                            dataType: 'json',
                            type: 'POST',
                            async: true,
                            cache: false,            
                            success: function ( xdata ) {                                                                 
                                if( xdata.rslt == 1 ){  
                                    $("#dvBaja").dialog('close');
                                    xGrid.refreshGrid();                                                                                                                                          
                                }else{      
                                    alert(xdata.campos);
                                    alert("No se guardaron los datos. " + xdata.rslt);                                                                                                            
                                }                                                                                                                               
                            },
                            error: function(objeto, detalle, otroobj){
                                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                            }
                        }); 
                        $(this).dialog('close');                                                                                                                                                
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');                        
                    }
                }
            });                                    
        }
    }
    
    //funcion que obtiene las diferentes clases segun el filtro
    function obtenerClases( id_tipo ){
        //alert( id_marca);
        $.ajax({
            url: xDcrypt($('#hdnUrlCla').val()),
            data: {'id_tipo': id_tipo},
            dataType: 'json',
            async: true,
            cache: false,
            beforeSend: function () {
                $('#cbxClase').html('<option>Cargando clases...</option>');
            },
            success: function (xdata) {
                if (xdata.rslt)
                    $('#cbxClase').html(xdata.html);
                else
                    shwError(xdata.error);
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    }
    //funcion que obtiene los diferentes modelos segun el filtro
    function obtenerModelos( id_calibre ){
        //alert( id_marca);
        $.ajax({
            url: xDcrypt($('#hdnUrlMod').val()),
            data: {'id_calibre': id_calibre},
            dataType: 'json',
            async: true,
            cache: false,
            beforeSend: function () {
                $('#cbxModelo').html('<option>Cargando modelos...</option>');
            },
            success: function (xdata) {
                if (xdata.rslt)
                    $('#cbxModelo').html(xdata.html);
                else
                    shwError(xdata.error);
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    }
    //funcion que obtiene las diferentes marcas segun el filtro
    function obtenerMarcas( id_clase, id_modelo ){        
        $.ajax({
            url: xDcrypt($('#hdnUrlMar').val()),
            data: {'id_clase': id_clase, "id_modelo":id_modelo},
            dataType: 'json',
            async: true,
            cache: false,
            beforeSend: function () {
                $('#cbxMarca').html('<option>Cargando marcas...</option>');
            },
            success: function (xdata) {
                if (xdata.rslt)
                    $('#cbxMarca').html(xdata.html);
                else
                    shwError(xdata.error);
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    }
    
    //funcion que obtiene las diferentes marcas segun el filtro
    function obtenerFoliod( id_folioc ){               
        $.ajax({
            url: xDcrypt($('#hdnUrlFoliod').val()),
            data: {'id_folioc': id_folioc},
            dataType: 'json',
            async: true,
            cache: false,
            beforeSend: function () {
                $('#cbxFoliod').html('<option>Cargando folios d...</option>');
            },
            success: function (xdata) {
                if (xdata.rslt)
                    $('#cbxFoliod').html(xdata.html);
                else
                    shwError(xdata.error);
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    }