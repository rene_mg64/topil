<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
//include 'includes/class/personal.class.php';

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Control de Armamento',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="log/equipamento/_js/inventario/index.js"></script>'), 
                                   '<script type="text/javascript" src="log/loc/_js/equipamento/arma_add.js"></script>',
                                   '<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',                
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <?php
    //-------------------------------- Barra de opciones ------------------------------------//
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 80%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 10%;">                    
                    <a href="#" id="btnLstBajas" class="Tool-Bar-Btn" style="" title="Lista de bajas de equipamento">
                        <img src="<?php echo PATH_IMAGES;?>icons/lst_bajas24.png" alt="" style="border: none;" /><br />Lista Bajas
                    </a>
                </td>
                <td style="text-align: right; width: 10%;">
                    <!-- Botones de opci�n... -->
                    <a href="#" id="btnEquip" class="Tool-Bar-Btn" style="" title="Registrar nuevo equipamento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Registrar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvGrid-Equip" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>  
                    <th style="width: 3%; text-align: center;">&nbsp;</th>
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">INVENTARIO</th>  
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">SERIE</th>       
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">MARCA</th> 
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">MODELO</th> 
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">TIPO</th>                                                                                                                                  
                    <th style="width: 7%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div id="xGridBody" class="xGrid-dvBody"></div>        
    </div>
    
    <!-- CONTENEDOR PARA LA EL FORMULARIO DE EQUIPAMENTO NUEVO------------- -->
    <div id="dvEquip" style="display: none;" title="x-">    
        <div id="dvEquipCont" class="dvForm-Data" style="border-bottom: 2px double #488ac7; min-height: 300px; min-width: 500px; overflow-y: hidden;"></div>
    </div>
    <!-- CONTENEDOR PARA LA EL FORMULARIO DE BAJA DE EQUIPAMENTO ------------- -->
    <div id="dvBaja" style="display: none;" title="Baja">    
        <div id="dvBajaCont" class="dvForm-Data" style="border-bottom: 2px double #488ac7; min-height: 210px; min-width: 650px; overflow-y: hidden;"></div>
    </div>
              
    <!-- CONTENEDOR PARA LISTA DE BAJAS DE ARMAMENTO ------------- -->
    <div id="dvLstBajas" style="display: none;" title="Lista de bajas de equipamento">            
            
        <div id="dvGrid-LstBajas" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
            <div class="xGrid-dvHeader gradient">
                <table class="xGrid-tbSearch">
                    <tr>
                        <td>Buscar: <input type="text" name="txtSearch" size="25" value="<?php if( isset($_SESSION['xSearchGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                    </tr>
                </table>
                <table class="xGrid-tbCols">
                    <tr>                          
                        <th style="width: 10%;" class="xGrid-tbCols-ColSortable">INVENTARIO</th>  
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">SERIE</th>
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">MARCA</th>  
                        <th style="width: 20%;" class="xGrid-tbCols-ColSortable">MODELO</th>                                     
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">TIPO</th>
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">FECHA BAJA</th>                        
                    </tr>
                </table>
            </div>
            <div id="xGridBodyBajas" class="xGrid-dvBody"></div>        
        </div>
                
    </div>              
                                         
    <input type="hidden" id="hdnUrlDatos"   value="<?php echo $objSys->encrypt('log/equipamento/inventario/ajx_obt_datos_grid.php');?>" />
    <input type="hidden" id="hdnUrlAdd"     value="<?php echo $objSys->encrypt('log/equipamento/inventario/ajx_frm_equipamento.php');?>" />
    <input type="hidden" id="hdnUrlAddRg"   value="<?php echo $objSys->encrypt('log/equipamento/inventario/ajx_reg_equipamento.php');?>" />
    <input type="hidden" id="hdnUrlBaja"    value="<?php echo $objSys->encrypt('log/equipamento/inventario/ajx_frm_baja_equipamento.php');?>" />        
    <input type="hidden" id="hdnUrlBajaRg"  value="<?php echo $objSys->encrypt('log/equipamento/inventario/ajx_reg_baja_equipamento.php');?>" />
    <input type="hidden" id="hdnUrlObtLstBajas" value="<?php echo $objSys->encrypt('log/equipamento/inventario/ajx_obt_lst_bajas.php');?>" />

<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>