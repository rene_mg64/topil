<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla de equipamento en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p�gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_equipamento.class.php';
    $objSys = new System();
    $objEq = new LogtblEquipamento();

    //--------------------- Recepci�n de par�metros --------------------------//
    // B�squeda...
    $sql_where = ' a.id_estatus = 1 ';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {
        $sql_where = ' mar.marca LIKE ? '
                   . ' OR t.tipo LIKE ? '
                   . ' OR a.serie LIKE ? '
                   . ' OR a.modelo LIKE ? ';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%');
    }
    // Ordenaci�n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'mar.marca ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
        $sql_order = 't.tipo ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'a.serie ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'a.modelo ' . $_GET['typeSort'];
    }
       
    // Paginaci�n...    
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];    
    
    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];    
    //------------------------------------------------------------------------//
       
    $datos = $objEq->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];
    
    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $objSys->encrypt($dato["matricula"]);
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_equipamento"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 3%;">';
                   $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';               
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["inventario"] . '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["serie"] . '</td>';
                $html .= '<td style="text-align: center; width: 20%;">' . $dato["marca"] . '</td>';               
                $html .= '<td style="text-align: center; width: 20%;">' . $dato["modelo"] . '</td>';
                $html .= '<td style="text-align: center; width: 20%;">' . $dato["tipo"] . '</td>';                				
//$url_edit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("_edit") . "&id=" . $id_crypt;
//$url_baja = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("vehiculo_baja") . "&id=" . $id_crypt;
                $html .= '<td style="text-align: center; width: 7%;">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid classModificar" id="Mod-' . $dato["id_equipamento"] .  '" title="Actualizar los datos..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';                                
                $html .= '  <a href="#" class="lnkBtnOpcionGrid classBaja" id="Baj-' . $dato["id_equipamento"] .  '" title="Dar de baja..." ><img src="' . PATH_IMAGES . 'icons/delete24.png" alt="baja" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';  		
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontro equipamento en esta consulta...';
        $html .= '</span>';
    } else {        
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>