var xGrid;
var xGrid2;
$(document).ready(function(){
    //-- Ajusta el tama�o del contenedor del grid...
    $('#dvGrid-Equip').css('height', ($(window).height() - 250) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGrid-Equip').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });        
    //-- Crea e inicializa el control del grid para la lista de bajas de armamento...
    xGrid2 = $('#dvGrid-LstBajas').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlObtLstBajas').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });  
    
    //-- Definici�n del dialog como formulario para dar de alta equipamento nuevo....
    $('#dvEquip').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 750,
        height: 480,
        modal: true,
        resizable: false,
        buttons:{            
            '-- Guardar --': function(){
                //funcion guardar los datos del arma                
                datosEquip();                
            },
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
    
    //-- Definici�n del dialog como formulario para la baja de equipamento....
    $('#dvBaja').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 720,
        height: 330,
        modal: true,
        resizable: false,
        buttons:{            
            '-- Guardar --': function(){
                //funcion guardar los datos del arma                      
                rgBajaEquip();                                                                    
            },
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
    
    //-- Definici�n del dialog para visualizar la lista de bajas de equipamento....
    $('#dvLstBajas').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 930,
        height: 500,
        modal: true,
        resizable: false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
    
    //-- Controla el bot�n para visualizar la lista de bajas de equipamento...
    $("#btnLstBajas").live("click", function(e){
        xGrid2.refreshGrid(); 
        $('#dvLstBajas').dialog('open'); 
    }); 

    //-- Controla el bot�n para visualizar el formulario de alta de equipamento...
    $("#btnEquip, .classModificar").live("click", function(e){
        var oper;
        var titulo;
        var ID;
        //se cambia el flujo del script segu sea insercion o actualizacion. 
        if( $(this).attr('id') == 'btnEquip' ){
            oper=0;            
            titulo='Registro';                                         
        }else{
            ID= $(this).attr('id').substring(4);
            oper=1;
            titulo='Actualizaci�n de caracteristicas';                    
        }        
        $.ajax({
            url: xDcrypt( $('#hdnUrlAdd').val() ),            
            dataType: 'json',
            type: 'POST',
            data: { 'oper' : oper , 'id_equipamento' : ID },
            async: true,
            cache: false,            
            success: function (xdata) {                    
                    $('#dvEquipCont').html(xdata.html);
                    $('#dvEquip').dialog('option','title',titulo);                                                           
                    $('#dvEquip').dialog('open');                                                                              
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    });          
    
    //-- Controla el bot�n para visualizar el formulario de baja de armamento...
    $(".classBaja").live("click", function(e){
        var ID = $(this).attr("id");
        $.ajax({
            url: xDcrypt( $('#hdnUrlBaja').val() ),  
            data: { 'id_equipamento': ID.substring(4) },          
            dataType: 'json',
            type: 'POST',
            async: true,
            cache: false,            
            success: function (xdata) {                    
                    $('#dvBajaCont').html(xdata.html);
                    $('#dvBaja').dialog('open');
                    $('#txtOficio').focus();
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    });            

});

 //-- Controla el bot�n para visualizar el formulario de alta o actualizacion de armamento...
    function datosEquip(){    
        var valida = $('#frmAlta').validate().form();
        var mensaje='';  
        var oper = $("#dtTypeOper").val();             
        if (valida) {
            if( oper == '1' ){
                mensaje= 'Confirme la actualizaci�n de los datos.';
            }else{
                mensaje='Confirme la alta en sistema.';
            }
            var frmPreg = $(getHTMLMensaje(mensaje, 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $.ajax({
                            url: xDcrypt( $('#hdnUrlAddRg').val() ),   
                            data: $( "#frmAlta" ).serialize(),         
                            dataType: 'json',
                            type: 'POST',
                            async: true,
                            cache: false,            
                            success: function ( xdata ) {                                   
                                if( xdata.rslt ){  
                                    if( oper == '1' ){                                   
                                        $("#dvEquip").dialog('close');                                        
                                        xGrid.refreshGrid();       
                                    }else{                                                            
                                        $("#txtInventario").focus();
                                        $("#txtInventario").attr("value","");                                                                                                                
                                        $("#txtSerie").attr("value","");
                                        $("#txtModelo").attr("value","");                                
                                        $("#xGridBody").html( xdata.html );                                        
                                        xGrid.refreshGrid();                                                                                
                                    }                                    
                                                                                                      
                                }else{
                                    $(this).dialog('close');
                                    alert("No se guardaron los datos. " + xdata.rslt);                                                                        
                                }                                                                                               
                            },
                            error: function(objeto, detalle, otroobj){
                                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                            }
                        });  
                        $(this).dialog('close');                                              
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });                        
        }
    }
    
    //-- Controla el bot�n para visualizar el formulario de baja de equipamento...
    function rgBajaEquip(){
        //alert("hola jojo");
        var valida2 = $('#frmBaja').validate().form();        
        if (valida2) {
            var frmPreg2 = $(getHTMLMensaje('Confirme la baja del equipamento.', 2));
            frmPreg2.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $.ajax({
                            url: xDcrypt( $('#hdnUrlBajaRg').val() ),   
                            data: $( "#frmBaja" ).serialize(),         
                            dataType: 'json',
                            type: 'POST',
                            async: true,
                            cache: false,            
                            success: function ( xdata ) {
                                //alert(xdata.campos)                                
                                if( xdata.rslt ){  
                                    $("#dvBaja").dialog('close');
                                    xGrid.refreshGrid();                                                                                                                                          
                                }else{      
                                    alert(xdata.campos);
                                    alert("No se guardaron los datos. " + xdata.rslt);                                                                                                            
                                }                
                                                                                                                                               
                            },
                            error: function(objeto, detalle, otroobj){
                                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                            }
                        }); 
                        $(this).dialog('close');                                                                                                                                                
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');                        
                    }
                }
            });                                    
        }
    }