<?php
/**
 *
 */
class LogtblArmBaja
{
    public $id_baja; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $matricula; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $oficio; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_motivo; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha_baja; /** @Tipo: timestamp, @Acepta Nulos: NO, @Llave: --, @Default: CURRENT_TIMESTAMP */
    public $id_usuario; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $Xtblusuarios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatArmMotivoBaja; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogtblArmArmamento; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'xtblusuarios.class.php';
        require_once 'logcat_arm_motivo_baja.class.php';
        require_once 'logtbl_arm_armamento.class.php';
        $this->Xtblusuarios = new Xtblusuarios();
        $this->LogcatArmMotivoBaja = new LogcatArmMotivoBaja();
        $this->LogtblArmArmamento = new LogtblArmArmamento();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_baja)
    {
        $sql = "SELECT id_baja, matricula, oficio, fecha_oficio, id_motivo, fecha_baja, id_usuario
                FROM logtbl_arm_baja
                WHERE id_baja=:id_baja;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_baja' => $id_baja));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_baja = $data['id_baja'];
            $this->matricula = $data['matricula'];
            $this->oficio = $data['oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->id_motivo = $data['id_motivo'];
            $this->fecha_baja = $data['fecha_baja'];
            $this->id_usuario = $data['id_usuario'];

            $this->Xtblusuarios->select($this->id_usuario);
            $this->LogcatArmMotivoBaja->select($this->id_motivo);
            $this->LogtblArmArmamento->select($this->matricula);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_baja, a.matricula, a.oficio, a.fecha_oficio, a.id_motivo, a.fecha_baja, a.id_usuario,
                  b.id_usuario, b.nom_usr, b.pswd, b.nombre, b.id_perfil, b.fecha_reg, b.fecha_edit, b.stat,
                  c.id_motivo, c.motivo, c.xstat,
                  d.matricula, d.serie, d.id_loc, d.id_foliod, d.id_situacion, d.id_propietario, d.id_estado, d.id_estatus, d.id_motivo_movimiento, d.id_marca
                FROM logtbl_arm_baja a 
                 LEFT JOIN xtblusuarios b ON a.id_usuario=b.id_usuario
                 LEFT JOIN logcat_arm_motivo_baja c ON a.id_motivo=c.id_motivo
                 LEFT JOIN logtbl_arm_armamento d ON a.matricula=d.matricula";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_baja' => $data['id_baja'],
                               'matricula' => $data['matricula'],
                               'oficio' => $data['oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'id_motivo' => $data['id_motivo'],
                               'fecha_baja' => $data['fecha_baja'],
                               'id_usuario' => $data['id_usuario'],
                               'xtblusuarios_nom_usr' => $data['nom_usr'],
                               'xtblusuarios_pswd' => $data['pswd'],
                               'xtblusuarios_nombre' => $data['nombre'],
                               'xtblusuarios_id_perfil' => $data['id_perfil'],
                               'xtblusuarios_fecha_reg' => $data['fecha_reg'],
                               'xtblusuarios_fecha_edit' => $data['fecha_edit'],
                               'xtblusuarios_stat' => $data['stat'],
                               'logcat_arm_motivo_baja_motivo' => $data['motivo'],
                               'logcat_arm_motivo_baja_xstat' => $data['xstat'],
                               'logtbl_arm_armamento_serie' => $data['serie'],
                               'logtbl_arm_armamento_id_loc' => $data['id_loc'],
                               'logtbl_arm_armamento_id_foliod' => $data['id_foliod'],
                               'logtbl_arm_armamento_id_situacion' => $data['id_situacion'],
                               'logtbl_arm_armamento_id_propietario' => $data['id_propietario'],
                               'logtbl_arm_armamento_id_estado' => $data['id_estado'],
                               'logtbl_arm_armamento_id_estatus' => $data['id_estatus'],
                               'logtbl_arm_armamento_id_motivo_movimiento' => $data['id_motivo_movimiento'],
                               'logtbl_arm_armamento_id_marca' => $data['id_marca'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlValues, cadena que contiene el parametro de busqueda.
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
     
     /*
     
create view viewarmlstbajas as
select a.matricula, a.serie,
                  c.estado,
                  d.estatus,
                  e.foliod,
                  g.loc,
                  h.marca,
                  i.motivo_movimiento,
                  j.propietario,
                  k.situacion,
                  l.modelo,
                  m.calibre,
                  fecha_baja
                FROM logtbl_arm_armamento a
                 LEFT JOIN logcat_arm_estado c ON a.id_estado=c.id_estado
                 LEFT JOIN logcat_arm_estatus d ON a.id_estatus=d.id_estatus
                 LEFT JOIN logcat_arm_foliod e ON a.id_foliod=e.id_foliod
                 LEFT JOIN logcat_arm_loc g ON a.id_loc=g.id_loc
                 LEFT JOIN logcat_arm_motivo_movimiento i ON a.id_motivo_movimiento=i.id_motivo_movimiento
                 LEFT JOIN logcat_arm_propietario j ON a.id_propietario=j.id_propietario
                 LEFT JOIN logcat_arm_situacion k ON a.id_situacion=k.id_situacion
                 LEFT JOIN logcat_arm_marca h ON a.id_marca=h.id_marca
                 LEFT JOIN logcat_arm_modelo l ON h.id_modelo=l.id_modelo
                 LEFT JOIN logcat_arm_calibre m ON l.id_calibre=m.id_calibre
                 LEFT JOIN logtbl_arm_baja n ON a.matricula=n.matricula
where a.id_situacion = 2
     */
    public function selectLstBajas($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "select matricula, serie, marca, modelo, calibre, fecha_baja from viewarmlstbajas ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";        
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
                //echo "si";
            } else {
                $qry->execute();
                //echo "no" . count($sqlValues);
            }
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'matricula' => $data['matricula'],
                               'serie' => $data['serie'],
                               'marca' => $data['marca'],
                               'modelo' => $data['modelo'],
                               'calibre' => $data['calibre'],
                               'fecha_baja' => $data['fecha_baja'],                              
                               );
            }
            //echo count( $datos );
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

/*
select matricula, serie, marca, modelo, clase, fecha_baja from viewlstbajas
*/

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        /*
        $sql = "INSERT INTO logtbl_arm_baja(id_baja, matricula, oficio, fecha_oficio, id_motivo, fecha_baja, id_usuario)
                VALUES(:id_baja, :matricula, :oficio, :fecha_oficio, :id_motivo, :fecha_baja, :id_usuario);";
        */
        $sql = "INSERT INTO logtbl_arm_baja(matricula, oficio, fecha_oficio, id_motivo, id_usuario)
                VALUES(:matricula, :oficio, :fecha_oficio, :id_motivo, :id_usuario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":matricula" => $this->matricula, ":oficio" => $this->oficio, ":fecha_oficio" => $this->fecha_oficio, ":id_motivo" => $this->id_motivo, ":id_usuario" => $this->id_usuario));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_arm_baja
                   SET matricula=:matricula, oficio=:oficio, fecha_oficio=:fecha_oficio, id_motivo=:id_motivo, fecha_baja=:fecha_baja, id_usuario=:id_usuario
                WHERE id_baja=:id_baja;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_baja" => $this->id_baja, ":matricula" => $this->matricula, ":oficio" => $this->oficio, ":fecha_oficio" => $this->fecha_oficio, ":id_motivo" => $this->id_motivo, ":fecha_baja" => $this->fecha_baja, ":id_usuario" => $this->id_usuario));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar datos de la tabla de logtbl_arm_armamento en el proceso de baja
     * @return boolean true si el proceso es satisfactorio
     */
    public function update_baja()
    {
        $sql = "UPDATE logtbl_arm_armamento
                   SET id_situacion=:id_situacion, id_estado=:id_estado, id_estatus=:id_estatus, id_motivo_movimiento=:id_motivo_movimiento
                WHERE matricula=:matricula;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":matricula" => $this->matricula, ":id_situacion" => 2, ":id_estado" => 1, ":id_estatus" => 6, ":id_motivo_movimiento" => 7));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>