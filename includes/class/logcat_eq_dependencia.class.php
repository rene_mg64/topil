<?php
/**
 *
 */
class LogcatEqDependencia
{
    public $id_dependencia; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $dependencia; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de dependencias dentro de un combobox.
     * @param int $id, id de la dependencia seleccionado por deafult     
     * @return array html(options)
     */
    public function shwDependencias($id=0){
        $aryDatos = $this->selectAll('a.xstat=1', 'a.dependencia Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_dependencia"] )
                $html .= '<option value="'.$datos["id_dependencia"].'" selected>'.$datos["dependencia"].'</option>';
            else
                $html .= '<option value="'.$datos["id_dependencia"].'" >'.$datos["dependencia"].'</option>';
        }
        return $html;
    } 

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_dependencia)
    {
        $sql = "SELECT id_dependencia, dependencia, xstat
                FROM logcat_eq_dependencia
                WHERE id_dependencia=:id_dependencia;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_dependencia' => $id_dependencia));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_dependencia = $data['id_dependencia'];
            $this->dependencia = $data['dependencia'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_dependencia, a.dependencia, a.xstat
                FROM logcat_eq_dependencia a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_dependencia' => $data['id_dependencia'],
                               'dependencia' => $data['dependencia'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_eq_dependencia(id_dependencia, dependencia, xstat)
                VALUES(:id_dependencia, :dependencia, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_dependencia" => $this->id_dependencia, ":dependencia" => $this->dependencia, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_eq_dependencia
                   SET dependencia=:dependencia, xstat=:xstat
                WHERE id_dependencia=:id_dependencia;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_dependencia" => $this->id_dependencia, ":dependencia" => $this->dependencia, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>