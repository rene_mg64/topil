<?php
/**
 *
 */
class AdmcatResultEval
{
    public $id_result_eval; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $resultado; /** @Tipo: varchar(45), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $aprobatorio; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }
    
    /**
     * Funci�n para mostrar la lista de los resultados de evaluaciones dentro de un combobox.
     * @param int $id, id del resultado de evaluaci�n seleccionado por deafult     
     * @return array html(options)
     */
    public function shwResultEval($id=0){
        $aryDatos = $this->selectAll('', 'id_result_eval Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_result_eval"] )
                $html .= '<option value="'.$datos["id_result_eval"].'" selected>'.$datos["resultado"].'</option>';
            else
                $html .= '<option value="'.$datos["id_result_eval"].'" >'.$datos["resultado"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_result_eval)
    {
        $sql = "SELECT id_result_eval, resultado, aprobatorio
                FROM admcat_result_eval
                WHERE id_result_eval=:id_result_eval;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_result_eval' => $id_result_eval));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_result_eval = $data['id_result_eval'];
            $this->resultado = $data['resultado'];
            $this->aprobatorio = $data['aprobatorio'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_result_eval, a.resultado, a.aprobatorio
                FROM admcat_result_eval a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_result_eval' => $data['id_result_eval'],
                               'resultado' => $data['resultado'],
                               'aprobatorio' => $data['aprobatorio'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_result_eval(id_result_eval, resultado, aprobatorio)
                VALUES(:id_result_eval, :resultado, :aprobatorio);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_result_eval" => $this->id_result_eval, ":resultado" => $this->resultado, ":aprobatorio" => $this->aprobatorio));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_result_eval
                   SET resultado=:resultado, aprobatorio=:aprobatorio
                WHERE id_result_eval=:id_result_eval;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_result_eval" => $this->id_result_eval, ":resultado" => $this->resultado, ":aprobatorio" => $this->aprobatorio));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>