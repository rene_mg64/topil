<?php
/**
 *
 */
class OpetblMcsConflictos
{
    public $id_conflicto; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_region; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_municipio; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $localidad; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_tipo_conflicto; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo_movimiento; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_organizacion_gpo; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $dirigentes; /** @Tipo: varchar(200), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_agregado; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_conflicto; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $hora_inicio; /** @Tipo: time, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $hora_fin; /** @Tipo: time, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $hechos_html; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $hechos_text; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $numero_involucrados; /** @Tipo: int(11), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMcsMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMcsRegiones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMcsTiposConflictos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMcsTiposMovimientos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatOrganizacionesGrupos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mcs_municipios.class.php';
        require_once 'opecat_mcs_regiones.class.php';
        require_once 'opecat_mcs_tipos_conflictos.class.php';
        require_once 'opecat_mcs_tipos_movimientos.class.php';
        require_once 'opecat_mcs_organizaciones_grupos.class.php';
        $this->OpecatMcsMunicipios = new OpecatMcsMunicipios();
        $this->OpecatMcsRegiones = new OpecatMcsRegiones();
        $this->OpecatMcsTiposConflictos = new OpecatMcsTiposConflictos();
        $this->OpecatMcsTiposMovimientos = new OpecatMcsTiposMovimientos();
        $this->OpecatOrganizacionesGrupos = new OpecatMcsOrganizacionesGrupos();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_conflicto)
    {
        $sql = "SELECT id_conflicto, id_region, id_municipio, localidad, id_tipo_conflicto, id_tipo_movimiento, id_organizacion_gpo, dirigentes, fecha_agregado, fecha_conflicto, hora_inicio, hora_fin, hechos_html, hechos_text, numero_involucrados
                FROM opetbl_mcs_conflictos
                WHERE id_conflicto=:id_conflicto;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_conflicto' => $id_conflicto));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_conflicto = $data['id_conflicto'];
            $this->id_region = $data['id_region'];
            $this->id_municipio = $data['id_municipio'];
            $this->localidad = $data['localidad'];
            $this->id_tipo_conflicto = $data['id_tipo_conflicto'];
            $this->id_tipo_movimiento = $data['id_tipo_movimiento'];
            $this->id_organizacion_gpo = $data['id_organizacion_gpo'];
            $this->dirigentes = $data['dirigentes'];
            $this->fecha_agregado = $data['fecha_agregado'];
            $this->fecha_conflicto = $data['fecha_conflicto'];
            $this->hora_inicio = $data['hora_inicio'];
            $this->hora_fin = $data['hora_fin'];
            $this->hechos_html = $data['hechos_html'];
            $this->hechos_text = $data['hechos_text'];
            $this->numero_involucrados = $data['numero_involucrados'];

            $this->OpecatMcsMunicipios->select($this->id_municipio);
            $this->OpecatMcsRegiones->select($this->id_region);
            $this->OpecatMcsTiposConflictos->select($this->id_tipo_conflicto);
            $this->OpecatMcsTiposMovimientos->select($this->id_tipo_movimiento);
            $this->OpecatOrganizacionesGrupos->select($this->id_organizacion_gpo);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_conflicto, a.id_region, a.id_municipio, a.localidad, a.id_tipo_conflicto, a.id_tipo_movimiento, a.id_organizacion_gpo, a.dirigentes, a.fecha_agregado, a.fecha_conflicto, a.hora_inicio, a.hora_fin, a.hechos_html, a.hechos_text, a.numero_involucrados,
                  b.id_municipio, b.id_estado, b.Municipio,
                  c.id_region, c.region,
                  d.id_tipo_conflicto, d.tipo_conflicto,
                  e.id_tipo_movimiento, e.tipo_movimiento,
                  f.id_organizacion_gpo, f.organizacion_grupo, f.fecha_creacion, f.observaciones
                FROM opetbl_mcs_conflictos a
                 LEFT JOIN opecat_mcs_municipios b ON a.id_municipio=b.id_municipio
                 LEFT JOIN opecat_mcs_regiones c ON a.id_region=c.id_region
                 LEFT JOIN opecat_mcs_tipos_conflictos d ON a.id_tipo_conflicto=d.id_tipo_conflicto
                 LEFT JOIN opecat_mcs_tipos_movimientos e ON a.id_tipo_movimiento=e.id_tipo_movimiento
                 LEFT JOIN opecat_mcs_organizaciones_grupos f ON a.id_organizacion_gpo=f.id_organizacion_gpo";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_conflicto' => $data['id_conflicto'],
                               'id_region' => $data['id_region'],
                               'id_municipio' => $data['id_municipio'],
                               'localidad' => $data['localidad'],
                               'id_tipo_conflicto' => $data['id_tipo_conflicto'],
                               'id_tipo_movimiento' => $data['id_tipo_movimiento'],
                               'id_organizacion_gpo' => $data['id_organizacion_gpo'],
                               'dirigentes' => $data['dirigentes'],
                               'fecha_agregado' => $data['fecha_agregado'],
                               'fecha_conflicto' => $data['fecha_conflicto'],
                               'hora_inicio' => $data['hora_inicio'],
                               'hora_fin' => $data['hora_fin'],
                               'hechos_html' => $data['hechos_html'],
                               'hechos_text' => $data['hechos_text'],
                               'numero_involucrados' => $data['numero_involucrados'],
                               'opecat_mcs_municipios_id_estado' => $data['id_estado'],
                               'opecat_mcs_municipios_Municipio' => $data['Municipio'],
                               'opecat_mcs_regiones_region' => $data['region'],
                               'opecat_mcs_tipos_conflictos_tipo_conflicto' => $data['tipo_conflicto'],
                               'opecat_mcs_tipos_movimientos_tipo_movimiento' => $data['tipo_movimiento'],
                               'opecat_mcs_organizaciones_grupos_organizacion_grupo' => $data['organizacion_grupo'],
                               'opecat_mcs_organizaciones_grupos_fecha_creacion' => $data['fecha_creacion'],
                               'opecat_mcs_organizaciones_grupos_observaciones' => $data['observaciones'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }



/**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }

        return $datos;
    }

 /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT c.id_conflicto,r.region,m.Municipio,c.localidad,
                c.fecha_conflicto,c.hora_inicio,c.hora_fin, tm.tipo_movimiento,
                og.organizacion_grupo, tc.tipo_conflicto,c.dirigentes,c.hechos_text
                FROM opetbl_mcs_conflictos c
                LEFT JOIN opecat_mcs_regiones r on c.id_region=r.id_region
                LEFT JOIN opecat_mcs_municipios m on c.id_municipio=m.id_municipio
                LEFT JOIN opecat_mcs_organizaciones_grupos og on c.id_organizacion_gpo=og.id_organizacion_gpo
                LEFT JOIN opecat_mcs_tipos_conflictos tc on c.id_tipo_conflicto=tc.id_tipo_conflicto
                LEFT JOIN opecat_mcs_tipos_movimientos tm on c.id_tipo_movimiento=tm.id_tipo_movimiento";

        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                                'id_conflicto' => $data['id_conflicto'],
                                'region' => $data['region'],
                                'Municipio' => $data['Municipio'],
                                'localidad' => $data['localidad'],
                                'fecha_conflicto' => $data['fecha_conflicto'],
                                'hora_inicio' => $data['hora_inicio'],
                                'hora_fin' => $data['hora_fin'],
                                'tipo_movimiento' => $data['tipo_movimiento'],
                                'organizacion_grupo' => $data['organizacion_grupo'],
                                'tipo_conflicto' => $data['tipo_conflicto'],
                                'dirigentes' => $data['dirigentes'],
                                'hechos_text' => $data['hechos_text'],
                                );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "select count(*)
				from opetbl_mcs_conflictos c
				left join opecat_mcs_regiones r on c.id_region=r.id_region
				left join opecat_mcs_municipios m on c.id_municipio=m.id_municipio
				left join opecat_mcs_organizaciones_grupos og on c.id_organizacion_gpo=og.id_organizacion_gpo
				left join opecat_mcs_tipos_conflictos tc on c.id_tipo_conflicto=tc.id_tipo_conflicto
				left join opecat_mcs_tipos_movimientos tm on c.id_tipo_movimiento=tm.id_tipo_movimiento";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }

        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $total_reg = $qry->fetchColumn();

            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }





    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mcs_conflictos(id_conflicto, id_region, id_municipio, localidad, id_tipo_conflicto, id_tipo_movimiento, id_organizacion_gpo, dirigentes, fecha_agregado, fecha_conflicto, hora_inicio, hora_fin, hechos_html, hechos_text, numero_involucrados)
                VALUES(:id_conflicto, :id_region, :id_municipio, :localidad, :id_tipo_conflicto, :id_tipo_movimiento, :id_organizacion_gpo, :dirigentes, :fecha_agregado, :fecha_conflicto, :hora_inicio, :hora_fin, :hechos_html, :hechos_text, :numero_involucrados);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_conflicto" => $this->id_conflicto, ":id_region" => $this->id_region, ":id_municipio" => $this->id_municipio, ":localidad" => $this->localidad, ":id_tipo_conflicto" => $this->id_tipo_conflicto, ":id_tipo_movimiento" => $this->id_tipo_movimiento, ":id_organizacion_gpo" => $this->id_organizacion_gpo, ":dirigentes" => $this->dirigentes, ":fecha_agregado" => $this->fecha_agregado, ":fecha_conflicto" => $this->fecha_conflicto, ":hora_inicio" => $this->hora_inicio, ":hora_fin" => $this->hora_fin, ":hechos_html" => $this->hechos_html, ":hechos_text" => $this->hechos_text, ":numero_involucrados" => $this->numero_involucrados));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mcs_conflictos
                   SET id_region=:id_region, id_municipio=:id_municipio, localidad=:localidad, id_tipo_conflicto=:id_tipo_conflicto, id_tipo_movimiento=:id_tipo_movimiento, id_organizacion_gpo=:id_organizacion_gpo, dirigentes=:dirigentes, fecha_agregado=:fecha_agregado, fecha_conflicto=:fecha_conflicto, hora_inicio=:hora_inicio, hora_fin=:hora_fin, hechos_html=:hechos_html, hechos_text=:hechos_text, numero_involucrados=:numero_involucrados
                WHERE id_conflicto=:id_conflicto;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_conflicto" => $this->id_conflicto, ":id_region" => $this->id_region, ":id_municipio" => $this->id_municipio, ":localidad" => $this->localidad, ":id_tipo_conflicto" => $this->id_tipo_conflicto, ":id_tipo_movimiento" => $this->id_tipo_movimiento, ":id_organizacion_gpo" => $this->id_organizacion_gpo, ":dirigentes" => $this->dirigentes, ":fecha_agregado" => $this->fecha_agregado, ":fecha_conflicto" => $this->fecha_conflicto, ":hora_inicio" => $this->hora_inicio, ":hora_fin" => $this->hora_fin, ":hechos_html" => $this->hechos_html, ":hechos_text" => $this->hechos_text, ":numero_involucrados" => $this->numero_involucrados));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>