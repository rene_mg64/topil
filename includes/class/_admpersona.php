<?php
/**
 * La clase Persona unifica todos los datos de una persona en una sola estructira
 * -> Datos Personales
 * -> Adscripci�n Laboral
 * -> Domicilio
 * -> Nivel de Estudios
 */
class Persona
{
    // Datos miembro de Datos Personales
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $nombre; /** @Tipo: varchar(35), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_paterno; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_materno; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_nac; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $genero; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $rfc; /** @Tipo: varchar(14), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $cuip; /** @Tipo: varchar(25), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $folio_ife; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $mat_cartilla; /** @Tipo: varchar(10), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $licencia_conducir; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $pasaporte; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_estado_civil; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo_sangre; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_nacionalidad; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_entidad; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_municipio; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $lugar_nac; /** @Tipo: varchar(250), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_status; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    // Datos miembro de Adscripci�n
    public $fecha_ing; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $cargo; /** @Tipo: varchar(150), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_categoria; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo_funcion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_especialidad; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_nivel_mando; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_area; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_municipio_ads; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_entidad_ads; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_corporacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_ubicacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_horario; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_plaza; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */ 
    // Datos miembre de Domicilio
    public $calle; /** @Tipo: varchar(60), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $n_exterior; /** @Tipo: varchar(6), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $n_interior; /** @Tipo: varchar(6), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $entre_calle1; /** @Tipo: varchar(60), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $entre_calle2; /** @Tipo: varchar(60), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $colonia; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $ciudad; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $cod_postal; /** @Tipo: int(6) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tel_fijo; /** @Tipo: varchar(15), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tel_movil; /** @Tipo: varchar(15), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_municipio_dom; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_entidad_dom; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    // Datos miembro de Nivel de Estudios
    public $id_nivel_estudios; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $eficencia_term; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $institucion; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $carrera; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $especialidad; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cct; /** @Tipo: varchar(25), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $documento; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $folio; /** @Tipo: varchar(25), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $n_cedula; /** @Tipo: varchar(10), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $promedio; /** @Tipo: double(4,2) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatEstadoCivil; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatNacionalidad; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatStatus; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatTipoSangre; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_municipios.class.php';
        require_once 'admcat_estado_civil.class.php';
        require_once 'admcat_nacionalidad.class.php';
        require_once 'admcat_status.class.php';
        require_once 'admcat_tipo_sangre.class.php';
        $this->AdmcatMunicipios = new AdmcatMunicipios();
        $this->AdmcatEstadoCivil = new AdmcatEstadoCivil();
        $this->AdmcatNacionalidad = new AdmcatNacionalidad();
        $this->AdmcatStatus = new AdmcatStatus();
        $this->AdmcatTipoSangre = new AdmcatTipoSangre();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT a.curp, a.nombre, a.a_paterno, a.a_materno, a.fecha_nac, a.genero, a.rfc, a.cuip, a.folio_ife, a.mat_cartilla, a.licencia_conducir, a.pasaporte, a.id_estado_civil, a.id_tipo_sangre, a.id_nacionalidad, a.id_entidad, a.id_municipio, a.lugar_nac, a.id_status,
                    b.fecha_ing, b.cargo, b.id_categoria, b.id_tipo_funcion, b.id_especialidad, b.id_nivel_mando, b.id_area, b.id_municipio AS id_municipio_ads, b.id_entidad AS id_entidad_ads, b.id_corporacion, b.id_ubicacion, b.id_horario, b.id_plaza,
                    c.calle, c.n_exterior, c.n_interior, c.entre_calle1, c.entre_calle2, c.colonia, c.ciudad, c.cod_postal, c.tel_fijo, c.tel_movil, c.id_municipio AS id_municipio_dom, c.id_entidad AS id_entidad_dom,
                    d.id_nivel_estudios, d.eficencia_term, d.institucion, d.carrera, d.especialidad, d.cct, d.documento, d.folio, d.n_cedula, d.promedio
                FROM admtbl_datos_personales a
                    JOIN admtbl_adscripcion b ON a.curp=b.curp
                    JOIN admtbl_domicilio c ON a.curp=c.curp
                    JOIN admtbl_nivel_estudios d ON a.curp=d.curp 
                WHERE a.curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));            
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            // Datos Personales
            $this->curp         = $data['curp'];
            $this->nombre       = $data['nombre'];
            $this->a_paterno    = $data['a_paterno'];
            $this->a_materno    = $data['a_materno'];
            $this->fecha_nac    = $data['fecha_nac'];
            $this->genero       = $data['genero'];
            $this->rfc          = $data['rfc'];
            $this->cuip         = $data['cuip'];
            $this->folio_ife    = $data['folio_ife'];
            $this->mat_cartilla = $data['mat_cartilla'];
            $this->licencia_conducir= $data['licencia_conducir'];
            $this->pasaporte    = $data['pasaporte'];
            $this->id_estado_civil  = $data['id_estado_civil'];
            $this->id_tipo_sangre   = $data['id_tipo_sangre'];
            $this->id_nacionalidad  = $data['id_nacionalidad'];
            $this->id_entidad   = $data['id_entidad'];
            $this->id_municipio = $data['id_municipio'];
            $this->lugar_nac    = $data['lugar_nac'];
            $this->id_status    = $data['id_status'];
            // Adscripci�n
            $this->fecha_ing    = $data['fecha_ing'];
            $this->cargo        = $data['cargo'];
            $this->id_categoria = $data['id_categoria'];
            $this->id_tipo_funcion  = $data['id_tipo_funcion'];
            $this->id_especialidad  = $data['id_especialidad'];
            $this->id_nivel_mando   = $data['id_nivel_mando'];
            $this->id_area      = $data['id_area'];
            $this->id_municipio = $data['id_municipio_ads'];
            $this->id_entidad   = $data['id_entidad_ads'];
            $this->id_corporacion   = $data['id_corporacion'];
            $this->id_ubicacion = $data['id_ubicacion'];
            $this->id_horario   = $data['id_horario'];
            $this->id_plaza     = $data['id_plaza'];
            // Domicilio
            $this->calle        = $data['calle'];
            $this->n_exterior   = $data['n_exterior'];
            $this->n_interior   = $data['n_interior'];
            $this->entre_calle1 = $data['entre_calle1'];
            $this->entre_calle2 = $data['entre_calle2'];
            $this->colonia      = $data['colonia'];
            $this->ciudad       = $data['ciudad'];
            $this->cod_postal   = $data['cod_postal'];
            $this->tel_fijo     = $data['tel_fijo'];
            $this->tel_movil    = $data['tel_movil'];
            $this->id_municipio = $data['id_municipio_dom'];
            $this->id_entidad   = $data['id_entidad_dom'];
            // Nivel de Estudios
            $this->id_nivel_estudios= $data['id_nivel_estudios'];
            $this->eficencia_term   = $data['eficencia_term'];
            $this->institucion  = $data['institucion'];
            $this->carrera      = $data['carrera'];
            $this->especialidad = $data['especialidad'];
            $this->cct          = $data['cct'];
            $this->documento    = $data['documento'];
            $this->folio        = $data['folio'];
            $this->n_cedula     = $data['n_cedula'];
            $this->promedio     = $data['promedio'];

            $this->AdmcatMunicipios->select($this->id_municipio);
            $this->AdmcatEstadoCivil->select($this->id_estado_civil);
            $this->AdmcatNacionalidad->select($this->id_nacionalidad);
            $this->AdmcatStatus->select($this->id_status);
            $this->AdmcatTipoSangre->select($this->id_tipo_sangre);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.nombre, a.a_paterno, a.a_materno, a.fecha_nac, a.genero, a.rfc, a.cuip, a.folio_ife, a.mat_cartilla, a.licencia_conducir, a.pasaporte, a.id_estado_civil, a.id_tipo_sangre, a.id_nacionalidad, a.id_entidad, a.id_municipio, a.lugar_nac, a.id_status,
                  b.id_municipio, b.municipio, b.cabecera, b.id_entidad, b.id_region,
                  c.id_estado_civil, c.estado_civil,
                  d.id_nacionalidad, d.nacionalidad,
                  e.id_status, e.status,
                  f.id_tipo_sangre, f.tipo_sangre
                FROM admtbl_datos_personales a 
                 LEFT JOIN admcat_municipios b ON a.id_municipio=b.id_municipio
                 LEFT JOIN admcat_estado_civil c ON a.id_estado_civil=c.id_estado_civil
                 LEFT JOIN admcat_nacionalidad d ON a.id_nacionalidad=d.id_nacionalidad
                 LEFT JOIN admcat_status e ON a.id_status=e.id_status
                 LEFT JOIN admcat_tipo_sangre f ON a.id_tipo_sangre=f.id_tipo_sangre";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],
                               'fecha_nac' => $data['fecha_nac'],
                               'genero' => $data['genero'],
                               'rfc' => $data['rfc'],
                               'cuip' => $data['cuip'],
                               'folio_ife' => $data['folio_ife'],
                               'mat_cartilla' => $data['mat_cartilla'],
                               'licencia_conducir' => $data['licencia_conducir'],
                               'pasaporte' => $data['pasaporte'],
                               'id_estado_civil' => $data['id_estado_civil'],
                               'id_tipo_sangre' => $data['id_tipo_sangre'],
                               'id_nacionalidad' => $data['id_nacionalidad'],
                               'id_entidad' => $data['id_entidad'],
                               'id_municipio' => $data['id_municipio'],
                               'lugar_nac' => $data['lugar_nac'],
                               'id_status' => $data['id_status'],
                               'admcat_municipios_municipio' => $data['municipio'],
                               'admcat_municipios_cabecera' => $data['cabecera'],
                               'admcat_municipios_id_entidad' => $data['id_entidad'],
                               'admcat_municipios_id_region' => $data['id_region'],
                               'admcat_estado_civil_estado_civil' => $data['estado_civil'],
                               'admcat_nacionalidad_nacionalidad' => $data['nacionalidad'],
                               'admcat_status_status' => $data['status'],
                               'admcat_tipo_sangre_tipo_sangre' => $data['tipo_sangre'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        
        $sql_dp = "INSERT INTO admtbl_datos_personales(curp, nombre, a_paterno, a_materno, fecha_nac, genero, rfc, cuip, folio_ife, mat_cartilla, licencia_conducir, pasaporte, id_estado_civil, id_tipo_sangre, id_nacionalidad, id_entidad, id_municipio, lugar_nac, id_status)
                   VALUES(:curp, :nombre, :a_paterno, :a_materno, :fecha_nac, :genero, :rfc, :cuip, :folio_ife, :mat_cartilla, :licencia_conducir, :pasaporte, :id_estado_civil, :id_tipo_sangre, :id_nacionalidad, :id_entidad, :id_municipio, :lugar_nac, :id_status);";
        $sql_ads = "INSERT INTO admtbl_adscripcion(curp, fecha_ing, cargo, id_categoria, id_tipo_funcion, id_especialidad, id_nivel_mando, id_area, id_municipio, id_entidad, id_corporacion, id_ubicacion, id_horario, id_plaza)
                    VALUES(:curp, :fecha_ing, :cargo, :id_categoria, :id_tipo_funcion, :id_especialidad, :id_nivel_mando, :id_area, :id_municipio, :id_entidad, :id_corporacion, :id_ubicacion, :id_horario, :id_plaza);";
        $sql_dom = "INSERT INTO admtbl_domicilio(curp, calle, n_exterior, n_interior, entre_calle1, entre_calle2, colonia, ciudad, cod_postal, tel_fijo, tel_movil, id_municipio, id_entidad)
                    VALUES(:curp, :calle, :n_exterior, :n_interior, :entre_calle1, :entre_calle2, :colonia, :ciudad, :cod_postal, :tel_fijo, :tel_movil, :id_municipio, :id_entidad);";
        $sql_ne = "INSERT INTO admtbl_nivel_estudios(curp, id_nivel_estudios, eficencia_term, institucion, carrera, especialidad, cct, documento, folio, n_cedula, promedio)
                   VALUES(:curp, :id_nivel_estudios, :eficencia_term, :institucion, :carrera, :especialidad, :cct, :documento, :folio, :n_cedula, :promedio);";
        try {
            // Se inicia la transacci�n
            $this->_conexBD->beginTransaction();
            // Se ejecuta el proceso para guardar los Datos Personales
            $qry_dp = $this->_conexBD->prepare($sql_dp);
            $qry_dp->execute(array(":curp" => $this->curp, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":fecha_nac" => $this->fecha_nac, ":genero" => $this->genero, ":rfc" => $this->rfc, ":cuip" => $this->cuip, ":folio_ife" => $this->folio_ife, ":mat_cartilla" => $this->mat_cartilla, ":licencia_conducir" => $this->licencia_conducir, ":pasaporte" => $this->pasaporte, ":id_estado_civil" => $this->id_estado_civil, ":id_tipo_sangre" => $this->id_tipo_sangre, ":id_nacionalidad" => $this->id_nacionalidad, ":id_entidad" => $this->id_entidad, ":id_municipio" => $this->id_municipio, ":lugar_nac" => $this->lugar_nac, ":id_status" => $this->id_status));
            if ($qry_dp) {
                // Se ejecuta el proceso para guardar los datos de la Adscripci�n Laboral
                $qry_ads = $this->_conexBD->prepare($sql_ads);
                $qry_ads->execute(array(":curp" => $this->curp, ":fecha_ing" => $this->fecha_ing, ":cargo" => $this->cargo, ":id_categoria" => $this->id_categoria, ":id_tipo_funcion" => $this->id_tipo_funcion, ":id_especialidad" => $this->id_especialidad, ":id_nivel_mando" => $this->id_nivel_mando, ":id_area" => $this->id_area, ":id_municipio" => $this->id_municipio, ":id_entidad" => $this->id_entidad, ":id_corporacion" => $this->id_corporacion, ":id_ubicacion" => $this->id_ubicacion, ":id_horario" => $this->id_horario, ":id_plaza" => $this->id_plaza));
                if ($qry_ads) {
                    // Se ejecuta el proceso para guardar los datos del Domicilio
                    $qry_dom = $this->_conexBD->prepare($sql_dom);
                    $qry_dom->execute(array(":curp" => $this->curp, ":calle" => $this->calle, ":n_exterior" => $this->n_exterior, ":n_interior" => $this->n_interior, ":entre_calle1" => $this->entre_calle1, ":entre_calle2" => $this->entre_calle2, ":colonia" => $this->colonia, ":ciudad" => $this->ciudad, ":cod_postal" => $this->cod_postal, ":tel_fijo" => $this->tel_fijo, ":tel_movil" => $this->tel_movil, ":id_municipio" => $this->id_municipio, ":id_entidad" => $this->id_entidad));
                    if ($qry_dom) {
                        // Se ejecuta el proceso para guardar los datos del Nivel de Estudios
                        $qry_ne = $this->_conexBD->prepare($sql_ne);
                        $qry_ne->execute(array(":curp" => $this->curp, ":id_nivel_estudios" => $this->id_nivel_estudios, ":eficencia_term" => $this->eficencia_term, ":institucion" => $this->institucion, ":carrera" => $this->carrera, ":especialidad" => $this->especialidad, ":cct" => $this->cct, ":documento" => $this->documento, ":folio" => $this->folio, ":n_cedula" => $this->n_cedula, ":promedio" => $this->promedio));
                        if ($qry_ne) {
                            $this->_conexBD->commit();
                            return true;
                        }
                        else {
                            $this->_conexBD->rollBack();
                            $this->msjError = "ERROR: Al guardar los datos del Nivel de Estudios";
                            return false;
                        }
                    }
                    else {
                        $this->_conexBD->rollBack();
                        $this->msjError = "ERROR: Al guardar los datos del Domicilio";
                        return false;
                    }
                }
                else {
                    $this->_conexBD->rollBack();
                    $this->msjError = "ERROR: Al guardar los datos de la Adscripci�n Laboral";
                    return false;
                }
            }
            else {                
                $this->_conexBD->rollBack();
                $this->msjError = "ERROR: Al guardar los Datos Personales";
                return false;                
            }
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_datos_personales
                   SET nombre=:nombre, a_paterno=:a_paterno, a_materno=:a_materno, fecha_nac=:fecha_nac, genero=:genero, rfc=:rfc, cuip=:cuip, folio_ife=:folio_ife, mat_cartilla=:mat_cartilla, licencia_conducir=:licencia_conducir, pasaporte=:pasaporte, id_estado_civil=:id_estado_civil, id_tipo_sangre=:id_tipo_sangre, id_nacionalidad=:id_nacionalidad, id_entidad=:id_entidad, id_municipio=:id_municipio, lugar_nac=:lugar_nac, id_status=:id_status
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":fecha_nac" => $this->fecha_nac, ":genero" => $this->genero, ":rfc" => $this->rfc, ":cuip" => $this->cuip, ":folio_ife" => $this->folio_ife, ":mat_cartilla" => $this->mat_cartilla, ":licencia_conducir" => $this->licencia_conducir, ":pasaporte" => $this->pasaporte, ":id_estado_civil" => $this->id_estado_civil, ":id_tipo_sangre" => $this->id_tipo_sangre, ":id_nacionalidad" => $this->id_nacionalidad, ":id_entidad" => $this->id_entidad, ":id_municipio" => $this->id_municipio, ":lugar_nac" => $this->lugar_nac, ":id_status" => $this->id_status));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
}


?>