<?php
/**
 *
 */
class AdmcatMfOjosColor
{
    public $id_ojos_color; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $ojos_color; /** @Tipo: varchar(20), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_ojos_color)
    {
        $sql = "SELECT id_ojos_color, ojos_color
                FROM admcat_mf_ojos_color
                WHERE id_ojos_color=:id_ojos_color;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_ojos_color' => $id_ojos_color));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_ojos_color = $data['id_ojos_color'];
            $this->ojos_color = $data['ojos_color'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_ojos_color, a.ojos_color
                FROM admcat_mf_ojos_color a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_ojos_color' => $data['id_ojos_color'],
                               'ojos_color' => $data['ojos_color'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_mf_ojos_color(id_ojos_color, ojos_color)
                VALUES(:id_ojos_color, :ojos_color);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_ojos_color" => $this->id_ojos_color, ":ojos_color" => $this->ojos_color));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_mf_ojos_color
                   SET ojos_color=:ojos_color
                WHERE id_ojos_color=:id_ojos_color;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_ojos_color" => $this->id_ojos_color, ":ojos_color" => $this->ojos_color));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>