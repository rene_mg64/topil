<?php
/**
 *
 */
class AdmtblCambios
{
    public $id_cambio; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_municipio; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_area; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha_reg; /** @Tipo: datetime, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_tipo_cambio; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $folio; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_noti; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $no_oficio; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $no_soporte; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_soporte; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $firmante_soporte; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cargo_firmante; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $capturo; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $curp_permuta; /** @Tipo: varchar(18), @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_ubicacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatTipoCambio; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatAreas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatUbicaciones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        require_once 'admcat_tipo_cambio.class.php';
        require_once 'admcat_municipios.class.php';
        require_once 'admcat_areas.class.php';
        require_once 'admtbl_datos_personales.class.php';
        require_once 'admcat_ubicaciones.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatTipoCambio = new AdmcatTipoCambio();
        $this->AdmcatMunicipios = new AdmcatMunicipios();
        $this->AdmcatAreas = new AdmcatAreas();
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatUbicaciones = new AdmcatUbicaciones();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_cambio)
    {
        $sql = "SELECT id_cambio, curp, id_municipio, id_area, fecha_reg, id_tipo_cambio, folio, fecha_noti, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante, capturo, curp_permuta, id_ubicacion
                FROM admtbl_cambios
                WHERE id_cambio=:id_cambio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_cambio' => $id_cambio));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_cambio = $data['id_cambio'];
            $this->curp = $data['curp'];
            $this->id_municipio = $data['id_municipio'];
            $this->id_area = $data['id_area'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->id_tipo_cambio = $data['id_tipo_cambio'];
            $this->folio = $data['folio'];
            $this->fecha_noti = $data['fecha_noti'];
            $this->no_oficio = $data['no_oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->no_soporte = $data['no_soporte'];
            $this->fecha_soporte = $data['fecha_soporte'];
            $this->firmante_soporte = $data['firmante_soporte'];
            $this->cargo_firmante = $data['cargo_firmante'];
            $this->capturo = $data['capturo'];
            $this->curp_permuta = $data['curp_permuta'];
            $this->id_ubicacion = $data['id_ubicacion'];

            $this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmcatTipoCambio->select($this->id_tipo_cambio);
            $this->AdmcatMunicipios->select($this->id_municipio);
            $this->AdmcatAreas->select($this->id_area);
            $this->AdmtblDatosPersonales->select($this->curp_permuta);
            $this->AdmcatUbicaciones->select($this->id_ubicacion);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_cambio, a.curp, a.id_municipio, a.id_area, a.fecha_reg, a.id_tipo_cambio, a.folio, a.fecha_noti, a.no_oficio, a.fecha_oficio, a.no_soporte, a.fecha_soporte, a.firmante_soporte, a.cargo_firmante, a.capturo, a.curp_permuta, a.id_ubicacion,
                  b.curp, b.nombre, b.a_paterno, b.a_materno, b.fecha_nac, b.genero, b.rfc, b.cuip, b.folio_ife, b.mat_cartilla, b.licencia_conducir, b.pasaporte, b.id_estado_civil, b.id_tipo_sangre, b.id_nacionalidad, b.id_entidad, b.id_municipio, b.lugar_nac, b.id_status,
                  c.id_tipo_cambio, c.tipo_cambio, c.modo,
                  d.id_municipio, d.municipio, d.cabecera, d.id_entidad, d.id_region,
                  e.id_area, e.area, e.titular, e.tel�fono, e.status, e.id_nivel, e.id_depende, e.orden,
                  f.curp, f.nombre, f.a_paterno, f.a_materno, f.fecha_nac, f.genero, f.rfc, f.cuip, f.folio_ife, f.mat_cartilla, f.licencia_conducir, f.pasaporte, f.id_estado_civil, f.id_tipo_sangre, f.id_nacionalidad, f.id_entidad, f.id_municipio, f.lugar_nac, f.id_status,
                  g.id_ubicacion, g.ubicacion, g.status
                FROM admtbl_cambios a 
                 LEFT JOIN admtbl_datos_personales b ON a.curp=b.curp
                 LEFT JOIN admcat_tipo_cambio c ON a.id_tipo_cambio=c.id_tipo_cambio
                 LEFT JOIN admcat_municipios d ON a.id_municipio=d.id_municipio
                 LEFT JOIN admcat_areas e ON a.id_area=e.id_area
                 LEFT JOIN admtbl_datos_personales f ON a.curp_permuta=f.curp
                 LEFT JOIN admcat_ubicaciones g ON a.id_ubicacion=g.id_ubicacion";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_cambio' => $data['id_cambio'],
                               'curp' => $data['curp'],
                               'id_municipio' => $data['id_municipio'],
                               'id_area' => $data['id_area'],
                               'fecha_reg' => $data['fecha_reg'],
                               'id_tipo_cambio' => $data['id_tipo_cambio'],
                               'folio' => $data['folio'],
                               'fecha_noti' => $data['fecha_noti'],
                               'no_oficio' => $data['no_oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'no_soporte' => $data['no_soporte'],
                               'fecha_soporte' => $data['fecha_soporte'],
                               'firmante_soporte' => $data['firmante_soporte'],
                               'cargo_firmante' => $data['cargo_firmante'],
                               'capturo' => $data['capturo'],
                               'curp_permuta' => $data['curp_permuta'],
                               'id_ubicacion' => $data['id_ubicacion'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               'admcat_tipo_cambio_tipo_cambio' => $data['tipo_cambio'],
                               'admcat_tipo_cambio_modo' => $data['modo'],
                               'admcat_municipios_municipio' => $data['municipio'],
                               'admcat_municipios_cabecera' => $data['cabecera'],
                               'admcat_municipios_id_entidad' => $data['id_entidad'],
                               'admcat_municipios_id_region' => $data['id_region'],
                               'admcat_areas_area' => $data['area'],
                               'admcat_areas_titular' => $data['titular'],
                               'admcat_areas_tel�fono' => $data['tel�fono'],
                               'admcat_areas_status' => $data['status'],
                               'admcat_areas_id_nivel' => $data['id_nivel'],
                               'admcat_areas_id_depende' => $data['id_depende'],
                               'admcat_areas_orden' => $data['orden'],
                               'admtbl_datos_personales_curp' => $data['curp'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               'admcat_ubicaciones_ubicacion' => $data['ubicacion'],
                               'admcat_ubicaciones_status' => $data['status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_cambios(curp, id_municipio, id_area, fecha_reg, id_tipo_cambio, folio, fecha_noti, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante, capturo, curp_permuta, id_ubicacion)
                VALUES(:curp, :id_municipio, :id_area, :fecha_reg, :id_tipo_cambio, :folio, :fecha_noti, :no_oficio, :fecha_oficio, :no_soporte, :fecha_soporte, :firmante_soporte, :cargo_firmante, :capturo, :curp_permuta, :id_ubicacion);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":id_municipio" => $this->id_municipio, ":id_area" => $this->id_area, ":fecha_reg" => $this->fecha_reg, ":id_tipo_cambio" => $this->id_tipo_cambio, ":folio" => $this->folio, ":fecha_noti" => $this->fecha_noti, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante, ":capturo" => $this->capturo, ":curp_permuta" => $this->curp_permuta, ":id_ubicacion" => $this->id_ubicacion));
            if ($qry){
                $sql_upd = "UPDATE admtbl_adscripcion
                       SET id_area=:id_area, id_municipio=:id_municipio 
                       WHERE curp=:curp;";
                try {
                    $qry_upd = $this->_conexBD->prepare($sql_upd);
                    $qry_upd->execute(array(":curp" => $this->curp, ":id_area" => $this->id_area, ":id_municipio" => $this->id_municipio ));
                    if ($qry_upd) 
                        return true;
                    else
                        return false;
                } catch(PDOException $e) {
                    $this->msjError = $e->getMessage();
                    return false;
                }
                    //return $this->_conexBD->lastInsertId();
            }
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    /**
     * Funci�n para seleccionar los datos  de la tabla admtbl_cambios para el grid
     * 
     */
    public function selectListTramite($sqlWhere='', $sqlValues=array() )
    {
        $sql = "select id_cambio, fecha_noti, folio, c.id_tipo_cambio, t.tipo_cambio, c.id_area, i.area, no_oficio,fecha_oficio 
                from admtbl_cambios c
                LEFT JOIN admcat_tipo_cambio t ON t.id_tipo_cambio=c.id_tipo_cambio 
                LEFT JOIN admcat_areas i ON c.id_area=i.id_area
                  ";
        if (!empty($sqlWhere))
            $sql .= " WHERE  $sqlWhere";                
        $sql .= ";";        
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);                
            } else {
                $qry->execute();                
            }
            //echo $sql;
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_cambio' => $data['id_cambio'],
                               'fecha_noti' => $data['fecha_noti'],
                               'folio' => $data['folio'],
                               'id_tipo_cambio' => $data['id_tipo_cambio'],
                               'tipo_cambio' => $data['tipo_cambio'],
                               'area' => $data['area'],
                               'no_oficio' => $data['no_oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],                              
                               );
            }
            //echo count( $datos );
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    } 

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_cambios
                   SET curp=:curp, id_municipio=:id_municipio, id_area=:id_area, fecha_reg=:fecha_reg, id_tipo_cambio=:id_tipo_cambio, folio=:folio, fecha_noti=:fecha_noti, no_oficio=:no_oficio, fecha_oficio=:fecha_oficio, no_soporte=:no_soporte, fecha_soporte=:fecha_soporte, firmante_soporte=:firmante_soporte, cargo_firmante=:cargo_firmante, capturo=:capturo, curp_permuta=:curp_permuta, id_ubicacion=:id_ubicacion
                WHERE id_cambio=:id_cambio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_cambio" => $this->id_cambio, ":curp" => $this->curp, ":id_municipio" => $this->id_municipio, ":id_area" => $this->id_area, ":fecha_reg" => $this->fecha_reg, ":id_tipo_cambio" => $this->id_tipo_cambio, ":folio" => $this->folio, ":fecha_noti" => $this->fecha_noti, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante, ":capturo" => $this->capturo, ":curp_permuta" => $this->curp_permuta, ":id_ubicacion" => $this->id_ubicacion));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>