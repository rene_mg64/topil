<?php
/**
 *
 */
class OpetblMidIncidentesVehiculos
{
    public $id_vehiculo; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_folio_incidente; /** @Tipo: int(10) unsigned zerofill, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_vehiculo_rol; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_vehiculo_categoria; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_vehiculo_marca; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $tipo; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $modelo; /** @Tipo: varchar(4), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $color; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $numero_serie; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $numero_motor; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $numero_placas; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $es_extranjero; /** @Tipo: tinyint(4), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $origen_placas; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_vehiculo_tipo_servicio; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_vehiculo_empresa; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $tipo_carga; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $observaciones; /** @Tipo: varchar(250), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si ?ste ocurre
    private $_conexBD; // objeto de conexi?n a la base de datos
    public $OpetblMidIncidentes; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidVehiculosMarcas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidVehiculosRoles; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidVehiculosCategorias; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidVehiculosEmpresas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidVehiculosTiposServicios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opetbl_mid_incidentes.class.php';
        require_once 'opecat_mid_vehiculos_marcas.class.php';
        require_once 'opecat_mid_vehiculos_roles.class.php';
        require_once 'opecat_mid_vehiculos_categorias.class.php';
        require_once 'opecat_mid_vehiculos_empresas.class.php';
        require_once 'opecat_mid_vehiculos_tipos_servicios.class.php';
        $this->OpetblMidIncidentes = new OpetblMidIncidentes();
        $this->OpecatMidVehiculosMarcas = new OpecatMidVehiculosMarcas();
        $this->OpecatMidVehiculosRoles = new OpecatMidVehiculosRoles();
        $this->OpecatMidVehiculosCategorias = new OpecatMidVehiculosCategorias();
        $this->OpecatMidVehiculosEmpresas = new OpecatMidVehiculosEmpresas();
        $this->OpecatMidVehiculosTiposServicios = new OpecatMidVehiculosTiposServicios();
        
    }
    
    /**
    * Funci�n para obtener el ultimo registro
    * @param  campos que conforman la clave primaria de la tabla
    * @return boolean true, si la consulta se realiz� con �xito
    */
    public function getIdUltimoVehiculo(){        
        $sql = "SELECT MAX(id_vehiculo) as id
                  FROM opetbl_mid_incidentes_vehiculos;";
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["id"];
            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
    * Funci�n para obtener un registro espec�fico de la tabla
    * @param  campos que conforman la clave primaria de la tabla
    * @return boolean true, si la consulta se realiz� con �xito
    */
    public function getTotalVehiculos( $id_folio_incidente ){        
        $sql = "SELECT count(*) as id
                  FROM opetbl_mid_incidentes_vehiculos 
                  WHERE id_folio_incidente = " . $id_folio_incidente;
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["id"];
            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci?n para obtener un registro espec?fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz? con ?xito
     */
    public function select($id_vehiculo)
    {
        $sql = "SELECT id_vehiculo, id_folio_incidente, id_vehiculo_rol, id_vehiculo_categoria, id_vehiculo_marca, tipo, modelo, color, numero_serie, numero_motor, numero_placas, es_extranjero, origen_placas, id_vehiculo_tipo_servicio, id_vehiculo_empresa, tipo_carga, observaciones
                FROM opetbl_mid_incidentes_vehiculos
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_vehiculo' => $id_vehiculo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_vehiculo = $data['id_vehiculo'];
            $this->id_folio_incidente = $data['id_folio_incidente'];
            $this->id_vehiculo_rol = $data['id_vehiculo_rol'];
            $this->id_vehiculo_categoria = $data['id_vehiculo_categoria'];
            $this->id_vehiculo_marca = $data['id_vehiculo_marca'];
            $this->tipo = $data['tipo'];
            $this->modelo = $data['modelo'];
            $this->color = $data['color'];
            $this->numero_serie = $data['numero_serie'];
            $this->numero_motor = $data['numero_motor'];
            $this->numero_placas = $data['numero_placas'];
            $this->es_extranjero = $data['es_extranjero'];
            $this->origen_placas = $data['origen_placas'];
            $this->id_vehiculo_tipo_servicio = $data['id_vehiculo_tipo_servicio'];
            $this->id_vehiculo_empresa = $data['id_vehiculo_empresa'];
            $this->tipo_carga = $data['tipo_carga'];
            $this->observaciones = $data['observaciones'];

            $this->OpetblMidIncidentes->select($this->id_folio_incidente);
            $this->OpecatMidVehiculosMarcas->select($this->id_vehiculo_marca);
            $this->OpecatMidVehiculosRoles->select($this->id_vehiculo_rol);
            $this->OpecatMidVehiculosCategorias->select($this->id_vehiculo_categoria);
            $this->OpecatMidVehiculosEmpresas->select($this->id_vehiculo_empresa);
            $this->OpecatMidVehiculosTiposServicios->select($this->id_vehiculo_tipo_servicio);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci?n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_vehiculo, a.id_folio_incidente, a.id_vehiculo_rol, a.id_vehiculo_categoria, a.id_vehiculo_marca, a.tipo, a.modelo, a.color, a.numero_serie, a.numero_motor, a.numero_placas, a.es_extranjero, a.origen_placas, a.id_vehiculo_tipo_servicio, a.id_vehiculo_empresa, a.tipo_carga, a.observaciones,
                  b.id_folio_incidente, b.prioridad, b.fecha_hora_captura, b.fecha_incidente, b.dia, b.mes, b.annio, b.hora_incidente, b.id_estado, b.id_region, b.id_cuartel, b.id_municipio, b.direccion, b.localidad, b.colonia, b.calle, b.latitud, b.longitud, b.id_asunto, b.id_fuente, b.hechos_html, b.hechos_text, b.ip_usuario, b.id_usuario,
                  c.id_vehiculo_marca, c.vehiculo_marca,
                  d.id_vehiculo_rol, d.vehiculo_rol,
                  e.id_vehiculo_categoria, e.vehiculo_categoria,
                  f.id_vehiculo_empresa, f.vehiculo_empresa,
                  g.id_vehiculo_tipo_servicio, g.vehiculo_tipo_servicio
                FROM opetbl_mid_incidentes_vehiculos a
                 LEFT JOIN opetbl_mid_incidentes b ON a.id_folio_incidente=b.id_folio_incidente
                 LEFT JOIN opecat_mid_vehiculos_marcas c ON a.id_vehiculo_marca=c.id_vehiculo_marca
                 LEFT JOIN opecat_mid_vehiculos_roles d ON a.id_vehiculo_rol=d.id_vehiculo_rol
                 LEFT JOIN opecat_mid_vehiculos_categorias e ON a.id_vehiculo_categoria=e.id_vehiculo_categoria
                 LEFT JOIN opecat_mid_vehiculos_empresas f ON a.id_vehiculo_empresa=f.id_vehiculo_empresa
                 LEFT JOIN opecat_mid_vehiculos_tipos_servicios g ON a.id_vehiculo_tipo_servicio=g.id_vehiculo_tipo_servicio";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_vehiculo' => $data['id_vehiculo'],
                               'id_folio_incidente' => $data['id_folio_incidente'],
                               'id_vehiculo_rol' => $data['id_vehiculo_rol'],
                               'id_vehiculo_categoria' => $data['id_vehiculo_categoria'],
                               'id_vehiculo_marca' => $data['id_vehiculo_marca'],
                               'tipo' => $data['tipo'],
                               'modelo' => $data['modelo'],
                               'color' => $data['color'],
                               'numero_serie' => $data['numero_serie'],
                               'numero_motor' => $data['numero_motor'],
                               'numero_placas' => $data['numero_placas'],
                               'es_extranjero' => $data['es_extranjero'],
                               'origen_placas' => $data['origen_placas'],
                               'id_vehiculo_tipo_servicio' => $data['id_vehiculo_tipo_servicio'],
                               'id_vehiculo_empresa' => $data['id_vehiculo_empresa'],
                               'tipo_carga' => $data['tipo_carga'],
                               'observaciones' => $data['observaciones'],
                               'opetbl_mid_incidentes_prioridad' => $data['prioridad'],
                               'opetbl_mid_incidentes_fecha_hora_captura' => $data['fecha_hora_captura'],
                               'opetbl_mid_incidentes_fecha_incidente' => $data['fecha_incidente'],
                               'opetbl_mid_incidentes_dia' => $data['dia'],
                               'opetbl_mid_incidentes_mes' => $data['mes'],
                               'opetbl_mid_incidentes_annio' => $data['annio'],
                               'opetbl_mid_incidentes_hora_incidente' => $data['hora_incidente'],
                               'opetbl_mid_incidentes_id_estado' => $data['id_estado'],
                               'opetbl_mid_incidentes_id_region' => $data['id_region'],
                               'opetbl_mid_incidentes_id_cuartel' => $data['id_cuartel'],
                               'opetbl_mid_incidentes_id_municipio' => $data['id_municipio'],
                               'opetbl_mid_incidentes_direccion' => $data['direccion'],
                               'opetbl_mid_incidentes_localidad' => $data['localidad'],
                               'opetbl_mid_incidentes_colonia' => $data['colonia'],
                               'opetbl_mid_incidentes_calle' => $data['calle'],
                               'opetbl_mid_incidentes_latitud' => $data['latitud'],
                               'opetbl_mid_incidentes_longitud' => $data['longitud'],
                               'opetbl_mid_incidentes_id_asunto' => $data['id_asunto'],
                               'opetbl_mid_incidentes_id_fuente' => $data['id_fuente'],
                               'opetbl_mid_incidentes_hechos_html' => $data['hechos_html'],
                               'opetbl_mid_incidentes_hechos_text' => $data['hechos_text'],
                               'opetbl_mid_incidentes_ip_usuario' => $data['ip_usuario'],
                               'opetbl_mid_incidentes_id_usuario' => $data['id_usuario'],
                               'opecat_mid_vehiculos_marcas_vehiculo_marca' => $data['vehiculo_marca'],
                               'opecat_mid_vehiculos_roles_vehiculo_rol' => $data['vehiculo_rol'],
                               'opecat_mid_vehiculos_categorias_vehiculo_categoria' => $data['vehiculo_categoria'],
                               'opecat_mid_vehiculos_empresas_vehiculo_empresa' => $data['vehiculo_empresa'],
                               'opecat_mid_vehiculos_tipos_servicios_vehiculo_tipo_servicio' => $data['vehiculo_tipo_servicio'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }



  /**
     * Funci?n que controla la obtenci?n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }

        return $datos;
    }

 /**
     * Funci?n para obtener los registros m?nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT v.id_vehiculo,v.id_folio_incidente,vr.vehiculo_rol,vc.vehiculo_categoria,vm.vehiculo_marca,v.tipo,v.modelo,v.color,
                v.numero_serie,v.numero_motor,v.numero_placas
                FROM opetbl_mid_incidentes_vehiculos v
                LEFT JOIN opecat_mid_vehiculos_roles vr ON v.id_vehiculo_rol=vr.id_vehiculo_rol
                LEFT JOIN opecat_mid_vehiculos_categorias vc ON v.id_vehiculo_categoria=vc.id_vehiculo_categoria
                LEFT JOIN opecat_mid_vehiculos_marcas vm ON v.id_vehiculo_marca=vm.id_vehiculo_marca";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                                'id_vehiculo' =>  $data['id_vehiculo'],
                                'id_folio_incidente' =>  $data['id_folio_incidente'],
                                'vehiculo_rol' =>  $data['vehiculo_rol'],
                                'vehiculo_categoria' =>  $data['vehiculo_categoria'],
                                'vehiculo_marca' =>  $data['vehiculo_marca'],
                                'tipo' =>  $data['tipo'],
                                'modelo' =>  $data['modelo'],
                                'color' =>  $data['color'],
                                'numero_serie' =>  $data['numero_serie'],
                                'numero_motor' =>  $data['numero_motor'],
                                'numero_placas' =>  $data['numero_placas'],
                              );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci?n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT count(*)
                FROM opetbl_mid_incidentes_vehiculos v
                LEFT JOIN opecat_mid_vehiculos_roles vr ON v.id_vehiculo_rol=vr.id_vehiculo_rol
                LEFT JOIN opecat_mid_vehiculos_categorias vc ON v.id_vehiculo_categoria=vc.id_vehiculo_categoria
                LEFT JOIN opecat_mid_vehiculos_marcas vm ON v.id_vehiculo_marca=vm.id_vehiculo_marca";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }

        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $total_reg = $qry->fetchColumn();

            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }






    /**
     * Funci?n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el ?ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mid_incidentes_vehiculos(id_folio_incidente, id_vehiculo_rol, 
                    id_vehiculo_categoria, id_vehiculo_marca, tipo, modelo, color, numero_serie, numero_motor, 
                    numero_placas, es_extranjero, origen_placas, id_vehiculo_tipo_servicio, id_vehiculo_empresa, 
                    tipo_carga, observaciones)
                VALUES(:id_folio_incidente, :id_vehiculo_rol, :id_vehiculo_categoria, :id_vehiculo_marca, 
                    :tipo, :modelo, :color, :numero_serie, :numero_motor, :numero_placas, :es_extranjero, :origen_placas, 
                    :id_vehiculo_tipo_servicio, :id_vehiculo_empresa, :tipo_carga, :observaciones);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_folio_incidente" => $this->id_folio_incidente, 
                        ":id_vehiculo_rol" => $this->id_vehiculo_rol, 
                        ":id_vehiculo_categoria" => $this->id_vehiculo_categoria, 
                        ":id_vehiculo_marca" => $this->id_vehiculo_marca, ":tipo" => $this->tipo, 
                        ":modelo" => $this->modelo, ":color" => $this->color, ":numero_serie" => $this->numero_serie, 
                        ":numero_motor" => $this->numero_motor, ":numero_placas" => $this->numero_placas, 
                        ":es_extranjero" => $this->es_extranjero, ":origen_placas" => $this->origen_placas, 
                        ":id_vehiculo_tipo_servicio" => $this->id_vehiculo_tipo_servicio, 
                        ":id_vehiculo_empresa" => $this->id_vehiculo_empresa, ":tipo_carga" => $this->tipo_carga, 
                        ":observaciones" => $this->observaciones));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci?n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mid_incidentes_vehiculos
                   SET id_folio_incidente=:id_folio_incidente, id_vehiculo_rol=:id_vehiculo_rol, id_vehiculo_categoria=:id_vehiculo_categoria, id_vehiculo_marca=:id_vehiculo_marca, tipo=:tipo, modelo=:modelo, color=:color, numero_serie=:numero_serie, numero_motor=:numero_motor, numero_placas=:numero_placas, es_extranjero=:es_extranjero, origen_placas=:origen_placas, id_vehiculo_tipo_servicio=:id_vehiculo_tipo_servicio, id_vehiculo_empresa=:id_vehiculo_empresa, tipo_carga=:tipo_carga, observaciones=:observaciones
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":id_folio_incidente" => $this->id_folio_incidente, ":id_vehiculo_rol" => $this->id_vehiculo_rol, ":id_vehiculo_categoria" => $this->id_vehiculo_categoria, ":id_vehiculo_marca" => $this->id_vehiculo_marca, ":tipo" => $this->tipo, ":modelo" => $this->modelo, ":color" => $this->color, ":numero_serie" => $this->numero_serie, ":numero_motor" => $this->numero_motor, ":numero_placas" => $this->numero_placas, ":es_extranjero" => $this->es_extranjero, ":origen_placas" => $this->origen_placas, ":id_vehiculo_tipo_servicio" => $this->id_vehiculo_tipo_servicio, ":id_vehiculo_empresa" => $this->id_vehiculo_empresa, ":tipo_carga" => $this->tipo_carga, ":observaciones" => $this->observaciones));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>