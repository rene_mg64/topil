<?php
/**
 *
 */
class AdmtblDoctosIdent
{
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $foto_frente; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $foto_der; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $foto_izq; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $firma; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $huella_pulg_der; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $huella_pulg_izq; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT curp, foto_frente, foto_der, foto_izq, firma, huella_pulg_der, huella_pulg_izq
                FROM admtbl_doctos_ident
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->foto_frente = $data['foto_frente'];
            $this->foto_der = $data['foto_der'];
            $this->foto_izq = $data['foto_izq'];
            $this->firma = $data['firma'];
            $this->huella_pulg_der = $data['huella_pulg_der'];
            $this->huella_pulg_izq = $data['huella_pulg_izq'];

            $this->AdmtblDatosPersonales->select($this->curp);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
       
    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_doctos_ident(curp, foto_frente, foto_der, foto_izq, firma, huella_pulg_der, huella_pulg_izq)
                VALUES(:curp, :foto_frente, :foto_der, :foto_izq, :firma, :huella_pulg_der, :huella_pulg_izq);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":foto_frente" => $this->foto_frente, ":foto_der" => $this->foto_der, ":foto_izq" => $this->foto_izq, ":firma" => $this->firma, ":huella_pulg_der" => $this->huella_pulg_der, ":huella_pulg_izq" => $this->huella_pulg_izq));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_doctos_ident SET ";
        $params = array();
        $params[":curp"] = $this->curp;
        $fields = '';
        if( !empty($this->foto_frente) ){
            $fields = "foto_frente=:foto_frente ";
            $params[":foto_frente"] = $this->foto_frente;
        }
        if( !empty($this->foto_der) ){
            $fields .= ( !empty($fields) ) ? "," : "";
            $fields .= "foto_der=:foto_der ";
            $params[":foto_der"] = $this->foto_der;
        }
        if( !empty($this->foto_izq) ){
            $fields .= ( !empty($fields) ) ? "," : "";
            $fields .= "foto_izq=:foto_izq ";
            $params[":foto_izq"] = $this->foto_izq;
        }
        if( !empty($this->firma) ){ 
            $fields .= ( !empty($fields) ) ? "," : "";
            $fields .= "firma=:firma ";
            $params[":firma"] = $this->firma;
        } 
        if( !empty($this->huella_pulg_der) ){
            $fields .= ( !empty($fields) ) ? "," : "";
            $fields .= "huella_pulg_der=:huella_pulg_der ";
            $params[":huella_pulg_der"] = $this->huella_pulg_der;
        }
        if( !empty($this->huella_pulg_izq) ){ 
            $fields .= ( !empty($fields) ) ? "," : "";
            $fields .= "huella_pulg_izq=:huella_pulg_izq ";
            $params[":huella_pulg_izq"] = $this->huella_pulg_izq;
        }                
        $sql .= $fields . "WHERE curp=:curp;";
    
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute($params);
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    public function chkExist($curp){
        $sql = "SELECT COUNT(*) 
                FROM admtbl_doctos_ident
                WHERE curp=:curp;";
        $qry = $this->_conexBD->prepare($sql);
        $qry->execute(array(':curp' => $curp));
        $qry->setFetchMode(PDO::FETCH_ASSOC);
        if( $qry->fetchColumn() == 1 )
            return true;
        else
            return false;
    }
    
    public function delete()
    {

    }
}


?>