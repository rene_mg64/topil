<?php
/**
 *
 */
class AdmtblDatosNomina
{
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $fecha_alta; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_tipo_plaza; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $nueva_creacion; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $clave_categoria; /** @Tipo: varchar(15), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $sueldo; /** @Tipo: decimal(10,2) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 0.00 */
    public $oficina_pagadora; /** @Tipo: varchar(15), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_banco; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $cuenta; /** @Tipo: varchar(25), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_plaza; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $num_empleado; /** @Tipo: varchar(6), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatBanco; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    //public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatTipoPlaza; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblPlantillaPlazas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_banco.class.php';
        //require_once 'admtbl_datos_personales.class.php';
        require_once 'admcat_tipo_plaza.class.php';
        require_once 'admtbl_plantilla_plazas.class.php';
        $this->AdmcatBanco = new AdmcatBanco();
        //$this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatTipoPlaza = new AdmcatTipoPlaza();
        $this->AdmtblPlantillaPlazas = new AdmtblPlantillaPlazas();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT curp, fecha_alta, id_tipo_plaza, nueva_creacion, clave_categoria, sueldo, oficina_pagadora, id_banco, cuenta, id_plaza, num_empleado
                FROM admtbl_datos_nomina
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->fecha_alta = $data['fecha_alta'];
            $this->id_tipo_plaza = $data['id_tipo_plaza'];
            $this->nueva_creacion = $data['nueva_creacion'];
            $this->clave_categoria = $data['clave_categoria'];
            $this->sueldo = $data['sueldo'];
            $this->oficina_pagadora = $data['oficina_pagadora'];
            $this->id_banco = $data['id_banco'];
            $this->cuenta = $data['cuenta'];
            $this->id_plaza = $data['id_plaza'];
            $this->num_empleado = $data['num_empleado'];

            $this->AdmcatBanco->select($this->id_banco);
            //$this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmcatTipoPlaza->select($this->id_tipo_plaza);
            $this->AdmtblPlantillaPlazas->select($this->id_plaza);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.fecha_alta, a.id_tipo_plaza, a.nueva_creacion, a.clave_categoria, a.sueldo, a.oficina_pagadora, a.id_banco, a.cuenta, a.id_plaza, a.num_empleado,
                  b.banco,
                  c.tipo_plaza,
                  d.id_plaza, d.folio, d.id_categoria, d.id_area, d.fecha_creacion, d.status, d.observaciones
                FROM admtbl_datos_nomina a 
                 LEFT JOIN admcat_banco b ON a.id_banco=b.id_banco
                 LEFT JOIN admcat_tipo_plaza c ON a.id_tipo_plaza=c.id_tipo_plaza
                 LEFT JOIN admtbl_plantilla_plazas d ON a.id_plaza=d.id_plaza";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'fecha_alta' => $data['fecha_alta'],
                               'id_tipo_plaza' => $data['id_tipo_plaza'],
                               'nueva_creacion' => $data['nueva_creacion'],
                               'clave_categoria' => $data['clave_categoria'],
                               'sueldo' => $data['sueldo'],
                               'oficina_pagadora' => $data['oficina_pagadora'],
                               'id_banco' => $data['id_banco'],
                               'cuenta' => $data['cuenta'],
                               'id_plaza' => $data['id_plaza'],
                               'num_empleado' => $data['num_empleado'],
                               'admcat_banco_banco' => $data['banco'],
                               'tipo_plaza' => $data['tipo_plaza'],
                               'folio' => $data['folio'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_datos_nomina(curp, fecha_alta, id_tipo_plaza, nueva_creacion, clave_categoria, sueldo, oficina_pagadora, id_banco, cuenta, id_plaza, num_empleado)
                VALUES(:curp, :fecha_alta, :id_tipo_plaza, :nueva_creacion, :clave_categoria, :sueldo, :oficina_pagadora, :id_banco, :cuenta, :id_plaza, :num_empleado);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":fecha_alta" => $this->fecha_alta, ":id_tipo_plaza" => $this->id_tipo_plaza, ":nueva_creacion" => $this->nueva_creacion, ":clave_categoria" => $this->clave_categoria, ":sueldo" => $this->sueldo, ":oficina_pagadora" => $this->oficina_pagadora, ":id_banco" => $this->id_banco, ":cuenta" => $this->cuenta, ":id_plaza" => $this->id_plaza, ":num_empleado" => $this->num_empleado));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_datos_nomina
                   SET fecha_alta=:fecha_alta, id_tipo_plaza=:id_tipo_plaza, nueva_creacion=:nueva_creacion, clave_categoria=:clave_categoria, sueldo=:sueldo, oficina_pagadora=:oficina_pagadora, id_banco=:id_banco, cuenta=:cuenta, id_plaza=:id_plaza, num_empleado=:num_empleado
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":fecha_alta" => $this->fecha_alta, ":id_tipo_plaza" => $this->id_tipo_plaza, ":nueva_creacion" => $this->nueva_creacion, ":clave_categoria" => $this->clave_categoria, ":sueldo" => $this->sueldo, ":oficina_pagadora" => $this->oficina_pagadora, ":id_banco" => $this->id_banco, ":cuenta" => $this->cuenta, ":id_plaza" => $this->id_plaza, ":num_empleado" => $this->num_empleado));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>