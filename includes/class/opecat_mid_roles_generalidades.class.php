<?php
/**
 *
 */
class OpecatMidRolesGeneralidades
{
    public $id_rol_generalidad; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $rol_generalidad; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos    

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();        
    }
    
    /**
     * Funci�n para mostrar la lista de .... dentro de un combobox.
     * @param int $id, id del .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Roles_Generalidades($id_rol_generalidad=0){
        $aryDatos = $this->selectAll('','id_rol_generalidad Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id_rol_generalidad == $datos["id_rol_generalidad"] )
                $html .= '<option value="'.$datos["id_rol_generalidad"].'" selected>'.$datos["rol_generalidad"].'</option>';
            else
                $html .= '<option value="'.$datos["id_rol_generalidad"].'" >'.$datos["rol_generalidad"].'</option>';
        }
        return $html;
    }


    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_rol_generalidad)
    {
        $sql = "SELECT id_rol_generalidad, rol_generalidad
                FROM opecat_mid_roles_generalidades
                WHERE id_rol_generalidad=:id_rol_generalidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_rol_generalidad' => $id_rol_generalidad));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_rol_generalidad = $data['id_rol_generalidad'];
            $this->rol_generalidad = $data['rol_generalidad'];
            
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_rol_generalidad, a.rol_generalidad                  
                FROM opecat_mid_roles_generalidades a";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_rol_generalidad' => $data['id_rol_generalidad'],
                               'rol_generalidad' => $data['rol_generalidad'],                               
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_roles_generalidades(id_rol_generalidad, rol_generalidad)
                VALUES(:id_rol_generalidad, :rol_generalidad);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_rol_generalidad" => $this->id_rol_generalidad, ":rol_generalidad" => $this->rol_generalidad));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_roles_generalidades
                   SET rol_generalidad=:rol_generalidad
                WHERE id_rol_generalidad=:id_rol_generalidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_rol_generalidad" => $this->id_rol_generalidad, ":rol_generalidad" => $this->rol_generalidad));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>