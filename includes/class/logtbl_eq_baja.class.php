<?php
/**
 *
 */
class LogtblEqBaja
{
    public $id_registro; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_equipamento; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $oficio; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_motivo; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha_baja; /** @Tipo: timestamp, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_usuario; /** @Tipo: smallint(5), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogcatEqMotivoBaja; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogtblEquipamento; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_eq_motivo_baja.class.php';
        require_once 'logtbl_equipamento.class.php';
        $this->LogcatEqMotivoBaja = new LogcatEqMotivoBaja();
        $this->LogtblEquipamento = new LogtblEquipamento();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_registro)
    {
        $sql = "SELECT id_registro, id_equipamento, oficio, fecha_oficio, id_motivo, fecha_baja, id_usuario
                FROM logtbl_eq_baja
                WHERE id_registro=:id_registro;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_registro' => $id_registro));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_registro = $data['id_registro'];
            $this->id_equipamento = $data['id_equipamento'];
            $this->oficio = $data['oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->id_motivo = $data['id_motivo'];
            $this->fecha_baja = $data['fecha_baja'];
            $this->id_usuario = $data['id_usuario'];

            $this->LogcatEqMotivoBaja->select($this->id_motivo);
            $this->LogtblEquipamento->select($this->id_equipamento);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_registro, a.id_equipamento, a.oficio, a.fecha_oficio, a.id_motivo, a.fecha_baja, a.id_usuario,
                  b.id_motivo, b.motivo, b.xstat,
                  c.id_equipamento, c.id_marca, c.id_tipo, c.id_entidad, c.id_dependencia, c.inventario, c.serie, c.modelo, c.id_estatus
                FROM logtbl_eq_baja a 
                 LEFT JOIN logcat_eq_motivo_baja b ON a.id_motivo=b.id_motivo
                 LEFT JOIN logtbl_equipamento c ON a.id_equipamento=c.id_equipamento";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_registro' => $data['id_registro'],
                               'id_equipamento' => $data['id_equipamento'],
                               'oficio' => $data['oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'id_motivo' => $data['id_motivo'],
                               'fecha_baja' => $data['fecha_baja'],
                               'id_usuario' => $data['id_usuario'],
                               'logcat_eq_motivo_baja_motivo' => $data['motivo'],
                               'logcat_eq_motivo_baja_xstat' => $data['xstat'],
                               'logtbl_equipamento_id_marca' => $data['id_marca'],
                               'logtbl_equipamento_id_tipo' => $data['id_tipo'],
                               'logtbl_equipamento_id_entidad' => $data['id_entidad'],
                               'logtbl_equipamento_id_dependencia' => $data['id_dependencia'],
                               'logtbl_equipamento_inventario' => $data['inventario'],
                               'logtbl_equipamento_serie' => $data['serie'],
                               'logtbl_equipamento_modelo' => $data['modelo'],
                               'logtbl_equipamento_id_estatus' => $data['id_estatus'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlValues, cadena que contiene el parametro de busqueda.
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos     
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectLstBajas($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "select inventario, serie, marca, modelo, tipo, fecha_baja from vieweqlstbajas ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";        
        $sql .= ";";        
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);                
            } else {
                $qry->execute();                
            }
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_equipamento' => $data['id_equipamento'],
                               'inventario' => $data['inventario'],
                               'serie' => $data['serie'],
                               'marca' => $data['marca'],
                               'modelo' => $data['modelo'],
                               'tipo' => $data['tipo'],
                               'fecha_baja' => $data['fecha_baja'],                              
                               );
            }
            //echo count( $datos );
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_eq_baja(id_equipamento, oficio, fecha_oficio, id_motivo, id_usuario)
                VALUES(:id_equipamento, :oficio, :fecha_oficio, :id_motivo, :id_usuario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_equipamento" => $this->id_equipamento, ":oficio" => $this->oficio, ":fecha_oficio" => $this->fecha_oficio, ":id_motivo" => $this->id_motivo, ":id_usuario" => $this->id_usuario));
            if ($qry)
                return $this->_conexBD->lastInsertId();
                //return "nada";
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update_baja()
    {
        $sql = "UPDATE logtbl_equipamento
                   SET id_estatus=:id_estatus
                WHERE id_equipamento=:id_equipamento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_equipamento" => $this->id_equipamento, ":id_estatus" => 2));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_eq_baja
                   SET id_equipamento=:id_equipamento, oficio=:oficio, fecha_oficio=:fecha_oficio, id_motivo=:id_motivo, fecha_baja=:fecha_baja, id_usuario=:id_usuario
                WHERE id_registro=:id_registro;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_registro" => $this->id_registro, ":id_equipamento" => $this->id_equipamento, ":oficio" => $this->oficio, ":fecha_oficio" => $this->fecha_oficio, ":id_motivo" => $this->id_motivo, ":fecha_baja" => $this->fecha_baja, ":id_usuario" => $this->id_usuario));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>