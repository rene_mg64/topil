<?php
/**
 *
 */
class AdmcatArchivoDoctos
{
    public $id_documento; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $documento; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $status; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de documentos dentro de un combobox.
     * @param int $id, id del documento seleccionada por deafult     
     * @return array html(options)
     */
    public function shwArchivoDoctos($id=0){
        $aryDatos = $this->selectAll('', 'documento Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_documento"] )
                $html .= '<option value="'.$datos["id_documento"].'" selected>'.$datos["documento"].'</option>';
            else
                $html .= '<option value="'.$datos["id_documento"].'" >'.$datos["documento"].'</option>';
        }
        return $html;
    }
    
    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_documento)
    {
        $sql = "SELECT id_documento, documento, status
                FROM admcat_archivo_doctos
                WHERE id_documento=:id_documento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_documento' => $id_documento));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_documento = $data['id_documento'];
            $this->documento = $data['documento'];
            $this->status = $data['status'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_documento, a.documento, a.status
                FROM admcat_archivo_doctos a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_documento' => $data['id_documento'],
                               'documento' => $data['documento'],
                               'status' => $data['status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_archivo_doctos(id_documento, documento, status)
                VALUES(:id_documento, :documento, :status);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_documento" => $this->id_documento, ":documento" => $this->documento, ":status" => $this->status));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_archivo_doctos
                   SET documento=:documento, status=:status
                WHERE id_documento=:id_documento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_documento" => $this->id_documento, ":documento" => $this->documento, ":status" => $this->status));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>