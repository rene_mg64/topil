<?php
/**
 *
 */
class AdmtblEstimulos
{
    public $id_estimulo; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $descripcion; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_estimulo)
    {
        $sql = "SELECT id_estimulo, curp, descripcion, fecha
                FROM admtbl_estimulos
                WHERE id_estimulo=:id_estimulo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_estimulo' => $id_estimulo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_estimulo = $data['id_estimulo'];
            $this->curp = $data['curp'];
            $this->descripcion = $data['descripcion'];
            $this->fecha = $data['fecha'];

            $this->AdmtblDatosPersonales->select($this->curp);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_estimulo, a.curp, a.descripcion, a.fecha
                FROM admtbl_estimulos a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_estimulo' => $data['id_estimulo'],
                               'curp' => $data['curp'],
                               'descripcion' => $data['descripcion'],
                               'fecha' => $data['fecha'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_estimulos(id_estimulo, curp, descripcion, fecha)
                VALUES(:id_estimulo, :curp, :descripcion, :fecha);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_estimulo" => $this->id_estimulo, ":curp" => $this->curp, ":descripcion" => $this->descripcion, ":fecha" => $this->fecha));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_estimulos
                   SET curp=:curp, descripcion=:descripcion, fecha=:fecha
                WHERE id_estimulo=:id_estimulo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_estimulo" => $this->id_estimulo, ":curp" => $this->curp, ":descripcion" => $this->descripcion, ":fecha" => $this->fecha));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>