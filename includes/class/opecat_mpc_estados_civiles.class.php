<?php
/**
 *
 */
class OpecatMpcEstadosCiviles
{
    public $id_estado_civil; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $estado_civil; /** @Tipo: varchar(40), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de estados civil dentro de un combobox.
     * @param int $id, id del estado civil seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_EstadosCiviles($id=0){
        $aryDatos = $this->selectAll('id_estado_civil Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_estado_civil"] )
                $html .= '<option value="'.$datos["id_estado_civil"].'" selected>'.$datos["estado_civil"].'</option>';
            else
                $html .= '<option value="'.$datos["id_estado_civil"].'" >'.$datos["estado_civil"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_estado_civil)
    {
        $sql = "SELECT id_estado_civil, estado_civil
                FROM opecat_mpc_estados_civiles
                WHERE id_estado_civil=:id_estado_civil;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_estado_civil' => $id_estado_civil));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_estado_civil = $data['id_estado_civil'];
            $this->estado_civil = $data['estado_civil'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_estado_civil, a.estado_civil
                FROM opecat_mpc_estados_civiles a ";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_estado_civil' => $data['id_estado_civil'],
                               'estado_civil' => $data['estado_civil'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mpc_estados_civiles(id_estado_civil, estado_civil)
                VALUES(:id_estado_civil, :estado_civil);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_estado_civil" => $this->id_estado_civil, ":estado_civil" => $this->estado_civil));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mpc_estados_civiles
                   SET estado_civil=:estado_civil
                WHERE id_estado_civil=:id_estado_civil;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_estado_civil" => $this->id_estado_civil, ":estado_civil" => $this->estado_civil));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>