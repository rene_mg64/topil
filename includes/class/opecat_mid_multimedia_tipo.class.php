<?php
/**
 *
 */
class OpecatMidMultimediaTipo
{
    public $id_multimedia_tipo; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $multimedia_tipo; /** @Tipo: varchar(60), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */
    public $icono; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: , @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();
    }

     /**
     * Funci�n para mostrar la lista de .... dentro de un combobox.
     * @param int $id, id del .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Multimedia_Tipo($id=0)
    {
        $aryDatos = $this->selectAll('id_multimedia_tipo Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_multimedia_tipo"] )
                $html .= '<option value="'.$datos["id_multimedia_tipo"].'" selected>'.$datos["multimedia_tipo"].'</option>';
            else
                $html .= '<option value="'.$datos["id_multimedia_tipo"].'" >'.$datos["multimedia_tipo"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_multimedia_tipo)
    {
        $sql = "SELECT id_multimedia_tipo, multimedia_tipo, icono
                FROM opecat_mid_multimedia_tipo
                WHERE id_multimedia_tipo=:id_multimedia_tipo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_multimedia_tipo' => $id_multimedia_tipo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_multimedia_tipo = $data['id_multimedia_tipo'];
            $this->multimedia_tipo = $data['multimedia_tipo'];
            $this->icono = $data['icono'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sql_order="", $sql_limit="")
    {
        $sql = "SELECT a.id_multimedia_tipo, a.multimedia_tipo, icono
                FROM opecat_mid_multimedia_tipo a ";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_multimedia_tipo' => $data['id_multimedia_tipo'],
                               'multimedia_tipo' => $data['multimedia_tipo'],
                               'icono' => $data['icono'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener el �cono correspondiente al tipo de archivo
     * @param string $tipo, nombre de la extensi�n del archivo
     * @return string, si el proceso es satisfactorio devuelve el nombre del icono
     */
    public function getIcon($tipo)
    {
        $sql = "SELECT id_multimedia_tipo, multimedia_tipo, icono
                FROM opecat_mid_multimedia_tipo
                WHERE multimedia_tipo=:tipo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':tipo' => $tipo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_multimedia_tipo = $data['id_multimedia_tipo'];
            $this->multimedia_tipo = $data['multimedia_tipo'];
            $this->icono = $data['icono'];
            
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_multimedia_tipo(id_multimedia_tipo, multimedia_tipo, icono)
                VALUES(:id_multimedia_tipo, :multimedia_tipo, :icono);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_multimedia_tipo" => $this->id_multimedia_tipo, ":multimedia_tipo" => $this->multimedia_tipo, ":icono" => $this->icono));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_multimedia_tipo
                   SET multimedia_tipo=:multimedia_tipo, icono=:icono
                WHERE id_multimedia_tipo=:id_multimedia_tipo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_multimedia_tipo" => $this->id_multimedia_tipo, ":multimedia_tipo" => $this->multimedia_tipo, ":icono" => $this->icono));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>