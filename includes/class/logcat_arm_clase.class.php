<?php
/**
 *
 */
class LogcatArmClase
{
    public $id_clase; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $clase; /** @Tipo: varchar(70), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_tipo; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogcatArmTipo; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_arm_tipo.class.php';
        $this->LogcatArmTipo = new LogcatArmTipo();
    }
    
    /**
     * Funci�n para mostrar la lista de clases dentro de un combobox.
     * @param int $id, id de la clase seleccionado por deafult  
     * @param int $id_tipo, id del tipo para el filtro de clases
     * @return array html(options)
     */
    public function shwClases($id, $id_tipo){
        if ($id_tipo != 0)
            $aryDatos = $this->selectAll('b.id_tipo=' . $id_tipo, 'b.tipo Asc');        
        else
            $aryDatos = $this->selectAll('', 'a.clase Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_clase"] )
                $html .= '<option value="'.$datos["id_clase"].'" selected>'.$datos["clase"].'</option>';
            else
                $html .= '<option value="'.$datos["id_clase"].'" >'.$datos["clase"].'</option>';
        }
        return $html;
    }    
    
    /**
     * Funci�n para obtener la clase de arma atravez de la tabla de clases
     * @param  se recibe el parametro id_marca para hacer el filtro
     * @return la clase
     */
     public function getClase( $id_marca ){
	
		$sql = "SELECT c.id_clase, c.clase, c.xstat
                FROM logcat_arm_clase c
				INNER JOIN logcat_arm_marca as m On m.id_clase=c.id_clase
                INNER JOIN logtbl_arm_armamento as a On a.id_marca=m.id_marca
				WHERE a.id_marca=:id_marca
				AND c.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_marca' => $id_marca, 'xstat' => '1' ) );
            $data = $qry->fetch(PDO::FETCH_ASSOC);            
            return $data['clase'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	}    
    
    /**
     * Funci�n para obtener el id del tipo de arma atravez de la tabla de clases
     * @param  se recibe el parametro id_clase para hacer el filtro
     * @return el id del tipo
     */
     public function getIdTipo( $id_clase ){
	
		$sql = "SELECT t.id_tipo
                FROM logcat_arm_tipo as t
				INNER JOIN logcat_arm_clase as c On t.id_tipo=c.id_tipo
				WHERE c.id_clase=:id_clase
				AND t.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_clase' => $id_clase, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);            
            return $data['id_tipo'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	}

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_clase)
    {
        $sql = "SELECT id_clase, clase, id_tipo, xstat
                FROM logcat_arm_clase
                WHERE id_clase=:id_clase;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_clase' => $id_clase));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_clase = $data['id_clase'];
            $this->clase = $data['clase'];
            $this->id_tipo = $data['id_tipo'];
            $this->xstat = $data['xstat'];

            $this->LogcatArmTipo->select($this->id_tipo);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_clase, a.clase, a.id_tipo, a.xstat,
                  b.id_tipo, b.tipo, b.xstat
                FROM logcat_arm_clase a 
                 LEFT JOIN logcat_arm_tipo b ON a.id_tipo=b.id_tipo";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_clase' => $data['id_clase'],
                               'clase' => $data['clase'],
                               'id_tipo' => $data['id_tipo'],
                               'xstat' => $data['xstat'],
                               'logcat_arm_tipo_tipo' => $data['tipo'],
                               'logcat_arm_tipo_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_clase(id_clase, clase, id_tipo, xstat)
                VALUES(:id_clase, :clase, :id_tipo, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_clase" => $this->id_clase, ":clase" => $this->clase, ":id_tipo" => $this->id_tipo, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_clase
                   SET clase=:clase, id_tipo=:id_tipo, xstat=:xstat
                WHERE id_clase=:id_clase;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_clase" => $this->id_clase, ":clase" => $this->clase, ":id_tipo" => $this->id_tipo, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>