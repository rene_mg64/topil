<?php
/**
 *
 */
class LogtblVehDatos
{
    public $id_vehiculo; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $fecha_res; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_reg; /** @Tipo: timestamp, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_municipio; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_operatividad; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_ubicacion; /** @Tipo: tinyint(3), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogtblVehiculos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehOperatividad; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehUbicacion; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logtbl_vehiculos.class.php';
        require_once 'admcat_municipios.class.php';
        require_once 'admtbl_datos_personales.class.php';
        require_once 'logcat_veh_operatividad.class.php';
        require_once 'logcat_veh_ubicacion.class.php';
        $this->LogtblVehiculos = new LogtblVehiculos();
        $this->AdmcatMunicipios = new AdmcatMunicipios();
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->LogcatVehOperatividad = new LogcatVehOperatividad();
        $this->LogcatVehUbicacion = new LogcatVehUbicacion();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_vehiculo)
    {
        $sql = "SELECT id_vehiculo, fecha_res, fecha_reg, id_municipio, curp, id_operatividad, id_ubicacion
                FROM logtbl_veh_datos
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_vehiculo' => $id_vehiculo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_vehiculo = $data['id_vehiculo'];
            $this->fecha_res = $data['fecha_res'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->id_municipio = $data['id_municipio'];
            $this->curp = $data['curp'];
            $this->id_operatividad = $data['id_operatividad'];
            $this->id_ubicacion = $data['id_ubicacion'];

            $this->LogtblVehiculos->select($this->id_vehiculo);
            $this->AdmcatMunicipios->select($this->id_municipio);
            $this->AdmtblDatosPersonales->select($this->curp);
            $this->LogcatVehOperatividad->select($this->id_operatividad);
            $this->LogcatVehUbicacion->select($this->id_ubicacion);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_vehiculo, a.fecha_res, a.fecha_reg, a.id_municipio, a.curp, a.id_operatividad, a.id_ubicacion,                  
                  c.id_municipio, c.municipio, c.id_entidad, c.id_region,
                  d.curp, d.nombre, d.a_paterno, d.a_materno, d.id_entidad, d.id_municipio,
                  e.id_operatividad, e.operatividad, e.xstat,
                  f.id_ubicacion, f.ubicacion, f.orden, f.xstat
                FROM logtbl_veh_datos a 
                 LEFT JOIN logtbl_vehiculos b ON a.id_vehiculo=b.id_vehiculo
                 LEFT JOIN admcat_municipios c ON a.id_municipio=c.id_municipio
                 LEFT JOIN admtbl_datos_personales d ON a.curp=d.curp
                 LEFT JOIN logcat_veh_operatividad e ON a.id_operatividad=e.id_operatividad
                 LEFT JOIN logcat_veh_ubicacion f ON a.id_ubicacion=f.id_ubicacion";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_vehiculo' => $data['id_vehiculo'],
                               'fecha_res' => $data['fecha_res'],
                               'fecha_reg' => $data['fecha_reg'],
                               'id_municipio' => $data['id_municipio'],
                               'curp' => $data['curp'],
                               'id_operatividad' => $data['id_operatividad'],
                               'id_ubicacion' => $data['id_ubicacion'],
                                                                         
                               'admcat_municipios_municipio' => $data['municipio'],                               
                               'admcat_municipios_id_entidad' => $data['id_entidad'],
                               'admcat_municipios_id_region' => $data['id_region'],
                               
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               
                               'logcat_veh_operatividad_operatividad' => $data['operatividad'],
                               'logcat_veh_operatividad_xstat' => $data['xstat'],
                               
                               'logcat_veh_ubicacion_ubicacion' => $data['ubicacion'],
                               'logcat_veh_ubicacion_orden' => $data['orden'],
                               'logcat_veh_ubicacion_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_veh_datos(id_vehiculo, fecha_res, id_municipio, curp, id_operatividad, id_ubicacion)
                VALUES(:id_vehiculo, :fecha_res, :id_municipio, :curp, :id_operatividad, :id_ubicacion);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":fecha_res" => $this->fecha_res, ":id_municipio" => $this->id_municipio, ":curp" => $this->curp, ":id_operatividad" => $this->id_operatividad, ":id_ubicacion" => $this->id_ubicacion));
            
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_veh_datos
                   SET fecha_res=:fecha_res, id_municipio=:id_municipio, curp=:curp, id_operatividad=:id_operatividad, id_ubicacion=:id_ubicacion
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":fecha_res" => $this->fecha_res, ":id_municipio" => $this->id_municipio, ":curp" => $this->curp, ":id_operatividad" => $this->id_operatividad, ":id_ubicacion" => $this->id_ubicacion));
            if ($qry){				
                return true;
            }else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
    
    /**
     * PARA PERSONAL ASIGNABLE COMO USUARIO DE LOS VEHICULOS
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGridPer($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        //echo $sqlWhere;
        $total_reg = $this->selectAllCountPer($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
        //echo $sqlWhere;                
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            //echo $sqlWhere;
            $registros = $this->selectAllMinPer($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }
        
        return $datos;
    }
    
    /**
     *  PARA PERSONAL QUE PUEDE SER INCLUIDO COMO USUARIO DE LOS VEHICULOS
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMinPer($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {    
        $sql = "SELECT a.curp, a.nombre, a.a_paterno, a.a_materno, a.genero, a.id_entidad, a.id_municipio, a.id_status,         
                    b.municipio, b.cabecera, h.categoria, i.area, j.especialidad  
                    FROM admtbl_datos_personales a 
                    LEFT JOIN admcat_municipios b ON a.id_municipio=b.id_municipio                                        
                    LEFT JOIN admcat_status e ON a.id_status=e.id_status                     
                    JOIN admtbl_adscripcion g ON a.curp=g.curp 
                    LEFT JOIN admcat_categorias h ON g.id_categoria=h.id_categoria 
                    LEFT JOIN admcat_areas i ON g.id_area=i.id_area 
                    LEFT JOIN admcat_especialidades j ON g.id_especialidad=j.id_especialidad
                    WHERE ( a.id_status=1 OR a.id_status=3 ) 
                        AND g.id_corporacion=1 
                        AND ( g.id_tipo_funcion=1 OR g.id_tipo_funcion=3 ) ";
        if (!empty($sqlWhere)) {
            $sql .= "\n AND $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sqlWhere;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],
                               //'fecha_nac' => $data['fecha_nac'],
                               'genero' => $data['genero'],
                               //'rfc' => $data['rfc'],
                               //'cuip' => $data['cuip'],
                               //'folio_ife' => $data['folio_ife'],                               
                               //'id_estado_civil' => $data['id_estado_civil'],
                               //'id_tipo_sangre' => $data['id_tipo_sangre'],                               
                               'id_entidad' => $data['id_entidad'],
                               'id_municipio' => $data['id_municipio'],
                               //'lugar_nac' => $data['lugar_nac'],
                               'id_status' => $data['id_status'],
                               'municipio' => $data['municipio'],
                               'cabecera' => $data['cabecera'],                               
                               //'estado_civil' => $data['estado_civil'],                               
                               //'status' => $data['status'],
                               //'tipo_sangre' => $data['tipo_sangre'],
                               'area' => $data['area'],
                               'categoria' => $data['categoria'],
                               'especialidad' => $data['especialidad'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * PARA PERSONAL EN LICENCIA CON SUS RESPECTIVOS CRITERIOS
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCountPer($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT COUNT(*) 
                FROM admtbl_datos_personales a                     
                    JOIN admtbl_adscripcion g ON a.curp=g.curp 
                    LEFT JOIN admcat_categorias h ON g.id_categoria=h.id_categoria 
                    LEFT JOIN admcat_areas i ON g.id_area=i.id_area 
                    LEFT JOIN admcat_especialidades j ON g.id_especialidad=j.id_especialidad
                    WHERE ( a.id_status=1 OR a.id_status=3 ) 
                        AND g.id_corporacion=1 
                        AND ( g.id_tipo_funcion=1 OR g.id_tipo_funcion=3 )";
        if (!empty($sqlWhere)) {
            $sql .= "\nAND $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }            
            $total_reg = $qry->fetchColumn();
            
            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
}


?>