<?php
/**
 *
 */
class AdmtblArchivoExped
{
    public $id_registro; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_archivo; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_documento; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha_reg; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha_docto; /** @Tipo: date, @Acepta Nulos: SI, @Llave: --, @Default: NULL */
    public $descripcion; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $ext; /** @Tipo: varchar(5), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatArchivoDoctos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblArchivo; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_archivo_doctos.class.php';
        require_once 'admtbl_archivo.class.php';
        $this->AdmcatArchivoDoctos = new AdmcatArchivoDoctos();
        $this->AdmtblArchivo = new AdmtblArchivo();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_registro)
    {
        $sql = "SELECT id_registro, id_archivo, id_documento, fecha_reg, fecha_docto, descripcion, ext
                FROM admtbl_archivo_exped
                WHERE id_registro=:id_registro;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_registro' => $id_registro));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_registro = $data['id_registro'];
            $this->id_archivo = $data['id_archivo'];
            $this->id_documento = $data['id_documento'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->fecha_docto = $data['fecha_docto'];
            $this->descripcion = $data['descripcion'];
            $this->ext = $data['ext'];

            $this->AdmcatArchivoDoctos->select($this->id_documento);
            $this->AdmtblArchivo->select($this->id_archivo);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_registro, a.id_archivo, a.id_documento, fecha_reg, fecha_docto, a.descripcion, a.ext,
                  b.documento,
                  c.expediente, c.curp
                FROM admtbl_archivo_exped a 
                 LEFT JOIN admcat_archivo_doctos b ON a.id_documento=b.id_documento
                 LEFT JOIN admtbl_archivo c ON a.id_archivo=c.id_archivo";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_registro' => $data['id_registro'],
                               'id_archivo' => $data['id_archivo'],
                               'id_documento' => $data['id_documento'],
                               'fecha_reg' => $data['fecha_reg'],
                               'fecha_docto' => $data['fecha_docto'],
                               'descripcion' => $data['descripcion'],
                               'documento' => $data['documento'],                               
                               'expediente' => $data['expediente'],
                               'ext' => $data['ext'],
                               'curp' => $data['curp'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener la cantidad de archivos correspondiente a un documento espec�fico
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return int $total, el total de registros contabilizados
     */
    public function selectCount($id_archivo, $id_documento)
    {
        $sql = "SELECT COUNT(*)
                FROM admtbl_archivo_exped a 
                WHERE a.id_archivo=:id_archivo AND a.id_documento=:id_documento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute( array(":id_archivo" => $id_archivo, ":id_documento" => $id_documento) );
            $total = $qry->fetchColumn();
            return $total;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_archivo_exped(id_registro, id_archivo, id_documento, fecha_reg, fecha_docto, descripcion, ext)
                VALUES(:id_registro, :id_archivo, :id_documento, :fecha_reg, :fecha_docto, :descripcion, :ext);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_registro" => $this->id_registro, ":id_archivo" => $this->id_archivo, ":id_documento" => $this->id_documento, ":fecha_reg" => $this->fecha_reg, ":fecha_docto" => $this->fecha_docto, ":descripcion" => $this->descripcion, ":ext" => $this->ext));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_archivo_exped
                   SET id_archivo=:id_archivo, id_documento=:id_documento, fecha_reg=:fecha_reg, fecha_docto:fecha_docto, descripcion=:descripcion
                WHERE id_registro=:id_registro;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_registro" => $this->id_registro, ":id_archivo" => $this->id_archivo, ":id_documento" => $this->id_documento, ":fecha_reg" => $this->fecha_reg, ":fecha_docto" => $this->fecha_docto, ":descripcion" => $this->descripcion));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete($id_registro)
    {
        $sql = "DELETE FROM admtbl_archivo_exped 
                WHERE id_registro=:id_registro;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_registro" => $id_registro));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
}


?>