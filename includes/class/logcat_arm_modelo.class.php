<?php
/**
 *
 */
class LogcatArmModelo
{
    public $id_modelo; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $modelo; /** @Tipo: varchar(70), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_calibre; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogcatArmCalibre; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_arm_calibre.class.php';
        $this->LogcatArmCalibre = new LogcatArmCalibre();
    }

    /**
     * Funci�n para obtener modelo del arma atravez de la tabla de clases
     * @param  se recibe el parametro id_marca para hacer el filtro
     * @return el modelo
     */
     public function getModelo( $id_marca ){
	
		$sql = "SELECT mo.id_modelo, mo.modelo, mo.xstat
                FROM logcat_arm_modelo as mo              
				INNER JOIN logcat_arm_marca as ma On ma.id_modelo=mo.id_modelo
                INNER JOIN logtbl_arm_armamento as a On a.id_marca=ma.id_marca
				WHERE a.id_marca=:id_marca
				AND mo.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_marca' => $id_marca, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);            
            return $data['modelo'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	} 

    /**
     * Funci�n para mostrar la lista de modelos dentro de un combobox.
     * @param int $id, id de modelo seleccionada por deafult     
     * @return array html(options)
     */
    public function shwModelos($id, $id_calibre){
        if ($id_calibre != 0)
            $aryDatos = $this->selectAll('b.id_calibre=' . $id_calibre, 'b.calibre Asc');        
        else
            $aryDatos = $this->selectAll('', 'modelo Asc');
            
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_modelo"] )
                $html .= '<option value="'.$datos["id_modelo"].'" selected>'.$datos["modelo"].'</option>';
            else
                $html .= '<option value="'.$datos["id_modelo"].'" >'.$datos["modelo"].'</option>';
        }
        return $html;
    }    
    
    /**
     * Funci�n para obtener el id del calibre del arma atravez de la tabla de modelos
     * @param  se recibe el parametro id_modelo para hacer el filtro
     * @return el id del calibre
     */
    public function getIdCalibre( $id_modelo ){
	
		$sql = "SELECT c.id_calibre
                FROM logcat_arm_calibre as c
				INNER JOIN logcat_arm_modelo as m On m.id_calibre=c.id_calibre
				WHERE m.id_modelo=:id_modelo
				AND m.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_modelo' => $id_modelo, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);            
            return $data['id_calibre'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	}

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_modelo)
    {
        $sql = "SELECT id_modelo, modelo, id_calibre, xstat
                FROM logcat_arm_modelo
                WHERE id_modelo=:id_modelo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_modelo' => $id_modelo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_modelo = $data['id_modelo'];
            $this->modelo = $data['modelo'];
            $this->id_calibre = $data['id_calibre'];
            $this->xstat = $data['xstat'];

            $this->LogcatArmCalibre->select($this->id_calibre);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_modelo, a.modelo, a.id_calibre, a.xstat,
                  b.id_calibre, b.calibre, b.xstat
                FROM logcat_arm_modelo a 
                 LEFT JOIN logcat_arm_calibre b ON a.id_calibre=b.id_calibre";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_modelo' => $data['id_modelo'],
                               'modelo' => $data['modelo'],
                               'id_calibre' => $data['id_calibre'],
                               'xstat' => $data['xstat'],
                               'logcat_arm_calibre_calibre' => $data['calibre'],
                               'logcat_arm_calibre_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_modelo(id_modelo, modelo, id_calibre, xstat)
                VALUES(:id_modelo, :modelo, :id_calibre, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_modelo" => $this->id_modelo, ":modelo" => $this->modelo, ":id_calibre" => $this->id_calibre, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_modelo
                   SET modelo=:modelo, id_calibre=:id_calibre, xstat=:xstat
                WHERE id_modelo=:id_modelo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_modelo" => $this->id_modelo, ":modelo" => $this->modelo, ":id_calibre" => $this->id_calibre, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>