<?php
/**
 *
 */
class LogcatArmCalibre
{
    public $id_calibre; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $calibre; /** @Tipo: varchar(70), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }
    
    /**
     * Funci�n para mostrar la lista de calibres dentro de un combobox.
     * @param int $id, id de calibre seleccionada por deafult     
     * @return array html(options)
     */
    public function shwCalibres($id=0){

        $aryDatos = $this->selectAll('', 'a.calibre Asc');
            
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_calibre"] )
                $html .= '<option value="'.$datos["id_calibre"].'" selected>'.$datos["calibre"].'</option>';
            else
                $html .= '<option value="'.$datos["id_calibre"].'" >'.$datos["calibre"].'</option>';
        }
        return $html;
    }
    
     /**
     * Funci�n para obtener calibre del arma atravez de la tabla de clases
     * @param  se recibe el parametro id_marca para hacer el filtro
     * @return el celibre
     */
     public function getCalibre( $id_marca ){
	
		$sql = "SELECT ca.id_calibre, ca.calibre, ca.xstat
                FROM logcat_arm_calibre as ca
                INNER JOIN logcat_arm_modelo as mo On mo.id_calibre=ca.id_calibre
				INNER JOIN logcat_arm_marca as ma On ma.id_modelo=mo.id_modelo
                INNER JOIN logtbl_arm_armamento as a On a.id_marca=ma.id_marca
				WHERE a.id_marca=:id_marca
				AND mo.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_marca' => $id_marca, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);            
            return $data['calibre'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	} 

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_calibre)
    {
        $sql = "SELECT id_calibre, calibre, xstat
                FROM logcat_arm_calibre
                WHERE id_calibre=:id_calibre;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_calibre_arma' => $id_calibre));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_calibre = $data['id_calibre'];
            $this->calibre = $data['calibre'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_calibre, a.calibre, a.xstat
                FROM logcat_arm_calibre a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_calibre' => $data['id_calibre'],
                               'calibre' => $data['calibre'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_calibre(id_calibre, calibre, xstat)
                VALUES(:id_calibre, :calibre, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_calibre" => $this->id_calibre, ":calibre" => $this->calibre, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_calibre
                   SET calibre=:calibre, xstat=:xstat
                WHERE id_calibre=:id_calibre;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_calibre" => $this->id_calibre, ":calibre" => $this->calibre, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>