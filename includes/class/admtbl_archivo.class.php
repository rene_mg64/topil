<?php
/**
 *
 */
class AdmtblArchivo
{
    public $id_archivo; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $expediente; /** @Tipo: varchar(45), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $status; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT id_archivo, expediente, curp, status
                FROM admtbl_archivo
                WHERE curp=:curp AND status=1;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_archivo = $data['id_archivo'];
            $this->expediente = $data['expediente'];
            $this->curp = $data['curp'];
            $this->status = $data['status'];

            $this->AdmtblDatosPersonales->select($this->curp);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_archivo, a.expediente, a.curp, status 
                FROM admtbl_archivo a";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_archivo' => $data['id_archivo'],
                               'expediente' => $data['expediente'],
                               'curp' => $data['curp'],
                               'status' => $data['status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function ctrl_archivo()
    {
        $sql = "SELECT id_archivo 
                FROM admtbl_archivo 
                WHERE curp=:curp AND status=1;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp));
            $qry->setFetchMode(PDO::FETCH_ASSOC);
            $val = $qry->fetch();
            if (!empty($val["id_archivo"])) {//Existe...
                $this->id_archivo = $val["id_archivo"];
                $result = $this->update();
            } else {//No existe...
                $this->id_archivo = 0;
                $this->id_archivo = $this->insert();
                $result = $this->id_archivo;
            }
            return $result;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_archivo(id_archivo, expediente, curp, status)
                VALUES(:id_archivo, :expediente, :curp, 1);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_archivo" => $this->id_archivo, ":expediente" => $this->expediente, ":curp" => $this->curp));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_archivo
                   SET expediente=:expediente, curp=:curp, status=:status
                WHERE id_archivo=:id_archivo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_archivo" => $this->id_archivo, ":expediente" => $this->expediente, ":curp" => $this->curp, ":status" => $this->status));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>