<?php
/**
 *
 */
class LogtblArmArmamento
{
    public $matricula; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $serie; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_loc; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */    
    public $id_foliod; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_situacion; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_propietario; /** @Tipo: tinyint(4), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */    
    public $id_estado; /** @Tipo: tinyint(4), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_estatus; /** @Tipo: tinyint(4), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_motivo_movimiento; /** @Tipo: tinyint(4), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */   
    public $id_marca; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */ 

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos    
    public $LogcatArmEstado; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatArmEstatus; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatArmFoliod; /** Objeto para acceder a las propiedades de la clase del mismo nombre */    
    public $LogcatArmLoc; /** Objeto para acceder a las propiedades de la clase del mismo nombre */    
    public $LogcatArmMotivoMovimiento; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatArmPropietario; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatArmSituacion; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatArmMarca; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_arm_ubicacion.class.php';
        require_once 'logcat_arm_estado.class.php';
        require_once 'logcat_arm_estatus.class.php';
        require_once 'logcat_arm_foliod.class.php';        
        require_once 'logcat_arm_loc.class.php';        
        require_once 'logcat_arm_motivo_movimiento.class.php';
        require_once 'logcat_arm_propietario.class.php';
        require_once 'logcat_arm_situacion.class.php';
        require_once 'logcat_arm_marca.class.php';     
        
        $this->LogcatArmEstado = new LogcatArmEstado();
        $this->LogcatArmEstatus = new LogcatArmEstatus();
        $this->LogcatArmFoliod = new LogcatArmFoliod();
        $this->LogcatArmLoc = new LogcatArmLoc();        
        $this->LogcatArmMotivoMovimiento = new LogcatArmMotivoMovimiento();
        $this->LogcatArmPropietario = new LogcatArmPropietario();
        $this->LogcatArmSituacion = new LogcatArmSituacion();    
        $this->LogcatArmMarca = new LogcatArmMarca();    
    }
    
    /**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);        
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }
        
        return $datos;
    }
    
    /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {    
        $sql = "SELECT a.matricula, a.serie, l.loc, m.marca, mo.modelo, cal.calibre, fd.foliod, fc.folioc, a.id_marca
                FROM logtbl_arm_armamento a
                    LEFT JOIN logcat_arm_loc as l On l.id_loc = a.id_loc 
                    LEFT JOIN logcat_arm_marca as m On m.id_marca = a.id_marca
                    LEFT JOIN logcat_arm_modelo as mo On mo.id_modelo = m.id_modelo
                    LEFT JOIN logcat_arm_calibre as cal On cal.id_calibre = mo.id_calibre
                    LEFT JOIN logcat_arm_foliod as fd On fd.id_foliod = a.id_foliod
                    LEFT JOIN logcat_arm_folioc as fc On fc.id_folioc = fd.id_folioc
                    LEFT JOIN logcat_arm_situacion as s On s.id_situacion = a.id_situacion
                    LEFT JOIN logcat_arm_propietario as p On p.id_propietario = a.id_propietario
                    LEFT JOIN logcat_arm_estado as e On e.id_estado = a.id_estado
                    LEFT JOIN logcat_arm_estatus as es On es.id_estatus = a.id_estatus
                    LEFT JOIN logcat_arm_motivo_movimiento as mov On mov.id_motivo_movimiento = a.id_motivo_movimiento ";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'matricula' => $data['matricula'],
                               'id_marca' => $data['id_marca'],
                               'tipo' => $data['tipo'],
                               'serie' => $data['serie'],
                               'loc' => $data['loc'],
                               'marca' => $data['marca'],
                               'modelo' => $data['modelo'],
                               'calibre' => $data['calibre'],
                               'foliod' => $data['foliod'],
                               'folioc' => $data['folioc'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }        
    
    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {    
        $sql = "SELECT COUNT(*) 
                FROM logtbl_arm_armamento a
                    LEFT JOIN logcat_arm_loc as l On l.id_loc = a.id_loc 
                    LEFT JOIN logcat_arm_marca as m On m.id_marca = a.id_marca
                    LEFT JOIN logcat_arm_modelo as mo On mo.id_modelo = m.id_modelo
                    LEFT JOIN logcat_arm_calibre as cal On cal.id_calibre = mo.id_calibre
                    LEFT JOIN logcat_arm_foliod as fd On fd.id_foliod = a.id_foliod
                    LEFT JOIN logcat_arm_folioc as fc On fc.id_folioc = fd.id_folioc
                    LEFT JOIN logcat_arm_situacion as s On s.id_situacion = a.id_situacion
                    LEFT JOIN logcat_arm_propietario as p On p.id_propietario = a.id_propietario
                    LEFT JOIN logcat_arm_estado as e On e.id_estado = a.id_estado
                    LEFT JOIN logcat_arm_estatus as es On es.id_estatus = a.id_estatus
                    LEFT JOIN logcat_arm_motivo_movimiento as mov On mov.id_motivo_movimiento = a.id_motivo_movimiento";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }            
            $total_reg = $qry->fetchColumn();
            
            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }         
    
    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  matricula que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($matricula)
    {
        $sql = "SELECT matricula, serie, id_loc, id_foliod, id_situacion, id_propietario, id_estado, id_estatus, id_motivo_movimiento, id_marca
                FROM logtbl_arm_armamento
                WHERE matricula=:matricula;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':matricula' => $matricula));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->matricula = $data['matricula'];
            $this->serie = $data['serie'];
            $this->id_loc = $data['id_loc'];            
            $this->id_foliod = $data['id_foliod'];
            $this->id_situacion = $data['id_situacion'];
            $this->id_propietario = $data['id_propietario'];            
            $this->id_estado = $data['id_estado'];
            $this->id_estatus = $data['id_estatus'];
            $this->id_motivo_movimiento = $data['id_motivo_movimiento'];           
            $this->id_marca = $data['id_marca'];         
            
            $this->LogcatArmLoc->select($this->id_loc);  
            $this->LogcatArmFoliod->select($this->id_foliod);
            $this->LogcatArmSituacion->select($this->id_situacion); 
            $this->LogcatArmPropietario->select($this->id_propietario);
            $this->LogcatArmEstado->select($this->id_estado);
            $this->LogcatArmEstatus->select($this->id_estatus);                                  
            $this->LogcatArmMotivoMovimiento->select($this->id_motivo_movimiento);                         
            $this->LogcatArmMarca->select($this->id_marca);          
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.matricula, a.serie, a.id_loc, a.id_foliod, a.id_situacion, a.id_propietario, a.id_estado, a.id_estatus, a.id_motivo_movimiento, a.id_marca,                  
                  c.id_estado, c.estado, c.xstat,
                  d.id_estatus, d.estatus, d.xstat,
                  e.id_foliod, e.foliod, e.id_folioc, e.xstat,                  
                  g.id_loc, g.loc, g.xstat,
                  h.id_marca, h.marca, h.id_clase, h.id_modelo, h.xstat,
                  i.id_motivo_movimiento, i.motivo_movimiento, i.xstat,
                  j.id_propietario, j.propietario, j.xstat,
                  k.id_situacion, k.situacion, k.xstat                  
                FROM logtbl_arm_armamento a                  
                 LEFT JOIN logcat_arm_estado c ON a.id_estado=c.id_estado
                 LEFT JOIN logcat_arm_estatus d ON a.id_estatus=d.id_estatus
                 LEFT JOIN logcat_arm_foliod e ON a.id_foliod=e.id_foliod                 
                 LEFT JOIN logcat_arm_loc g ON a.id_loc=g.id_loc                 
                 LEFT JOIN logcat_arm_motivo_movimiento i ON a.id_motivo_movimiento=i.id_motivo_movimiento
                 LEFT JOIN logcat_arm_propietario j ON a.id_propietario=j.id_propietario
                 LEFT JOIN logcat_arm_situacion k ON a.id_situacion=k.id_situacion
                 LEFT JOIN logcat_arm_marca h ON a.id_marca=h.id_marca";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'matricula' => $data['matricula'],
                               'serie' => $data['serie'],
                               'id_loc' => $data['id_loc'],                               
                               'id_foliod' => $data['id_foliod'],
                               'id_situacion' => $data['id_situacion'],
                               'id_propietario' => $data['id_propietario'],                               
                               'id_estado' => $data['id_estado'],
                               'id_estatus' => $data['id_estatus'],
                               'id_marca' => $data['id_marca'],
                               'id_motivo_movimiento' => $data['id_motivo_movimiento'],                                                              
                               'logcat_arm_estado_estado' => $data['estado'],
                               'logcat_arm_estado_xstat' => $data['xstat'],
                               'logcat_arm_estatus_estatus' => $data['estatus'],
                               'logcat_arm_estatus_xstat' => $data['xstat'],
                               'logcat_arm_foliod_foliod' => $data['foliod'],
                               'logcat_arm_foliod_id_folioc' => $data['id_folioc'],
                               'logcat_arm_foliod_xstat' => $data['xstat'],                               
                               'logcat_arm_loc_loc' => $data['loc'],
                               'logcat_arm_loc_xstat' => $data['xstat'],
                               'logcat_arm_marca_marca' => $data['marca'],
                               'logcat_arm_marca_id_clase' => $data['id_clase'],
                               'logcat_arm_marca_id_modelo' => $data['id_modelo'],
                               'logcat_arm_marca_xstat' => $data['xstat'],
                               'logcat_arm_motivo_movimiento_motivo_movimiento' => $data['motivo_movimiento'],
                               'logcat_arm_motivo_movimiento_xstat' => $data['xstat'],
                               'logcat_arm_propietario_propietario' => $data['propietario'],
                               'logcat_arm_propietario_xstat' => $data['xstat'],
                               'logcat_arm_situacion_situacion' => $data['situacion'],
                               'logcat_arm_situacion_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }        

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return boolean true, si el proceso es satisfactorio devuelve valor true
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_arm_armamento(matricula, serie, id_loc, id_foliod, id_situacion, id_propietario, id_estado, id_estatus, id_motivo_movimiento, id_marca)
                VALUES(:matricula, :serie, :id_loc, :id_foliod, :id_situacion, :id_propietario, :id_estado, :id_estatus, :id_motivo_movimiento, :id_marca);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":matricula" => $this->matricula, ":serie" => $this->id_serie, ":id_loc" => $this->id_loc, ":id_foliod" => $this->id_foliod, ":id_situacion" => $this->id_situacion, ":id_propietario" => $this->id_propietario, ":id_estado" => $this->id_estado, ":id_estatus" => $this->id_estatus, ":id_motivo_movimiento" => $this->id_motivo_movimiento, ":id_marca" => $this->id_marca));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }         
    
    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_arm_armamento
                   SET serie=:serie, id_loc=:id_loc, id_foliod=:id_foliod, id_situacion=:id_situacion, id_propietario=:id_propietario, id_estado=:id_estado, id_estatus=:id_estatus, id_motivo_movimiento=:id_motivo_movimiento, id_marca=:id_marca
                WHERE matricula=:matricula;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":matricula" => $this->matricula, ":serie" => $this->serie, ":id_loc" => $this->id_loc, ":id_foliod" => $this->id_foliod, ":id_situacion" => $this->id_situacion, ":id_propietario" => $this->id_propietario, ":id_estado" => $this->id_estado, ":id_estatus" => $this->id_estatus, ":id_motivo_movimiento" => $this->id_motivo_movimiento, ":id_marca" => $this->id_marca));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>