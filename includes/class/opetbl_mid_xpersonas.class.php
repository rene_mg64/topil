<?php
/**
 *
 */
class OpetblMidXpersonas
{
    public $FolioId; /** @Tipo: int(11), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $RolPersonaId; /** @Tipo: int(11), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $Nombre; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $ApellidoPaterno; /** @Tipo: varchar(40), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $ApellidoMaterno; /** @Tipo: varchar(40), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $Edad; /** @Tipo: int(11), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $Sexo; /** @Tipo: char(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $Domicilio; /** @Tipo: varchar(250), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $CausaId; /** @Tipo: int(11), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select()
    {
        $sql = "SELECT FolioId, RolPersonaId, Nombre, ApellidoPaterno, ApellidoMaterno, Edad, Sexo, Domicilio, CausaId
                FROM opetbl_mid_xpersonas
                WHERE ;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array());
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->FolioId = $data['FolioId'];
            $this->RolPersonaId = $data['RolPersonaId'];
            $this->Nombre = $data['Nombre'];
            $this->ApellidoPaterno = $data['ApellidoPaterno'];
            $this->ApellidoMaterno = $data['ApellidoMaterno'];
            $this->Edad = $data['Edad'];
            $this->Sexo = $data['Sexo'];
            $this->Domicilio = $data['Domicilio'];
            $this->CausaId = $data['CausaId'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.FolioId, a.RolPersonaId, a.Nombre, a.ApellidoPaterno, a.ApellidoMaterno, a.Edad, a.Sexo, a.Domicilio, a.CausaId
                FROM opetbl_mid_xpersonas a ";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'FolioId' => $data['FolioId'],
                               'RolPersonaId' => $data['RolPersonaId'],
                               'Nombre' => $data['Nombre'],
                               'ApellidoPaterno' => $data['ApellidoPaterno'],
                               'ApellidoMaterno' => $data['ApellidoMaterno'],
                               'Edad' => $data['Edad'],
                               'Sexo' => $data['Sexo'],
                               'Domicilio' => $data['Domicilio'],
                               'CausaId' => $data['CausaId'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mid_xpersonas(FolioId, RolPersonaId, Nombre, ApellidoPaterno, ApellidoMaterno, Edad, Sexo, Domicilio, CausaId)
                VALUES(:FolioId, :RolPersonaId, :Nombre, :ApellidoPaterno, :ApellidoMaterno, :Edad, :Sexo, :Domicilio, :CausaId);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":FolioId" => $this->FolioId, ":RolPersonaId" => $this->RolPersonaId, ":Nombre" => $this->Nombre, ":ApellidoPaterno" => $this->ApellidoPaterno, ":ApellidoMaterno" => $this->ApellidoMaterno, ":Edad" => $this->Edad, ":Sexo" => $this->Sexo, ":Domicilio" => $this->Domicilio, ":CausaId" => $this->CausaId));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mid_xpersonas
                   SET FolioId=:FolioId, RolPersonaId=:RolPersonaId, Nombre=:Nombre, ApellidoPaterno=:ApellidoPaterno, ApellidoMaterno=:ApellidoMaterno, Edad=:Edad, Sexo=:Sexo, Domicilio=:Domicilio, CausaId=:CausaId
                WHERE ;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":FolioId" => $this->FolioId, ":RolPersonaId" => $this->RolPersonaId, ":Nombre" => $this->Nombre, ":ApellidoPaterno" => $this->ApellidoPaterno, ":ApellidoMaterno" => $this->ApellidoMaterno, ":Edad" => $this->Edad, ":Sexo" => $this->Sexo, ":Domicilio" => $this->Domicilio, ":CausaId" => $this->CausaId));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>