<?php
/**
 *
 */
class OpetblMidIncidentes
{
    public $id_folio_incidente; /** @Tipo: int(10) unsigned zerofill, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $prioridad; /** @Tipo: tinyint(4), @Acepta Nulos: NO, @Llave: --, @Default: 1 */
    public $fecha_hora_captura; /** @Tipo: datetime, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha_incidente; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $dia; /** @Tipo: varchar(10), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $mes; /** @Tipo: varchar(12), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $annio; /** @Tipo: varchar(4), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $hora_incidente; /** @Tipo: time, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_estado; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_region; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_cuartel; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_municipio; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $direccion; /** @Tipo: varchar(250), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $localidad; /** @Tipo: varchar(250), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $colonia; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $calle; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $latitud; /** @Tipo: geometry, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $longitud; /** @Tipo: geometry, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_asunto; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_fuente; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $hechos_html; /** @Tipo: longtext, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $hechos_text; /** @Tipo: mediumtext, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $ip_usuario; /** @Tipo: varchar(15), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_usuario; /** @Tipo: varchar(16), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMidCuarteles; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidAsuntos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidFuentes; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidEstados; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidRegionesOperativas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mid_cuarteles.class.php';
        require_once 'opecat_mid_asuntos.class.php';
        require_once 'opecat_mid_fuentes.class.php';
        require_once 'opecat_mid_estados.class.php';
        require_once 'opecat_mid_municipios.class.php';
        require_once 'opecat_mid_regiones_operativas.class.php';
        $this->OpecatMidCuarteles = new OpecatMidCuarteles_();
        $this->OpecatMidAsuntos = new OpecatMidAsuntos();
        $this->OpecatMidFuentes = new OpecatMidFuentes();
        $this->OpecatMidEstados = new OpecatMidEstados();
        $this->OpecatMidMunicipios = new OpecatMidMunicipios();
        $this->OpecatMidRegionesOperativas = new OpecatMidRegionesOperativas();
    }
    
    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function getIdUltimoIncidente(){        
        $sql = "SELECT MAX(id_folio_incidente) as id
                  FROM opetbl_mid_incidentes;";
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["id"];
            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return x;
        }
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_folio_incidente)
    {
        $sql = "SELECT id_folio_incidente, prioridad, fecha_hora_captura, fecha_incidente, dia, mes, annio, hora_incidente, id_estado, id_region, id_cuartel, id_municipio, direccion, localidad, colonia, calle, latitud, longitud, id_asunto, id_fuente, hechos_html, hechos_text, ip_usuario, id_usuario
                FROM opetbl_mid_incidentes
                WHERE id_folio_incidente=:id_folio_incidente;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_folio_incidente' => $id_folio_incidente));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_folio_incidente = $data['id_folio_incidente'];
            $this->prioridad = $data['prioridad'];
            $this->fecha_hora_captura = $data['fecha_hora_captura'];
            $this->fecha_incidente = $data['fecha_incidente'];
            $this->dia = $data['dia'];
            $this->mes = $data['mes'];
            $this->annio = $data['annio'];
            $this->hora_incidente = $data['hora_incidente'];
            $this->id_estado = $data['id_estado'];
            $this->id_region = $data['id_region'];
            $this->id_cuartel = $data['id_cuartel'];
            $this->id_municipio = $data['id_municipio'];
            $this->direccion = $data['direccion'];
            $this->localidad = $data['localidad'];
            $this->colonia = $data['colonia'];
            $this->calle = $data['calle'];
            $this->latitud = $data['latitud'];
            $this->longitud = $data['longitud'];
            $this->id_asunto = $data['id_asunto'];
            $this->id_fuente = $data['id_fuente'];
            $this->hechos_html = $data['hechos_html'];
            $this->hechos_text = $data['hechos_text'];
            $this->ip_usuario = $data['ip_usuario'];
            $this->id_usuario = $data['id_usuario'];           
    
            $this->OpecatMidCuarteles->select($this->id_cuartel);            
            $this->OpecatMidAsuntos->select($this->id_asunto);
            $this->OpecatMidFuentes->select($this->id_fuente);
            $this->OpecatMidEstados->select($this->id_estado);
            $this->OpecatMidMunicipios->select($this->id_municipio);
            $this->OpecatMidRegionesOperativas->select($this->id_region);  
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_folio_incidente, a.prioridad, a.fecha_hora_captura, a.fecha_incidente, a.dia, a.mes, a.annio, a.hora_incidente, a.id_estado, a.id_region, a.id_cuartel, a.id_municipio, a.direccion, a.localidad, a.colonia, a.calle, a.latitud, a.longitud, a.id_asunto, a.id_fuente, a.hechos_html, a.hechos_text, a.ip_usuario, a.id_usuario,
                  b.id_cuartel, b.id_region, b.id_cuartel_tipo, b.cuartel, b.oficial,
                  c.id_asunto, c.asunto, c.icono,
                  d.id_fuente, d.fuente,
                  e.id_estado, e.estado,
                  f.id_municipio, f.id_estado, f.municipio, f.alcalde,
                  g.id_region, g.region
                FROM opetbl_mid_incidentes a
                 LEFT JOIN opecat_mid_cuarteles b ON a.id_cuartel=b.id_cuartel
                 LEFT JOIN opecat_mid_asuntos c ON a.id_asunto=c.id_asunto
                 LEFT JOIN opecat_mid_fuentes d ON a.id_fuente=d.id_fuente
                 LEFT JOIN opecat_mid_estados e ON a.id_estado=e.id_estado
                 LEFT JOIN opecat_mid_municipios f ON a.id_municipio=f.id_municipio
                 LEFT JOIN opecat_mid_regiones_operativas g ON a.id_region=g.id_region";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_folio_incidente' => $data['id_folio_incidente'],
                               'prioridad' => $data['prioridad'],
                               'fecha_hora_captura' => $data['fecha_hora_captura'],
                               'fecha_incidente' => $data['fecha_incidente'],
                               'dia' => $data['dia'],
                               'mes' => $data['mes'],
                               'annio' => $data['annio'],
                               'hora_incidente' => $data['hora_incidente'],
                               'id_estado' => $data['id_estado'],
                               'id_region' => $data['id_region'],
                               'id_cuartel' => $data['id_cuartel'],
                               'id_municipio' => $data['id_municipio'],
                               'direccion' => $data['direccion'],
                               'localidad' => $data['localidad'],
                               'colonia' => $data['colonia'],
                               'calle' => $data['calle'],
                               'latitud' => $data['latitud'],
                               'longitud' => $data['longitud'],
                               'id_asunto' => $data['id_asunto'],
                               'id_fuente' => $data['id_fuente'],
                               'hechos_html' => $data['hechos_html'],
                               'hechos_text' => $data['hechos_text'],
                               'ip_usuario' => $data['ip_usuario'],
                               'id_usuario' => $data['id_usuario'],
                               'opecat_mid_cuarteles_id_region' => $data['id_region'],
                               'opecat_mid_cuarteles_id_cuartel_tipo' => $data['id_cuartel_tipo'],
                               'opecat_mid_cuarteles_cuartel' => $data['cuartel'],
                               'opecat_mid_cuarteles_oficial' => $data['oficial'],
                               'opecat_mid_asuntos_asunto' => $data['asunto'],
                               'opecat_mid_asuntos_icono' => $data['icono'],
                               'opecat_mid_fuentes_fuente' => $data['fuente'],
                               'opecat_mid_estados_estado' => $data['estado'],
                               'opecat_mid_municipios_id_estado' => $data['id_estado'],
                               'opecat_mid_municipios_municipio' => $data['municipio'],
                               'opecat_mid_municipios_alcalde' => $data['alcalde'],
                               'opecat_mid_regiones_operativas_region' => $data['region'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

/**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }

        return $datos;
    }

 /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT i.id_folio_incidente, i.prioridad,i.fecha_incidente,
                i.id_region, r.region, i.id_cuartel,c.cuartel, i.id_asunto, a.asunto,
                i.id_fuente, f.fuente, i.hechos_text
                FROM opetbl_mid_incidentes i
                LEFT JOIN opecat_mid_regiones_operativas r on i.id_region = r.id_region
                LEFT JOIN opecat_mid_cuarteles c on i.id_cuartel = c.id_cuartel
                LEFT JOIN opecat_mid_asuntos a on i.id_asunto=a.id_asunto
                LEFT JOIN opecat_mid_fuentes f on i.id_fuente=f.id_fuente";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                                'id_folio_incidente' => $data['id_folio_incidente'],
                                'prioridad' => $data['prioridad'],
                                'fecha_incidente' => $data['fecha_incidente'],
                                'id_region' => $data['id_region'],
                                'region' => $data['region'],
                                'id_cuartel' => $data['id_cuartel'],
                                'cuartel' => $data['cuartel'],
                                'id_asunto' => $data['id_asunto'],
                                'asunto' => $data['asunto'],
                                'id_fuente' => $data['id_fuente'],
                                'fuente' => $data['fuente'],
                                'hechos_text' => $data['hechos_text'],
                                );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT count(*)
                FROM opetbl_mid_incidentes i
                LEFT JOIN opecat_mid_regiones_operativas r on i.id_region = r.id_region
                LEFT JOIN opecat_mid_cuarteles c on i.id_cuartel = c.id_cuartel
                LEFT JOIN opecat_mid_asuntos a on i.id_asunto=a.id_asunto
                LEFT JOIN opecat_mid_fuentes f on i.id_fuente=f.id_fuente";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }

        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $total_reg = $qry->fetchColumn();

            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }



    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mid_incidentes(prioridad, fecha_incidente, dia, mes, annio, hora_incidente, id_estado, 
                                                id_region, id_cuartel, id_municipio, direccion, localidad, colonia, calle, 
                                                latitud, longitud, id_asunto, id_fuente, hechos_html, hechos_text, ip_usuario, 
                                                id_usuario)
                VALUES(:prioridad, :fecha_incidente, :dia, :mes, :annio, :hora_incidente, :id_estado, 
                        :id_region, :id_cuartel, :id_municipio, :direccion, :localidad, :colonia, :calle, :latitud, :longitud, 
                        :id_asunto, :id_fuente, :hechos_html, :hechos_text, :ip_usuario, :id_usuario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":prioridad" => $this->prioridad, ":fecha_incidente" => $this->fecha_incidente, 
                                ":dia" => $this->dia, ":mes" => $this->mes, ":annio" => $this->annio, 
                                ":hora_incidente" => $this->hora_incidente, ":id_estado" => $this->id_estado, 
                                ":id_region" => $this->id_region, ":id_cuartel" => $this->id_cuartel, 
                                ":id_municipio" => $this->id_municipio, ":direccion" => $this->direccion, 
                                ":localidad" => $this->localidad, ":colonia" => $this->colonia, ":calle" => $this->calle, 
                                ":latitud" => $this->latitud, ":longitud" => $this->longitud, ":id_asunto" => $this->id_asunto, 
                                ":id_fuente" => $this->id_fuente, ":hechos_html" => $this->hechos_html, 
                                ":hechos_text" => $this->hechos_text, ":ip_usuario" => $this->ip_usuario, 
                                ":id_usuario" => $this->id_usuario));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            //$this->msjError = $sql;
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mid_incidentes
                   SET prioridad=:prioridad, fecha_incidente=:fecha_incidente, dia=:dia, mes=:mes, annio=:annio, 
                        hora_incidente=:hora_incidente, id_estado=:id_estado, id_region=:id_region, id_cuartel=:id_cuartel, 
                        id_municipio=:id_municipio, direccion=:direccion, localidad=:localidad, colonia=:colonia, 
                        calle=:calle, latitud=:latitud, longitud=:longitud, id_asunto=:id_asunto, id_fuente=:id_fuente, 
                        hechos_html=:hechos_html, hechos_text=:hechos_text
                WHERE id_folio_incidente=:id_folio_incidente;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_folio_incidente" => $this->id_folio_incidente, ":prioridad" => $this->prioridad, 
                                ":fecha_incidente" => $this->fecha_incidente, ":dia" => $this->dia, ":mes" => $this->mes, 
                                ":annio" => $this->annio, ":hora_incidente" => $this->hora_incidente, 
                                ":id_estado" => $this->id_estado, ":id_region" => $this->id_region, 
                                ":id_cuartel" => $this->id_cuartel, ":id_municipio" => $this->id_municipio, 
                                ":direccion" => $this->direccion, ":localidad" => $this->localidad, ":colonia" => $this->colonia, 
                                ":calle" => $this->calle, ":latitud" => $this->latitud, ":longitud" => $this->longitud, 
                                ":id_asunto" => $this->id_asunto, ":id_fuente" => $this->id_fuente, 
                                ":hechos_html" => $this->hechos_html, ":hechos_text" => $this->hechos_text));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>