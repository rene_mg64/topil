<?php
/**
 *
 */
class AdmcatMfCabelloForma
{
    public $id_cabello_forma; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $cabello_forma; /** @Tipo: varchar(25), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_cabello_forma)
    {
        $sql = "SELECT id_cabello_forma, cabello_forma
                FROM admcat_mf_cabello_forma
                WHERE id_cabello_forma=:id_cabello_forma;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_cabello_forma' => $id_cabello_forma));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_cabello_forma = $data['id_cabello_forma'];
            $this->cabello_forma = $data['cabello_forma'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_cabello_forma, a.cabello_forma
                FROM admcat_mf_cabello_forma a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_cabello_forma' => $data['id_cabello_forma'],
                               'cabello_forma' => $data['cabello_forma'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_mf_cabello_forma(id_cabello_forma, cabello_forma)
                VALUES(:id_cabello_forma, :cabello_forma);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_cabello_forma" => $this->id_cabello_forma, ":cabello_forma" => $this->cabello_forma));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_mf_cabello_forma
                   SET cabello_forma=:cabello_forma
                WHERE id_cabello_forma=:id_cabello_forma;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_cabello_forma" => $this->id_cabello_forma, ":cabello_forma" => $this->cabello_forma));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>