<?php
/**
 *
 */
class OpetblMidIncidentesMultimedia
{
    public $id_multimedia; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_folio_incidente; /** @Tipo: int(10) unsigned zerofill, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_multimedia_tipo; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $descripcion; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $archivo; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpetblMidIncidentes; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidMultimediaTipo; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opetbl_mid_incidentes.class.php';
        require_once 'opecat_mid_multimedia_tipo.class.php';
        $this->OpetblMidIncidentes = new OpetblMidIncidentes();
        $this->OpecatMidMultimediaTipo = new OpecatMidMultimediaTipo();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_multimedia)
    {
        $sql = "SELECT id_multimedia, id_folio_incidente, id_multimedia_tipo, descripcion, archivo
                FROM opetbl_mid_incidentes_multimedia
                WHERE id_multimedia=:id_multimedia;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_multimedia' => $id_multimedia));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_multimedia = $data['id_multimedia'];
            $this->id_folio_incidente = $data['id_folio_incidente'];
            $this->id_multimedia_tipo = $data['id_multimedia_tipo'];
            $this->descripcion = $data['descripcion'];
            $this->archivo = $data['archivo'];

            $this->OpetblMidIncidentes->select($this->id_folio_incidente);
            $this->OpecatMidMultimediaTipo->select($this->id_multimedia_tipo);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_multimedia, a.id_folio_incidente, a.descripcion, a.archivo, b.multimedia_tipo, b.icono
                FROM opetbl_mid_incidentes_multimedia a
                    LEFT JOIN opecat_mid_multimedia_tipo b ON a.id_multimedia_tipo=b.id_multimedia_tipo";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_multimedia' => $data['id_multimedia'],
                               'id_folio_incidente' => $data['id_folio_incidente'],
                               'id_multimedia_tipo' => $data['id_multimedia_tipo'],
                               'descripcion' => $data['descripcion'],
                               'archivo' => $data['archivo'],
                               'multimedia_tipo' => $data['multimedia_tipo'],
                               'icono' => $data['icono'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }

        return $datos;
    }
    

    /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT m.id_multimedia, m.id_folio_incidente, mt.multimedia_tipo, m.descripcion, m.archivo
                FROM opetbl_mid_incidentes_multimedia m
                LEFT JOIN opecat_mid_multimedia_tipo mt ON m.id_multimedia_tipo=mt.id_multimedia_tipo";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                                'id_multimedia' =>  $data['id_multimedia'],
                                'id_folio_incidente' =>  $data['id_folio_incidente'],
                                'multimedia_tipo' =>  $data['multimedia_tipo'],
                                'descripcion' =>  $data['descripcion'],
                                'archivo' =>  $data['archivo'],
                                );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT count(*)
                FROM opetbl_mid_incidentes_multimedia m
                LEFT JOIN opecat_mid_multimedia_tipo mt ON m.id_multimedia_tipo=mt.id_multimedia_tipo";

        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }

        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $total_reg = $qry->fetchColumn();

            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
    * Funci�n para obtener el total de registros multimedia de un incidente
    * @param  int, id del incidente
    * @return int, total de registros encontrados
    */
    public function getTotalMultimedia( $id_incidente ){        
        $sql = "SELECT COUNT(*) AS total
                  FROM opetbl_mid_incidentes_multimedia 
                  WHERE id_folio_incidente = :id_incidente;";
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_incidente" => $id_incidente));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["total"];
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mid_incidentes_multimedia(id_multimedia, id_folio_incidente, id_multimedia_tipo, descripcion, archivo)
                VALUES(:id_multimedia, :id_folio_incidente, :id_multimedia_tipo, :descripcion, :archivo);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_multimedia" => $this->id_multimedia, ":id_folio_incidente" => $this->id_folio_incidente, ":id_multimedia_tipo" => $this->id_multimedia_tipo, ":descripcion" => $this->descripcion, ":archivo" => $this->archivo));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mid_incidentes_multimedia
                   SET id_folio_incidente=:id_folio_incidente, id_multimedia_tipo=:id_multimedia_tipo, descripcion=:descripcion, archivo=:archivo
                WHERE id_multimedia=:id_multimedia;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_multimedia" => $this->id_multimedia, ":id_folio_incidente" => $this->id_folio_incidente, ":id_multimedia_tipo" => $this->id_multimedia_tipo, ":descripcion" => $this->descripcion, ":archivo" => $this->archivo));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete($id_multimedia)
    {
        $sql = "DELETE FROM opetbl_mid_incidentes_multimedia
                WHERE id_multimedia=:id_multimedia;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_multimedia" => $id_multimedia));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
}


?>