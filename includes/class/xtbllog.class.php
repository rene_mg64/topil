<?php
/**
 *
 */
class Xtbllog
{
    public $id_log; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_usuario; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $fecha; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $hora; /** @Tipo: time, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $tabla; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_reg; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $oper; /** @Tipo: varchar(15), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $ip; /** @Tipo: varchar(16), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $Xtblusuarios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'xtblusuarios.class.php';
        $this->Xtblusuarios = new Xtblusuarios();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_log)
    {
        $sql = "SELECT id_log, id_usuario, fecha, hora, tabla, id_reg, oper, ip
                FROM xtbllog
                WHERE id_log=:id_log;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_log' => $id_log));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_log = $data['id_log'];
            $this->id_usuario = $data['id_usuario'];
            $this->fecha = $data['fecha'];
            $this->hora = $data['hora'];
            $this->tabla = $data['tabla'];
            $this->id_reg = $data['id_reg'];
            $this->oper = $data['oper'];
            $this->ip = $data['ip'];

            $this->Xtblusuarios->select($this->id_usuario);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_log, a.id_usuario, a.fecha, a.hora, a.tabla, a.id_reg, a.oper, a.ip,
                  b.id_usuario, b.nom_usr, b.pswd, b.nombre, b.id_perfil, b.fecha_reg, b.fecha_edit, b.stat
                FROM xtbllog a 
                 LEFT JOIN xtblusuarios b ON a.id_usuario=b.id_usuario";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_log' => $data['id_log'],
                               'id_usuario' => $data['id_usuario'],
                               'fecha' => $data['fecha'],
                               'hora' => $data['hora'],
                               'tabla' => $data['tabla'],
                               'id_reg' => $data['id_reg'],
                               'oper' => $data['oper'],
                               'ip' => $data['ip'],
                               'xtblusuarios_nom_usr' => $data['nom_usr'],
                               'xtblusuarios_pswd' => $data['pswd'],
                               'xtblusuarios_nombre' => $data['nombre'],
                               'xtblusuarios_id_perfil' => $data['id_perfil'],
                               'xtblusuarios_fecha_reg' => $data['fecha_reg'],
                               'xtblusuarios_fecha_edit' => $data['fecha_edit'],
                               'xtblusuarios_stat' => $data['stat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO xtbllog(id_log, id_usuario, fecha, hora, tabla, id_reg, oper, ip)
                VALUES(:id_log, :id_usuario, :fecha, :hora, :tabla, :id_reg, :oper, :ip);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_log" => $this->id_log, ":id_usuario" => $this->id_usuario, ":fecha" => $this->fecha, ":hora" => $this->hora, ":tabla" => $this->tabla, ":id_reg" => $this->id_reg, ":oper" => $this->oper, ":ip" => $this->ip));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE xtbllog
                   SET id_usuario=:id_usuario, fecha=:fecha, hora=:hora, tabla=:tabla, id_reg=:id_reg, oper=:oper, ip=:ip
                WHERE id_log=:id_log;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_log" => $this->id_log, ":id_usuario" => $this->id_usuario, ":fecha" => $this->fecha, ":hora" => $this->hora, ":tabla" => $this->tabla, ":id_reg" => $this->id_reg, ":oper" => $this->oper, ":ip" => $this->ip));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>