<?php
/**
 *
 */
class AdmtblCursos
{
    public $id_curso; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo_curso; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $curso; /** @Tipo: varchar(250), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $institucion; /** @Tipo: varchar(150), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $eficiencia_term; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha_ini; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_fin; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $duracion; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $observaciones; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatTipoCurso; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        require_once 'admcat_tipo_curso.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatTipoCurso = new AdmcatTipoCurso();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_curso)
    {
        $sql = "SELECT id_curso, curp, id_tipo_curso, curso, institucion, eficiencia_term, fecha_ini, fecha_fin, duracion, observaciones
                FROM admtbl_cursos
                WHERE id_curso=:id_curso;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_curso' => $id_curso));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_curso = $data['id_curso'];
            $this->curp = $data['curp'];
            $this->id_tipo_curso = $data['id_tipo_curso'];
            $this->curso = $data['curso'];
            $this->institucion = $data['institucion'];
            $this->eficiencia_term = $data['eficiencia_term'];
            $this->fecha_ini = $data['fecha_ini'];
            $this->fecha_fin = $data['fecha_fin'];
            $this->duracion = $data['duracion'];
            $this->observaciones = $data['observaciones'];

            $this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmcatTipoCurso->select($this->id_tipo_curso);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_curso, a.curp, a.id_tipo_curso, a.curso, a.institucion, a.eficiencia_term, a.fecha_ini, a.fecha_fin, a.duracion, a.observaciones,
                  b.tipo_curso
                FROM admtbl_cursos a 
                 LEFT JOIN admcat_tipo_curso b ON a.id_tipo_curso=b.id_tipo_curso";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_curso' => $data['id_curso'],
                               'curp' => $data['curp'],
                               'id_tipo_curso' => $data['id_tipo_curso'],
                               'curso' => $data['curso'],
                               'institucion' => $data['institucion'],
                               'eficiencia_term' => $data['eficiencia_term'],
                               'fecha_ini' => $data['fecha_ini'],
                               'fecha_fin' => $data['fecha_fin'],
                               'duracion' => $data['duracion'],
                               'observaciones' => $data['observaciones'],                               
                               'tipo_curso' => $data['tipo_curso'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);        
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }
        
        return $datos;
    }
    
    /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {    
        $sql = "SELECT a.id_curso, a.curp, a.id_tipo_curso, a.curso, a.eficiencia_term, a.fecha_ini, a.fecha_fin, b.tipo_curso
                FROM admtbl_cursos a 
                 LEFT JOIN admcat_tipo_curso b ON a.id_tipo_curso=b.id_tipo_curso";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_curso' => $data['id_curso'],
                               'curp' => $data['curp'],
                               'id_tipo_curso' => $data['id_tipo_curso'],
                               'curso' => $data['curso'],                               
                               'eficiencia_term' => $data['eficiencia_term'],
                               'fecha_ini' => $data['fecha_ini'],
                               'fecha_fin' => $data['fecha_fin'],
                               'tipo_curso' => $data['tipo_curso'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT COUNT(*) 
                FROM admtbl_cursos a 
                 LEFT JOIN admcat_tipo_curso b ON a.id_tipo_curso=b.id_tipo_curso";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }            
            $total_reg = $qry->fetchColumn();
            
            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_cursos(id_curso, curp, id_tipo_curso, curso, institucion, eficiencia_term, fecha_ini, fecha_fin, duracion, observaciones)
                VALUES(:id_curso, :curp, :id_tipo_curso, :curso, :institucion, :eficiencia_term, :fecha_ini, :fecha_fin, :duracion, :observaciones);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_curso" => $this->id_curso, ":curp" => $this->curp, ":id_tipo_curso" => $this->id_tipo_curso, ":curso" => $this->curso, ":institucion" => $this->institucion, ":eficiencia_term" => $this->eficiencia_term, ":fecha_ini" => $this->fecha_ini, ":fecha_fin" => $this->fecha_fin, ":duracion" => $this->duracion, ":observaciones" => $this->observaciones));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_cursos
                   SET curp=:curp, id_tipo_curso=:id_tipo_curso, curso=:curso, institucion=:institucion, eficiencia_term=:eficiencia_term, fecha_ini=:fecha_ini, fecha_fin=:fecha_fin, duracion=:duracion, observaciones=:observaciones
                WHERE id_curso=:id_curso;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_curso" => $this->id_curso, ":curp" => $this->curp, ":id_tipo_curso" => $this->id_tipo_curso, ":curso" => $this->curso, ":institucion" => $this->institucion, ":eficiencia_term" => $this->eficiencia_term, ":fecha_ini" => $this->fecha_ini, ":fecha_fin" => $this->fecha_fin, ":duracion" => $this->duracion, ":observaciones" => $this->observaciones));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>