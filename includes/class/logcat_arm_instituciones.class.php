<?php
/**
 *
 */
class LogcatArmInstituciones
{
    public $id_instituciones; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $instituciones; /** @Tipo: varchar(255), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $siglas; /** @Tipo: varchar(255), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_dependencia; /** @Tipo: tinyint(4), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogcatArmDependencias; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_arm_dependencias.class.php';
        $this->LogcatArmDependencias = new LogcatArmDependencias();
    }

    /**
     * Funci�n para mostrar la lista de instituciones dentro de un combobox.
     * @param int $id, id de instituciones seleccionado por deafult     
     * @return array html(options)
     */
    public function shwInstituciones( $id=0 ){
        $aryDatos = $this->selectAll('a.xstat=1', 'a.instituciones Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_instituciones"] )
                $html .= '<option value="'.$datos["id_instituciones"].'" selected>'.$datos["instituciones"].'</option>';
            else
                $html .= '<option value="'.$datos["id_instituciones"].'" >'.$datos["instituciones"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener el id del tipo de dependencia atravez de la tabla de marcas
     * @param  $int id_marca, se recibe el parametro id_tipodependencia para hacer el filtro
     * @return el id de la clase
     */
    public function getIdDependencia( $id_inst ){
	
		$sql = "SELECT d.id_dependencia
                FROM logcat_arm_dependencias as d
				INNER JOIN logcat_arm_instituciones as i On d.id_dependencia=i.id_dependencia
				WHERE d.id_instituciones=:id_instituciones
				AND d.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_instituciones' => $id_inst, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);            
            return $data['id_dependencia'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	}

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_instituciones)
    {
        $sql = "SELECT id_instituciones, instituciones, siglas, id_dependencia, xstat
                FROM logcat_arm_instituciones
                WHERE id_instituciones=:id_instituciones;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_instituciones' => $id_instituciones));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_instituciones = $data['id_instituciones'];
            $this->instituciones = $data['instituciones'];
            $this->siglas = $data['siglas'];
            $this->id_dependencia = $data['id_dependencia'];
            $this->xstat = $data['xstat'];

            $this->LogcatArmDependencias->select($this->id_dependencia);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_instituciones, a.instituciones, a.siglas, a.id_dependencia, a.xstat,
                  b.id_dependencia, b.dependencia, b.id_tipo_dependencia, b.xstat
                FROM logcat_arm_instituciones a 
                 LEFT JOIN logcat_arm_dependencias b ON a.id_dependencia=b.id_dependencia";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_instituciones' => $data['id_instituciones'],
                               'instituciones' => $data['instituciones'],
                               'siglas' => $data['siglas'],
                               'id_dependencia' => $data['id_dependencia'],
                               'xstat' => $data['xstat'],
                               'logcat_arm_dependencias_dependencia' => $data['dependencia'],
                               'logcat_arm_dependencias_id_tipo_dependencia' => $data['id_tipo_dependencia'],
                               'logcat_arm_dependencias_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_instituciones(id_instituciones, instituciones, siglas, id_dependencia, xstat)
                VALUES(:id_instituciones, :instituciones, :siglas, :id_dependencia, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_instituciones" => $this->id_instituciones, ":instituciones" => $this->instituciones, ":siglas" => $this->siglas, ":id_dependencia" => $this->id_dependencia, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_instituciones
                   SET instituciones=:instituciones, siglas=:siglas, id_dependencia=:id_dependencia, xstat=:xstat
                WHERE id_instituciones=:id_instituciones;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_instituciones" => $this->id_instituciones, ":instituciones" => $this->instituciones, ":siglas" => $this->siglas, ":id_dependencia" => $this->id_dependencia, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>