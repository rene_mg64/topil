<?php
/**
 *
 */
class AdmcatMfColorPiel
{
    public $id_color_piel; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $color_piel; /** @Tipo: varchar(25), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_color_piel)
    {
        $sql = "SELECT id_color_piel, color_piel
                FROM admcat_mf_color_piel
                WHERE id_color_piel=:id_color_piel;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_color_piel' => $id_color_piel));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_color_piel = $data['id_color_piel'];
            $this->color_piel = $data['color_piel'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_color_piel, a.color_piel
                FROM admcat_mf_color_piel a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_color_piel' => $data['id_color_piel'],
                               'color_piel' => $data['color_piel'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_mf_color_piel(id_color_piel, color_piel)
                VALUES(:id_color_piel, :color_piel);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_color_piel" => $this->id_color_piel, ":color_piel" => $this->color_piel));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_mf_color_piel
                   SET color_piel=:color_piel
                WHERE id_color_piel=:id_color_piel;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_color_piel" => $this->id_color_piel, ":color_piel" => $this->color_piel));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>