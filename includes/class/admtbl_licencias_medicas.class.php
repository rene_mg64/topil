<?php
/**
 *
 */
class AdmtblLicenciasMedicas
{
    public $id_licencia_medica; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha_inicio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_fin; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $num_dias; /** @Tipo: smallint(3), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $folio; /** @Tipo: varchar(25), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $diagnostico; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $observacion; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_reg; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $folio_finanzas; /** @Tipo: varchar(25), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $no_oficio; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $no_soporte; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_soporte; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $firmante_soporte; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cargo_firmante; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_licencia_medica)
    {
        $sql = "SELECT id_licencia_medica, curp, fecha_inicio, fecha_fin, num_dias, folio, diagnostico, observacion, fecha_reg, folio_finanzas, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante
                FROM admtbl_licencias_medicas
                WHERE id_licencia_medica=:id_licencia_medica;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_licencia_medica' => $id_licencia_medica));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_licencia_medica = $data['id_licencia_medica'];
            $this->curp = $data['curp'];
            $this->fecha_inicio = $data['fecha_inicio'];
            $this->fecha_fin = $data['fecha_fin'];
            $this->num_dias = $data['num_dias'];
            $this->folio = $data['folio'];
            $this->diagnostico = $data['diagnostico'];
            $this->observacion = $data['observacion'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->folio_finanzas = $data['folio_finanzas'];
            $this->no_oficio = $data['no_oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->no_soporte = $data['no_soporte'];
            $this->fecha_soporte = $data['fecha_soporte'];
            $this->firmante_soporte = $data['firmante_soporte'];
            $this->cargo_firmante = $data['cargo_firmante'];

            $this->AdmtblDatosPersonales->select($this->curp);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para seleccionar los datos  de la tabla admtbl_permisos para el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos 
     */
    public function selectListTramite($sqlWhere='', $sqlValues=array() )
    {
        $sql = "select id_licencia_medica, fecha_inicio, fecha_fin, folio_finanzas, diagnostico, num_dias, folio_finanzas, folio  
                from admtbl_licencias_medicas
                ";
        if (!empty($sqlWhere))
            $sql .= " WHERE  $sqlWhere";                
        $sql .= ";";        
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);                
            } else {
                $qry->execute();                
            }
            //echo $sql;
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                //$tipo_licencia = ($data['tipo_licencia'] == 1)? "CON SUELDO":"SIN SUELDO";
                $datos[] = array(
                               'id_licencia_medica' => $data['id_licencia_medica'],
                               'fecha_inicio' => $data['fecha_inicio'],
                               'fecha_fin' => $data['fecha_fin'],
                               'folio_finanzas' => $data['folio_finanzas'],
                               'num_dias' => $data['num_dias'],
                               'diagnostico' => $data['diagnostico'],
                               'folio' => $data['folio'],                              
                               );
            }
            //echo count( $datos );
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }


    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_licencia_medica, a.curp, a.fecha_inicio, a.fecha_fin, a.num_dias, a.folio, a.diagnostico, a.observacion, a.fecha_reg, a.folio_finanzas, a.no_oficio, a.fecha_oficio, a.no_soporte, a.fecha_soporte, a.firmante_soporte, a.cargo_firmante,
                  b.curp, b.nombre, b.a_paterno, b.a_materno, b.fecha_nac, b.genero, b.rfc, b.cuip, b.folio_ife, b.mat_cartilla, b.licencia_conducir, b.pasaporte, b.id_estado_civil, b.id_tipo_sangre, b.id_nacionalidad, b.id_entidad, b.id_municipio, b.lugar_nac, b.id_status
                FROM admtbl_licencias_medicas a 
                 LEFT JOIN admtbl_datos_personales b ON a.curp=b.curp";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_licencia_medica' => $data['id_licencia_medica'],
                               'curp' => $data['curp'],
                               'fecha_inicio' => $data['fecha_inicio'],
                               'fecha_fin' => $data['fecha_fin'],
                               'num_dias' => $data['num_dias'],
                               'folio' => $data['folio'],
                               'diagnostico' => $data['diagnostico'],
                               'observacion' => $data['observacion'],
                               'fecha_reg' => $data['fecha_reg'],
                               'folio_finanzas' => $data['folio_finanzas'],
                               'no_oficio' => $data['no_oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'no_soporte' => $data['no_soporte'],
                               'fecha_soporte' => $data['fecha_soporte'],
                               'firmante_soporte' => $data['firmante_soporte'],
                               'cargo_firmante' => $data['cargo_firmante'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_licencias_medicas( curp, fecha_inicio, fecha_fin, num_dias, folio, diagnostico, observacion, fecha_reg, folio_finanzas, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante)
                VALUES(:curp, :fecha_inicio, :fecha_fin, :num_dias, :folio, :diagnostico, :observacion, :fecha_reg, :folio_finanzas, :no_oficio, :fecha_oficio, :no_soporte, :fecha_soporte, :firmante_soporte, :cargo_firmante);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":fecha_inicio" => $this->fecha_inicio, ":fecha_fin" => $this->fecha_fin, ":num_dias" => $this->num_dias, ":folio" => $this->folio, ":diagnostico" => $this->diagnostico, ":observacion" => $this->observacion, ":fecha_reg" => date("Y-m-d"), ":folio_finanzas" => $this->folio_finanzas, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_licencias_medicas
                   SET curp=:curp, fecha_inicio=:fecha_inicio, fecha_fin=:fecha_fin, num_dias=:num_dias, folio=:folio, diagnostico=:diagnostico, observacion=:observacion, fecha_reg=:fecha_reg, folio_finanzas=:folio_finanzas, no_oficio=:no_oficio, fecha_oficio=:fecha_oficio, no_soporte=:no_soporte, fecha_soporte=:fecha_soporte, firmante_soporte=:firmante_soporte, cargo_firmante=:cargo_firmante
                WHERE id_licencia_medica=:id_licencia_medica;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_licencia_medica" => $this->id_licencia_medica, ":curp" => $this->curp, ":fecha_inicio" => $this->fecha_inicio, ":fecha_fin" => $this->fecha_fin, ":num_dias" => $this->num_dias, ":folio" => $this->folio, ":diagnostico" => $this->diagnostico, ":observacion" => $this->observacion, ":fecha_reg" => $this->fecha_reg, ":folio_finanzas" => $this->folio_finanzas, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

   public function delete()
    {
        $sql = "DELETE FROM admtbl_licencias_medicas
                WHERE id_licencia_medica=:id_licencia_medica 
                AND curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_licencia_medica" => $this->id_licencia_medica, ":curp" => $this->curp));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
}


?>