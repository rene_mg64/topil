<?php
/**
 *
 */
class LogtblArmAlta
{
    public $id_alta; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $matricula; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $oficio; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $asunto; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_folio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_alta; /** @Tipo: timestamp, @Acepta Nulos: NO, @Llave: --, @Default: CURRENT_TIMESTAMP */
    public $usuario; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogtblArmArmamento; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logtbl_arm_armamento.class.php';
        $this->LogtblArmArmamento = new LogtblArmArmamento();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_alta)
    {
        $sql = "SELECT id_alta, matricula, oficio, fecha_oficio, asunto, fecha_folio, fecha_alta, usuario
                FROM logtbl_arm_alta
                WHERE id_alta=:id_alta;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_alta' => $id_alta));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_alta = $data['id_alta'];
            $this->matricula = $data['matricula'];
            $this->oficio = $data['oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->asunto = $data['asunto'];
            $this->fecha_folio = $data['fecha_folio'];
            $this->fecha_alta = $data['fecha_alta'];
            $this->usuario = $data['usuario'];

            $this->LogtblArmArmamento->select($this->matricula);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_alta, a.matricula, a.oficio, a.fecha_oficio, a.asunto, a.fecha_folio, a.fecha_alta, a.usuario,
                  b.matricula, b.serie, b.id_loc, b.id_foliod, b.id_situacion, b.id_propietario, b.id_estado, b.id_estatus, b.id_motivo_movimiento, b.id_marca
                FROM logtbl_arm_alta a 
                 LEFT JOIN logtbl_arm_armamento b ON a.matricula=b.matricula";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_alta' => $data['id_alta'],
                               'matricula' => $data['matricula'],
                               'oficio' => $data['oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'asunto' => $data['asunto'],
                               'fecha_folio' => $data['fecha_folio'],
                               'fecha_alta' => $data['fecha_alta'],
                               'usuario' => $data['usuario'],
                               'logtbl_arm_armamento_serie' => $data['serie'],
                               'logtbl_arm_armamento_id_loc' => $data['id_loc'],
                               'logtbl_arm_armamento_id_foliod' => $data['id_foliod'],
                               'logtbl_arm_armamento_id_situacion' => $data['id_situacion'],
                               'logtbl_arm_armamento_id_propietario' => $data['id_propietario'],
                               'logtbl_arm_armamento_id_estado' => $data['id_estado'],
                               'logtbl_arm_armamento_id_estatus' => $data['id_estatus'],
                               'logtbl_arm_armamento_id_motivo_movimiento' => $data['id_motivo_movimiento'],
                               'logtbl_arm_armamento_id_marca' => $data['id_marca'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     
    public function insert()
    {
        $sql = "INSERT INTO logtbl_arm_alta(id_alta, matricula, oficio, fecha_oficio, asunto, fecha_folio, fecha_alta, usuario)
                VALUES(:id_alta, :matricula, :oficio, :fecha_oficio, :asunto, :fecha_folio, :fecha_alta, :usuario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_alta" => $this->id_alta, ":matricula" => $this->matricula, ":oficio" => $this->oficio, ":fecha_oficio" => $this->fecha_oficio, ":asunto" => $this->asunto, ":fecha_folio" => $this->fecha_folio, ":fecha_alta" => $this->fecha_alta, ":usuario" => $this->usuario));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
*/

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_arm_alta(matricula, usuario)
                VALUES(:matricula, :usuario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":matricula" => $this->matricula, ":usuario" => $this->usuario));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_arm_alta
                   SET matricula=:matricula, oficio=:oficio, fecha_oficio=:fecha_oficio, asunto=:asunto, fecha_folio=:fecha_folio, fecha_alta=:fecha_alta, usuario=:usuario
                WHERE id_alta=:id_alta;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_alta" => $this->id_alta, ":matricula" => $this->matricula, ":oficio" => $this->oficio, ":fecha_oficio" => $this->fecha_oficio, ":asunto" => $this->asunto, ":fecha_folio" => $this->fecha_folio, ":fecha_alta" => $this->fecha_alta, ":usuario" => $this->usuario));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>