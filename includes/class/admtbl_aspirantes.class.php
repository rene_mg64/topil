<?php
/**
 *
 */
class AdmtblAspirantes
{
    public $curp; /** @Tipo: varchar(20), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $nombre; /** @Tipo: varchar(35), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_paterno; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_materno; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $genero; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_nac; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_estado_civil; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_corporacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_area; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_categoria; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $cargo; /** @Tipo: varchar(150), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_especialidad; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo_funcion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_region_adscrip; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_nivel_estudios; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $calle; /** @Tipo: varchar(60), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $num_ext; /** @Tipo: varchar(6), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $colonia; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $ciudad; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tel_fijo; /** @Tipo: varchar(15), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tel_movil; /** @Tipo: varchar(15), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_municipio_domi; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $status; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */
    public $stat_aspi; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatNivelEstudios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatCorporaciones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatAreas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatCategorias; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatEspecialidades; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatTipoFunciones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatRegiones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatEstadoCivil; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_nivel_estudios.class.php';
        require_once 'admcat_corporaciones.class.php';
        require_once 'admcat_areas.class.php';
        require_once 'admcat_categorias.class.php';
        require_once 'admcat_especialidades.class.php';
        require_once 'admcat_tipo_funciones.class.php';
        require_once 'admcat_regiones.class.php';
        require_once 'admcat_estado_civil.class.php';
        require_once 'admcat_municipios.class.php';
        $this->AdmcatNivelEstudios = new AdmcatNivelEstudios();
        $this->AdmcatCorporaciones = new AdmcatCorporaciones();
        $this->AdmcatAreas = new AdmcatAreas();
        $this->AdmcatCategorias = new AdmcatCategorias();
        $this->AdmcatEspecialidades = new AdmcatEspecialidades();
        $this->AdmcatTipoFunciones = new AdmcatTipoFunciones();
        $this->AdmcatRegiones = new AdmcatRegiones();
        $this->AdmcatEstadoCivil = new AdmcatEstadoCivil();
        $this->AdmcatMunicipios = new AdmcatMunicipios();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT curp, nombre, a_paterno, a_materno, genero, fecha_nac, id_estado_civil, id_area, id_categoria, cargo, id_especialidad, id_tipo_funcion, id_region_adscrip, id_nivel_estudios, calle, num_ext, colonia, ciudad, tel_fijo, tel_movil, id_municipio_domi, status, stat_aspi
                FROM admtbl_aspirantes
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->nombre = $data['nombre'];
            $this->a_paterno = $data['a_paterno'];
            $this->a_materno = $data['a_materno'];
            $this->genero = $data['genero'];
            $this->fecha_nac = $data['fecha_nac'];
            $this->id_estado_civil = $data['id_estado_civil'];
            $this->id_area = $data['id_area'];
            $this->id_categoria = $data['id_categoria'];
            $this->cargo = $data['cargo'];
            $this->id_especialidad = $data['id_especialidad'];
            $this->id_tipo_funcion = $data['id_tipo_funcion'];
            $this->id_region_adscrip = $data['id_region_adscrip'];
            $this->id_nivel_estudios = $data['id_nivel_estudios'];
            $this->calle = $data['calle'];
            $this->num_ext = $data['num_ext'];
            $this->colonia = $data['colonia'];
            $this->ciudad = $data['ciudad'];
            $this->tel_fijo = $data['tel_fijo'];
            $this->tel_movil = $data['tel_movil'];
            $this->id_municipio_domi = $data['id_municipio_domi'];
            $this->status = $data['status'];
            $this->stat_aspi = $data['stat_aspi'];

            $this->AdmcatNivelEstudios->select($this->id_nivel_estudios);
            $this->AdmcatAreas->select($this->id_area);
            $this->AdmcatCorporaciones->select($this->id_corporacion);
            $this->AdmcatCategorias->select($this->id_categoria);
            $this->AdmcatEspecialidades->select($this->id_especialidad);
            $this->AdmcatTipoFunciones->select($this->id_tipo_funcion);
            $this->AdmcatRegiones->select($this->id_region_adscrip);
            $this->AdmcatEstadoCivil->select($this->id_estado_civil);
            $this->AdmcatMunicipios->select($this->id_municipio_domi);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.nombre, a.a_paterno, a.a_materno, a.genero, a.fecha_nac, a.id_estado_civil, a.id_corporacion a.id_area, a.id_categoria, a.cargo, a.id_especialidad, a.id_tipo_funcion, a.id_region_adscrip, a.id_nivel_estudios, a.calle, a.num_ext, a.colonia, a.ciudad, a.tel_fijo, a.tel_movil, a.id_municipio_domi, a.status, a.stat_aspi,
                  b.nivel_estudios,
                  c.area, 
                  d.categoria,
                  e.especialidad,
                  f.tipo_funcion,
                  g.region,
                  h.estado_civil,
                  i.municipio, i.cabecera,
                  j.corporacion
                FROM admtbl_aspirantes a 
                 LEFT JOIN admcat_nivel_estudios b ON a.id_nivel_estudios=b.id_nivel_estudios
                 LEFT JOIN admcat_areas c ON a.id_area=c.id_area
                 LEFT JOIN admcat_categorias d ON a.id_categoria=d.id_categoria
                 LEFT JOIN admcat_especialidades e ON a.id_especialidad=e.id_especialidad
                 LEFT JOIN admcat_tipo_funciones f ON a.id_tipo_funcion=f.id_tipo_funcion
                 LEFT JOIN admcat_regiones g ON a.id_region_adscrip=g.id_region
                 LEFT JOIN admcat_estado_civil h ON a.id_estado_civil=h.id_estado_civil
                 LEFT JOIN admcat_municipios i ON a.id_municipio_domi=i.id_municipio
                 LEFT JOIN admcat_corporaciones j ON a.id_corporacion=j.id_corporacion";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],
                               'genero' => $data['genero'],
                               'fecha_nac' => $data['fecha_nac'],
                               'id_estado_civil' => $data['id_estado_civil'],
                               'estado_civil' => $data['estado_civil'],
                               'id_corporacion' => $data['id_corporacion'],
                               'corporacion' => $data['corporacion'],
                               'id_area' => $data['id_area'],
                               'area' => $data['area'],
                               'id_categoria' => $data['id_categoria'],
                               'categoria' => $data['categoria'],
                               'cargo' => $data['cargo'],
                               'id_especialidad' => $data['id_especialidad'],
                               'especialidad' => $data['especialidad'],
                               'id_tipo_funcion' => $data['id_tipo_funcion'],
                               'tipo_funcion' => $data['tipo_funcion'],
                               'id_region_adscrip' => $data['id_region_adscrip'],
                               'region_adscrip' => $data['region'],
                               'id_nivel_estudios' => $data['id_nivel_estudios'],
                               'nivel_estudios' => $data['nivel_estudios'],
                               'calle' => $data['calle'],
                               'num_ext' => $data['num_ext'],
                               'colonia' => $data['colonia'],
                               'ciudad' => $data['ciudad'],
                               'tel_fijo' => $data['tel_fijo'],
                               'tel_movil' => $data['tel_movil'],
                               'id_municipio_domi' => $data['id_municipio_domi'], 
                               'municipio_domi' => $data['municipio'],
                               'cabecera' => $data['cabecera'],
                               'status' => $data['status'],                              
                               'stat_aspi' => $data['stat_aspi'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);        
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }
        
        return $datos;
    }
    
    /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {    
        $sql = "SELECT a.curp, a.nombre, a.a_paterno, a.a_materno, a.genero, a.fecha_nac, a.id_corporacion, a.id_area, a.id_categoria, a.cargo, a.id_especialidad, a.id_tipo_funcion, a.id_region_adscrip, a.status, a.stat_aspi, 
                    b.categoria, 
                    c.area, 
                    d.especialidad, 
                    e.region,
                    f.corporacion 
                FROM admtbl_aspirantes a 
                    LEFT JOIN admcat_categorias b ON a.id_categoria=b.id_categoria 
                    LEFT JOIN admcat_areas c ON a.id_area=c.id_area 
                    LEFT JOIN admcat_especialidades d ON a.id_especialidad=d.id_especialidad 
                    LEFT JOIN admcat_regiones e ON a.id_region_adscrip=e.id_region
                    LEFT JOIN admcat_corporaciones f ON a.id_corporacion=f.id_corporacion";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],
                               'genero' => $data['genero'],
                               'fecha_nac' => $data['fecha_nac'], 
                               'id_corporacion' => $data['id_corporacion'],
                               'corporacion' => $data['corporacion'],
                               'id_area' => $data['id_area'],
                               'area' => $data['area'],
                               'id_categoria' => $data['id_categoria'],
                               'categoria' => $data['categoria'],
                               'cargo' => $data['cargo'],
                               'id_especialidad' => $data['id_especialidad'],
                               'especialidad' => $data['especialidad'],
                               'id_tipo_funcion' => $data['id_tipo_funcion'],
                               'tipo_funcion' => $data['tipo_funcion'],
                               'id_region_adscrip' => $data['id_region_adscrip'],
                               'region' => $data['region'],
                               'status' => $data['status'],
                               'stat_aspi' => $data['stat_aspi']
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT COUNT(*) 
                FROM admtbl_aspirantes a 
                    LEFT JOIN admcat_categorias b ON a.id_categoria=b.id_categoria 
                    LEFT JOIN admcat_areas c ON a.id_area=c.id_area 
                    LEFT JOIN admcat_especialidades d ON a.id_especialidad=d.id_especialidad 
                    LEFT JOIN admcat_regiones e ON a.id_region_adscrip=e.id_region
                    LEFT JOIN admcat_corporaciones f ON a.id_corporacion=f.id_corporacion";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }            
            $total_reg = $qry->fetchColumn();
            
            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_aspirantes(curp, nombre, a_paterno, a_materno, genero, fecha_nac, id_estado_civil, id_corporacion, id_area, id_categoria, cargo, id_especialidad, id_tipo_funcion, id_region_adscrip, id_nivel_estudios, calle, num_ext, colonia, ciudad, tel_fijo, tel_movil, id_municipio_domi, status, stat_aspi)
                VALUES(:curp, :nombre, :a_paterno, :a_materno, :genero, :fecha_nac, :id_estado_civil, :id_corporacion, :id_area, :id_categoria, :cargo, :id_especialidad, :id_tipo_funcion, :id_region_adscrip, :id_nivel_estudios, :calle, :num_ext, :colonia, :ciudad, :tel_fijo, :tel_movil, :id_municipio_domi, 1, 1);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":genero" => $this->genero, ":fecha_nac" => $this->fecha_nac, ":id_estado_civil" => $this->id_estado_civil, ":id_corporacion" => $this->id_corporacion, ":id_area" => $this->id_area, ":id_categoria" => $this->id_categoria, ":cargo" => $this->cargo, ":id_especialidad" => $this->id_especialidad, ":id_tipo_funcion" => $this->id_tipo_funcion, ":id_region_adscrip" => $this->id_region_adscrip, ":id_nivel_estudios" => $this->id_nivel_estudios, ":calle" => $this->calle, ":num_ext" => $this->num_ext, ":colonia" => $this->colonia, ":ciudad" => $this->ciudad, ":tel_fijo" => $this->tel_fijo, ":tel_movil" => $this->tel_movil, ":id_municipio_domi" => $this->id_municipio_domi));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_aspirantes
                   SET nombre=:nombre, a_paterno=:a_paterno, a_materno=:a_materno, genero=:genero, fecha_nac=:fecha_nac, id_estado_civil=:id_estado_civil, id_corporacion=:id_corporacion, id_area=:id_area, id_categoria=:id_categoria, cargo=:cargo, id_especialidad=:id_especialidad, id_tipo_funcion=:id_tipo_funcion, id_region_adscrip=:id_region_adscrip, id_nivel_estudios=:id_nivel_estudios, calle=:calle, num_ext=:num_ext, colonia=:colonia, ciudad=:ciudad, tel_fijo=:tel_fijo, tel_movil=:tel_movil, id_municipio_domi=:id_municipio_domi
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":genero" => $this->genero, ":fecha_nac" => $this->fecha_nac, ":id_estado_civil" => $this->id_estado_civil, ":id_corporacion" => $this->id_corporacion, ":id_area" => $this->id_area, ":id_categoria" => $this->id_categoria, ":cargo" => $this->cargo, ":id_especialidad" => $this->id_especialidad, ":id_tipo_funcion" => $this->id_tipo_funcion, ":id_region_adscrip" => $this->id_region_adscrip, ":id_nivel_estudios" => $this->id_nivel_estudios, ":calle" => $this->calle, ":num_ext" => $this->num_ext, ":colonia" => $this->colonia, ":ciudad" => $this->ciudad, ":tel_fijo" => $this->tel_fijo, ":tel_movil" => $this->tel_movil, ":id_municipio_domi" => $this->id_municipio_domi));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n que actualiza el status del aspirante para marcarlo como personal activo
     * @param string $curp, curp del aspirante a modificar
     * @return boolean true si el proceso es satisfactorio 
     */ 
    public function updStatus($curp)
    {
        $sql = "UPDATE admtbl_aspirantes
                   SET status=2, stat_aspi=3
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $curp));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n que actualiza el status del aspirante para marcarlo como baja
     * @param string $curp, curp del aspirante a dar de baja
     * @return boolean true si el proceso es satisfactorio 
     */ 
    public function delete($curp)
    {
        $sql = "UPDATE admtbl_aspirantes
                   SET status=0
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $curp));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
}


?>