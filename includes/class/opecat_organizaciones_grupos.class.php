<?php
/**
 *
 */
class OpecatOrganizacionesGrupos
{
    public $id_organizacion_gpo; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $organizacion_grupo; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */
    public $fecha_creacion; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $observaciones; /** @Tipo: varchar(200), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_organizacion_gpo)
    {
        $sql = "SELECT id_organizacion_gpo, organizacion_grupo, fecha_creacion, observaciones
                FROM opecat_organizaciones_grupos
                WHERE id_organizacion_gpo=:id_organizacion_gpo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_organizacion_gpo' => $id_organizacion_gpo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_organizacion_gpo = $data['id_organizacion_gpo'];
            $this->organizacion_grupo = $data['organizacion_grupo'];
            $this->fecha_creacion = $data['fecha_creacion'];
            $this->observaciones = $data['observaciones'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_organizacion_gpo, a.organizacion_grupo, a.fecha_creacion, a.observaciones
                FROM opecat_organizaciones_grupos a ";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_organizacion_gpo' => $data['id_organizacion_gpo'],
                               'organizacion_grupo' => $data['organizacion_grupo'],
                               'fecha_creacion' => $data['fecha_creacion'],
                               'observaciones' => $data['observaciones'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_organizaciones_grupos(id_organizacion_gpo, organizacion_grupo, fecha_creacion, observaciones)
                VALUES(:id_organizacion_gpo, :organizacion_grupo, :fecha_creacion, :observaciones);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_organizacion_gpo" => $this->id_organizacion_gpo, ":organizacion_grupo" => $this->organizacion_grupo, ":fecha_creacion" => $this->fecha_creacion, ":observaciones" => $this->observaciones));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_organizaciones_grupos
                   SET organizacion_grupo=:organizacion_grupo, fecha_creacion=:fecha_creacion, observaciones=:observaciones
                WHERE id_organizacion_gpo=:id_organizacion_gpo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_organizacion_gpo" => $this->id_organizacion_gpo, ":organizacion_grupo" => $this->organizacion_grupo, ":fecha_creacion" => $this->fecha_creacion, ":observaciones" => $this->observaciones));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>