;(function($){
    $.fn.xGrid = function(options_user){
        var defaults = {
            xIdGrid             : '', //Id del contenedor del grid...
            xNameId             : '', //Nombre del id del contenedor del grid...
            xWidth              : 0, //-- Ancho del grid...
            xHeight             : 0, //-- Alto del grid...
            xIdRowSelected      : "", //-- Id. de la fila seleccionada actualmente...
            xClassRowSelected   : "", //-- Color original de la fila anteriormente seleccionada...
            xColSort            : 1, //-- Columna actual por la que se ordenan los datos...
            xTypeSort           : "Asc", //-- Tipo de ordenaci�n (Ascendente o Descendente)...
            xTypeSelect         : 1, //-- Tipo de selector para las filas: 1=radiobutton, 2=checkbox...
            xTypeSearch         : 1, //-- Tipo de b�squeda: 1=Normal, 2=En tiempo real...
            xSetSearch          : false, //-- Controla el status de las b�squedas...
            xOnlySearch         : false, //-- Define si el grid es s�lo para b�squedas...
            xTotalRows          : 0, //-- Total de registros generados por la consulta actual...
            xRowsDisplay        : 50, //-- Registros mostrados por p�gina...
            xTotalPage          : 0, //-- Cantidad de p�ginas generadas por la consulta actual...
            xCurrentPage        : 1, //-- P�gina actual de datos...
            xRowStartPage       : 0, //-- N�mero del registro por el que comienza la p�gina acutal...
            xLoadDataStart      : 0,    
            xUrlData            : "", //-- Url del origen de los datos...
            xAjaxParam          : "", //-- Contiene los par�metros que se enviar�n a la url por el m�todo GET...
            xTypeDataAjax       : "json", //-- Tipo de contenido que se obtendr� con la consulta ajax...            
        };
        
        var options = $.extend(defaults, options_user);
        
        //-----------------------------------------------------//
        //------- Definici�n y Funcionalidad del Grid ---------//
        //-----------------------------------------------------//
        var myGrid = $(this);
        options.xIdGrid = '#'+myGrid.attr('id');//Se obtiene el id del contenedor del grid...
        options.xNameId = myGrid.attr('id');//Se obtiene solo el nombre del id del contenedor del grid...
        
        options.xWidth = $(options.xIdGrid).width();
        options.xHeight= $(options.xIdGrid).height();
        
        //-- Controla el cambio de dimensiones del documento(p�gina)...
        $(window).resize(function(){
            fxSetWidth(); 
        });
                       
        //-- Control la opci�n de b�squeda...
        $(options.xIdGrid + ' table.xGrid-tbSearch :text').attr('id', options.xNameId + '-txtBuscar');
        $(options.xIdGrid + '-txtBuscar').keyup(function(evt){
            evt.preventDefault();
            //-- B�queda normal...
            if( options.xTypeSearch == 1 ){
                if( evt.keyCode == 13 && (($(this).val() != '' && options.xTotalRows > 0) || options.xSetSearch == true) )                
                    fxSearchData();
            }
            //-- B�queda en tiempo real...
            else{
                if( $(this).val().length > 3 || ($(this).val().length > 3 && !options.xSetSearch) || ($(this).val() == '' && options.xSetSearch && !options.xOnlySearch) )
                    fxSearchData();
                else if( $(this).val().length <= 3 && options.xOnlySearch ){
                    options.xSetSearch = false;
                    $(options.xIdGrid + ' div.xGrid-dvBody').html('');
                    options.xTotalRows = 0;
                    //-- Se desplega la informaci�n de la paginaci�n de los datos...
                    fxShowInfPage();
                    //-- Se resetean las variables de control...
                    options.xIdRowSelected = 0;
                    options.xClassRowSelected = "";    
                }
            }
            
            if( evt.keyCode == 27 ){
                $(this).val('');
                if( options.xTypeSearch == 2 && options.xSetSearch && !options.xOnlySearch )
                    fxSearchData();
                else if( options.xOnlySearch ){
                    options.xSetSearch = false;
                    $(options.xIdGrid + ' div.xGrid-dvBody').html('');
                    options.xTotalRows = 0;
                    //-- Se desplega la informaci�n de la paginaci�n de los datos...
                    fxShowInfPage();
                    //-- Se resetean las variables de control...
                    options.xIdRowSelected = 0;
                    options.xClassRowSelected = "";
                }
            }
        });
        $(options.xIdGrid + ' table.xGrid-tbSearch a').click(function(){
            if( ($(options.xIdGrid + '-txtBuscar').val() != '' && options.xTotalRows>0) || options.xSetSearch == true )                
                fxSearchData();
        });
        options.xSetSearch = ( $(options.xIdGrid + '-txtBuscar').val() != '' ) ? true : false;
        
        //-- Ajusta las medidas de la tabla de columnas del grid...
        $(options.xIdGrid + ' table.xGrid-tbCols').attr("id", options.xNameId + '-tbCols');       
                
        //-- Detalla las columnas del grid...
        xtbCols = $(options.xIdGrid + '-tbCols th');
        if( options.xTypeSelect == 2 ){
            xtbCols.eq(0).html( '<input type="checkbox" name="' + options.xNameId + '-ckMain" id="' + options.xNameId + '-ckMain" />' );
            xtbCols.eq(0).find('input:checkbox').click(function(){
                if( $(options.xIdGrid + ' table.xGrid-tbBody').length > 0 )
                    fxSelectAllRow($(this));
            });
        }        
        xtbCols.eq(options.xColSort).addClass('xGrid-tbCols-ColSort' + options.xTypeSort);
        xtbCols.click(function(){
            if( options.xTotalRows > 0 && $(this).hasClass('xGrid-tbCols-ColSortable') ){
                var xcol_click = $(this).index();
                fxSortData(xcol_click);
            }
        });
        
        //-- Controla la selecci�n de una fila de datos...
        $(options.xIdGrid).delegate('table.xGrid-tbBody tr', 'click', function(){
            fxSetRowSelected($(this), 1);
        });
        $(options.xIdGrid).delegate('table.xGrid-tbBody input[type=checkbox]', 'click', function(evt){
            fxSetRowSelected($(this).parents('tr'), 2);
            evt.stopPropagation();
        });
            
        //-- Genera y ajusta el Footer del grid...        
        var html = '';
        html  = '<div id="' + options.xNameId + '-dvFooter" class="xGrid-dvFooter gradient">';
        html += '   <table id="' + options.xNameId + '-tbFooter" class="xGrid-tbFooter">';
        html += '       <tr>';
        html += '           <td id="' + options.xNameId + '-tbFooter-tdInfRows" style="text-align: left;">Mostrando: 0 a 0 de 0 registros</td>';
        html += '           <td class="xGrid-tbFooter-tdNavPag" style="padding-right: 1px;">';
        html += '               <table class="xGrid-tbFooter-tbNavPag">';
        html += '                   <tr>';
        html += '                       <td><a href="#" id="' + options.xNameId + '-btnNav-First" class="xGrid-tbFooter-tbNavPag-btnNav-D" rel="first" title="Primer p�gina..."> |&lt; </a></td>';         
        html += '                       <td><a href="#" id="' + options.xNameId + '-btnNav-Prev" class="xGrid-tbFooter-tbNavPag-btnNav-D" rel="prev" title="P�gina anterior..."> &lt; </a></td>';
        html += '                       <td style="padding: 0 3px 0 3px; vertical-align: middle;">';
        html += '                           <input type="text" id="' + options.xNameId + '-txtCurrentPage" value="0" maxlength="5" readonly="true" class="xGrid-dvFooter-txtCurrentPage" />';
        html += '                           /&nbsp;<span class="xGrid-tbFooter-spTotalPage" id="' + options.xNameId + '-spTotalPage" title="Total de p�ginas">0</span>';
        html += '                       </td>';
        html += '                       <td><a href="#" id="' + options.xNameId + '-btnNav-Next" class="xGrid-tbFooter-tbNavPag-btnNav-D" rel="next" title="Siguiente p�gina..."> &gt; </a></td>';
        html += '                       <td><a href="#" id="' + options.xNameId + '-btnNav-Last" class="xGrid-tbFooter-tbNavPag-btnNav-D" rel="last" title="�ltima p�gina..."> &gt;| </a></td>';
        html += '                   </tr>';
        html += '               </table>';
        html += '           </td>';
        html += '       </tr>';         
        html += '   </table>';
        html += '</div>';
        $(options.xIdGrid).append(html);
        //-- Asigna los eventos para la paginaci�n...
        $(options.xIdGrid + ' table.xGrid-tbFooter-tbNavPag a').click(function(){
            if( $(this).hasClass("xGrid-tbFooter-tbNavPag-btnNav-E") ){                
                fxNavPage( $(this).attr("rel") );
                $(this).blur();
            } 
        });
        $(options.xIdGrid + '-txtCurrentPage').focus(function(evt){                
            if( $(this).attr('readonly') == false )
                $(this).select();
        });
        $(options.xIdGrid + '-txtCurrentPage').keyup(function(evt){
            evt.preventDefault();
            if( evt.keyCode == 13 )
                fxGoPage();
            return false;            
        });           
        //-----------------------------------------------------//  
                      
        //-----------------------------------------------------//
        //---------------- Funciones Privadas -----------------//
        //-----------------------------------------------------//
        //-- Funci�n que controla la b�squeda dentro del grid...
        var fxSearchData = function(){
            options.xSetSearch = ( $(options.xIdGrid + '-txtBuscar').val() != '' ) ? true : false;
            options.xCurrentPage = 1;
            fxLoadData();
        };
        
        //-- Funci�n que contrla el ordenamiento del grid...
        var fxSortData = function(colSort){
            //-- Se respaldan los valores de las variables que controlan el orden del grid...
            var bkpColSort = options.xColSort;
            var bkpTypeSort = options.xTypeSort;
            //-- Se asignan los nuevos valores de ordenaci�n para que la funci�n fxLoadData los tome y cargue los datos...
            options.xColSort = colSort;                         
            if( colSort == bkpColSort )
                options.xTypeSort = ( options.xTypeSort == 'Asc' ) ? 'Desc' : 'Asc';
            else
                options.xTypeSort = 'Asc';
            if( fxLoadData() ){
                //-- Se le quita el estilo a la columna actual de ordenado...
                $(options.xIdGrid + '-tbCols th').eq(bkpColSort).removeClass('xGrid-tbCols-ColSort' + bkpTypeSort);   
                //-- Se asigna el estilo a la columna donde se di� click...               
                $(options.xIdGrid + '-tbCols th').eq(options.xColSort).addClass('xGrid-tbCols-ColSort' + options.xTypeSort);
            }
            else{
                options.xColSort = bkpColSort;
                options.xTypeSort = bkpTypeSort;
            }
        };
        
        //-- Funci�n que controla la paginaci�n del grid...
        var fxNavPage = function(xgo){
            switch(xgo){
                case "first":
                    if( options.xCurrentPage > 1 ){
                        options.xCurrentPage = 1;
                        options.xRowStartPage = 0;
                    } 
                break;
                case "prev":
                    if( options.xCurrentPage > 1 ){
                        options.xCurrentPage--;
                        options.xRowStartPage = ( (options.xCurrentPage - 1) * options.xRowsDisplay );
                    }
                break;
                case "next":
                    if( options.xCurrentPage < options.xTotalPage ){
                        options.xCurrentPage++;
                        options.xRowStartPage = ( (options.xCurrentPage - 1) * options.xRowsDisplay );
                    }
                break;
                case "last":
                    if( options.xCurrentPage < options.xTotalPage ){
                        options.xCurrentPage = options.xTotalPage;
                        options.xRowStartPage = ( (options.xCurrentPage - 1) * options.xRowsDisplay );
                    }
                break; 
            }
            
            fxLoadData();
        };
        
        //-- Funci�n para ir a una p�gina espec�fica de datos...
        var fxGoPage = function(){
            var pagGo = $(options.xIdGrid + '-txtCurrentPage').val();
            if( parseInt(pagGo) && (pagGo > 0 && pagGo <= options.xTotalPage) ){
                options.xCurrentPage = pagGo;
                options.xRowStartPage = ( (options.xCurrentPage - 1) * options.xRowsDisplay );
                fxLoadData();
            }
            else{
                $(options.xIdGrid + '-txtCurrentPage').val( options.xCurrentPage );
                $(options.xIdGrid + '-txtCurrentPage').blur();                
                //-- Mensaje...
                $(options.xIdGrid + ' div.xGrid-dvBody').html('<p style="color: #ff0000; margin: auto auto; padding: 5px; text-align: center; width: auto;">El dato introducido NO es V�lido</p>');
            }
        };
                
        //-- Funci�n que obtiene los datos via ajax y los muestra en el grid...
        var fxLoadData = function(){
            var bandResult = true;
            if( options.xUrlData != '' ){
                var txtSearch = $(options.xIdGrid + '-txtBuscar').val();
                $.ajax({
                    url: options.xUrlData + '?' + options.xAjaxParam,
                    data: { 'txtSearch': txtSearch, 
                            'colSort': options.xColSort, 
                            'typeSort': options.xTypeSort,
                            'rowsDisplay': options.xRowsDisplay,
                            'rowStart': options.xRowStartPage,
                            'IdGrid': options.xNameId },
                    type: 'get',
                    dataType: options.xTypeDataAjax,
                    async: true,
                    cache: false,
                    beforeSend: fxWaitLoading(),
                    success: function(dataHtml){            
                        //-- Se asignan los resultados obtenidos por la consulta ajax...
                        if( options.xTypeDataAjax == 'json' ){
                            $(options.xIdGrid + ' div.xGrid-dvBody').html(dataHtml.html_dat);
                            options.xTotalRows = dataHtml.total;
                        }
                        else if( options.xTypeDataAjax == 'html' ){
                            $(options.xIdGrid + ' div.xGrid-dvBody').html(dataHtml);
                            options.xTotalRows = ( $(options.xIdGrid + '-TotalRows').length >  0 ) ? $('#xGrid-TotalRows').val() : 0;
                        }
                        //-- Se desplega la informaci�n de la paginaci�n de los datos...
                        fxShowInfPage();
                        //-- Se asigna el estilo intercalado de cada fila...
                        $(options.xIdGrid + ' table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                        $(options.xIdGrid + ' table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                        //-- Se seleccionan todas las filas si el tipo de selecci�n es m�ltiple...                            
                        if( options.xTypeSelect == 2 ){
                            var chkBoxMain = $(options.xIdGrid + '-tbCols th').eq(0).find('input:checkbox');
                            if( chkBoxMain.is(':checked') )
                                fxSelectAllRow( chkBoxMain );
                        }
                        //-- Se resetean las variables de control...
                        options.xIdRowSelected = 0;
                        options.xClassRowSelected = "";
                    },
                    error: function(objeto, detalle, otroobj){
                        bandResult = false;
                        //-- Mensaje...
                        $(options.xIdGrid + ' div.xGrid-dvBody').html('<p style="color: #ff0000; margin: auto auto; padding: 5px; text-align: center; width: auto;">Lo sentimos pero ha ocurrido un error.<br />Detalles: "' + detalle + ', ' + otroobj + '"</p>');
                    }
                });               
            }
            else{
                bandResult = false;
                //-- Mensaje...
                $(options.xIdGrid + ' div.xGrid-dvBody').html('<p style="color: #ff0000; margin: auto auto; padding: 5px; text-align: center; width: auto;">A�n no ha especificado el origen de los datos para el Grid "' + options.xNameId + '".</p>');            
            }
            
            return bandResult;
        };
        
        //-- Funci�n que muestra el mensaje de espera mientras se cargan los datos...
        var fxWaitLoading = function(){
            var hgtBody = ($(options.xIdGrid + ' div.xGrid-dvBody').height() / 2) - 20;
            $(options.xIdGrid + ' div.xGrid-dvBody').html('<div class="xGrid-dvWait-LoadData" style="padding-top: ' + hgtBody + 'px;"> Cargando datos, por favor espere... </div>');
        };
        
        //-- Funci�n que selecciona la fila sobre la cual se da click...
        var fxSetRowSelected = function(idRow, tpClick){
            var id_row_currrent = options.xIdRowSelected;
            options.xIdRowSelected = idRow.attr('id');
            //-- Se restaura el estilo de la ultima fila seleccionada...
            if( id_row_currrent != 0 && options.xTypeSelect == 1 )
                $('#' + id_row_currrent).removeClass('xGrid-tbBody-trSelected').addClass(options.xClassRowSelected);
            
            //-- Marca la fila seleccionada dependiendo del tipo de componente...
            if( tpClick == 1 ){
                if( options.xTypeSelect == 1 )//- Radiobutton
                    idRow.find('input:radio').attr('checked', true);
                else if( options.xTypeSelect == 2 ){//- Checkbox
                    var chkRow = idRow.find('input:checkbox');
                    if( chkRow.is(':checked') == true ){
                        chkRow.attr('checked', false);
                        idRow.css('color', '#000000');
                    }
                    else{
                        chkRow.attr('checked', true);
                        idRow.css('color', '#1531ec');
                    }
                }
            }
            else if( tpClick == 2 && options.xTypeSelect == 2 ){
                var chkRow = idRow.find('input:checkbox');
                if( chkRow.is(':checked') == true )
                    idRow.css('color', '#1531ec');
                else
                    idRow.css('color', '#000000');
            }
            
            if( options.xTypeSelect == 1 ){
                //-- Se guarda el color de fondo actual de la fila seleccionada...
                options.xClassRowSelected = idRow.attr('class');
                //-- Se asigna el nuevo estilo a la fila selecionada actualmente...
                idRow.addClass('xGrid-tbBody-trSelected');
            }
        };
        
        //-- Funci�n que selecciona o deselecciona todas filas cuando el tipo de selecci�n definida es m�ltiple... 
        var fxSelectAllRow = function(obj){
            if( obj.is(':checked') ){
                $(options.xIdGrid + ' table.xGrid-tbBody tr').each(function(){
                    $(this).find('input[type=checkbox]').attr('checked', true);
                    $(this).css('color', '#1531ec');
                });
            }
            else{
                $(options.xIdGrid + ' table.xGrid-tbBody tr').each(function(){
                    $(this).find('input[type=checkbox]').attr('checked', false);
                    $(this).css('color', '#000000');
                });                    
            }
        };
        
        //-- Funci�n que redimensiona los componentes del grid...
        var fxSetWidth = function(){
            var _height = ( ($(window).height() - 150) < options.xHeight ) ? ($(window).height() - 150) : options.xHeight;
            var _width = ( $(window).width() < options.xWidth ) ? ($(window).width() - 10) : options.xWidth;
            if( _width < 900 )
                _width = 900;
            //-- Ajusta las medidas del Header del grid...
            $(options.xIdGrid+' div.xGrid-dvHeader').css('width', _width + 'px');
            //-- Ajusta las medidas del bloque de columnas del grid...
            $(options.xIdGrid + ' table.xGrid-tbCols').css('width', (_width - 17) + 'px');
            //-- Ajusta las medidas del Body del grid...
            $(options.xIdGrid + ' div.xGrid-dvBody').css('width', _width + 'px');
            if( options.xHeight > 250 && options.xHeight < 270 )
                $(options.xIdGrid + ' div.xGrid-dvBody').css('height', ($(window).height() - 320) + 'px');
            else
                $(options.xIdGrid + ' div.xGrid-dvBody').css('height', (_height - 65) + 'px');
            //-- Ajusta las medidas del Footer del grid...
            $(options.xIdGrid + ' div.xGrid-dvFooter').css('width', _width + 'px');
        };
        
        //-- Funci�n que controla la informaci�n y componentes del Footer...        
        var fxShowInfPage = function(){
            //-- Bloque informativo de registros mostrados...
            var xLimit = 0;
            if( (options.xRowStartPage + options.xRowsDisplay) > options.xTotalRows )
                xLimit = options.xTotalRows;
            else
                xLimit = options.xRowStartPage + options.xRowsDisplay;
            $(options.xIdGrid + '-tbFooter-tdInfRows').html("Mostrando " + (options.xRowStartPage + 1) + " a " + xLimit + " de " + options.xTotalRows + " registros" );
            
            //-- Bloque de los botones de paginaci�n...
            if( options.xTotalRows == 0 ){
                options.xCurrentPage = 0;
                options.xTotalPage = 0;                    
            }
            else if( options.xTotalRows <= options.xRowsDisplay )
                options.xTotalPage = 1;
            else{
                options.xTotalPage = ( (options.xTotalRows % options.xRowsDisplay) > 0 ) ? parseInt(options.xTotalRows / options.xRowsDisplay) + 1 : options.xTotalRows / options.xRowsDisplay;
            }
            if( options.xTotalRows > 0 && options.xCurrentPage == 0 )
                options.xCurrentPage = 1;
            $(options.xIdGrid + '-txtCurrentPage').val( options.xCurrentPage );
            $(options.xIdGrid + '-spTotalPage').html( options.xTotalPage );
            if( options.xTotalRows > 0 ){
                $(options.xIdGrid + '-txtCurrentPage').attr('readonly', false);
            }
            else{
                $(options.xIdGrid + '-txtCurrentPage').attr('readonly', true);
            }
            //-- Asigna los estilos a los botones...
            var btnFirst= $(options.xIdGrid + '-btnNav-First');     btnFirst.removeClass('xGrid-tbFooter-tbNavPag-btnNav-E');    btnFirst.removeClass('xGrid-tbFooter-tbNavPag-btnNav-D');
            var btnPrev = $(options.xIdGrid + '-btnNav-Prev');      btnPrev.removeClass('xGrid-tbFooter-tbNavPag-btnNav-E');     btnPrev.removeClass('xGrid-tbFooter-tbNavPag-btnNav-D');
            var btnNext = $(options.xIdGrid + '-btnNav-Next');      btnNext.removeClass('xGrid-tbFooter-tbNavPag-btnNav-E');     btnNext.removeClass('xGrid-tbFooter-tbNavPag-btnNav-D');
            var btnLast = $(options.xIdGrid + '-btnNav-Last');      btnLast.removeClass('xGrid-tbFooter-tbNavPag-btnNav-E');     btnLast.removeClass('xGrid-tbFooter-tbNavPag-btnNav-D');
            if( options.xTotalPage == 0 ){
                btnFirst.addClass('xGrid-tbFooter-tbNavPag-btnNav-D');
                btnPrev.addClass('xGrid-tbFooter-tbNavPag-btnNav-D');
                btnNext.addClass('xGrid-tbFooter-tbNavPag-btnNav-D');
                btnLast.addClass('xGrid-tbFooter-tbNavPag-btnNav-D');
            }
            else if( options.xCurrentPage == 1 ){
                btnFirst.addClass('xGrid-tbFooter-tbNavPag-btnNav-D');
                btnPrev.addClass('xGrid-tbFooter-tbNavPag-btnNav-D');
                if( options.xTotalPage > 1 ){
                    btnNext.addClass('xGrid-tbFooter-tbNavPag-btnNav-E');
                    btnLast.addClass('xGrid-tbFooter-tbNavPag-btnNav-E');
                }
                else{
                    btnNext.addClass('xGrid-tbFooter-tbNavPag-btnNav-D');
                    btnLast.addClass('xGrid-tbFooter-tbNavPag-btnNav-D');
                }
            }
            else if( options.xCurrentPage > 1 && options.xCurrentPage < options.xTotalPage ){
                btnFirst.addClass('xGrid-tbFooter-tbNavPag-btnNav-E');
                btnPrev.addClass('xGrid-tbFooter-tbNavPag-btnNav-E');
                btnNext.addClass('xGrid-tbFooter-tbNavPag-btnNav-E');
                btnLast.addClass('xGrid-tbFooter-tbNavPag-btnNav-E');
            }
            else if( options.xCurrentPage == options.xTotalPage ){
                btnFirst.addClass('xGrid-tbFooter-tbNavPag-btnNav-E');
                btnPrev.addClass('xGrid-tbFooter-tbNavPag-btnNav-E');
                btnNext.addClass('xGrid-tbFooter-tbNavPag-btnNav-D');
                btnLast.addClass('xGrid-tbFooter-tbNavPag-btnNav-D');
            }            
        };
        //-----------------------------------------------------//
        
        //-----------------------------------------------------//
        //------------- Inicializaci�n del Grid ---------------//
        //-----------------------------------------------------//
        //-- Se ajustan las medidas de los componentes del grid...
        fxSetWidth();
        
        //-- Carga los datos al iniciar si as� lo define el usuario...
        if( options.xLoadDataStart == 1 )
            fxLoadData();
        
        //-----------------------------------------------------//
        
        //-----------------------------------------------------//
        //--------------- Funciones P�blicas ------------------//
        //-----------------------------------------------------//
        //-- Funci�n que devuelve el Id de la fila seleccionada actualmente...
        this.getIdRow = function(){
            return options.xIdRowSelected;
        }
        //-- Funci�n para agregar par�metros que ser�n enviados a la url de datos...
        /*
        * NOTA: los par�metros agregados por esta funci�n, no sustituyen los par�mteros anteriores.
        */
        this.addParamAjax = function(params){
            options.xAjaxParam += params;
        }
        //-- Funci�n que reset�a la variable que contiene los par�metros enviados a la url de datos...
        this.resetAjaxParam = function(){
            options.xAjaxParam = '';
        }
        //-- Funci�n para actualizar los datos del grid...
        this.refreshGrid = function(){
            fxLoadData();
        }
        //-- Funci�n para redimensionar el grid...
        this.resizeGrid = function(){
            fxSetWidth();
        }
        
        return this;
    };
})(jQuery);