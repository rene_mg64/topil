$(document).ready(function(){
    //------------------------------------------------------------------//
    // Controla el scroll para seguir mostrando el header y el toolbar. //
    //------------------------------------------------------------------//
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });


    //------------------------------------------------------------------//
    //------------ Asignacion de funcionalidad a controles -------------//
    //------------------------------------------------------------------//
    //Agregar Mask txtHora
    $('#txtHoraInicio').mask("99:99",{placeholder:"_"});
    $('#txtHoraFin').mask("99:99",{placeholder:"_"});

    $('#txtFechaConflicto').mask("99/99/9999",{placeholder:"_"});
    $('#txtFechaConflicto').datepicker({
        yearRange: 'c-1:c',
    });

    // Control para la carga dinámica de municipios
    $('#cbxIdRegion').change(function(){        
        obtenerMunicipios($(this).val());  
    });    
    
    //------------------------------------------------------------------//
    //- Convierte a mayúsculas el contenido de los textbox y textarea. -//
    //------------------------------------------------------------------//
    $('#txtLocalidad').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtDirigentes').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });


    //------------------------------------------------------------------//
    //----------- Configuracion del editor de texto TINYMCE ------------//
    //------------------------------------------------------------------//
    tinymce.init({
        selector: "#txtAreaHechosHtml",
        language : 'es',
        plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste fullpage textcolor"
        ],
    
        toolbar1: "fontselect fontsizeselect forecolor | bullist numlist | table | hr removeformat |  spellchecker | searchreplace   undo redo | inserttime|",
        toolbar2: "preview print | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | cut copy paste ",
    
        menubar: false,
        toolbar_items_size: 'big'
    });

    //------------------------------------------------------------------//
    //-------- Asignacion de validacion para el formulario -------------//
    //------------------------------------------------------------------//
    $('#frmConflictosSociales').validate({
        rules: {
            txtFechaFechaConflicto: 'required',
            cbxIdRegion: {
                required: true,
                min: 1,
            },
            cbxIdMunicipio: {
                required: true,
                min: 1,
            },
            cbxIdTipoConflicto: {
                required: true,
                min: 1,
            },
            cbxIdTipoMovimiento: {
                required: true,
                min: 1,
            },
            cbxIdOrganizacionGpo: {
                required: true,
                min: 1,
            },
        },
        messages:{
            txtFechaFechaConflicto: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxIdRegion: {
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxIdMunicipio: {
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxIdTipoConflicto: {
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxIdTipoMovimiento: {
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxIdOrganizacionGpo: {
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('error').addClass('success');
        }
    });

    //------------------------------------------------------------------//
    //----- Asignacion de funcionalidad de los Botones de Acction  -----//
    //------------------------------------------------------------------//
    // Acción del botón Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmConflictosSociales').validate().form();
        if (valida) {
            var frmPreg = $(getHTMLMensaje('¿Está seguro de guardar los cambios realizados?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $('#frmConflictosSociales').submit();
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });
        }
    });
    
});

function obtenerMunicipios(id){    
    $.ajax({
        url: xDcrypt($('#hdnUrlMpio').val()),
        data: {'id_region': id},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#cbxIdMunicipio').html('<option>Cargando municipios...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#cbxIdMunicipio').html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}