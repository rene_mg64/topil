$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });


/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/



/********************************************************************/
/***********  Configuracion del editor de texto TINYMCE  ************/
/********************************************************************/
tinymce.init({
        selector: "#txtArea=======",
        language : 'es',
        plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste fullpage textcolor"
        ],

        toolbar1: "fontselect fontsizeselect forecolor | bullist numlist | table | hr removeformat |  spellchecker | searchreplace   undo redo | link unlink | inserttime|",
        toolbar2: "preview print | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | cut copy paste ",

        menubar: false,
        toolbar_items_size: 'big'
});



/********************************************************************/
/** Convierte a mayúsculas el contenido de los textbox y textarea. **/
/********************************************************************/




/********************************************************************/
/********* Asignacion de validacion para los controles **************/
/********************************************************************/

/***** Aplicación de las reglas de validación de los campos del formulario *****/
  $('#frmXXXX').validate({
      rules: {
      /***** Reglas de validacion para el formulario de xxxxx *****/


     },
     messages:{
     /***** Mensajes de validacion para el formulario de xxxxx *****/


     },
      errorClass: "help-inline",
      errorElement: "span",
      highlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('success').addClass('error');
      },
        unhighlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('error').addClass('success');
      }
  });

/***** Fin del document.ready *****/
});