var xGrid;
$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });


/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/
/*** Ajusta el tamaño del contenedor del grid... ***/
    //$('#dvGridVehiculos').css('height', ($(window).height() - 300) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGridVehiculos').xGrid({
        xColSort: 0,
        xTypeSort: 'Desc',
        xLoadDataStart: 1,
        xRowsDisplay: 10,
        xTypeSelect: 0,
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xAjaxParam : "id_folio_incidente="+$('#id_folio_incidente').val(),
        xTypeDataAjax: 'json'
    });

/********************************************************************/
/** Convierte a mayúsculas el contenido de los textbox y textarea. **/
/********************************************************************/
    $('#txtModelo,#txtColor,#txtTipo,#txtNumeroSerie,#txtNumeroMotor,#txtNumeroPlacas,#txtOrigenPlacas,#txtEmpresa,#txtTipoCarga,#txtAreaObservaciones').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });


/********************************************************************/
/********* Asignacion de validacion para los controles **************/
/********************************************************************/
/***** Aplicación de las reglas de validación de los campos del formulario *****/
    $('#frmIncidentesVehiculos').validate({
        rules: {
            /***** INICIO de Reglas de validacion para el formulario *****/
            txtIdVehiculo:{
                required:true,digits:true,
            },
            cbxIdRolVehiculo:{
                required:true,min: 1,
            },
            cbxIdMarca:{
                required:true,min: 1,
            },
            cbxIdCategoria:{
                required:true,min: 1,
            },
            txtModelo:{
                required:true,
            },
            txtColor:{
                required:true,
            },
            txtTipo:{required:true,
            },
            txtNumeroSerie:{
                required:true,
            },
            txtNumeroMotor:{
                required:true,
            },
            rbnExranjero:{
                required:true,min: 1,
            },
            txtOrigenPlacas:{
                required:true,
            },
            txtNumeroPlacas:{
                required:true,
            },
            cbxIdEmpresa:{
                required:true,min: 1,
            },
            txtEmpresa:{
                required:true,
            },
            cbxIdTipoServicio:{
                required:true,min: 1,
            },
            cbxIdSubtipoServicio:{
                required:true,min: 1,
            },
            txtTipoCarga:{
                required:true,
            },
            txtAreaObservaciones:{
                required:true,
            },
        /***** FIN de Reglas de validacion para el formulario *****/
        },
        messages:{
        /***** INICIO de Mensajes de validacion para el formulario *****/
            txtIdVehiculo:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                digits: '<span class="ValidateError" title="Este campo sólo acepta digitos"></span>',
            },
            cbxIdRolVehiculo:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxIdMarca:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxIdCategoria:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtModelo:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtColor:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtTipo:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtNumeroSerie:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtNumeroMotor:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            rbnExranjero:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtOrigenPlacas:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtNumeroPlacas:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxIdEmpresa:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtEmpresa:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxIdTipoServicio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxIdSubtipoServicio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtTipoCarga:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtAreaObservaciones:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
        /***** FIN de Mensajes de validacion para el formulario *****/
        },
          errorClass: "help-inline",
          errorElement: "span",
          highlight: function (element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
        },
            unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('error').addClass('success');
        }
    /***** Fin de validacion *****/
    });


/********************************************************************/
/***** Control del formulario para agregar/modifiar un vehiculo *****/
/********************************************************************/
    $('#dvFormIncidentesVehiculos').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        modal: true,
        resizable: false,
        draggable: false,        
        width: 700,
        buttons: {
            'Guardar Vehiculo': function(){
                guardarVehiculo();                
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        },
    });
    
    //mostrar el formulario de alta de vehiculos
    $("#btnAgregar, .classModificar").live("click", function(e){
        if( $(this).attr('id') != 'btnAgregar' ){            
            $("#id_vehiculo").val( $(this).attr('id').substring(4) );
        }else{
            $("#id_vehiculo").val('');
        }
        $.ajax({
            url: xDcrypt( $('#hdnUrlVeh').val() ),                          
            dataType: 'json',
            data: { 'id_vehiculo' : $("#id_vehiculo").val(), 'id_folio_incidente' : $("#id_folio_incidente").val() },
            type: 'POST',
            async: true,
            cache: false,            
            success: function ( xdata ) {                     
                $("#dvIncVehCont").html( xdata.html );
                $('#dvFormIncidentesVehiculos').dialog('option','title',"Datos del Vehiculo");   
                $('#dvFormIncidentesVehiculos').dialog('open');                                                                                                          
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        }); 
    });
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGridVehiculos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGridVehiculos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');



/********************************************************************/
/****** Asignacion de funcionalidad de los Botones de Acction  ******/
/********************************************************************/
    // Acción del botón Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmIncidentesVehiculos').validate().form();
        if (valida) {
            var frmPreg = $(getHTMLMensaje('¿Está seguro de guardar los cambios realizados?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $('#frmIncidentesVehiculos').submit();
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });
        }
    });


/***** Fin del document.ready *****/
});

function guardarVehiculo(){
    var valida = $('#frmIncidentesVehiculos').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('¿ Esta seguro de guardar los datos actuales ?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 380,
            buttons:{
                'Aceptar': function(){
                    $.ajax({
                        url: xDcrypt( $('#hdnUrlSave').val() ),
                        data: $('#frmIncidentesVehiculos').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        success: function (xdata) {
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                frmPreg.dialog('close');
                                xGrid.refreshGrid();  
                                $('#dvFormIncidentesVehiculos').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                                frmPreg.dialog('close');
                                alert(xdata.msj);
                            }
                        },
                        error: function(objeto, detalle, otroobj){                            
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}