var xGrid;
$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });


/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/
/*** Ajusta el tamaño del contenedor del grid... ***/
    $('#dvGridIncidentesMultimedia').css('height', ($(window).height() - 300) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGridIncidentesMultimedia').xGrid({
        xColSort: 1,
        xTypeSort: 'Desc',
        xLoadDataStart: 1,
        xRowsDisplay: 10,
        xTypeSelect: 0,
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xTypeDataAjax: 'json',
    });




/*Asignamos funcionalidad a los controles para realizar una captura mas rapida*/
    $('#cbxIdTipoMultimedia').change(function() {
       $('#txtArchivo').focus();
    });


/********************************************************************/
/***********  Configuracion del editor de texto TINYMCE  ************/
/********************************************************************/




/********************************************************************/
/** Convierte a mayúsculas el contenido de los textbox y textarea. **/
/********************************************************************/
$('#txtDescripcion,#txtArchivo').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });


/********************************************************************/
/********* Asignacion de validacion para los controles **************/
/********************************************************************/
/***** Aplicación de las reglas de validación de los campos del formulario *****/
$('#frmIncidentesMultimedia').validate({
      rules: {
      /***** INICIO de Reglas de validacion para el formulario *****/
        txtIdMultimedia:{
            required:true,digits:true,
        },
        cbxIdTipoMultimedia:{
            required:true,min: 1,
        },
        txtDescripcion:{
            required:true,
        },
        txtArchivo:{
            required:true,
        },
        fileUpdArchivo:{
            required:true,
        },
        
    /***** FIN de Reglas de validacion para el formulario *****/
    },
    messages:{
    /***** INICIO de Mensajes de validacion para el formulario *****/
        txtIdMultimedia:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            digits: '<span class="ValidateError" title="Este campo sólo acepta digitos"></span>',
        },
        cbxIdTipoMultimedia:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtDescripcion:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtArchivo:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        fileUpdArchivo:{
            required: '<span class="ValidateError" title="Este campo es obligatorio. Seleccione un archivo"></span>',
        },
    /***** FIN de Mensajes de validacion para el formulario *****/
    },
    errorClass: "help-inline",
    errorElement: "span",
    highlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('success').addClass('error');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('error').addClass('success');
    }
  /***** Fin de validacion *****/
});

/********************************************************************/
/***** Control del formulario para agregar/modifiar una persona *****/
/********************************************************************/
    $('#dvFormIncidentesMultimedia').dialog({
        show: {
            effect: "fade",duration: 500
        },
        hide: {
            effect: "puff",duration: 500
        },
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        buttons: {
            'Guardar Multimedia': function(){
                guardarMultimedia();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        },
        open: function(){
            resetFormulario();
            if ($('#txtIdMultimedia').val() == 0) {
                $(this).dialog('option', 'title', 'SISP :: Agregar Multimedia');
            } else{
                $(this).dialog('option', 'title', 'SISP :: Modificar Multimedia');
                //datosReferencia();
            }
        }
    });

    $('#btnAgregar').click(function(){
        $('#hdnIdRef').val('0');
        $('#hdnTypeOper').val('1');
        $('#dvFormIncidentesMultimedia').dialog('open');
    });


    $('#dvGridMultimedia a.lnkBtnOpcionGrid').live('click', function(){
        var xId = $(this).attr("rel").split('-');
        if( xId[0] == 'edt' ){
            $('#hdnIdRef').val( xId[1] );
            $('#hdnTypeOper').val('2');
            $('#dvFormReferencia').dialog('open');
        }
    });

    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGridMultimedia table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGridMultimedia table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');

/********************************************************************/
/****** Asignacion de funcionalidad de los Botones de Acction  ******/
/********************************************************************/
// Acción del botón Guardar
$('#btnGuardar').click(function(){
    var valida = $('#frmIncidentesMultimedia').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('¿Está seguro de guardar los cambios realizados?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 350,
            buttons:{
                'Aceptar': function(){
                    $('#frmIncidentesMultimedia').submit();
                },
                'Cancelar': function(){
                    $(this).dialog('close');
                }
            }
        });
    }
});


/***** Fin del document.ready *****/
});



function guardarMultimedia(){
    var valida = $('#frmIncidentesMultimedia').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('¿Está seguro de guardar los datos actuales?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 380,
            buttons:{
                'Aceptar': function(){
                    // Se genera el diálogo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando, por favor espere...</div></div>');
                    // Se procesan los datos vía ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSave').val()),
                        data: $('#frmIncidentesMultimedia').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // Díalogo de espera
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            });
                        },
                        success: function (xdata) {
                            // Cierra el diálogo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGridIncidentesMultimedia div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGridIncidentesMultimedia table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGridIncidentesMultimedia table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se cierra el formulario principal
                                $('#dvFormIncidentesMultimedia').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}


function resetFormulario(){
    $('#txtIdMultimedia').val(0);
    $('#cbxIdTipoMultimedia').val(0); 
    $('#txtArchivo').val('');
    $('#txtDescripcion').val('');
    $('#fileUpdArchivo').val('');

    //Removemos las clases de error por si las moscas
    $('span').removeClass('ValidateError'); //Con esto elimina los iconos de error en la validacion
    $('td').removeClass('error'); //Elimina la clase para quitar el contorno de color rojo
}