$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });


/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/
/*Agregar Mask txtHora*/
/*$('#txtHoraIncidente').mask("99:99",{placeholder:"_"});*/

    $('#txtFechaIncidente').datepicker({
        yearRange: 'c-1:c',
    });

    $('#RadioGroup_Baja').focus();
    //$("#txtFechaIncidente").val();
    //$("#txtFechaIncidente").focus();


   

/********************************************************************/
/***********  Configuracion del editor de texto TINYMCE  ************/
/********************************************************************/
tinymce.init({
        selector: "#txtHechosHtml",
        language : 'es',
        plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste fullpage textcolor"
        ],
        toolbar1: "fontselect fontsizeselect forecolor | bullist numlist | table | hr removeformat |  spellchecker | searchreplace   undo redo | link unlink | inserttime|",
        toolbar2: "preview print | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | cut copy paste ",
        menubar: false,
        toolbar_items_size: 'big'
});


/********************************************************************/
/** Convierte a mayúsculas el contenido de los textbox y textarea. **/
/********************************************************************/
$('#txtLocalidad,#txtLocalidad,#txtColonia,#txtCalle,#txtLatitud,#txtLongitud').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });


/********************************************************************/
/********* Asignacion de validacion para los controles **************/
/********************************************************************/

/***** Aplicación de las reglas de validación de los campos del formulario *****/
$('#frmIncidentes').validate({
    rules: {
    /***** Reglas de validacion para el formulario de xxxxx *****/
        /*
        txtIdFolioIncidente:{
            required:true,
        },
        */
        dvRadioGroup:{
            required:true,
            min:1,
        },
        txtFechaIncidente:{
            required:true,
        },
        txtHoraIncidente:{
            required:true,
        },
        cbxIdEstado:{
            required:true,
            min: 1,
        },
        cbxIdRegion:{
            required:true,
            min: 1,
        },
        cbxIdCuartel:{
            required:true,
            min: 1,
        },
        cbxIdMunicipio:{
            required:true,
            min: 1,
        },
        txtLocalidad:{
            required:true,
        },
        txtColonia:{
            required:true,
        },
        txtCalle:{
            required:true,
        },
        cbxIdAsunto:{
            required:true,
            min: 1,
        },
        cbxIdFuente:{
            required:true,
            min: 1,
        },
        txtHechosHtml:{
            required:true,
        },
    /***** FIN de Reglas de validacion para el formulario de xxxxx *****/
    },
    messages:{
    /***** Mensajes de validacion para el formulario de xxxxx *****/
       dvRadioGroup:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtFechaIncidente:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtHoraIncidente:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        cbxIdEstado:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        cbxIdRegion:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        cbxIdCuartel:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        cbxIdMunicipio:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtLocalidad:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },
        txtColonia:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },
        txtCalle:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        cbxIdAsunto:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },
        cbxIdFuente:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },
        txtHechosHtml:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },
    /***** FIN de Mensajes de validacion para el formulario de xxxxx *****/
    },
    errorClass: "help-inline",
    errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('error').addClass('success');
    }
/***** Fin de validacion *****/
 });



/********************************************************************/
/****** Asignacion de funcionalidad de los Botones de Acction  ******/
/********************************************************************/
// Acción del botón Guardar
$('#btnGuardar').click(function(){
    var valida = $('#frmIncidentes').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('¿Esta seguro de guardar los cambios realizados a Incidente?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 350,
            buttons:{
                'Aceptar': function(){
                    $('#frmIncidentes').submit();
                },
                'Cancelar': function(){
                    $(this).dialog('close');
                }
            }
        });
    }
});






/***** Fin del document.ready *****/
});




