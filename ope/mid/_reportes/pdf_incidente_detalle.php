<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */


 $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_panel');
 echo $urlSave."<br/><br/>";

echo dirname(__FILE__).'\\'."<br/><br/>";

 // get the HTML
 ob_start();
 include(dirname(__FILE__).'\\'. "html_incidente_detalle.php");
 $content = ob_get_clean();

 // convert in PDF  dirname(__FILE__).
 require_once('includes/lib/html2pdf/html2pdf.class.php');
 try
 {
     $html2pdf = new HTML2PDF('L', 'Letter', 'es', true, 'UTF-8', array(10, 5, 10, 5));
     $html2pdf->pdf->SetDisplayMode('fullpage');
     $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
     $html2pdf->Output('incidente_detalle.pdf');
 }
 catch(HTML2PDF_exception $e) {
     echo $e;
     exit;
 }




?>