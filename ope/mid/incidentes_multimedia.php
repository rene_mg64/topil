<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Incidencia Delictiva
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi?n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/opetbl_mid_incidentes_multimedia.class.php';
$objMult = new OpetblMidIncidentesMultimedia();

//-----------------------------------------------------------------//
//-- Bloque de definici?n de par?metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Incidentes->multimedia',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<link type="text/css" href="ope/mid/_css/mid.css" rel="stylesheet"/>',
                                   '<link type="text/css" href="ope/mid/_css/sty_mult.css" rel="stylesheet" />', 
                                   '<script type="text/javascript" src="includes/js/file_uploader/AjaxUpload.2.0.min.js"></script>',
                                   '<script type="text/javascript" src="ope/mid/_js/incidentes_multimedia.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din?mico...
//-----------------------------------------------------------------//
    $_SESSION['xIdIncidente'] = ( isset($_GET['id']) ) ? $objSys->decrypt($_GET['id']) : $_SESSION['xIdIncidente'];

    $urlRegresar = "index.php?m=" . $_SESSION["xIdMenu"]. '&mod=' . $objSys->encrypt('incidentes_panel') . '&id_folio_incidente=' . $_GET['id'];
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('perfil_criminal');
?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table>
            <tr>
                <td class="tdNombreModulo">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion">
                    <a href="<?php echo $urlRegresar?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>

    <div id="dvForm-Multimedia" class="dvForm-Data" style="min-height: 300px; padding-bottom: 50px;">
        <span class="dvForm-Data-pTitle">
            <img src="<?php echo PATH_IMAGES;?>icons/multimedia24.png" class="icono"/>
             Multimedia :: Incidente[<?php echo $_SESSION["xIdIncidente"];?>]
        </span>

        <!-- Boton de Agregar mediante una caja de Dialogo -->
        <div class="dvBar-Normal" style="margin: auto auto; margin-top: 7px; padding: 3px 1px 3px 1px; text-align: right; width: 98%;">
          <a href="#" id="btnAgregar" class="Tool-Bar-Btn gradient" style="height: 30px; width: 120px;" title="Agregar multimedia del incidente ...">
              <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none; margin-right: 4px; vertical-align: middle;" />Agregar
          </a>
        </div>
        
        <div id="dvListaContMult">            
            <?php
            $listMult = $objMult->selectAll("a.id_folio_incidente=" . $_SESSION["xIdIncidente"]);
            if( count($listMult) > 0 ){
                echo '<table id="tbListFiles" class="tbListFiles">';
                $mult_dir = 'ope/mid/_multimedia/';
                foreach( $listMult As $reg => $dato ){ //obtenemos un archivo y luego otro sucesivamente
                    echo '<tr>';
                    echo '  <td style="width: 100px;">';
                    $pos = strpos("jpg,jpeg,png,gif,bmp", $dato["multimedia_tipo"]);
                    if( $pos !== false ){
                        $img_mini = $mult_dir . $_SESSION["xIdIncidente"] . '/' . $_SESSION["xIdIncidente"] . '_' . $dato["id_multimedia"] . '-thb.' . $dato["multimedia_tipo"];
                        echo '  <div class="dvPreviewFile">
                                    <a href="#" rel="' . $dato["id_multimedia"] . '-' . $dato["multimedia_tipo"] . '" title="' . $dato["archivo"] . '">
                                        <img src="' .  $img_mini . '" alt="' . $dato["archivo"] . '" />
                                    </a>
                                </div>';
                    } else {
                        echo '  <div class="dvPreviewFile">
                                    <a href="#" rel="' . $dato["id_multimedia"] . '-' . $dato["multimedia_tipo"] . '" title="' . $dato["archivo"] . '">
                                        <img src="' .  PATH_IMAGES . 'icons/' . $dato["icono"] . '" alt="' . $dato["archivo"] . '" />
                                    </a>
                                </div>';
                    }                
                    echo '  </td>';
                    echo '  <td class="tdNameFile"><span class="spnNameFile">' . $dato["archivo"] . '</span></td>';
                    echo '  <td class="tdNameFile"><span class="spnNameFile">' . $dato["descripcion"] . '</span></td>';
                    echo '  <td class="tdBtnOption"><a href="#" class="btnDeleteFile" rel="' . $dato["id_multimedia"] . '" title="Eliminar el archivo ' . $archivo_normal . '">Eliminar</a></td>';
                    echo '</tr>';
                }
                echo '</table>';
            } else {
                echo '<p style="text-align: center; width: 100%;">No existe contenido multimedia para este incidente...</p>';
            }
            /*
            $archivo = '00000004_9.mp3';
            echo '<object classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95">
                    <param name="FileName" value="'.$archivo.'" />
                    <param name="autoplay" value="false" />
                    <object type="audio/mpeg" data="'.$archivo.'">
                        <param name="controller" value="true" />
                        <param name="autoplay" value="false" />
                        <a href="'.$archivo.'">Your browser is not able to embed this media here so click this link to play the file in an external application </a>
                    </object>
                </object>';
            */
            ?>            
        </div>        
    </div>

    <!-- INICIO FORMULARIO DE DIALOGO -->
    <div id="dvForm-Add" title="SISP :: Agregar archivo multimedia">
        <form id="frmAddFile" action="#" autocomplete="off" method="post" enctype="multipart/form-data">
        <fieldset class="fsetForm-Data">
             <table class="tbForm-Data" style="width: auto;">
                <tr>
                    <td rowspan="4" colspan="2" style="border-right: 1px solid #9e9c9b;">
                        <div id="dvPrevImg">
                        
                        </div>
                        <input type="hidden" name="hdnFileMultimed" id="hdnFileMultimed" value="" />
                    </td>
                </tr>
                <tr>
                   <td style="width: 160px;"><label for="txtNombreArchivo">Nombre del Archivo:</label></td>
                   <td class="validation">
                      <input type="text" name="txtNombreArchivo" id="txtNombreArchivo" style="width: 350px;" value="" readonly="true" />
                      <span class="pRequerido">*</span>
                    </td>
               </tr>
               <tr>
                   <td><label for="txtDescripcion">Descripci�n:</label></td>
                   <td class="validation">
                      <textarea name="txtDescripcion" id="txtDescripcion" tabindex="0" style="height: 50px; width: 350px;"></textarea>
                      <span class="pRequerido">*</span>
                    </td>
               </tr> 
             </table>
         </fieldset>
         <p style="font-size: 8pt; margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
         </form>
    </div>    
    <!-- FIN FORMULARIO DE DIALOGO -->
    
    <div id="dvForm-Prev" title="" style="text-align: center;">
        <div id="dvShowFile"></div>
    </div>
    
    <input type="hidden" id="hdnUrlUploadFile" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_upload_multimedia.php');?>" />
    <input type="hidden" id="hdnUrlDirTempUpload" value="<?php echo $objSys->encrypt('ope/mid/_upload/');?>" />
    <input type="hidden" id="hdnUrlSaveFile" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_reg_multimedia.php');?>" />
    <input type="hidden" id="hdnUrlDltFileTemp" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_dlt_mult_temp.php');?>" />
    <input type="hidden" id="hdnUrlDltFile" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_dlt_multimedia.php');?>" />
    <input type="hidden" id="hdnUrlLoadFile" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_load_multimedia.php');?>" />
    <?php
    $tipos = $objMult->OpecatMidMultimediaTipo->selectAll();
    $cant_reg = count($tipos);
    $list_type = '';
    foreach( $tipos As $reg => $tipo ){
        $list_type .= $tipo["multimedia_tipo"];
        if( $reg < ($cant_reg - 1) )
            $list_type .= ",";
    }
    ?>
    <input type="hidden" id="hdnTypeFile" value="<?php echo $list_type;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>