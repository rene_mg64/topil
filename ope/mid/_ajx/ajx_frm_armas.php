<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    
    include $path . 'includes/class/opetbl_mid_incidentes_armas.class.php';   
    include $path . 'includes/class/config/system.class.php';
    $objArm = new OpetblMidIncidentesArmas();
    $objSys = new System();  
    
    //se reciben parametros
    //id_folio_incidente
    $id_folio_incidente = $objSys->decrypt( $_POST["id_folio_incidente"] );
    //id_persona
    if( !empty( $_POST["id_arma"] ) ){
        $id_arma = $_POST["id_arma"];
        $objArm->select( $id_arma );
        $oper = 1;
    }else{
        $id_arma = $objArm->getIdUltimaArma() + 1;
        $oper = 2;
    }
    
    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//          
    $html='';  
                  
    $html .= '<form id="frmIncidentesArmas" method="post" enctype="multipart/form-data">';
    $html .= '  <fieldset id="fsetopetbl_mid_incidentes_armas" class="fsetForm-Data">';
    $html .= '   <!--<legend>opetbl_mid_incidentes_armas</legend>-->';
    $html .= '<table id="tbfrmopetbl_mid_incidentes_armas" class="tbForm-Data">';
    $html .= ' <tr>';
        $html .= ' <td class="descripcion"><label for="txtIdArma">Arma:</label></td>';
        $html .= ' <td class="validation">';
        $html .= '<input type="text" name="txtIdArma" id="txtIdArma" value="' . $id_arma . '" maxlength="" class="" readonly="true"/>';
        $html .= '<span class="pRequerido">*</span>';
        $html .= '  </td>';
    $html .= ' </tr>';
    $html .= ' <tr>';
        $html .= ' <td class="descripcion"><label for="cbxIdRolArma">Rol:</label></td>';
        $html .= ' <td class="validation">';
        $html .= '<select name="cbxIdRolArma" id="cbxIdRolArma" style="width: 300px;">';
        $html .= $objArm->OpecatMidArmasRoles->getCat_Armas_Roles( $objArm->id_arma_rol );
        $html .= '</select>';
        $html .= '<span class="pRequerido">*</span>';
        $html .= '  </td>';
    $html .= ' </tr>';
    $html .= ' <tr>';
        $html .= ' <td class="descripcion"><label for="cbxIdMarcaArma">Marca:</label></td>';
        $html .= ' <td class="validation">';
        $html .= '<select name="cbxIdMarcaArma" id="cbxIdMarcaArma" style="width: 300px;">';
        $html .= $objArm->OpecatMidArmasMarcas->getCat_mid_Armas_Marcas( $objArm->id_arma_marca );
        $html .= '</select>';
        $html .= '<span class="pRequerido">*</span>';
        $html .= '  </td>';
    $html .= ' </tr>';
    $html .= ' <tr>';
        $html .= ' <td class="descripcion"><label for="cbxIdTipoArma">Tipo:</label></td>';
        $html .= ' <td class="validation">';
        $html .= '<select name="cbxIdTipoArma" id="cbxIdTipoArma" style="width: 300px;">';
        $html .= $objArm->OpecatMidArmasTiposCalibres->OpecatMidArmasTipos->getCat_mid_Armas_Tipos( $objArm->id_arma_tipo );
        $html .= '</select>';
        $html .= '<span class="pRequerido">*</span>';
        $html .= '  </td>';
    $html .= ' </tr>';
    $html .= ' <tr>';
        $html .= ' <td class="descripcion"><label for="cbxIdCalibreArma">Calibre:</label></td>';
        $html .= ' <td class="validation">';
        $html .= '<select name="cbxIdCalibreArma" id="cbxIdCalibreArma" style="width: 300px;">';
        $html .= $objArm->OpecatMidArmasTiposCalibres->OpecatMidArmasCalibres->getCat_mid_Armas_Calibres( $objArm->id_arma_tipo, $objArm->id_arma_calibre );
        $html .= '</select>';
        $html .= '<span class="pRequerido">*</span>';
        $html .= '  </td>';
    $html .= ' </tr>';
    $html .= ' <tr>';
        $html .= ' <td class="descripcion"><label for="txtNumeroSerie">NumeroSerie:</label></td>';
        $html .= ' <td class="validation">';
        $html .= '<input type="text" name="txtNumeroSerie" id="txtNumeroSerie" style="width: 250px;" value="' . $objArm->numero_serie . '" maxlength="60" class="" />';
        $html .= '  </td>';
    $html .= ' </tr>';
    $html .= ' <tr>';
        $html .= ' <td class="descripcion"><label for="txtModelo">Modelo:</label></td>';
        $html .= ' <td class="validation">';
        $html .= '<input type="text" name="txtModelo" id="txtModelo" style="width: 250px;" value="' . $objArm->modelo . '" maxlength="60" class="" />';
        $html .= '  </td>';
    $html .= ' </tr>';
    $html .= ' <tr>';
        $html .= ' <td class="descripcion"><label for="txtMatricula">Matricula:</label></td>';
        $html .= ' <td class="validation">';
        $html .= '<input type="text" name="txtMatricula" id="txtMatricula" style="width: 250px;" value="' . $objArm->matricula . '" maxlength="60" class="" />';
        $html .= '  </td>';
    $html .= ' </tr>';
    $html .= '</table>';
    $html .= '  </fieldset>';
    $html .= '<p class="infoRequerida"><img alt="" src="' . PATH_IMAGES . 'icons/Warning24.png" /> Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios y no podr? continuar hasta que los complete.</p>';

    $html .= '   <input type="hidden" id="hdnUrlDatos" value="' . $objSys->encrypt('ope/mid/_ajx/grid_incidentes_armas.php') . '" />';
    $html .= '   <input type="hidden" id="hdnUrlCalibres" value="' . $objSys->encrypt('ope/mid/_ajx/ajx_obt_calibres.php') . '" />';
    $html .= '   <input type="hidden" id="txtFolioIncidente" name="txtFolioIncidente" value="' . $objSys->decrypt( $_POST["id_folio_incidente"] ) . '" />   ';
    $html .= '   <input type="hidden" id="oper" name="oper" value="' . $oper . '" />';
    $html .= '</form>';

    // Formatea los datos y los envia al grid...
    $ajx_datos["html"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>