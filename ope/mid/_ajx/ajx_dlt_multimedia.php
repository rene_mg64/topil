<?php
/**
 * Complemento del llamado ajax para eliminar el registro y archivo(s) multimedia
 * Lista de parámetros recibidos por POST 
 * @param int id, contiene el id del registro multimedia a eliminar.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    $path = '../../../';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes_multimedia.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objMult = new OpetblMidIncidentesMultimedia();
    
    $id_multimedia = $_POST["id"];
    $objMult->select($id_multimedia);
    $ext = $objMult->OpecatMidMultimediaTipo->multimedia_tipo;
    
    if( $objMult->delete($id_multimedia) ){
        $objSys->registroLog($objUsr->idUsr, 'opetbl_mid_incidentes_multimedia', $id_multimedia, 'Dlt');
        
        $path_exped = '../_multimedia/';
        $archivo_normal = $path_exped . $_SESSION["xIdIncidente"] . '/' . $_SESSION["xIdIncidente"] . '_' . $id_multimedia . '.' . $ext; 
        unlink($archivo_normal);
        //-- Verifica si el archivo es una imagen...
        $pos = strpos("jpg,jpeg,png,gif,bmp", $ext);
        if( $pos !== false ){
            $archivo_mini = $path_exped . $_SESSION["xIdIncidente"] . '/' . $_SESSION["xIdIncidente"] . '_' . $id_multimedia . '-thb.' . $ext;
            unlink($archivo_mini);
        }
        
        $ajx_datos['rslt'] = true;
        $ajx_datos["error"] = '';
    } else {
        $ajx_datos['rslt'] = false;
        $ajx_datos["error"] = $objExped->msjError;
    }
    
    echo json_encode($ajx_datos);
}
?>