<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par?metros recibidos por GET
 * @param string txtSearch, contiene el texto especificado por el usuario para una b?squeda.
 * @param int colSort, contiene el ?ndice de la columna por la cual se ordenar?n los datos.
 * @param string typeSort, define el tipo de ordenaci?n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n?mero de fila por la cual inicia la paginaci?n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar?n en cada p?gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes_generalidades.class.php';
    $objSys = new System();
    $objDataGridGeneralidades = new OpetblMidIncidentesGeneralidades();

    //--------------------- Recepci?n de par?metros --------------------------//
    // B?squeda...
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {

        $sql_where = ' g.id_folio_incidente = ? ' 
                    . 'OR g.id_igeneralidad LIKE ? '
                    . 'OR g.id_folio_incidente LIKE ? '
                    . 'OR gr.rol_generalidad LIKE ? '
                    . 'OR gc.generalidad LIKE ? '
                    . 'OR gu.unidad LIKE ? '
                    . 'OR g.Cantidad LIKE ? '
                    . 'OR g.Observaciones LIKE ? ';
        $sql_values = array($objSys->decrypt( $_GET["id_folio_incidente"] ),
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            );
   }else{
        $sql_where = ' g.id_folio_incidente = ? '; 
        $sql_values = array($objSys->decrypt( $_GET["id_folio_incidente"] ) );
   }
      
    // Ordenaci?n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'g.id_igeneralidad ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
         $sql_order = 'gr.rol_generalidad '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'gc.generalidad ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'gu.unidad ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'g.Cantidad ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 6) {
        $sql_order = 'g.Observaciones ' . $_GET['typeSort'];
    }
    
    // Paginaci?n...
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];

    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];
    //------------------------------------------------------------------------//

    $datos = $objDataGridGeneralidades->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];

    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $dato["id_igeneralidad"]; //$objSys->encrypt($dato["id_igeneralidad"]);
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_igeneralidad"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 1%;">';
                // $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 3%; font-size:13px; color:#FF0000;"><strong>'.$dato["id_igeneralidad"].'</strong></td>';
               //<a href="#" class="toolTipTrigger" rel="' . $id_crypt . '">' . $dato["id_igeneralidad"] . '</a></td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["rol_generalidad"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["tipo_generalidad"] . '</td>';
                $html .= '<td style="text-align: center; width: 12%;">' . $dato["generalidad"] . '</td>';
                $html .= '<td style="text-align: center; width: 13%;">' . $dato["unidad"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["Cantidad"] . '</td>';
                //$html .= '<td style="text-align: center; width: 20%;">' . $dato["Observaciones"] . '</td>';
               
                $url_edit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("incidentes_panel") . "&id=" . $id_crypt;
                $url_del = "ope/mid/_reportes/reporte_incidente.pdf";
                $html .= '<td style="text-align: center; width: 7%;">';
                $html .= '  <a href="#" id="gen-' . $dato["id_igeneralidad"] . '" class="lnkBtnOpcionGrid classModificar" title="Editar Incidente ..." ><img src="' . PATH_IMAGES . 'icons/edit24.png" alt="modificar" /></a>';
                $html .= '  <a href="' . $url_del . '" target="_blank" class="lnkBtnOpcionGrid" title="Imprimir Incidente ..." ><img src="' . PATH_IMAGES . 'icons/pdf24.png" alt="baja" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//... '.$dato["hechos_text"] .'
       		$html .= '</tr>';
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron registros de incidentes ...';
        $html .= '</span>';
    } else {
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }

    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi?n...";
}
?>