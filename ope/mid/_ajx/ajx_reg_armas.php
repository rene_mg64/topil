<?php
/**
 * Complemento ajax para guardar los datos del los vehiculos involucrados en incidentes
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/mysql.class.php';     
    include $path . 'includes/class/opetbl_mid_incidentes_armas.class.php';    
    include $path . 'includes/class/config/system.class.php';
    
    $conexBD = new MySQLPDO();
    $objSys  = new System();  
    $objUsr  = new Usuario();  
    $objArm  = new OpetblMidIncidentesArmas();           
    
    // Datos del armamento    
    $objArm->id_arma                    = $_POST["txtIdArma"];                
    $objArm->id_folio_incidente         = $_POST["txtFolioIncidente"];
    $objArm->id_arma_rol                = $_POST["cbxIdRolArma"];
    $objArm->id_arma_tipo               = $_POST["cbxIdTipoArma"];
    $objArm->id_arma_calibre            = $_POST["cbxIdCalibreArma"];
    $objArm->id_arma_marca              = $_POST["cbxIdMarcaArma"];        
    $objArm->numero_serie               = strtoupper( $_POST["txtNumeroSerie"] );
    $objArm->modelo                     = strtoupper( $_POST["txtModelo"] );    
    $objArm->matricula                  = strtoupper( $_POST["txtMatricula"] );
    
    
    //se ejecuta la operacion de ingreso o actualizacion
    $conexBD->beginTransaction();   
    if( $_POST["oper"] == 1 ){
        $result = $objArm->update() ;      
        $oper = "Upd";
    }elseif( $_POST["oper"] == 2 ){
        $result = $objArm->insert();          
        $oper = "Ins";        
    } 
    //se verifica si hubo exito en la consulta de insercion o actualizacion del registro
    if( $result ){
        $objSys->registroLog($objUsr->idUsr, 'opetbl_mid_incidentes_armas', $objArm->id_arma, $oper);                   
        $ajx_datos['rslt']  = true;                
        $ajx_datos['error'] = "";        
        $conexBD->commit();
    }else{
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] = $objArm->msjError;          
        $conexBD->rollBack();             
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;    
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>