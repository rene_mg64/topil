<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    
    include $path . 'includes/class/opetbl_mid_incidentes_generalidades.class.php';   
    include $path . 'includes/class/config/system.class.php';
    $objGen = new OpetblMidIncidentesGeneralidades();
    $objSys = new System();  
    
    //se reciben parametros
    //id_folio_incidente
    $id_folio_incidente = $objSys->decrypt( $_POST["id_folio_incidente"] );
    //id_generalidad
    if( !empty( $_POST["id_gen"] ) ){
        $id_gen = $_POST["id_gen"];
        $objGen->select( $id_gen );
        $oper = 1;
    }else{
        $id_gen = $objGen->getIdUltimaGeneralidad() + 1;
        $oper = 2;
    }
    
    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//          
    $html='';  
                  
    $html .= '<form id="frmIncidentesGen" method="post" enctype="multipart/form-data">';
    $html .= '   <fieldset id="fsetopetbl_mid_incidentes_generalidades" class="fsetForm-Data">';
    $html .= '<!--<legend>opetbl_mid_incidentes_generalidades</legend>-->';
    $html .= '<table id="tbfrmopetbl_mid_incidentes_generalidades" class="tbForm-Data">';
        $html .= '   <tr>';
            $html .= '   <td class="descripcion"><label for="txtIdIgeneralidad">N&uacute;mero:</label></td>';
            $html .= '   <td class="validation">';
            $html .= '  <input type="text" name="txtIdIgeneralidad" id="txtIdIgeneralidad" value="' . $id_gen . '" maxlength="" class="" readonly="true" />';
            $html .= '  <span class="pRequerido">*</span>';
            $html .= '</td>';
        $html .= '   </tr>';
        $html .= '   <!--<tr>';
            $html .= '   <td class="descripcion"><label for="txtIdFolioIncidente">Folio del incidente:</label></td>';
            $html .= '   <td class="validation">';
            $html .= '  <select name="txtIdFolioIncidente" id="txtIdFolioIncidente">';
            //$html .= $objGen->folio;
            $html .= '</select>';
            $html .= '  <span class="pRequerido">*</span>';
            $html .= '</td>';
        $html .= '   </tr>-->';
        $html .= '   <tr>';
            $html .= '   <td class="descripcion"><label for="cbxIdRolGeneralidad">Rol:</label></td>';            
            $html .= '   <td class="validation">';
            $html .= '  <select name="cbxIdRolGeneralidad" id="cbxIdRolGeneralidad" style="width: 300px;">';
            $html .=  $objGen->OpecatMidRolesGeneralidades->getCat_mid_Roles_Generalidades( $objGen->id_rol_generalidad );
            $html .= '  </select>';
            $html .= '  <span class="pRequerido">*</span>';
            $html .= '</td>';
        $html .= '   </tr>';
        $html .= '   <tr>';
            $html .= '   <td class="descripcion"><label for="cbxIdTipo">Tipo:</label></td>';
            $html .= '   <td class="validation">';
            $html .= '  <select name="cbxIdTipo" id="cbxIdTipo" style="width: 300px;">';
            $html .= $objGen->OpecatMidGeneralidadesUnidades->OpecatMidGeneralidades->OpecatMidTiposGeneralidades->getCat_mid_Tipos_Generalidades( $objGen->OpecatMidGeneralidadesUnidades->OpecatMidGeneralidades->getIdTipoGeneralidad( $objGen->id_generalidad ) );
            $html .= '  </select>';
            $html .= '  <span class="pRequerido">*</span>';
            $html .= '</td>';
        $html .= '   </tr>';
        $html .= '   <tr>';
            $html .= '   <td class="descripcion"><label for="cbxIdCategoria">Categoria:</label></td>';
            $html .= '   <td class="validation">';
            $html .= '  <select name="cbxIdCategoria" id="cbxIdCategoria" style="width: 300px;">';
            $html .= $objGen->OpecatMidGeneralidadesUnidades->OpecatMidGeneralidades->getCat_mid_Generalidades(0,  $objGen->id_generalidad );
            $html .= '  </select>';
            $html .= '  <span class="pRequerido">*</span>';
            $html .= '</td>';
        $html .= '   </tr>';
        $html .= '   <tr>';
            $html .= '   <td class="descripcion"><label for="cbxIdUnidad">Unidad:</label></td>';
            $html .= '   <td class="validation">';
            $html .= '  <select name="cbxIdUnidad" id="cbxIdUnidad" style="width: 300px;">';
            $html .= $objGen->OpecatMidGeneralidadesUnidades->OpecatMidUnidadesGeneralidades->getCat_Unidades_Generalidades( $objGen->id_generalidad, $objGen->id_unidad );
            $html .= '  </select>';
            $html .= '  <span class="pRequerido">*</span>';
            $html .= '</td>';
        $html .= '   </tr>';
        $html .= '   <tr>';
            $html .= '   <td class="descripcion"><label for="txtCantidad">Cantidad:</label></td>';
            $html .= '   <td class="validation">';
            $html .= '  <input type="text" name="txtCantidad" id="txtCantidad"  style="width: 100px;" value="' . $objGen->Cantidad . '" maxlength="" class="" />';
            $html .= '</td>';
        $html .= '   </tr>';
        $html .= '   <tr>';
            $html .= '   <td class="descripcion"><label for="txtObservaciones">Observaciones:</label></td>';
            $html .= '   <td class="validation">';
            $html .= '  <textarea name="txtObservaciones" id="txtObservaciones" cols="45" rows="2"> ' . $objGen->Observaciones . '</textarea>';
            $html .= '</td>';
        $html .= '   </tr>';
    $html .= ' </table>';
    $html .= '  </fieldset>';
    $html .= '<p class="infoRequerida"><img alt="" src="' . PATH_IMAGES . 'icons/Warning24.png" /> Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios y no podr? continuar hasta que los complete.</p>';

    
    $html .= '   <input type="hidden" id="hdnUrlCategorias" value="' . $objSys->encrypt('ope/mid/_ajx/ajx_obt_categorias.php') . '" />';
    $html .= '   <input type="hidden" id="hdnUrlUnidades" value="' . $objSys->encrypt('ope/mid/_ajx/ajx_obt_unidades.php') . '" />';
    $html .= '   <input type="hidden" id="txtFolioIncidente" name="txtFolioIncidente" value="' . $objSys->decrypt( $_POST["id_folio_incidente"] ) . '" />   ';
    $html .= '   <input type="hidden" id="oper" name="oper" value="' . $oper . '" />';
    $html .= '</form>';

    // Formatea los datos y los envia al grid...
    $ajx_datos["html"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>