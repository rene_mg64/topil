<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par?metros recibidos por GET
 * @param string txtSearch, contiene el texto especificado por el usuario para una b?squeda.
 * @param int colSort, contiene el ?ndice de la columna por la cual se ordenar?n los datos.
 * @param string typeSort, define el tipo de ordenaci?n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n?mero de fila por la cual inicia la paginaci?n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar?n en cada p?gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes_personas.class.php';
    $objSys = new System();
    $objDataGridPersonas = new OpetblMidIncidentesPersonas();

    //--------------------- Recepci?n de par?metros --------------------------//
    // B?squeda...
    $sql_where = 'a.id_folio_incidente=' . $_SESSION["xIdIncidente"] .' ';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {
        $sql_where .= 'AND (b.persona_rol LIKE ? '
                    . 'OR c.persona_causa LIKE ? '
                    . 'OR (a.nombre LIKE ? OR a.apellido_paterno LIKE ? OR a.apellido_materno LIKE ?) '
                    . 'OR a.domicilio LIKE ?)';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            );
   }
    // Ordenaci?n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
         $sql_order = 'b.persona_rol '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
        $sql_order = 'c.persona_causa ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'a.nombre, a.apellido_paterno, apellido_materno ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'a.sexo ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'a.edad ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 6) {
        $sql_order = 'a.domicilio ' . $_GET['typeSort'];
    }
    // Paginación...
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];

    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];
    //------------------------------------------------------------------------//

    $datos = $objDataGridPersonas->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];

    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_reg = $dato["id_persona"];
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_persona"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 4%;">';
                    $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["persona_rol"] . '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["persona_causa"] . '</td>';
                $html .= '<td style="text-align: center; width: 22%;">' . $dato["nombre"] .' '. $dato["apellido_paterno"] .' '. $dato["apellido_materno"] . '</td>';
                $sexo = ( $dato["sexo"] == 1 ) ? "M" : "F";
                $html .= '<td style="text-align: center; width: 6%;">' . $sexo . '</td>';
                $html .= '<td style="text-align: center; width: 6%;">' . $dato["edad"] . '</td>';
                $html .= '<td style="text-align: left; width: 23%;">' . $dato["domicilio"].'</td>';
                $html .= '<td style="text-align: center; width: 10%;">';
                $html .= '  <a href="#" rel="edt-' . $id_reg . '" class="lnkBtnOpcionGrid" title="Editar los datos de la persona ..." ><img src="' . PATH_IMAGES . 'icons/edit24.png" alt="modificar" /></a>';
                $html .= '  <a href="#" rel="dlt-' . $id_reg . '" class="lnkBtnOpcionGrid" title="Eliminar el registro de la persona ..." ><img src="' . PATH_IMAGES . 'icons/delete24.png" alt="baja" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron registros de personas involucradas...';
        $html .= '</span>';
    } else {
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }

    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi?n...";
}
?>