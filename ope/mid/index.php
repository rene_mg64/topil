<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Incidencia Delictiva
 */

//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include('ope/funciones.class.php');
$fn=new funciones;

include  'includes/class/opetbl_mid_incidentes.class.php';
$GridIncidentes=new OpetblMidIncidentes();
//-----------------------------------------------------------------//
//-- Bloque de definici?n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Incidencia Delictiva',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="ope/mid/_js/index.js"></script>'),
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>

<?php
    //-------------------------------- Barra de opciones ------------------------------------//
?>
<div id="dvTool-Bar" class="dvTool-Bar">
    <table style="width: 100%;">
    <tr>
        <td style="text-align: left; width: 50%;">
            <?php $plantilla->mostrarNombreModulo();?>
        </td>
        <td style="text-align: right; width: 50%;">
            <!-- Botones de opci?n... -->
            <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes');?>" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Agregar Nuevo Incidente...">
                <img src="includes/css/imgs/icons/police_maker24.png" alt="" style="border: none;" /><br />Incidentes
            </a>
            <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_estadisticas');?>" id="xReportes" class="Tool-Bar-Btn" style="" title="Estadisticas de Incidentes...">
                <img src="includes/css/imgs/icons/reportes24.png" alt="" style="border: none;" /><br />Reportes
            </a>
        </td>
    </tr>
    </table>
</div>

<div id="dvGridIncidentes" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
    <div class="xGrid-dvHeader gradient">
        <table class="xGrid-tbSearch">
            <tr>
                <td>Buscar: <input type="text" name="txtBuscar" size="40" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
            </tr>
        </table>
        <table class="xGrid-tbCols">
            <tr>
                <th style ="width: 1%; text-align: center;">&nbsp;</th>
                <th style ="width: 2%;" class="xGrid-tbCols-ColSortable">#</th>
                <th style ="width: 5%;" class="xGrid-tbCols-ColSortable">FOLIO</th>
                <th style ="width: 6%;" class="xGrid-tbCols-ColSortable">FECHA</th>
                <th style ="width: 6%;" class="xGrid-tbCols-ColSortable">REGION</th>
                <th style ="width: 10%;" class="xGrid-tbCols-ColSortable">CUARTEL</th>
                <th style ="width: 10%;" class="xGrid-tbCols-ColSortable">ASUNTO</th>
                <th style ="width: 10%;" class="xGrid-tbCols-ColSortable">FUENTE</th>
                <th style ="width: 35%;" class="xGrid-thNo-Class">HECHOS</th>
                <th style ="width: 7%;" class="xGrid-thNo-Class"></th>
            </tr>
          </table>
    </div>
    <div class="xGrid-dvBody">
        <table class="xGrid-tbBody">
          <!--
            Aqui informacion para dinamica de la Tabla de Incidentes
            Cargar los ultimos 100 incidentes en orden descendente por fechas
            -->
        </table>
    </div>
</div>


<input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('ope/mid/_ajx/grid_incidentes_listado.php');?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>
