<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo de Incidencia Delictiva
 * Panel para agregar informacion de involucrados al sistema
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi?n de las clases...
//-----------------------------------------------------------------//
//getIdUltimoIncidente()
include 'includes/class/opetbl_mid_incidentes.class.php';
include 'includes/class/opetbl_mid_incidentes_vehiculos.class.php';
include 'includes/class/opetbl_mid_incidentes_armas.class.php';
include 'includes/class/opetbl_mid_incidentes_generalidades.class.php';
include 'includes/class/opetbl_mid_incidentes_personas.class.php';
include 'includes/class/opetbl_mid_incidentes_multimedia.class.php';
include 'includes/class/ope_funciones.class.php';

$objInc             =   new OpetblMidIncidentes;
$objVeh             =   new OpetblMidIncidentesVehiculos;
$objArm             =   new OpetblMidIncidentesArmas;
$objGen             =   new OpetblMidIncidentesGeneralidades;
$objPersonas        =   new OpetblMidIncidentesPersonas();
$objMult            =   new OpetblMidIncidentesMultimedia();
$objFunciones       =   new Ope_Funciones;

//se reciben parametros
$id_folio_incidente = $objSys->decrypt($_GET["id_folio_incidente"]);
$objInc->select( $id_folio_incidente );


//-----------------------------------------------------------------//
//-- Bloque de definici?n de par?metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Incidente Panel',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<link type="text/css" href="ope/mid/_css/mid.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mid/_js/incidentes_panel.js"></script>',),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');

//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din?mico...
//-----------------------------------------------------------------//
  $urlRegresar = "index.php?m=" . $_SESSION["xIdMenu"]. '&mod=' . $objSys->encrypt('incidentes');
  $urlCancelar = "index.php?m=" . $_SESSION["xIdMenu"]. '&mod=' . $objSys->encrypt('index');
  
?>

<div id="dvTool-Bar" class="dvTool-Bar">
  <table>
      <tr>
          <td class="tdNombreModulo">
              <?php  $plantilla->mostrarNombreModulo();?>
          </td>
          <td class="tdBotonesAccion">
               <!--
               <a href="<?php //echo $urlRegresar?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Regresar a pantalla del incidente ...">
                    <img src="<?php //echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
               </a>
               -->
               
               <a href="<?php echo $urlCancelar?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Regresar a pantalla del incidente ...">
                    <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
               </a>               
               
               <!--
               <a href="<?php echo $urlCancelar?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Cancelar la alta/edicion del incidente ...">
                    <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
               </a>
               -->
          </td>
      </tr>
  </table>
</div>

  <form id="frmPanel" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Panel" class="dvForm-Data">
            <span class="dvForm-Data-pTitle">
                <img src="<?php echo PATH_IMAGES;?>icons/add.png" class="icono"/>
                Resumen del Incidente
            </span>

    <!-- Contenido del Formulario -->
<fieldset id="fsetopetbl_mid_incidentes_panel" class="fsetForm-Data">
     <!-- <legend>Informacion del Incidente</legend> -->
  <table id="tblPanel">
    <tr>
      <td rowspan="3" class="CelInformacion">
       <div id="divPanelInfo">
          <table id="tblPanelInfo">
            <tr>
              <td class="descripcion" style="border-top:none;">Folio:</td>
              <td class="informacion" style="border-top:none;  color:#F40006; font-weight:bold; fon;t-size: 16px;">
               <?php echo $objInc->id_folio_incidente; ?>
              </td>
            </tr>
            <tr>
              <td class="descripcion">Prioridad:</td>
              <td class="informacion">              
                 <?php 
                    switch( $objInc->prioridad ){
                        case 1:
                            $prioridad = "Baja";
                            break;
                        case 2:
                            $prioridad = "Media";
                            break;
                        case 3:
                            $prioridad = "Alta";
                            break;
                        default:
                            $prioridad = "No indicada";
                            break;
                    }
                    echo $prioridad; 
                 ?>
              </td>
            </tr>
            <tr>
              <td class="descripcion">Fecha y Hora:</td>
              <td class="informacion">
                <?php
                    //parametros de la fecha
                    $fecha  = explode("-", $objInc->fecha_incidente);
                    $year   = $fecha["0"];
                    $mes    = $fecha["1"];
                    $dia    = $fecha["2"];
                    
                    //parametros de la hora                    
                    $hora       = explode(":", $objInc->hora_incidente);
                    $horas      = $hora[0];
                    $minutos    = $hora[1];
                
                    
                    echo $objSys->nombreDiaLargo( date("w", $objInc->fecha_incidente) ) . " " . $dia . " de " . $objSys->nombreMesLargo( $mes ) .  " de " . $year . "," . $horas . ":" . $minutos . " hrs."; 
                ?>
              </td>
            </tr>
            <tr>
              <td class="descripcion">Region:</td>
              <td class="informacion">
                <?php 
                    $objInc->OpecatMidRegionesOperativas->select( $objInc->id_region );
                    echo $objInc->OpecatMidRegionesOperativas->region; 
                ?>
              </td>
            </tr>
            <!-- <tr>
              <td class="descripcion">Cuartel:</td>
              <td class="informacion">
                <?php 
                    $objInc->OpecatMidCuarteles->select( $objInc->id_cuartel );
                    echo $objInc->OpecatMidCuarteles->cuartel; 
                ?>
              </td>
            </tr> -->
            <tr>
              <td class="descripcion">Municipio:</td>
              <td class="informacion">
                <?php 
                    $objInc->OpecatMidMunicipios->select( $objInc->id_municipio ); 
                    echo $objInc->OpecatMidMunicipios->municipio; 
                ?>
              </td>
            </tr>
            <tr>
              <td class="descripcion">Localidad:</td>
              <td class="informacion">
                <?php echo $objInc->localidad; ?>
              </td>
            </tr>
            <tr>
              <td class="descripcion">Direccion:</td>
              <td class="informacion">
                <?php echo $objInc->direccion; ?>
              </td>
            </tr>
            <tr>
              <td class="descripcion">Asunto:</td>
              <td class="informacion">
                <?php
                    $objInc->OpecatMidAsuntos->select( $objInc->id_asunto ); 
                    echo $objInc->OpecatMidAsuntos->asunto; 
                ?>
              </td>
            </tr>
            <tr>
              <td class="descripcion">Fuente:</td>
              <td class="informacion">
                <?php
                    $objInc->OpecatMidFuentes->select( $objInc->id_fuente ); 
                    echo $objInc->OpecatMidFuentes->fuente; 
                ?>
              </td>
            </tr>
            <tr>
              <td class="descripcion">Hechos:</td>
              <td class="informacion"></td>
            </tr>
            <tr>
              <td colspan="2"  style="border-bottom:none;">
              <!-- <textarea name="txtAreaHechos" id="txtAreaHechos" cols="30" rows="4" class="txtAreaHechos" disabled="disabled">-->
              <div class="txtAreaHechos">
                    <?php echo $objInc->hechos_text; ?>
             </div>
               <!-- </textarea>-->
              </td>
            </tr>
        </table>
        </div>
      </td>
      <td class="botones">
            <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes') . '&id_folio_incidente=' . $objSys->encrypt( $objInc->id_folio_incidente );?>" class="BotonPanel" title="Modificar el Incidente">
              <img src="<?php echo PATH_IMAGES;?>icons/police_maker64.png" style="width: 64px; height: 64px;" alt="" />
              <br />Modificar<br />              
            </a>
      </td>
      <td class="botones">
            <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_personas'). '&id_folio_incidente=' . $objSys->encrypt( $objInc->id_folio_incidente );?>" class="BotonPanel" title="Agregar Personas Involucradas en el Incidente">
              <img src="<?php echo PATH_IMAGES;?>icons/people64.png" alt="" />
              <br />Personas<br />
              <div class="numeroCentro" title="Numero de Personas Involucradas"><?php echo $objPersonas->selectAllCount("a.id_folio_incidente=" . $objInc->id_folio_incidente); ?></div>
            </a>
      </td>
      <td class="botones">
            <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_vehiculos') . '&id_folio_incidente=' . $objSys->encrypt( $objInc->id_folio_incidente );?>" class="BotonPanel" title="Agregar Vehiculos Involucrados en el Incidente">
              <img src="<?php echo PATH_IMAGES;?>icons/vehiculos64.png" alt="" />
              <br />Vehiculos<br />
              <div class="numeroCentro" title="Numero de Vehiculos Involucrados"><?php echo $objVeh->getTotalVehiculos( $id_folio_incidente ); ?></div>
            </a>
        </td>      
      </tr>
    <tr>
        <td class="botones">
            <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_armas') . '&id_folio_incidente=' . $objSys->encrypt( $objInc->id_folio_incidente );?>" class="BotonPanel" title="Agregar Armas Involucradas en el Incidente">
              <img src="<?php echo PATH_IMAGES;?>icons/armas64.png" alt="" />
              <br />Armas<br />
              <div class="numeroCentro" title="Numero de Armas Involucradas"><?php echo $objArm->getTotalArmas( $id_folio_incidente ); ?></div>
            </a>
      </td>
      <td class="botones">
        <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_generalidades') . '&id_folio_incidente=' . $objSys->encrypt( $objInc->id_folio_incidente );?>" class="BotonPanel" title="Agregar Generalidades (Cualquier Otro Objeto Involucrado)">
          <img src="<?php echo PATH_IMAGES;?>icons/generalidades64.png" alt="" />
          <br />Generalidades<br />
          <div class="numeroCentro" title="Numero de Objetos Involucrados"><?php echo $objGen->getTotalGeneralidades( $id_folio_incidente ); ?></div>
          </a>
        </td>
      <td class="botones">
        <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_multimedia') . '&id=' . $objSys->encrypt($id_folio_incidente);?>" class="BotonPanel" title="Agregar Multimedia (Fotografias, Audios, Videos, Documentos)">
          <img src="<?php echo PATH_IMAGES;?>icons/multimedia64.png" alt="" />
          <br />Multimedia<br />
          <div class="numeroCentro" title="Numero de Elementos Multimedia"><?php echo $objMult->getTotalMultimedia($id_folio_incidente); ?></div>
          </a>
      </td>
      <td class="botones">

      </td>
      </tr>
    <tr>
      <td colspan="3">
        <div style="width:100%; height: 320px; border:1px solid #888; border-radius:10px; background-color: #CDE0EB; background-image: url('http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $objInc->latitud . "," . $objInc->longitud; ?>&zoom=17&size=320x400&maptype=roadmap&markers=icon:http://maps.google.com/mapfiles/kml/pushpin/red-pushpin.png|<?php echo $objInc->latitud . "," . $objInc->longitud; ?>&sensor=true_or_false'); background-repeat: no-repeat; background-size: 100% 100%; -moz-background-size: 100% 100%;">
        <!-- <div style="width:100%; height: 320px; border:1px solid #888; border-radius:10px; background-color: #CDE0EB; background-image: url('http://maps.googleapis.com/maps/api/staticmap?center=17.551535,-99.500632&zoom=17&size=320x400&maptype=roadmap&markers=icon:http://maps.google.com/mapfiles/kml/pushpin/red-pushpin.png|17.551535,-99.500632&sensor=true_or_false'); background-repeat: no-repeat; background-size: 100% 100%; -moz-background-size: 100% 100%;"> -->
        
        <?php //echo $objInc->longitud . "," . $objInc->latitud ; ?>      
        </div>
      </td>
    </tr>
    </table>
</fieldset>
   </div>
</form>

<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>