<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Incidencia Delictiva
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi?n de las clases...
//-----------------------------------------------------------------//

//-----------------------------------------------------------------//
//-- Bloque de definici?n de par?metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Estadisticas',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<link type="text/css" href="ope/mid/_css/mid.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mid/_js/incidentes_estadisticas.js"></script>',
                                   '<script src="includes/js/RGraph/libraries/RGraph.common.core.js"></script>',
                                   '<script src="includes/js/RGraph/libraries/RGraph.common.effects.js"></script>',                                   
                                   '<script src="includes/js/RGraph/libraries/RGraph.common.dynamic.js"></script>',
                                   '<script src="includes/js/RGraph/libraries/RGraph.common.key.js"></script>',
                                   '<script src="includes/js/RGraph/libraries/RGraph.common.tooltips.jsRGraph.hbar.js"></script>',
                                   '<script src="includes/js/RGraph/libraries/RGraph.common.effects.js"></script>',
                                   '<script src="includes/js/RGraph/libraries/RGraph.hbar.js"></script>',
                                   '<script src="includes/js/RGraph/libraries/RGraph.pie.js"></script>',
                                   '<script src="includes/js/RGraph/libraries/RGraph.line.js"></script>',
                                   '<script src="includes/js/RGraph/libraries/RGraph.bar.js"></script>',                                   
                                   '<script src="includes/js/RGraph/libraries/RGraph.drawing.rect.js" ></script>',                                   
                                   '<!--[if lt IE 9]><script src="../../includes/js/RGraph/RGraph/excanvas/excanvas.js"></script><![endif]-->'),                                  
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din?mico...
//-----------------------------------------------------------------//
  $urlRegresar = "index.php?m=" . $_SESSION["xIdMenu"];
  $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('-----');
  $urlMapa = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_estadisticas_mapa');

?>
  <div id="dvTool-Bar" class="dvTool-Bar">
        <table>
            <tr>
                <td class="tdNombreModulo">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion">
                    <a href="#" id="btnAnterior" class="Tool-Bar-Btn gradient" style="display: none; width: 60px;" title="Validar y continuar...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back241.png" alt="" style="border: none;" /><br />Anterior
                    </a>
                    <a href="<?php echo $urlMapa; ?>" id="btnSiguiente" class="Tool-Bar-Btn gradient" style="width: 60px;" title="Estadisticas en Mapa...">
                        <img src="<?php echo PATH_IMAGES;?>icons/pin_24.png" alt="" style="border: none;" /><br />Mapa
                    </a>
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="display: none; width: 110px;" title="Guardar los datos del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <a href="<?php echo $urlRegresar?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
     </div>

  <form id="frmPerfilCriminalAgregar" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Perfil" class="dvForm-Data">
            <span class="dvForm-Data-pTitle">
                <img src="<?php echo PATH_IMAGES;?>icons/reportes24.png" class="icono"/>
                Indice Delictivo Semanal
            </span>

    <!-- Contenido del Formulario -->

<div id="divTabGraficos">
    <ul>
        <li><a href="#gbarras">Gafico de Barras</a></li>
        <li><a href="#gbarrasH">Gafico de Barras</a></li>
        <li><a href="#gpastel">Grafico de Pastel</a></li>
        <li><a href="#glineas">Grafico de Lineas</a></li>
    </ul>
    <div id="gbarras">
        <canvas id="cvs" width="900" height="400">[No canvas support]</canvas>
    </div>
    <div id="gbarrasH">
        <canvas id="barraH" width="900" height="400">[No canvas support]</canvas>
    </div>
    <div id="gpastel">
        <canvas id="pastel" width="450" height="300">[No canvas support]</canvas>
    </div>
    <div id="glineas">
        <canvas id="lineas" width="1000" height="300">[No canvas support]</canvas>
    </div>
</div>

    <?php
        $values="";
        $arrVal=array();
        for ($i=1; $i <=8; $i++) {
            $numero=rand(0,99);
            $arrVal[]=$numero;
        }

       $maximo=max($arrVal)+10;
       //rsort($arrVal); //Ordena de Mayor a menor
        $valRand="";

       foreach ($arrVal as $key => $value) {
          $values.=$value . ",";
          $valRand1.= rand(1,60) . ",";
          $valRand2.= rand(10,80) . ",";
          $valRand3.= rand(40,90) . ",";
       }

    echo"<script>
        window.onload = function ()
        {
            var hbar = new RGraph.HBar('cvs', [".$values."])
                .Set('background.grid.hlines', true)
                .Set('xmax', ".$maximo.")
                .Set('scale.decimals', 1)
                .Set('labels', ['ACCIDENTE CARRETERO','ASALTO A TRANSEUNTE','EJECUTADO','PORTACION DE ARMA DE FUEGO','SUICIDIO','TRATA DE PERSONAS','ASALTO A TRANSPORTE','ASALTO BANCARIO'])
                .Set('colors', ['#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080'])
                .Set('colors.sequential', true)
                .Set('gutter.left', 250)
                .Set('labels.above', true)
                .Set('labels.above.decimals', 1)
                .Set('noxaxis', true)
                .Set('xlabels', true)
            RGraph.Effects.HBar.Grow(hbar);
            
            
             var bar1 = new RGraph.Bar('barraH', [[8,4,1,],[4,8,8,3],[1,2,2,1],[3,3,4,0],[6,5,2,2],[5,8,8,1],[4,8,5,3]])
                    .Set('key', ['Asalto a Transeunte','Accidente Transito','Privacion de la Libertad','Ejecutado'])
                    .Set('key.interactive', true)
                    .Set('labels', ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'])
                    .Draw();
            
            
            
            // Create the Pie chart
            pie = new RGraph.Pie('pastel', [".$values."])
                .Set('labels', ['Lunes', 'Martes','Mercoles','Jueves','Viernes','Sabado','Domingo'])
                .Set('text.color', '#000')
                .Set('colors', ['#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080','#0080FF','#FF00FF','#FFFF80','#80FF80','#FF0000','#FF8000','#8000FF','#808080','#FF0080'])
                .Set('exploded', 5)
                .Set('radius', 100)
                .Draw();
                           
            // Add the click listener for the third segment
            pie.onmousemove = function (e, shape)
            {
                var idx = shape['index'];
    
                if (typeof(pie.Get('exploded')) == 'number' || pie.Get('exploded')[idx] <= 5) {
                    pie.Set('exploded', 5)
                    pie.Explode(idx, 25);
                }
                
                e.target.style.cursor = 'pointer';                
                e.stopPropagation();
            }
    
            // Add the window click listener that resets the Pie chart
            window.onclick = function (e)
            {
                pie.Set('exploded', 5);
                RGraph.Redraw();
            }
            
            
            
            
            
            var line = new RGraph.Line('lineas', [".$valRand1."], [".$valRand2."],[".$valRand3."])
                .Set('curvy', true)
                .Set('curvy.tickmarks', true)
                .Set('curvy.tickmarks.fill', null)
                .Set('curvy.tickmarks.stroke', '#aaa')
                .Set('curvy.tickmarks.stroke.linewidth', 4)
                .Set('curvy.tickmarks.size', 5)
                .Set('linewidth', 2)
                .Set('hmargin', 5)
                .Set('labels', ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'])
                .Set('tooltips', ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'])
                .Set('tickmarks', 'circle')
                .Draw();                
                //RGraph.Effects.Line.jQuery.Trace(line);
            
            
            
        }
    </script>";
    ?>






<!-- Fin del Contenido del Formulario -->
      </div>
</form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>