<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
include 'includes/class/opetbl_mpc_perfiles_criminales.class.php';
$objPefilCrim = new OpetblMpcPerfilesCriminales();

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Perfiles Criminales',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/rhumanos/_js/personal/datosrg.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Actualización');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>

<?php
// Se asignan los valores a los campos
// Falta agregar los cmpos de los conroles en el POT_
$objPefilCrim->id_perfil_criminal = $_POST["id_perfil_criminal"];
$objPefilCrim->id_situacion = $_POST["id_situacion"];
$objPefilCrim->nombre = $_POST["nombre"];
$objPefilCrim->apellido_paterno = $_POST["apellido_paterno"];
$objPefilCrim->apellido_materno = $_POST["apellido_materno"];
$objPefilCrim->alias = $_POST["alias"];
$objPefilCrim->sexo = $_POST["sexo"];
$objPefilCrim->fecha_nacimineto = $_POST["fecha_nacimineto"];
$objPefilCrim->id_nacionalidad = $_POST["id_nacionalidad"];
$objPefilCrim->lugar_origen = $_POST["lugar_origen"];
$objPefilCrim->id_estado_civil = $_POST["id_estado_civil"];
$objPefilCrim->complexion = $_POST["complexion"];
$objPefilCrim->ocupacion = $_POST["ocupacion"];
$objPefilCrim->domicilio = $_POST["domicilio"];
$objPefilCrim->estatura = $_POST["estatura"];
$objPefilCrim->Peso = $_POST["Peso"];
$objPefilCrim->cabello = $_POST["cabello"];
$objPefilCrim->colorPiel = $_POST["colorPiel"];
$objPefilCrim->ColorOjos = $_POST["ColorOjos"];

 //-------------------------------------------------------------------//
    if ($objAdscrip->update()) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_datos_personales', $curp, "Upd");
    } else {
        $error = (!empty($objAdscrip->msjError)) ? $objAdscrip->msjError : 'Error al guardar los Datos Personales';
    }

    $mod = (empty($error)) ? $objSys->encrypt("persona_menu") : $objSys->encrypt("adscrip_edit");
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>