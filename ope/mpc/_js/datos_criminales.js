$(document).ready(function(){
   /*--------------------------------------------------------------------//
     * Controla el scroll para seguir mostrando el header y el toolbar...
     *-------------------------------------------------------------------*/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });

/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/
/*** Ajusta el tama�o del contenedor del grid... ***/
    $('#dvGridDatosCriminales').css('height', ($(window).height() - 300) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGridDatosCriminales').xGrid({
        xColSort: 1,
        xTypeSort: 'Desc',
        xLoadDataStart: 1,
        xRowsDisplay: 10,
        xTypeSelect: 0,
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xTypeDataAjax: 'json'
    });


//Aplica la propiedad de ToolTip a todos los elementos que contengan la propiedad title
    $(document).tooltip({
          tooltipClass: "toolTipDetails",
          content: function () {
              return $(this).prop('title');
          },
          show:{
              effect: "clip",
              delay: 250
          },
          position: { my: "left center", at: "right center" }
      }).addClass("efecto-over");


/********************************************************************/
/** Convierte a may�sculas el contenido de los textbox y textarea. **/
/********************************************************************/
$('#txtCargoOcupa').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
$('#txtZonaOperacion').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
$('#txtModusOperandis').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
$('#txtBuscadoPor').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
$('#txtObservaciones').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });



/********************************************************************/
/********* Asignacion de validacion para el formulario **************/
/********************************************************************/

/***** Aplicaci�n de las reglas de validaci�n de los campos del formulario *****/
  $('#frmDatosCriminales').validate({
      rules: {
      /***** INICIO de Reglas de validacion para el formulario de xxxxx *****/
            cbxIdGrupoDelictivo:{
                required:true,
                min:1,
            },
            txtCargoOcupa:{
                required:true,
            },
            txtZonaOperacion:{
                required:true,
            },
            txtModusOperandis:{
                required:true,
            },
            txtBuscadoPor:{
                required:true,
            },
      /***** FIN de Reglas de validacion para el formulario de xxxxx *****/
     },
     messages:{
     /***** INICIO de Mensajes de validacion para el formulario de xxxxx *****/
            cbxIdGrupoDelictivo:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError"title="Este campo es obligatorio"></span>',
            },
            txtCargoOcupa:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtZonaOperacion:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtModusOperandis:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtBuscadoPor:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
     /***** FIN de Mensajes de validacion para el formulario de xxxxx *****/
     },
      errorClass: "help-inline",
      errorElement: "span",
      highlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('success').addClass('error');
      },
        unhighlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('error').addClass('success');
      }
  /***** Fin de validacion *****/
  });

/********************************************************************/
/***** Control del formulario para agregar/modifiar             *****/
/********************************************************************/
 $('#dvFormDatosCriminales').dialog({
        show: {
            effect: "fade",duration: 500
        },
        hide: {
            effect: "puff",duration: 500
        },
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        buttons: {
            'Guardar': function(){
                GuardarDatosCriminales();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        },
        open: function(){
            resetFormulario();
            if ($('#txtIdPerfilCriminal').val() == 0) {
                $(this).dialog('option', 'title', 'SISP :: Agregar Datos Crimnales');
            }else{
                $(this).dialog('option', 'title', 'SISP :: Datos Crimnales');
                //datosReferencia();
            }
        }
    });

    $('#btnAgregar').click(function(){
        //$('#hdnIdRef').val('0');
        //$('#hdnTypeOper').val('1');
        $('#dvFormDatosCriminales').dialog('open');
    });


/***** Fin del document.ready *****/
});


/********************************************************************/
/****** Asignacion de funcionalidad de los Botones de Acction  ******/
/********************************************************************/
// Acci�n del bot�n Guardar
function GuardarDatosCriminales(){
    var valida = $('#frmDatosCriminales').validate().form();
     var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los cambios realizados a Datos Criminales?', 2));
     var mensaje='';  
     var oper = $("#txtIdDatoCriminal").val(); 
        
    if (valida) {
         if( oper == '1' ){
                mensaje= 'Confirme la actualizaci�n de los datos.';
            }else{
                mensaje='Confirme la alta en sistema.';
            }
            var frmPreg = $(getHTMLMensaje(mensaje, 2));
            
         var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los cambios realizados a Datos Criminales?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 350,
            buttons:{
                'Aceptar': function(){
                    $.ajax({
                            url: xDcrypt( $('#hdnUrlSaveDatoCriminal').val() ),   
                            data: $( "#frmDatosCriminales" ).serialize(),         
                            dataType: 'json',
                            type: 'POST',
                            async: true,
                            cache: false,            
                            success: function ( xdata ) {                                   
                                if( xdata.rslt ){  
                                    if( oper == '1' ){                                   
                                        $("#dvEquip").dialog('close');                                        
                                        xGrid.refreshGrid();       
                                    }else{                                                            
                                        $("#txtInventario").focus();
                                        $("#txtInventario").attr("value","");                                                                                                                
                                        $("#txtSerie").attr("value","");
                                        $("#txtModelo").attr("value","");                                
                                        $("#xGridBody").html( xdata.html );                                        
                                        xGrid.refreshGrid();                                                                                
                                    }                                    
                                                                                                      
                                }else{
                                    $(this).dialog('close');
                                    alert("No se guardaron los datos. " + xdata.rslt);                                                                        
                                }                                                                                               
                            },
                            error: function(objeto, detalle, otroobj){
                                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                            }
                        });  
                        $(this).dialog('close');
                },
                'Cancelar': function(){
                     $(this).dialog('close');
                }
            }
        });
    }
}


/*Reinicia Formulario de la captura de datos criminales*/
function resetFormulario(){
    $('#txtIdDatoCriminal').val(0);
    $('#cbxIdGrupoDelictivo').val(0);
    $('#txtCargoOcupa').val('');
    $('#txtZonaOperacion').val('');
    $('#txtModusOperandis').val('');
    $('#txtBuscadoPor').val('');
    //$('input:radio[name=rbnSexo]').attr('checked',false);
    $('#txtObservaciones').val('');

    //Removemos las clases de error por si las moscas
    $('span').removeClass('ValidateError'); //Con esto elimina los iconos de error en la validacion
    $('td').removeClass('error'); //Elimina la clase para quitar el contorno de color rojo
}